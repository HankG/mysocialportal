using System;
using System.Collections.Generic;
using MySocialPortalDesktop.ViewModels;
using MySocialPortalLib.Model;

namespace MySocialPortalDesktop.Converter
{
    public class LabelValueToViewModelConverter<T>
    {
        public LabelValueViewModel LabelViewToViewModel(LabeledValue<T> labeledValue)
        {
            return new LabelValueViewModel(labeledValue?.Label ?? "", labeledValue?.Value.ToString() ?? "", false);
        }

        public LabeledValue<string> StringLabeledValueFromViewModel(LabelValueViewModel viewModel)
        {
            return new LabeledValue<string>(viewModel?.Label ?? "", viewModel.Value ?? "");
        }

        public IEnumerable<LabelValueViewModel> StringDictionaryToLabelValueViewModels(IDictionary<string, string> dictionary)
        {
            var vms = new List<LabelValueViewModel>();

            foreach (var (key, value) in dictionary)
            {
                vms.Add(new LabelValueViewModel(key, value, false));
            }
            return vms;
        }

        public IEnumerable<LabelValueViewModel> SocialMediaDictionaryToLabelValueViewModels(IDictionary<string, SocialMediaAccountData> dictionary)
        {
            var vms = new List<LabelValueViewModel>();

            foreach (var (key, value) in dictionary)
            {
                var id = value.ProfileId;
                if (value.SocialMediaSystemName == StandardSocialNetworkNames.Twitter && value.AdditionalProperties.TryGetValue("ScreenName", out var twitterName))
                {
                    id = twitterName;
                }
                vms.Add(new LabelValueViewModel(key, id, false));
            }
            return vms;
        }

    }
}