using System;
using System.Collections.Generic;
using MySocialPortalDesktop.ViewModels;

namespace MySocialPortalDesktop.Comparer
{
    public class PersonViewModelComparer : IComparer<PersonViewModel>
    {
        private readonly int _multiplier;
        private readonly SortFieldEnum _sortField;

        public PersonViewModelComparer():this(true, SortFieldEnum.DisplayName)
        {

        }

        public PersonViewModelComparer(bool increasing, SortFieldEnum sortOn)
        {
            if(increasing)
            {
                _multiplier = 1;
            }
            else
            {
                _multiplier = -1;
            }
            
            _sortField = sortOn;
        }

        public int Compare(PersonViewModel x, PersonViewModel y)
        {
            if (x == null || y == null)
            {
                return 0;
            }
            
            if(_sortField == SortFieldEnum.DisplayName)
            {
                return _multiplier * string.Compare(x.DisplayName, y.DisplayName, StringComparison.Ordinal);
            }

            return _multiplier * string.Compare(x.DisplayUsername, y.DisplayUsername, StringComparison.Ordinal);

        }
        
        public enum SortFieldEnum
        {
            DisplayName,
            DisplayUsername
        }

    }
}