using System;
using System.Linq;
using Avalonia.Media.Imaging;
using MySocialPortalDesktop.Factory;
using MySocialPortalDesktop.Services;
using MySocialPortalDesktop.Util;
using MySocialPortalLib.Model;
using ReactiveUI;

namespace MySocialPortalDesktop.ViewModels
{
    public class PersonViewModel : ViewModelBase
    {
        private string _displayName;
        private string _displayUsername;
        private Bitmap _displayIcon;
        private Person _person;
        private PostTimelineViewModel _timelineToDrive;
        private ProfileViewModel _profileViewToDrive;
        private IMultiViewUpdater _viewToUpdate;
        private bool _isFavorite;

        public PostTimelineViewModel TimelineToDrive
        {
            get => _timelineToDrive;
            set => this.RaiseAndSetIfChanged(ref _timelineToDrive, value);
        }

        public string DisplayName
        {
            get => _displayName;
            set => this.RaiseAndSetIfChanged(ref _displayName, value);
        }

        public string DisplayUsername
        {
            get => _displayUsername;
            set => this.RaiseAndSetIfChanged(ref _displayUsername, value);
        }

        public Bitmap DisplayIcon
        {
            get => _displayIcon;
            set => this.RaiseAndSetIfChanged(ref _displayIcon, value);
        }

        public bool IsFavorite
        {
            get => _isFavorite;
            set => this.RaiseAndSetIfChanged(ref _isFavorite, value);
        }

        public IMultiViewUpdater ViewToUpdate
        {
            get => _viewToUpdate;
            set => this.RaiseAndSetIfChanged(ref _viewToUpdate, value);
        }

        public Person Person
        {
            get => _person;
            set
            {
                this.RaiseAndSetIfChanged(ref _person, value);
                UpdatePersonData();
            }
        }

        public ProfileViewModel ProfileViewToDrive
        {
            get => _profileViewToDrive;
            set => this.RaiseAndSetIfChanged(ref _profileViewToDrive, value);
        }

        public PersonViewModel(Person person)
        {
            Person = person;
        }

        public void ViewTimeline()
        {
            Console.WriteLine($"Change timeline to be for user {Person.Name}");
            TimelineToDrive?.ConfigureSources(Person);
            ViewToUpdate.ActivatePostsView();
        }

        public void ViewProfile()
        {
            Console.WriteLine($"Setting profile view for {Person.Name}");
            ProfileViewToDrive.Person = Person;
            ViewToUpdate.ActivateProfileView();
        }

        public void AddToFavorites()
        {
            RepositoryFactory.Instance.ListsRepository.Add(Person.Id, DefaultValueService.FavoriteListName);
            IsFavorite = true;
        }

        public void RemoveFromFavorites()
        {
            RepositoryFactory.Instance.ListsRepository.RemoveIdFromList(Person.Id, DefaultValueService.FavoriteListName);
            IsFavorite = false;
        }
        
        private void UpdatePersonData()
        {
            DisplayName = string.IsNullOrWhiteSpace(Person.Name) ? "" : Person.Name;
            DisplayUsername = PersonDataGenerator.GetDefaultUserName(Person).Result;
            DisplayIcon = PersonDataGenerator.LoadProfileImage(Person).Result;
            IsFavorite = RepositoryFactory.Instance.ListsRepository
                .GetAllListsForId(Person.Id)
                .Contains(DefaultValueService.FavoriteListName);
        }

    }
}