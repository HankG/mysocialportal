using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MySocialPortalLib.Model;
using ReactiveUI;

namespace MySocialPortalDesktop.ViewModels
{
    public class LabelValueListControlViewModel : ViewModelBase
    {
        private bool _isEditing;
        private LabelValueViewModel _selectedItem;

        public bool IsEditing
        {
            get => _isEditing;
            set
            {
                if (_isEditing != value)
                {
                    Items.ToList().ForEach(i => i.IsEditing = value);
                }

                this.RaiseAndSetIfChanged(ref _isEditing, value);
            }
        }

        public ObservableCollection<LabelValueViewModel> Items { get; }

        public LabelValueViewModel SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }

        public LabelValueListControlViewModel() : this(false)
        {

        }

        public LabelValueListControlViewModel(bool isEditing) : this(new List<LabelValueViewModel>(), isEditing)
        {

        }

        public LabelValueListControlViewModel(IEnumerable<LabelValueViewModel> initialItems, bool isEditing)
        {
            Items = new ObservableCollection<LabelValueViewModel>(initialItems);
            IsEditing = isEditing;
        }

        public void AddItem()
        {
            Items.Add(new LabelValueViewModel("<new label>", "<new value>", IsEditing));
        }

        public void RemoveItem()
        {
            if (SelectedItem == null)
            {
                return;
            }

            Items.Remove(SelectedItem);
        }
    }
}