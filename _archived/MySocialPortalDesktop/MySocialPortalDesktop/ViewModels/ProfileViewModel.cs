using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Avalonia.Controls;
using Avalonia.Media.Imaging;
using DynamicData;
using MySocialPortalDesktop.Converter;
using MySocialPortalDesktop.Factory;
using MySocialPortalDesktop.Util;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using ReactiveUI;

namespace MySocialPortalDesktop.ViewModels
{
    public class ProfileViewModel : ViewModelBase
    {
        private const string EditingButtonStartEditing = "Start Editing";
        private const string EditingButtonStopEditing = "Cancel Editing";
        private Bitmap _profileIcon;
        private Person _person;
        private bool _isAddressesFieldVisible;
        private bool _isBirthdayFieldVisible;
        private bool _isEditable;
        private bool _isEmailFieldVisible;
        private bool _isProfileViewEnabled;
        private bool _isPropertyFieldVisible;
        private bool _isPhoneNumbersFieldVisible;
        private bool _isSocialMediaFieldVisible;
        private bool _isWebsitesFieldVisible;
        private string _editingButtonLabel;

        private string _realName;
        private string _birthday;

        public bool IsProfileViewEnabled
        {
            get => _isProfileViewEnabled;
            set => this.RaiseAndSetIfChanged(ref _isProfileViewEnabled, value);
        }

        public LabelValueListControlViewModel AdditionalPropertiesViewModel { get; }

        public LabelValueListControlViewModel AddressesViewModel { get; }

        public string Birthday
        {
            get => _birthday;
            set => this.RaiseAndSetIfChanged(ref _birthday, value);
        }

        public LabelValueListControlViewModel EmailsViewModel { get; }

        public string EditingButtonLabel
        {
            get => _editingButtonLabel;
            set => this.RaiseAndSetIfChanged(ref _editingButtonLabel, value);
        }

        public bool IsAddressesFieldVisible
        {
            get => _isAddressesFieldVisible;
            set => this.RaiseAndSetIfChanged(ref _isAddressesFieldVisible, value);
        }
    
        public bool IsBirthdayFieldVisible
        {
            get => IsBirthdayFieldVisible;
            set => this.RaiseAndSetIfChanged(ref _isBirthdayFieldVisible, value);
        }

        public bool IsEditable
        {
            get => _isEditable;
            private set
            {
                if (_isEditable != value)
                {

                }
                this.RaiseAndSetIfChanged(ref _isEditable, value);
            }
        }

        public bool IsEmailFieldVisible
        {
            get => _isEmailFieldVisible;
            set => this.RaiseAndSetIfChanged(ref _isEmailFieldVisible, value);
        }

        public bool IsPhoneNumbersFieldVisible
        {
            get => _isPhoneNumbersFieldVisible;
            set => this.RaiseAndSetIfChanged(ref _isPhoneNumbersFieldVisible, value);
        }
        public bool IsPropertyFieldVisible
        {
            get => _isPropertyFieldVisible;
            set => this.RaiseAndSetIfChanged(ref _isPropertyFieldVisible, value);
        }

        public bool IsSocialMediaFieldVisible
        {
            get => _isSocialMediaFieldVisible;
            set => this.RaiseAndSetIfChanged(ref _isSocialMediaFieldVisible, value);
        }

        public bool IsWebsitesFieldVisible
        {
            get => _isWebsitesFieldVisible;
            set => this.RaiseAndSetIfChanged(ref _isWebsitesFieldVisible, value);
        }

        public Person Person
        {
            get => _person;
            set
            {
                if (value != null && !value.Equals(_person))
                {
                    UpdateFieldsWithPerson(value);
                }

                this.RaiseAndSetIfChanged(ref _person, value);
                UpdateFieldVisibility(false);
            }
        }

        public IPersonsRepository PersonsRepository { get; set; }

        public LabelValueListControlViewModel PhoneNumbersViewModel { get; }

        public Bitmap ProfilePhoto
        {
            get => _profileIcon;
            set
            {
                ThrowIfReadOnly();
                this.RaiseAndSetIfChanged(ref _profileIcon, value);
            }
        }

        public string RealName
        {
            get => _realName;
            set
            {
                ThrowIfReadOnly();
                this.RaiseAndSetIfChanged(ref _realName, value);
            }
        }

        public LabelValueListControlViewModel SocialMediaAccountsViewModel { get; }

        public LabelValueListControlViewModel WebsitesViewModel { get; }
 
        public ProfileViewModel() : this(null)
        {

        }

        public ProfileViewModel(Person person) : this(person, RepositoryFactory.Instance.MainPeopleRepository)
        {

        }

        public ProfileViewModel(Person person, IPersonsRepository personsRepository)
        {
            if (person == null)
            {
                person = RepositoryFactory.Instance.MainPeopleRepository.FindById("80BBDD51-93F5-4D3F-98C7-E120A8856D50");
            }
            IsEditable = false;
            EditingButtonLabel = EditingButtonStartEditing;
            PersonsRepository = personsRepository;
            AdditionalPropertiesViewModel = new LabelValueListControlViewModel(IsEditable);
            AddressesViewModel = new LabelValueListControlViewModel(IsEditable);
            PhoneNumbersViewModel = new LabelValueListControlViewModel(IsEditable);
            EmailsViewModel = new LabelValueListControlViewModel(IsEditable);
            SocialMediaAccountsViewModel = new LabelValueListControlViewModel(IsEditable);
            WebsitesViewModel = new LabelValueListControlViewModel(IsEditable);
            Person = person;
        }

        public void CommitChanges()
        {
            if (!IsEditable)
            {
                return;
            }

            UpdatePersonWithNewData();
            PersonsRepository?.UpdatePerson(Person);
            StopEditing();
        }

        public void StartEditing()
        {
            IsEditable = true;
            EditingButtonLabel = EditingButtonStopEditing;
            ToggleSubViewsEditing(true);
            UpdateFieldVisibility(true);
        }

        public void StopEditing()
        {
            IsEditable = false;
            ToggleSubViewsEditing(false);
            EditingButtonLabel = EditingButtonStartEditing;
            UpdateFieldsWithPerson(Person);
            UpdateFieldVisibility(false);
        }

        public void ToggleEditing()
        {
            if (IsEditable)
            {
                StopEditing();
                return;
            }

            StartEditing();
        }

        private void ThrowIfReadOnly()
        {
            if (!IsEditable)
            {
                throw new ReadOnlyException("Values shouldn't be edited at this time");
            }
        }

        private void ToggleSubViewsEditing(bool value)
        {
            AdditionalPropertiesViewModel.IsEditing = value;
            AddressesViewModel.IsEditing = value;
            EmailsViewModel.IsEditing = value;
            PhoneNumbersViewModel.IsEditing = value;
            SocialMediaAccountsViewModel.IsEditing = value;
            WebsitesViewModel.IsEditing = value;
        }

        private void UpdateFieldsWithPerson(Person newPerson)
        {
            if (newPerson == null)
            {
                return;
            }

            var oldEditable = IsEditable;
            var labelValueConvert = new LabelValueToViewModelConverter<string>();
            _isEditable = true;
            RealName = newPerson.Name;

            
            Birthday = newPerson.Birthday;
            AdditionalPropertiesViewModel.Items.Clear();
            AdditionalPropertiesViewModel.Items.AddRange(labelValueConvert.StringDictionaryToLabelValueViewModels(newPerson.AdditionalProperties));

            AddressesViewModel.Items.Clear();
            AddressesViewModel.Items.AddRange(newPerson.Addresses.Select(a => labelValueConvert.LabelViewToViewModel(a)));

            EmailsViewModel.Items.Clear();
            EmailsViewModel.Items.AddRange(newPerson.Emails.Select(a => labelValueConvert.LabelViewToViewModel(a)));

            PhoneNumbersViewModel.Items.Clear();
            PhoneNumbersViewModel.Items.AddRange(newPerson.PhoneNumbers.Select(a => labelValueConvert.LabelViewToViewModel(a)));

            SocialMediaAccountsViewModel.Items.Clear();
            SocialMediaAccountsViewModel.Items.AddRange(labelValueConvert.SocialMediaDictionaryToLabelValueViewModels(newPerson.SocialMediaAccounts));

            WebsitesViewModel.Items.Clear();
            WebsitesViewModel.Items.AddRange(newPerson.Websites.Select(a => labelValueConvert.LabelViewToViewModel(a)));

            ProfilePhoto = PersonDataGenerator.LoadProfileImage(newPerson).Result;
            _isEditable = oldEditable;
        }

        private void UpdateFieldVisibility(bool alwaysOn)
        {
            IsPropertyFieldVisible = alwaysOn || Person.AdditionalProperties.Any();
            IsAddressesFieldVisible = alwaysOn || Person.AdditionalProperties.Any();
            IsBirthdayFieldVisible = alwaysOn || string.IsNullOrWhiteSpace(Person.Birthday);
            IsEmailFieldVisible = alwaysOn || Person.Emails.Any();
            IsPhoneNumbersFieldVisible = alwaysOn || Person.PhoneNumbers.Any();
            IsSocialMediaFieldVisible = alwaysOn || Person.SocialMediaAccounts.Any();
            IsWebsitesFieldVisible = alwaysOn || Person.Websites.Any();
        }

        private void UpdatePersonWithNewData()
        {
            var labelValueConverter = new LabelValueToViewModelConverter<string>();
            Person.Name = RealName;
            Person.Birthday = Birthday;
            var updatedAddresses = AddressesViewModel.Items.Select(a => labelValueConverter.StringLabeledValueFromViewModel(a));
            Person.Addresses.Clear();
            Person.Addresses.AddRange(updatedAddresses);

            var updatedEmails = EmailsViewModel.Items.Select(a => labelValueConverter.StringLabeledValueFromViewModel(a));
            Person.Emails.Clear();
            Person.Emails.AddRange(updatedEmails);

            var updatedPhoneNumbers = PhoneNumbersViewModel.Items.Select(a => labelValueConverter.StringLabeledValueFromViewModel(a));
            Person.PhoneNumbers.Clear();
            Person.PhoneNumbers.AddRange(updatedPhoneNumbers);

            var updatedWebsites = WebsitesViewModel.Items.Select(a => labelValueConverter.StringLabeledValueFromViewModel(a));
            Person.Websites.Clear();
            Person.Websites.AddRange(updatedWebsites);

            Person.AdditionalProperties.Clear();
            foreach(var vm in AdditionalPropertiesViewModel.Items)
            {
                Person.AdditionalProperties[vm.Label] = vm.Value;
            }

            var updatedSocialMedia = new Dictionary<string, SocialMediaAccountData>();
            foreach (var vm in SocialMediaAccountsViewModel.Items)
            {
                if (!Person.SocialMediaAccounts.TryGetValue(vm.Label, out var smaData))
                { 
                    smaData = new SocialMediaAccountData
                    {
                        SocialMediaSystemName = vm.Label,
                        ProfileId = vm.Value
                    };
                }

                if (vm.Label == StandardSocialNetworkNames.Twitter)
                {
                    smaData.AdditionalProperties["ScreenName"] = vm.Value;
                }
                else
                {
                    smaData.ProfileId = vm.Value;
                }
                
                updatedSocialMedia[vm.Label] = smaData;
            }
            Person.SocialMediaAccounts = updatedSocialMedia;
        }
    }
}