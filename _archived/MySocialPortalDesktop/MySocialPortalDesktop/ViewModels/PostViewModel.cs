using System;
using Avalonia.Media.Imaging;
using MySocialPortalDesktop.Factory;
using MySocialPortalLib.Model;
using MySocialPortalLib.Service;
using ReactiveUI;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using LinqToTwitter.Net;
using MySocialPortalDesktop.Services;
using MySocialPortalDesktop.Util;

namespace MySocialPortalDesktop.ViewModels
{
    [SuppressMessage("ReSharper", "CA1031")]
    public class PostViewModel : ViewModelBase
    {
        private string _body;
        private DateTime _date;
        private string _title;
        private string _originalSocialMediaSystem;
        private Bitmap _profileImage;

        public string Title
        {
            get => _title;
            set => this.RaiseAndSetIfChanged(ref _title, value);
        }
        
        public string Body 
        {
            get => _body;
            set => this.RaiseAndSetIfChanged(ref _body, value);
        }

        public DateTime Date
        {
            get => _date;
            set => _date = this.RaiseAndSetIfChanged(ref _date, value);
        }
        
        public ObservableCollection<ExternalLinkViewModel> Links { get; private set; }

        public string OriginalSocialMediaSystem
        {
            get => _originalSocialMediaSystem;
            set => this.RaiseAndSetIfChanged(ref _originalSocialMediaSystem, value);
        }

        public Bitmap ProfileImage
        {
            get => _profileImage;
            set => this.RaiseAndSetIfChanged(ref _profileImage, value);
        }

        public PostViewModel():this(new Post())
        {
 
        }

        public PostViewModel(Post post)
        {
            if (post == null)
            {
                throw new ArgumentNullException(nameof(post));
            }
            Links = new ObservableCollection<ExternalLinkViewModel>();
            Post = post;
            Title = "Unknown Author";
            Body = post.Body;
            ProfileImage = DefaultValueService.DefaultProfileImage;
            Date = post.PostDateTime.LocalDateTime;
            
            var author = RepositoryFactory.Instance.MainPeopleRepository.FindById(post.UserId);
            if (author != null)
            {
                Title = author.Name;
                ProfileImage = PersonDataGenerator.LoadProfileImage(author).Result;
            }

            OriginalSocialMediaSystem = post.OriginalSocialMediaSystem;
            FillLinkVMs();
        }

        public ExternalLinkViewModel PreviewLink
        {
            get
            {
                if (Links.Any())
                {
                    return Links[0];
                }
            
                return new ExternalLinkViewModel();
            }
        }

        protected Post Post { get; set; }

        protected async void FillLinkVMs()
        {
            if (Post == null)
            {
                return;
            }
            
            var imagePath = ServiceFactory.Instance.LinkPreviewService.ImageCacheFolderPath;

            foreach (var link in Post.Links)
            {
                try
                {
                    var preview = await ServiceFactory.Instance.LinkPreviewService.GetPreview(link.Url).ConfigureAwait(false);
                    Links.Add(ExternalLinkViewModel.FromLinkPreview(preview, imagePath));
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error building VM for link: {link.Url}");
                    Console.WriteLine(e);
                }
            }
        }
    }
}