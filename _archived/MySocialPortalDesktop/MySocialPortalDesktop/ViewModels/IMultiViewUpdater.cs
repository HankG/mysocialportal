namespace MySocialPortalDesktop.ViewModels
{
    public interface IMultiViewUpdater
    {
        void ActivateDefaultView();
        
        void ActivatePostsView();

        void ActivateProfileView();
    }
}