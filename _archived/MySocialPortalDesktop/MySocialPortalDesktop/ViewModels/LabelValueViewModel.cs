using System;
using MySocialPortalLib.Model;
using ReactiveUI;

namespace MySocialPortalDesktop.ViewModels
{
    public class LabelValueViewModel : ViewModelBase
    {
        private const int BorderThicknessDefault = 3;
        private const int BorderThicknessEditing = 4;

        private int _borderThickness;
        private bool _isEditing;
        private string _label;
        private string _value;

        public int BorderThicknessValue
        {
            get => _borderThickness;
            set => this.RaiseAndSetIfChanged(ref _borderThickness, value);
        }

        public bool IsEditing
        {
            get => _isEditing;
            set
            {
                this.RaiseAndSetIfChanged(ref _isEditing, value);
                BorderThicknessValue = value ? BorderThicknessEditing : BorderThicknessDefault;
            }
        }

        public string Label
        {
            get => _label;
            set => this.RaiseAndSetIfChanged(ref _label, value);
        }

        public string Value
        {
            get => _value;
            set => this.RaiseAndSetIfChanged(ref _value, value);
        }

        public LabelValueViewModel() : this("", "", false)
        {

        }

        public LabelValueViewModel(string label, string value, bool isEditing)
        {
            Label = label;
            Value = value;
            IsEditing = isEditing;
        }
        
    }
}