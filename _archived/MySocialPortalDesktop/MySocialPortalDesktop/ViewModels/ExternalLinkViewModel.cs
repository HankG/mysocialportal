using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Avalonia.Media.Imaging;
using MySocialPortalLib.Model;
using ReactiveUI;

namespace MySocialPortalDesktop.ViewModels
{
    [SuppressMessage("ReSharper", "CA1056")]
    public class ExternalLinkViewModel : ViewModelBase, IDisposable
    {
        private const int DefaultMaxHeight = 150;
        private const int DefaultMaxWidth = 250;

        private bool _disposed = false;

        private int _imageMaxHeight;
        private int _imageMaxWidth;

        public string Title { get; protected set; }

        public string Description { get; protected set; }

        public bool HasImage { get; protected set; }

        public bool HasPreview { get; protected set; }

        public Bitmap Image { get; protected set; }

        public bool IsAvailable { get; protected set; }

        public int ImageMaxHeight 
        { 
            get => _imageMaxHeight;
            set => this.RaiseAndSetIfChanged(ref _imageMaxHeight, value); 
        }

        public int ImageMaxWidth
        {
            get => _imageMaxWidth;
            set => this.RaiseAndSetIfChanged(ref _imageMaxWidth, value);
        }


        public string Url { get; protected set; }

        public ExternalLinkViewModel()
        {
            HasPreview = false;
            IsAvailable = false;
            ImageMaxHeight = DefaultMaxHeight;
            ImageMaxWidth = DefaultMaxWidth;
        }

        public static ExternalLinkViewModel FromLinkPreview(LinkPreview item, string baseFolder)
        {
            var vm = new ExternalLinkViewModel();

            if (item != null && item.UrlFound)
            {
                vm.Title = item.Title;
                vm.Description = item.Description ?? "";
                vm.HasPreview = true;
                vm.Url = item.RequestUrl;
                vm.IsAvailable = true;
                if (!item.HasPreviewImage)
                {
                    return vm;
                }

                try
                {
                    var bitmapPath = Path.Combine(baseFolder, item.PreviewImageName);
                    var bitmap = new Bitmap(bitmapPath);
                    vm.Image = bitmap;
                    vm.HasImage = true;
                }
                catch (IOException e)
                {
                    Console.WriteLine(e);
                }
            }

            return vm;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                Image?.Dispose();
            }

            _disposed = true;
        }
    }
}