﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using DynamicData;
using LiteDB;
using MySocialPortalDesktop.Factory;
using MySocialPortalDesktop.Services;
using MySocialPortalLib.Model;
using MySocialPortalLib.Service.SocialMediaConnectors;
using MySocialPortalLib.Util;
using ReactiveUI;

namespace MySocialPortalDesktop.ViewModels
{
    [SuppressMessage("ReSharper", "CA1822")]
    [SuppressMessage("ReSharper", "CA1303")]
    [SuppressMessage("ReSharper", "CA1031")]
    public class MainWindowViewModel : ViewModelBase, IMultiViewUpdater
    {
        private const int DefaultMaxPosts = 50;
        private const bool InitialProfileViewEnabled = false;
        private const bool InitialPostViewEnable = !InitialProfileViewEnabled;
        
        private ProfileViewModel _currentProfileViewModel;

        public ProfileViewModel CurrentProfileViewModel
        {
            get => _currentProfileViewModel;
            set => this.RaiseAndSetIfChanged(ref _currentProfileViewModel, value);
        }

        public PeopleListViewModel PeopleListViewModel { get; }
        
        public PostTimelineViewModel PostTimelineViewModel { get; }
        
        public ObservableCollection<string> UserLists { get; }
        
        public MainWindowViewModel()
        {
            ImportedPosts = new List<Post>();
            var person = new Person
            {
                Name = "Real name"
            };
            CurrentProfileViewModel = new ProfileViewModel(person);
            CurrentProfileViewModel.IsProfileViewEnabled = InitialProfileViewEnabled;
            PostTimelineViewModel = DefaultValueService.GetDefaultHomePostTimelineViewModel(DefaultMaxPosts);
            PostTimelineViewModel.IsPostTimelineEnabled = InitialPostViewEnable;
            PeopleListViewModel = new PeopleListViewModel(PostTimelineViewModel, CurrentProfileViewModel, this);
            UserLists = new ObservableCollection<string>();
            var userLists = RepositoryFactory.Instance.ListsRepository.GetAllLists();
            UserLists.AddRange(userLists);
        }

        public void ActivateDefaultView()
        {
            ActivatePostsView();
        }

        public void ActivatePostsView()
        {
            CurrentProfileViewModel.IsProfileViewEnabled = false;
            PostTimelineViewModel.IsPostTimelineEnabled = true;
        }

        public void ActivateProfileView()
        {
            PostTimelineViewModel.IsPostTimelineEnabled = false;
            CurrentProfileViewModel.IsProfileViewEnabled = true;
        }

        public void DoExit()
        {
            var app = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            app?.Shutdown();
        }

        public void GoHome()
        {
            ActivatePostsView();
            PostTimelineViewModel.GoHome();
        }

        public void LoadNewer()
        {
            ActivatePostsView();
            PostTimelineViewModel.LoadNewer();
        }

        public void LoadOlder()
        {
            ActivatePostsView();
            PostTimelineViewModel.LoadOlder();
        }
        
        public async void ImportPosts()
        {
            var app = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            var window = app.MainWindow;
            var fileDialog = new OpenFileDialog
            {
                
                Directory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Title = "Import Posts Database",
                AllowMultiple = false,
                Filters = new List<FileDialogFilter>
                {
                    new FileDialogFilter
                    {
                        Name = "Posts JSON",
                        Extensions = new List<string>{"json"}
                    }
                }
            };
            await fileDialog.ShowAsync(window).ConfigureAwait(false);
            Console.WriteLine("Will import posts when get better post processor");
        }

        public async void ImportPeople()
        {
            var app = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            var window = app.MainWindow;
            var fileDialog = new OpenFileDialog
            {
                
                Directory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Title = "Import People Database",
                AllowMultiple = false,
                Filters = new List<FileDialogFilter>
                {
                    new FileDialogFilter
                    {
                        Name = "People JSON",
                        Extensions = new List<string>{"json"}
                    }
                }
            };
            var result = await fileDialog.ShowAsync(window).ConfigureAwait(false);
            await ImportPeopleJsonToDatabase(result?.First()).ConfigureAwait(false);
            PeopleListViewModel.ShowAll(true);
            Console.WriteLine("Done importing people");
        }

        private List<Post> ImportedPosts { get; }

        private async Task<bool> ImportPeopleJsonToDatabase(string path)
        {
            if (path == null)
            {
                Console.WriteLine("Path null not doing an import");
                return false;
            }

            if (!File.Exists(path))
            {
                Console.WriteLine($"Requested JSON file doesn't exist or don't have read permissions: {path}");
            }

            try
            {
                Console.WriteLine("Read in JSON");
                var peopleFromJson = await Task.Run(() => System.Text.Json.JsonSerializer
                    .Deserialize<List<Person>>(File.ReadAllText(path)))
                    .ConfigureAwait(false);
                Console.WriteLine("Load all people from DB");
                var peopleFromDb = await Task.Run(() => RepositoryFactory.Instance.MainPeopleRepository
                    .ListAll())
                    .ConfigureAwait(false);
                Console.WriteLine("Merge two sets of people");
                var (newPersons, mergedPersons, _) = PersonDatabaseMergingUtils.Merge(peopleFromDb, peopleFromJson);
                Console.WriteLine($"New People: {newPersons?.Count}");
                Console.WriteLine($"Merged People: {mergedPersons?.Count}");
                Console.WriteLine("Write new people to the database");
                await Task.Run(() => newPersons.ToList()
                    .ForEach(p => RepositoryFactory.Instance.MainPeopleRepository.AddPerson(p)))
                    .ConfigureAwait(false);
                Console.WriteLine("Update existing people in database");
                await Task.Run(() => mergedPersons.ToList()
                    .ForEach(p => RepositoryFactory.Instance.MainPeopleRepository.UpdatePerson(p)))
                    .ConfigureAwait(false);
                Console.WriteLine("Update People list view");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;
        }

    }
}
