using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using DynamicData;
using MySocialPortalDesktop.Factory;
using MySocialPortalLib.Model;
using MySocialPortalLib.Service.SocialMediaConnectors;
using MySocialPortalLib.Util;
using ReactiveUI;

namespace MySocialPortalDesktop.ViewModels
{
    [SuppressMessage("ReSharper", "CA1303")]
    [SuppressMessage("ReSharper", "CA1031")]
    public class PostTimelineViewModel : ViewModelBase
    {
        private const int MaxPostsQuery = 20;
        private const int MaxPostHistory = 50;

        private bool _isPostTimelineEnabled;

        public bool IsPostTimelineEnabled
        {
            get => _isPostTimelineEnabled;
            set => this.RaiseAndSetIfChanged(ref _isPostTimelineEnabled, value);
        }

        public ObservableCollection<PostViewModel> PostViewModels { get; }

        public PostTimelineViewModel(IEnumerable<Post> initialPosts)
        {
            PostViewModels = new ObservableCollection<PostViewModel>();
            TwitterConnector = SocialMediaConnectorFactory.GetNewTwitterConnector();
            IsPostTimelineEnabled = true;
            LoadPosts(initialPosts);     
        }
        
        public PostTimelineViewModel():this(null)
        {
            
        }

        public void ConfigureSources(Person person = null, string network = "")
        {
            IEnumerable<Post> posts;
            CurrentPerson = person;
            if (person == null)
            {
                Console.WriteLine("Getting 'Home' view");
                posts = RepositoryFactory.Instance.AllPostsRepository.GetPosts(MaxPostHistory);
            }
            else
            {
                Console.WriteLine($"Configuring timeline for User '{CurrentPerson.Name}' and network '{network}'");
                posts = RepositoryFactory.Instance.AllPostsRepository.GetPosts(MaxPostHistory, CurrentPerson.Id, network);
            }
            PostViewModels.Clear();
            LoadPosts(posts);
        }

        public void GoHome()
        {
            ConfigureSources(null);
        }

        public void LoadNewer()
        {
            try
            {
                var posts = CurrentPerson == null ? 
                    TwitterConnector.GetNewerHomeTimeline(10).ToList() : 
                    TwitterConnector.GetNewerUserTimeline(CurrentPerson, MaxPostsQuery).ToList();
                
                
                PostDatabaseMergingUtils.AddOrUpdateToRepository(RepositoryFactory.Instance.AllPostsRepository, posts);
                AddPostsToViewList(posts, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void LoadOlder()
        {
            try
            {
                var posts = CurrentPerson == null ? 
                    TwitterConnector.GetOlderHomeTimeline(10).ToList() : 
                    TwitterConnector.GetOlderUserTimeline(CurrentPerson, MaxPostsQuery).ToList();
                
                PostDatabaseMergingUtils.AddOrUpdateToRepository(RepositoryFactory.Instance.AllPostsRepository, posts);
                AddPostsToViewList(posts, false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        
        
        public async void RefreshList(string listName)
        {
            if (string.IsNullOrWhiteSpace(listName))
            {
                return;
            }

            Console.WriteLine($"Attempting to pull updates for users in list: {listName}");

            var newPosts = await UserListPostUpdaterFactory.GetDefault().Update(listName).ConfigureAwait(false);
            Console.WriteLine($"# posts pulled on updates: {newPosts?.Count}");
            AddPostsToViewList(newPosts, false);
        }

        
        private Person CurrentPerson { get; set; }
        private TwitterConnector TwitterConnector { get; }

        private void LoadPosts(IEnumerable<Post> posts)
        {
            if (posts == null)
            {
                return;
            }
            var postsList = posts.ToList();
            postsList.Sort(new PostComparisonDescending());
            postsList.ForEach(p => PostViewModels.Add(new PostViewModel(p)));
        }

        private void AddPostsToViewList(List<Post> posts, bool insertTop)
        {
            if (posts.Count == 0)
            {
                Console.WriteLine("No posts returned");
                return;
            }

            if (insertTop)
            {
                posts.Sort(new PostComparison(true));
                posts.ForEach(p => PostViewModels.Insert(0, new PostViewModel(p)));
            }
            else
            {
                posts.Sort(new PostComparison(false));
                
                foreach (var post in posts)
                {
                    var found = false;
                    var currentGuessIndex = 0;
                    while (!found)
                    {
                        if (PostViewModels.Count == 0)
                        {
                            PostViewModels.Add(new PostViewModel(post));
                            found = true;
                            continue;
                        }
                    
                        var postDate = post.PostDateTime.LocalDateTime;
                        var guessDate = PostViewModels[currentGuessIndex].Date;

                        if (postDate >= guessDate)
                        {
                            PostViewModels.Insert(currentGuessIndex, new PostViewModel(post));
                            found = true;
                            continue;
                        }

                        if (currentGuessIndex == PostViewModels.Count - 1)
                        {
                            PostViewModels.Insert(currentGuessIndex, new PostViewModel(post));
                            found = true;
                            continue;
                        }

                        currentGuessIndex++;
                        
                    }
                }
                //PostViewModels.AddRange(posts.Select(p => new PostViewModel(p)));
            }
        }
    }
}