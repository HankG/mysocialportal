using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using MySocialPortalDesktop.Comparer;
using MySocialPortalDesktop.Factory;
using MySocialPortalLib.Model;
using ReactiveUI;

namespace MySocialPortalDesktop.ViewModels
{
    public class PeopleListViewModel : ViewModelBase
    {
        private string _filterText;
        public string FilterText
        {
            get => _filterText;
            set
            {
                this.RaiseAndSetIfChanged(ref _filterText, value);
                UpdateViewFilter();
            }
        }

        public ObservableCollection<PersonViewModel> People { get; }

        public PeopleListViewModel(PostTimelineViewModel relatedTimelineViewModel,
            ProfileViewModel relatedProfileViewModel,
            IMultiViewUpdater relatedViewUpdater)
        {
            RelatedTimelineViewModel = relatedTimelineViewModel;
            RelatedMultiViewUpdater = relatedViewUpdater;
            RelatedProfileViewModel = relatedProfileViewModel;
            var peopleVms = GetAllPersonsFromDatabase();
            People = new ObservableCollection<PersonViewModel>(peopleVms);
            FullPeopleList = new List<PersonViewModel>(peopleVms);
            ListFilteredPeople = new List<PersonViewModel>(peopleVms);
            CurrentViewFilter = FilterText = "";
        }
        
        public void ChangeSortOrderCommand(string sortName)
        {
            switch(sortName)
            {
                case "Name (A-Z)":
                    CurrentComparer = new PersonViewModelComparer(true, PersonViewModelComparer.SortFieldEnum.DisplayName);
                    break;
                case "Name (Z-A)":
                    CurrentComparer = new PersonViewModelComparer(false, PersonViewModelComparer.SortFieldEnum.DisplayName);
                    break;
                case "Username (A-Z)":
                    CurrentComparer = new PersonViewModelComparer(true, PersonViewModelComparer.SortFieldEnum.DisplayUsername);
                    break;
                case "Username (Z-A)":
                    CurrentComparer = new PersonViewModelComparer(false, PersonViewModelComparer.SortFieldEnum.DisplayUsername);
                    break;
                default:
                    CurrentComparer = null;
                    break;
            }

            UpdateSortedView();
        }

        public void ShowAll(bool resetFromDb = false)
        {
            if (resetFromDb)
            {
                FullPeopleList.Clear();
                FullPeopleList.AddRange(GetAllPersonsFromDatabase());
            }
            ListFilteredPeople.Clear();
            ListFilteredPeople.AddRange(FullPeopleList);
            UpdateViewFilter(true);
        }
        
        public void ToggleVisibleList(string list)
        {
            var idsInList = RepositoryFactory.Instance.ListsRepository.GetAllIdsForList(list).ToHashSet();
            var peopleVmsInList = FullPeopleList.FindAll(p => idsInList.Contains(p.Person.Id));
            ListFilteredPeople.Clear();
            ListFilteredPeople.AddRange(peopleVmsInList);
            UpdateViewFilter(true);
        }

        private IComparer<PersonViewModel> CurrentComparer { get; set; }
        
        private string CurrentViewFilter { get; set; }
        
        private List<PersonViewModel> ListFilteredPeople { get; }
        
        private List<PersonViewModel> FullPeopleList { get; }
        
        private IMultiViewUpdater RelatedMultiViewUpdater { get; }
        
        private ProfileViewModel RelatedProfileViewModel { get; }
        
        private PostTimelineViewModel RelatedTimelineViewModel { get; }
        
        private void UpdateSortedView()
        {
            if(CurrentComparer == null)
            {
                return;
            }

            var sortedFriends = new List<PersonViewModel>(People);
            sortedFriends.Sort(CurrentComparer);
            People.Clear();
            sortedFriends.ForEach(f => People.Add(f));
        }

        private void UpdateViewFilter(bool forceRefresh = false)
        {
            if(FilterText == CurrentViewFilter && !forceRefresh)
            {
                return;
            }

            CurrentViewFilter = FilterText;
            var filter = FilterText.ToUpperInvariant();
            var filteredPeople = FilterText.Length == 0 ? new List<PersonViewModel>(ListFilteredPeople) 
                : FullPeopleList.FindAll(
                    f => f.DisplayName.ToUpperInvariant().Contains(filter, StringComparison.InvariantCulture) 
                         || f.DisplayName.Contains(filter, StringComparison.InvariantCulture));
            
            if(CurrentComparer != null)
            {
                filteredPeople.Sort(CurrentComparer);
            }
            People.Clear();
            filteredPeople.ForEach(f => People.Add(f));
        }

        private List<PersonViewModel> GetAllPersonsFromDatabase()
        {
            var allPeople = RepositoryFactory.Instance.MainPeopleRepository.ListAll();
            var peopleVms = allPeople?.Select(p => new PersonViewModel(p)).ToList() ?? new List<PersonViewModel>();
            if (RelatedTimelineViewModel != null)
            {
                peopleVms.ForEach(p =>
                {
                    p.TimelineToDrive = RelatedTimelineViewModel;
                    p.ViewToUpdate = RelatedMultiViewUpdater;
                    p.ProfileViewToDrive = RelatedProfileViewModel;
                });
            }

            return peopleVms;
        }
    }
}