using System;
using System.Diagnostics.CodeAnalysis;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using MySocialPortalDesktop.ViewModels;
using MySocialPortalDesktop.Views;

namespace MySocialPortalDesktop
{
    [SuppressMessage("ReSharper", "CA1303")]
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            var window = new MainWindow
            {
                DataContext = new MainWindowViewModel(),
            };
            
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
                desktop.MainWindow = window;
            else if (ApplicationLifetime is ISingleViewApplicationLifetime singleView)
                throw new ArgumentException("View apps aren't supported at this time");

            base.OnFrameworkInitializationCompleted();
        }
    }
}