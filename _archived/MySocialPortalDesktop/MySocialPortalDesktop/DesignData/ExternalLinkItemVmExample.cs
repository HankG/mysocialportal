using MySocialPortalDesktop.Factory;
using MySocialPortalDesktop.ViewModels;

namespace MySocialPortalDesktop.DesignData
{
    public class ExternalLinkItemVmExample : ExternalLinkViewModel
    {
        public ExternalLinkItemVmExample() : base()
        {
            var lps = ServiceFactory.Instance.LinkPreviewService;
            var refItem = lps.GetPreview("https://everydayastronaut.com/aerospikes/").Result;
            var tmp = ExternalLinkViewModel.FromLinkPreview(refItem, lps.ImageCacheFolderPath);
            Title = tmp.Title;
            Description = tmp.Description;
            HasPreview = true;
            HasImage = tmp.HasImage;
            Image = tmp.Image;
            Url = tmp.Url;
        }
    }
}