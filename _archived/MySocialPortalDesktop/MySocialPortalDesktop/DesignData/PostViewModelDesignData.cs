using Avalonia.Media.Imaging;
using MySocialPortalDesktop.ViewModels;
using MySocialPortalLib.Model;
using MySocialPortalLib.Service;
using System.IO;

namespace MySocialPortalDesktop.DesignData
{
    public class PostViewModelDesignData : PostViewModel
    {
        public PostViewModelDesignData():base()
        {
            var p = this.Post;
            Title = p.Title = "Rugby Wiki";
            Body = p.Body = "I have lots of friends that play rugby. Seeing this photo in my stream decided to find a Wiki on it and share...";
            OriginalSocialMediaSystem = p.OriginalSocialMediaSystem = "HandJam";
            p.Links.Add(new ExternalLink
            {
                Name = "Rugby Football",
                Url = "https://en.wikipedia.org/wiki/Rugby_football"
            });
            //p.Media.Add(new MySocialPortalLib.Model.MediaData
            //{
            //    Description = "Guys playing rugby",
            //    LocalPath = "ManRugby.jpg",
            //    OriginalUrl = "https://www.flickr.com/photos/naparazzi/18700282260/in/photolist-uutQpE-2hmHERb-iRhnz-2cfFTC-bebnbT-2hEvfBp-6vejuD-bzrDNY-23rEk6j-93DvNp-6e4wcn-5sxjDa-5sBDH9-5sxhTR-5sxjwc-5sxjB4-5sxhNK-5sxjtr-5sBDLU-5sxhSc-5sBDR3-W4bHQq-cd6prf-215tBJ7-6ve93v-2gSrjSs-6vecs4-6vikyj-6vembx-2hFRkU1-6ve7Er-28F746f-WXgnU2-6vegbH-6ve73g-6veeQp-6vij4J-MRR7T8-6vebiM-6vius5-6veati-AXwY3S-BRPiXJ-2cijvy-85BZqJ-6vitsY-2ehzkL8-2cinNd-XNwqEE-2cb8ZX",
            //    MediaType = "Photo",
            //    Title = "Brett Manning"
            //});

            Links?.Add(new ExternalLinkItemVmExample() as ExternalLinkViewModel);
            //FillLinkVMs();
        }
    }
}