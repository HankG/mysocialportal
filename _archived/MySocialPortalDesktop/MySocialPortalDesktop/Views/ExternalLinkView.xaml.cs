using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using MySocialPortalDesktop.ViewModels;
using MySocialPortalLib.Service;

namespace MySocialPortalDesktop.Views
{
    public class ExternalLinkView : UserControl
    {
        public ExternalLinkView()
        {
            InitializeComponent();
            SetupEventHandlers();
        }
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private void SetupEventHandlers()
        {
            var grid = this.FindControl<Grid>("MainGrid");
            grid.PointerPressed += (e, a) =>
            {
                if (DataContext is ExternalLinkViewModel vm
                    && !string.IsNullOrEmpty(vm.Url))
                {
                    ExternalAppService.OpenWebPage(vm.Url);
                }
            };

        }


    }
}