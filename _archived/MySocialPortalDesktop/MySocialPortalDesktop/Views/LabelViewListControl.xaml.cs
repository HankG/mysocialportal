using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace MySocialPortalDesktop.Views
{
    public class LabelValueListControl : UserControl
    {
        public LabelValueListControl()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}