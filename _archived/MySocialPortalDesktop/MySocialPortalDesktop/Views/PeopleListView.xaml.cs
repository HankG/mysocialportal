using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using MySocialPortalDesktop.ViewModels;

namespace MySocialPortalDesktop.Views
{
    public class PeopleListView : UserControl
    {
        public PeopleListView()
        {
            InitializeComponent();
            SetupCallbacks();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private void SetupCallbacks()
        {
            var sortOrder = this.Find<ComboBox>("SortOrderComboBox");
            sortOrder.SelectionChanged += (sender, e) =>
            {
                var vm = this.DataContext as PeopleListViewModel;
                if (vm == null)
                {
                    return;
                }

                var selection = (e?.AddedItems[0] as ComboBoxItem)?.Content?.ToString();
                vm.ChangeSortOrderCommand(selection);
            };
            
            
            var visibleUsers = this.Find<ComboBox>("VisibleUsersSelection");
            visibleUsers.SelectionChanged += (sender, e) =>
            {
                var vm = this.DataContext as PeopleListViewModel;
                if (vm == null)
                {
                    return;
                }

                var selection = (e?.AddedItems[0] as ComboBoxItem)?.Content?.ToString();
                if (selection == "All")
                {
                    vm.ShowAll();
                }
                else
                {
                    vm.ToggleVisibleList(selection);
                }
            };

        }
    }
}