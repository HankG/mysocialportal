using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Ribbon;
using Avalonia.Markup.Xaml;
using MySocialPortalDesktop.Factory;
using MySocialPortalDesktop.ViewModels;

namespace MySocialPortalDesktop.Views
{
    public class MainWindow : Window
    {
        private RibbonComboButton _refreshUsersFromListCombo;
        public MainWindow()
        {
            InitializeComponent();
            InitializeCallbacks();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private void InitializeCallbacks()
        {
            _refreshUsersFromListCombo = this.FindControl<RibbonComboButton>("RefreshPeopleInList");
            _refreshUsersFromListCombo.SelectionChanged += RefreshSelectedUserList;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            RepositoryFactory.Instance.Dispose();
        }
        
        private void RefreshSelectedUserList(object sender, SelectionChangedEventArgs args)
        {
            if (args.AddedItems.Count == 0)
            {
                return;
            }

            var listName = args.AddedItems[0] as string;
            (DataContext as MainWindowViewModel)?.PostTimelineViewModel?.RefreshList(listName);
        }

    }
}