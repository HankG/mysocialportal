using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace MySocialPortalDesktop.Views
{
    public class ProfileView : UserControl
    {
        public ProfileView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}