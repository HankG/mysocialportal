using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace MySocialPortalDesktop.Views
{
    public class PostView : UserControl
    {
        public PostView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}