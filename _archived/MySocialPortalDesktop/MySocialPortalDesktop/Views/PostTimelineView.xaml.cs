using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace MySocialPortalDesktop.Views
{
    public class PostTimelineView : UserControl
    {
        public PostTimelineView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}