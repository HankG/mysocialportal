using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Avalonia.Media.Imaging;
using MySocialPortalDesktop.Factory;
using MySocialPortalDesktop.ViewModels;
using MySocialPortalLib.Model;
using MySocialPortalLib.Service;

namespace MySocialPortalDesktop.Services
{
    [SuppressMessage("ReSharper", "CA1031")]
    public static class DefaultValueService
    {
        private const string DefaultProfileImageName = "default_profile_icon.png";
        public const string FavoriteListName = "Favorites";

        private static readonly Lazy<Bitmap> DefaultProfileImageLazy = new Lazy<Bitmap>(BuildDefaultProfileImage);

        public static Bitmap DefaultProfileImage => DefaultProfileImageLazy.Value;
        
        private static Bitmap BuildDefaultProfileImage()
        {
            try
            {
                string directory = DirectoryServices.Instance.ProfileImagesDirectory();
                return new Bitmap(Path.Combine(directory, DefaultProfileImageName));
            }
            catch
            {
                return null;
            }
        }

        public static PostTimelineViewModel GetDefaultHomePostTimelineViewModel(int maxPosts)
        {
            var posts = RepositoryFactory.Instance.AllPostsRepository.GetPosts(maxPosts);
            return new PostTimelineViewModel(posts);
        }
    }
}