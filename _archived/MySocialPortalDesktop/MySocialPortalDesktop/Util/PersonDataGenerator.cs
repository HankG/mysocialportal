using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Avalonia.Media.Imaging;
using JetBrains.Annotations;
using MySocialPortalDesktop.Factory;
using MySocialPortalDesktop.Services;
using MySocialPortalLib.Model;
using MySocialPortalLib.Service;

namespace MySocialPortalDesktop.Util
{
    [SuppressMessage("ReSharper", "CA1031")]
    public static class PersonDataGenerator
    {
        public static async Task<string> GetDefaultUserName(Person person)
        {
            const string unknownValue = "N/A";
            const string screenNameKey = "ScreenName";
            if (person == null)
            {
                return unknownValue;
            }

            var screenName = "";
            var sma = person.SocialMediaAccounts;
            if (sma.ContainsKey(StandardSocialNetworkNames.Twitter))
            {
                person.SocialMediaAccounts[StandardSocialNetworkNames.Twitter]
                    ?.AdditionalProperties.TryGetValue(screenNameKey, out screenName);
            }

            return string.IsNullOrWhiteSpace(screenName) ? unknownValue : screenName;
        }

        public static async Task<Bitmap> LoadProfileImage(Person person)
        {
            if (person == null)
            {
                return DefaultValueService.DefaultProfileImage;
            }
            
            try
            {
                var profileImageUrl = person.SocialMediaAccounts.First().Value.ProfilePhotoPath;
                if (string.IsNullOrWhiteSpace(profileImageUrl))
                {
                    return DefaultValueService.DefaultProfileImage;
                }
                
                if (Uri.IsWellFormedUriString(profileImageUrl, UriKind.Absolute))
                {
                    var cancelToken = new CancellationTokenSource(2000);
                    var task = ServiceFactory.Instance.ProfileImageService
                        .GetRemoteImage(new Uri(profileImageUrl));
                    task.Wait(100, cancelToken.Token);
                    if (task.IsCompletedSuccessfully)
                    {
                        var (found, imageFileInfo) = task.Result;
                        if (found)
                        {
                            return new Bitmap(imageFileInfo.FullName);
                        }
                    }
                }

                var localImagePath = Path.Combine(DirectoryServices.Instance.ProfileImagesDirectory(), profileImageUrl);
                if (File.Exists(localImagePath))
                {
                    return new Bitmap(localImagePath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return DefaultValueService.DefaultProfileImage;
        }
        
    }
}