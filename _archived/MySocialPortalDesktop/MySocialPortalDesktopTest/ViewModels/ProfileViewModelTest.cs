using System.Data;
using System.IO;
using MySocialPortalDesktop.ViewModels;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using Xunit;

namespace MySocialPortalDesktopTest.ViewModels
{
    public class ProfileViewModelTest
    {
        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(new ProfileViewModel());
        }

        [Fact]
        public void TestStartStopEditing()
        {
            var vm = GetTempViewModel(new Person());
            Assert.False(vm.IsEditable);
            vm.StartEditing();
            Assert.True(vm.IsEditable);
            vm.StartEditing();
            Assert.True(vm.IsEditable);
            vm.StopEditing();
            Assert.False(vm.IsEditable);
            vm.StopEditing();
            Assert.False(vm.IsEditable);
        }

        [Fact]
        public void TestFailOnReadOnlyUpdate()
        {
            var person = new Person();
            var vm = GetTempViewModel(person);
            Assert.Throws<ReadOnlyException>(() => vm.RealName = "New Name");
            vm.StartEditing();
            var expectedRealName = "New Name";
            vm.RealName = expectedRealName;
            Assert.Equal(expectedRealName, vm.RealName);
        }

        [Fact]
        public void TestCommitChanges()
        {
            var person = new Person
            {
                Name = "Original Name"
            };

            
            var vm = GetTempViewModel(person);
            vm.PersonsRepository.AddPerson(person);
            vm.StartEditing();

            var expectedNewName = "New Name";
            vm.RealName = expectedNewName;

            var personFromDb = vm.PersonsRepository.FindById(person.Id);
            Assert.NotEqual(expectedNewName, personFromDb.Name);
            Assert.NotEqual(expectedNewName, person.Name);
            vm.CommitChanges();
            
            personFromDb = vm.PersonsRepository.FindById(person.Id);
            Assert.Equal(expectedNewName, personFromDb.Name);
            Assert.Equal(expectedNewName, person.Name);
        }

        [Fact]
        public void TestRevertChanges()
        {
            
        }


        private ProfileViewModel GetTempViewModel(Person person)
        {
            return new ProfileViewModel(person, GetTempPersonDb());
        }

        private PersonsLiteDbRepository GetTempPersonDb()
        {
            return new PersonsLiteDbRepository(new FileStream(Path.GetTempFileName() + "persons.db",
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }
    }
}