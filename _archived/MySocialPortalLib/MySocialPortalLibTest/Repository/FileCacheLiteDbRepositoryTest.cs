using System;
using System.IO;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using Xunit;

namespace MySocialPortalLibTest.Repository
{
    public class FileCacheLiteDbRepositoryTest
    {
        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(GetTempDb());
        }

        [Fact]
        public void TestAddItem()
        {
            var db = GetTempDb();
            var fci = new FileCacheItem();
            Assert.Equal(0, db.Count());
            db.AddOrUpdate(fci);
            Assert.Equal(1, db.Count());
            db.AddOrUpdate(fci);
            Assert.Equal(1, db.Count());
            db.AddOrUpdate(new FileCacheItem());
            Assert.Equal(2, db.Count());
        }

        [Fact]
        public void TestUpdateItem()
        {
            var db = GetTempDb();
            var id = Guid.NewGuid().ToString();
            var fci1 = new FileCacheItem()
            {
                Id = id,
                CreationTime = DateTimeOffset.UtcNow,
                LocalFileName = "localname1",
                OriginalUrl = "http://server1"
            };
            db.AddOrUpdate(fci1);
            Assert.True(fci1.AllEquals(db.FindById(id)));

            var fci2 = new FileCacheItem()
            {
                Id = id,
                CreationTime = DateTimeOffset.UtcNow,
                LocalFileName = "localname2",
                OriginalUrl = "http://server2"
            };
            db.AddOrUpdate(fci2);
            Assert.False(fci1.AllEquals(db.FindById(id)));
            Assert.True(fci2.AllEquals(db.FindById(id)));
        }
        
        [Fact]
        public void TestQueryById()
        {
            var db = GetTempDb();
            var id = Guid.NewGuid().ToString();
            var fci1 = new FileCacheItem()
            {
                Id = id,
            };
            db.AddOrUpdate(fci1);
            Assert.Equal(fci1,db.FindById(id));
        }

        [Fact]
        public void TestQueryByUrl()
        {
            var db = GetTempDb();
            var url = "http://server1";
            var fci1 = new FileCacheItem()
            {
                OriginalUrl = url
            };
            db.AddOrUpdate(fci1);
            Assert.Equal(fci1,db.FindByUrl(url));
        }

        [Fact]
        public void TestRemoveItem()
        {
            var db = GetTempDb();
            var fci = new FileCacheItem();
            db.AddOrUpdate(fci);
            db.AddOrUpdate(new FileCacheItem());
            Assert.Equal(2, db.Count());
            Assert.True(db.Remove(fci));
            Assert.Equal(1, db.Count());
        }

        [Fact]
        public void TestRemoveItemById()
        {
            var db = GetTempDb();
            var fci = new FileCacheItem();
            db.AddOrUpdate(fci);
            db.AddOrUpdate(new FileCacheItem());
            Assert.Equal(2, db.Count());
            Assert.True(db.RemoveById(fci.Id));
            Assert.Equal(1, db.Count());
        }

        [Fact]
        public void TestRemoveWithProblems()
        {
            var db = GetTempDb();
            db.AddOrUpdate(new FileCacheItem()); 
            db.AddOrUpdate(new FileCacheItem());
            Assert.Equal(2, db.Count());
            Assert.False(db.Remove(null));
            Assert.False(db.Remove(new FileCacheItem {Id = Guid.NewGuid().ToString()}));
            Assert.False(db.RemoveById(null));
            Assert.False(db.RemoveById(Guid.NewGuid().ToString()));
            Assert.Equal(2, db.Count());
        }

        private FileCacheLiteDbRepository GetTempDb()
        {
            return new FileCacheLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }
    }
}