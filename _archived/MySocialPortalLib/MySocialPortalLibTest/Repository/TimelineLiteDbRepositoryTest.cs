using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using Xunit;

namespace MySocialPortalLibTest.Repository
{
    public class TimelineLiteDbRepositoryTest
    {
        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(GetTempDb());
        }

        [Fact]
        public void TestAddInterval()
        {
            var db = GetTempDb();
            var interval = new TimelineInterval
            {
                IntervalStart = 0,
                IntervalStop = 1,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval);
            Assert.Equal(1, db.Count());
            db.AddOrUpdate(interval);
            Assert.Equal(1, db.Count());
            db.AddOrUpdate(new TimelineInterval());
            Assert.Equal(2, db.Count());
        }
        
        [Fact]
        public void TestAddIntervals()
        {
            var db = GetTempDb();
            var intervals = new List<TimelineInterval>
            {
                new TimelineInterval
                {
                    IntervalStart = 0,
                    IntervalStop = 1,
                    TimelineName = "Name1"
                },
                new TimelineInterval
                {
                    IntervalStart = 5,
                    IntervalStop = 6,
                    TimelineName = "Name1"
                }
            };
            Assert.True(db.AddOrUpdate(intervals));
            Assert.Equal(2, db.Count());
        }

        
        [Fact]
        public void TestUpdateInterval()
        {
            var db = GetTempDb();
            string id = Guid.NewGuid().ToString();
            var interval1 = new TimelineInterval
            {
                Id = id,
                IntervalStart = 0,
                IntervalStop = 1,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval1);
            Assert.Equal(1, db.Count());
            Assert.True(interval1.AllEquals(db.FindById(id)));
            var interval2 = new TimelineInterval
            {
                Id = id,
                IntervalStart = 0,
                IntervalStop = 1,
                TimelineName = "Name2"
            };
            db.AddOrUpdate(interval2);
            Assert.Equal(1, db.Count());
            Assert.False(interval1.AllEquals(db.FindById(id)));
            Assert.True(interval2.AllEquals(db.FindById(id)));
        }

        [Fact]
        public void TestFindById()
        {
            var db = GetTempDb();
            string id = Guid.NewGuid().ToString();
            var interval1 = new TimelineInterval
            {
                Id = id,
                IntervalStart = 0,
                IntervalStop = 1,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval1);
            Assert.True(interval1.AllEquals(db.FindById(id)));
            Assert.Null(db.FindById("1234"));
        }

        [Fact]
        public void TestFindByInterval()
        {
            var db = GetTempDb();
            var baseIntervalValue = ulong.MaxValue - 10000;
            var interval1 = new TimelineInterval
            {
                IntervalStart = baseIntervalValue + 4,
                IntervalStop = baseIntervalValue + 6,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval1);
            
            var interval2 = new TimelineInterval
            {
                IntervalStart = baseIntervalValue + 8,
                IntervalStop = baseIntervalValue + 9,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval2);
            
            var interval3 = new TimelineInterval
            {
                IntervalStart = baseIntervalValue + 10,
                IntervalStop = baseIntervalValue + 12,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval3);
            
            var interval4 = new TimelineInterval
            {
                IntervalStart = baseIntervalValue + 0,
                IntervalStop = baseIntervalValue + 1,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval4);
            
            var interval5 = new TimelineInterval
            {
                IntervalStart = baseIntervalValue + 15,
                IntervalStop = baseIntervalValue + 17,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval5);

            var results = db.FindByInterval(baseIntervalValue + 5, baseIntervalValue + 11);
            Assert.Equal(3, results.Count);
            Assert.True(results.Contains(interval1));
            Assert.True(results.Contains(interval2));
            Assert.True(results.Contains(interval3));
        }

        [Fact]
        public void TestFindByIntervalAndName()
        {
            var db = GetTempDb();
            var name1 = "Name1";
            var interval1 = new TimelineInterval
            {
                IntervalStart = 0,
                IntervalStop = 2,
                TimelineName = name1
            };
            db.AddOrUpdate(interval1);
            
            var interval2 = new TimelineInterval
            {
                IntervalStart = 8,
                IntervalStop = 9,
                TimelineName = name1
            };
            db.AddOrUpdate(interval2);
            
            var interval3 = new TimelineInterval
            {
                IntervalStart = 10,
                IntervalStop = 12,
                TimelineName = "name2"
            };
            db.AddOrUpdate(interval3);
            
            var results = db.FindByTimelineInterval(name1, 1, 11);
            Assert.Equal(2, results.Count);
            Assert.True(results.Contains(interval1));
            Assert.True(results.Contains(interval2));
            Assert.False(results.Contains(interval3));
        }

        [Fact]
        public void TestRemoveByName()
        {
            var db = GetTempDb();
            var name1 = "Name1";
            var interval1 = new TimelineInterval
            {
                IntervalStart = 0,
                IntervalStop = 2,
                TimelineName = name1
            };
            db.AddOrUpdate(interval1);
            
            var interval2 = new TimelineInterval
            {
                IntervalStart = 8,
                IntervalStop = 9,
                TimelineName = name1
            };
            db.AddOrUpdate(interval2);
            
            var interval3 = new TimelineInterval
            {
                IntervalStart = 10,
                IntervalStop = 12,
                TimelineName = "name2"
            };
            db.AddOrUpdate(interval3);
            
            var results = db.FindByTimelineName(name1);
            Assert.Equal(2, results.Count);
            Assert.Equal(3, db.Count());
            Assert.True(db.RemoveByTimelineName(name1));
            Assert.Equal(1, db.Count());
        }
        
        [Fact]
        public void TestRemoveById()
        {
            var db = GetTempDb();
            string id = Guid.NewGuid().ToString();
            var interval1 = new TimelineInterval
            {
                Id = id,
                IntervalStart = 0,
                IntervalStop = 1,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval1);
            db.AddOrUpdate(new TimelineInterval());
            Assert.Equal(2, db.Count());
            Assert.True(db.RemoveById(id));
            Assert.Equal(1, db.Count());
        }

        [Fact]
        public void TestRemove()
        {
            var db = GetTempDb();
            string id = Guid.NewGuid().ToString();
            var interval1 = new TimelineInterval
            {
                Id = id,
                IntervalStart = 0,
                IntervalStop = 1,
                TimelineName = "Name1"
            };
            db.AddOrUpdate(interval1);
            db.AddOrUpdate(new TimelineInterval());
            Assert.Equal(2, db.Count());
            Assert.True(db.Remove(interval1));
            Assert.Equal(1, db.Count());
        }

        [Fact]
        public void TestRemoveRange()
        {
            var db = GetTempDb();
            var interval1 = new TimelineInterval
            {
                IntervalStart = 0,
                IntervalStop = 1,
                TimelineName = "Name1"
            };
            var interval2 = new TimelineInterval
            {
                IntervalStart = 0,
                IntervalStop = 1,
                TimelineName = "Name1"
            };
            var interval3 = new TimelineInterval
            {
                IntervalStart = 0,
                IntervalStop = 1,
                TimelineName = "Name1"
            };

            db.AddOrUpdate(interval1);
            db.AddOrUpdate(interval2);
            db.AddOrUpdate(interval3);
            db.AddOrUpdate(new TimelineInterval());
            Assert.Equal(4, db.Count());
            Assert.True(db.RemoveRange(new List<TimelineInterval> {interval2, interval3}));
            Assert.Equal(2, db.Count());
            Assert.Equal(interval1, db.FindById(interval1.Id));
            Assert.Null(db.FindById(interval2.Id));
            Assert.Null(db.FindById(interval3.Id));
        }

        private TimelineLiteDbRepository GetTempDb()
        {
            return new TimelineLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }
    }
}