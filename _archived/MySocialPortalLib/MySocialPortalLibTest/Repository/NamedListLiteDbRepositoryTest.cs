using System.IO;
using LiteDB;
using MySocialPortalLib.Repository;
using Xunit;

namespace MySocialPortalLibTest.Repository
{
    public class NamedListLiteDbRepositoryTest
    {
        [Fact]
        public void TestInsertionAndQuery()
        {
            var repo = GetRepo();
            repo.Add("id2", "list1");
            repo.Add("id1", "list1");
            var ids = repo.GetAllIdsForList("list1");
            Assert.Equal(3, ids.Count);
            Assert.Contains("id1", ids);
            Assert.Contains("id2", ids);
            Assert.Contains("id3", ids);
            Assert.Empty(repo.GetAllIdsForList("list5"));
            Assert.Empty(repo.GetAllIdsForList(""));
            Assert.Empty(repo.GetAllIdsForList(null));
        }

        [Fact]
        public void TestGetListNames()
        {
            var repo = GetRepo();
            var lists = repo.GetAllLists();
            Assert.Equal(2, lists.Count);
            Assert.Contains("list1", lists);
            Assert.Contains("list2", lists);
        }

        [Fact]
        public void TestGetListForId()
        {
            var repo = GetRepo();
            var lists = repo.GetAllListsForId("id1");
            Assert.Equal(2, lists.Count);
            Assert.Contains("list1", lists);
            Assert.Contains("list2", lists);
            Assert.Empty(repo.GetAllListsForId("id20"));
            Assert.Empty(repo.GetAllListsForId(""));
            Assert.Empty(repo.GetAllListsForId(null));
        }

        [Fact]
        public void TestRemoveIdFromList()
        {
            var repo = GetRepo();
            repo.RemoveIdFromList("id1", "list1");
            Assert.DoesNotContain("id1", repo.GetAllIdsForList("list1"));
            Assert.DoesNotContain("list1", repo.GetAllListsForId("id1"));
            repo.RemoveIdFromList("","");
            repo.RemoveIdFromList(null,null);
        }
        
        [Fact]
        public void TestRemoveList()
        {
            var repo = GetRepo();
            repo.RemoveList("list1");
            Assert.DoesNotContain("list1", repo.GetAllLists());
            Assert.DoesNotContain("list1", repo.GetAllListsForId("id1"));
            repo.RemoveList("Someotherlist");
        }
        
        [Fact]
        public void TestRemoveId()
        {
            var repo = GetRepo();
            repo.RemoveId("id1");
            Assert.DoesNotContain("id1", repo.GetAllIdsForList("list1"));
            repo.RemoveId("aoeuaeuaoe");
            repo.RemoveId(null);
            repo.RemoveId("");
        }

        private NamedListLiteDbRepository GetRepo()
        {
            var fileStream = new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose);
            var repo = new LiteRepository(fileStream);
            var listRepo = new NamedListLiteDbRepository(repo);
            
            listRepo.Add("id1", "list1");
            listRepo.Add("id1", "list2");
            listRepo.Add("id3", "list1");

            return listRepo;
        }
    }
}