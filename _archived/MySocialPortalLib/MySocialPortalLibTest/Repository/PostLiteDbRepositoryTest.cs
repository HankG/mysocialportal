using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Service
{
    public class PostLiteDbRepositoryTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public PostLiteDbRepositoryTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(GetTempDb());
        }

        [Fact]
        public void TestAddAndCount()
        {
            using var postDb = GetTempDb();
            Assert.Equal(0, postDb.Count());
            postDb.AddOrUpdate(new Post{Title = "Test Post1"});
            Assert.Equal(1, postDb.Count());
        }
        
        [Fact]
        public void TestUpdate()
        {
            using var postDb = GetTempDb();
            Assert.Equal(0, postDb.Count());
            var post1 = new Post {Title = "Test Post1"};
            var post2 = new Post {Id = post1.Id, Title = "Test Title 2"};
            postDb.AddOrUpdate(post1);
            postDb.AddOrUpdate(post2);
            Assert.Equal(1, postDb.Count());
            var pulledPost = postDb.GetById(post2.Id);
            Assert.Equal(post2.Title, pulledPost.Title);
        }
        
        [Fact]
        public void TestAddMultipleAndCount()
        {
            using var postDb = GetTempDb();
            var posts = new List<Post> {new Post(), new Post()};
            Assert.Equal(0, postDb.Count());
            postDb.AddPosts(posts);
            Assert.Equal(2, postDb.Count());
        }
        
        [Fact]
        public void TestGetPosts()
        {
            using var postDb = GetTempDb();
            var postCount = 10;
            var posts = GeneratePosts(postCount);
            Assert.Equal(0, postDb.Count());
            postDb.AddPosts(posts);
            var postsResults = postDb.GetPosts().ToList();
            Assert.Equal(posts.Count, postsResults.Count());
            Assert.Equal(posts.First().Title, postsResults.Last().Title);
        }

        [Fact]
        public void TestGetPostsWithLimit()
        {
            using var postDb = GetTempDb();
            var postLimit = 5;
            var posts = GeneratePosts(2 * postLimit);
            Assert.Equal(0, postDb.Count());
            postDb.AddPosts(posts);
            var postsResults = postDb.GetPosts(postLimit).ToList();
            Assert.Equal(postLimit, postsResults.Count());
        }

        [Fact]
        public void TestGetPostsWithDate()
        {
            using var postDb = GetTempDb();
            var postCount = 10;
            var posts = GeneratePosts(postCount);
            postDb.AddPosts(posts);
            var newestTime = posts[postCount / 2].PostDateTime;
            var postsResults = postDb.GetPosts(newestTime).ToList();
            Assert.Equal(postCount / 2 + 1, postsResults.Count());
            Assert.Empty(postsResults.FindAll(p => p.PostDateTime > newestTime));
        }

        [Fact]
        public void TestGetPostsWithDateNotInclusive()
        {
            using var postDb = GetTempDb();
            var postCount = 10;
            var posts = GeneratePosts(postCount);
            postDb.AddPosts(posts);
            var newestTime = posts[postCount / 2].PostDateTime;
            var postsResults = postDb.GetPosts(newestTime, inclusive:false).ToList();
            Assert.Equal(postCount / 2, postsResults.Count());
            Assert.Empty(postsResults.FindAll(p => p.PostDateTime > newestTime));
        }

        [Fact]
        public void TestGetPostsWithDateAndLimit()
        {
            using var postDb = GetTempDb();
            var postCount = 10;
            var postLimit = 3;
            var posts = GeneratePosts(postCount);
            postDb.AddPosts(posts);
            var oldestTime = posts[postCount / 2].PostDateTime;
            var postsResults = postDb.GetPosts(oldestTime, postLimit).ToList();
            Assert.Equal(postLimit, postsResults.Count());
        }

        [Fact]
        public void TestGetPostForAUser()
        {
            using var postDb = GetTempDb();
            var postCount = 10;
            var postLimit = 3;
            var posts = GeneratePosts(postCount);
            var userId = Guid.NewGuid().ToString();
            postDb.AddPosts(posts);
            var post = new Post
            {
                UserId = userId
            };
            postDb.AddOrUpdate(post);
            var postResults = postDb.GetPosts(personId: userId);
            Assert.Equal(1, postResults.Count());
            Assert.Equal(post, postResults.First());
            postResults = postDb.GetPosts(personId: "1234");
            Assert.False(postResults.Any());
        }

        [Fact]
        public void TestGetPostForAUserNetwork()
        {
            using var postDb = GetTempDb();
            var postCount = 10;
            var postLimit = 3;
            var posts = GeneratePosts(postCount);
            var userId = Guid.NewGuid().ToString();
            postDb.AddPosts(posts);
            var post1 = new Post
            {
                UserId = userId,
                OriginalSocialMediaSystem = StandardSocialNetworkNames.Twitter
            };
            postDb.AddOrUpdate(post1);
            
            var post2 = new Post
            {
                UserId = userId,
                OriginalSocialMediaSystem = StandardSocialNetworkNames.Diaspora
            };
            postDb.AddOrUpdate(post2);

            var post3 = new Post
            {
                UserId = Guid.NewGuid().ToString(),
                OriginalSocialMediaSystem = StandardSocialNetworkNames.Diaspora
            };
            postDb.AddOrUpdate(post3);
            var postResults = postDb.GetPosts(personId: userId, networkName: StandardSocialNetworkNames.Diaspora);
            Assert.Equal(1, postResults.Count());
            Assert.Equal(post2, postResults.First());
            postResults = postDb.GetPosts(networkName: StandardSocialNetworkNames.Diaspora);
            Assert.Equal(2, postResults.Count());
            postResults = postDb.GetPosts(personId: "1234");
            Assert.False(postResults.Any());
            postResults = postDb.GetPosts(personId: userId, networkName: "OtherNetwork");
            Assert.False(postResults.Any());
        }

        [Fact]
        public void TestGetByOriginalNetworkId()
        {
            using var postDb = GetTempDb();
            var postCount = 10;
            var postLimit = 3;
            var posts = GeneratePosts(postCount);
            postDb.AddPosts(posts);
            var networkIdCollision = "123456";
            var otherNetworkName = "OtherNetwork";
            var post1 = new Post
            {
                OriginalSocialMediaSystem = StandardSocialNetworkNames.Twitter,
                OriginalLinkUrlOrId = networkIdCollision
            };
            postDb.AddOrUpdate(post1);
            
            var post2 = new Post
            {
                OriginalSocialMediaSystem = StandardSocialNetworkNames.Diaspora,
                OriginalLinkUrlOrId = "DA5794C2-DD24-4B71-803A-AA5E06EAFA91"
            };
            postDb.AddOrUpdate(post2);

            var post3 = new Post
            {
                OriginalSocialMediaSystem = otherNetworkName,
                OriginalLinkUrlOrId = networkIdCollision
            };
            postDb.AddOrUpdate(post3);
            Assert.Equal(post1, postDb.GetByOriginalNetworkId(StandardSocialNetworkNames.Twitter, networkIdCollision));         
            Assert.Equal(post2, postDb.GetByOriginalNetworkId(StandardSocialNetworkNames.Diaspora, post2.OriginalLinkUrlOrId));         
            Assert.Equal(post3, postDb.GetByOriginalNetworkId(otherNetworkName, networkIdCollision)); 
            Assert.Null(postDb.GetByOriginalNetworkId(StandardSocialNetworkNames.Diaspora, networkIdCollision));
        }

        [Fact]
        public void TestPagingFlow()
        {
            using var postDb = GetTempDb();
            var posts = GeneratePosts(20);
            var oldestDate = DateTimeOffset.Now;
            postDb.AddPosts(posts);
            var postsPerPage = 3;
            var morePages = true;
            var pageNumber = 1;
            while (morePages)
            {
                var postResults = postDb.GetPosts(oldestDate, postsPerPage, false).ToList();
                oldestDate = postResults.Last().PostDateTime;
                    _testOutputHelper.WriteLine($"Page {pageNumber++}");
                postResults.ToList().ForEach(p => _testOutputHelper.WriteLine($"{p.Title} @ {p.PostDateTime}"));
                morePages = postResults.Count == postsPerPage;
            }
        }

        private PostsLiteDbRepository GetTempDb()
        {
            return new PostsLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }

        private List<Post> GeneratePosts(int count)
        {
            var posts = new List<Post>(count);
            for (int i = 0; i < count; i++)
            {
                posts.Add(new Post
                {
                    Title = $"Title {i}",
                    PostDateTime = DateTimeOffset.UnixEpoch.AddDays(i)
                });
            }
            return posts;
        }
    }
}