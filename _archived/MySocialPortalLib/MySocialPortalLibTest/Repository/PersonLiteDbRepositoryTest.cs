using System;
using System.IO;
using System.Linq;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;
using Xunit;

namespace MySocialPortalLibTest.Service
{
    public class PersonLiteDbRepositoryTest
    {
        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(GetTempDb());
        }

        [Fact]
        public void TestAddPerson()
        {
            var db = GetTempDb();
            Assert.Equal(0, db.Count());
            db.AddPerson(new Person());
            Assert.Equal(1, db.Count());
        }

        [Fact]
        public void TestSearchById()
        {
            var db = GetTempDb();
            var p1 = new Person {Id = Guid.NewGuid().ToString()};
            var p2 = new Person {Id = Guid.NewGuid().ToString()};
            db.AddPerson(p1);
            var fromDbP1 = db.FindById(p1.Id);
            Assert.Equal(p1, fromDbP1);
            Assert.Null(db.FindById(p2.Id));
        }

        [Fact]
        public void TestSearchByNetworkId()
        {
            var db = GetTempDb();
            var p1 = new Person {Id = Guid.NewGuid().ToString()};
            var networkName1 = "Network1";
            var p1UserId = "user1@network1.social";
            p1.SocialMediaAccounts.Add(networkName1,new SocialMediaAccountData {ProfileId = p1UserId});
            var p2 = new Person {Id = Guid.NewGuid().ToString()};
            var networkName2 = "Network2";
            var p2UserId = "user2@network1.social";
            p2.SocialMediaAccounts.Add(networkName2,new SocialMediaAccountData {ProfileId = p2UserId});
            db.AddPerson(p1);
            db.AddPerson(p2);
            var fromDbP2 = db.FindByNetworkId(networkName2, p2UserId);
            Assert.Equal(p2, fromDbP2);
            Assert.Null(db.FindByNetworkId(networkName2, p1UserId));
            Assert.Null(db.FindByNetworkId("Network3", ""));
        }

        [Fact]
        public void TestListAll()
        {
            var db = GetTempDb();
            var p1 = new Person {Id = Guid.NewGuid().ToString()};
            var p2 = new Person {Id = Guid.NewGuid().ToString()};
            db.AddPerson(p1);
            db.AddPerson(p2);
            var allPeople = db.ListAll();
            Assert.Equal(2, allPeople.Count);
            var ids = allPeople.Select(p => p.Id).ToList();
            Assert.Contains(p1.Id, ids);
            Assert.Contains(p2.Id, ids);
        }

        private PersonsLiteDbRepository GetTempDb()
        {
            return new PersonsLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }
        
    }
}