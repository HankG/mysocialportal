using System;
using System.IO;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using Xunit;

namespace MySocialPortalLibTest.Service
{
    public class LinkPreviewLiteDbRepositoryTest
    {
        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(GetTempDb());
        }

        [Fact]
        public void TestAddItem()
        {
            var db = GetTempDb();
            var lp = new LinkPreview();
            Assert.Equal(0, db.Count());
            db.AddOrUpdate(lp);
            Assert.Equal(1, db.Count());
            db.AddOrUpdate(lp);
            Assert.Equal(1, db.Count());
            db.AddOrUpdate(new LinkPreview());
            Assert.Equal(2, db.Count());
        }

        [Fact]
        public void TestUpdateItem()
        {
            var db = GetTempDb();
            var id = Guid.NewGuid().ToString();
            var lp1 = new LinkPreview
            {
                Id = id,
                Title = "Title1",
                Description = "Description1",
                TimeGeneraterated = DateTimeOffset.UtcNow,
                RequestUrl = "http://server1",
                HasPreviewImage = true,
                PreviewImageName = "Preview1",
                UrlFound = true
            };
            db.AddOrUpdate(lp1);
            Assert.True(lp1.AllEquals(db.FindById(id)));

            var lp2 = new LinkPreview
            {
                Id = id,
                Title = "Title2",
                Description = "Description1",
                TimeGeneraterated = DateTimeOffset.UtcNow,
                RequestUrl = "http://server1",
                HasPreviewImage = true,
                PreviewImageName = "Preview1",
                UrlFound = true
            };
            db.AddOrUpdate(lp2);
            Assert.False(lp1.AllEquals(db.FindById(id)));
            Assert.True(lp2.AllEquals(db.FindById(id)));

            var lp3 = new LinkPreview
            {
                Id = id,
                Title = "Title3",
                Description = "Description3",
                TimeGeneraterated = DateTimeOffset.UtcNow.AddDays(-3),
                RequestUrl = "http://server3",
                HasPreviewImage = false,
                PreviewImageName = "Preview3",
                UrlFound = false
            };
            db.AddOrUpdate(lp3);
            Assert.False(lp1.AllEquals(db.FindById(id)));
            Assert.False(lp2.AllEquals(db.FindById(id)));
            Assert.True(lp3.AllEquals(db.FindById(id)));
        }
        
        [Fact]
        public void TestQueryById()
        {
            var db = GetTempDb();
            var id = Guid.NewGuid().ToString();
            var lp1 = new LinkPreview
            {
                Id = id
            };
            db.AddOrUpdate(lp1);
            Assert.Equal(lp1, db.FindById(id));
            Assert.Null(db.FindById(Guid.NewGuid().ToString()));
        }

        [Fact]
        public void TestQueryByUrl()
        {
            var db = GetTempDb();
            var url = "http://server1";
            var lp1 = new LinkPreview
            {
                RequestUrl = url
            };
            db.AddOrUpdate(lp1);
            Assert.Equal(lp1, db.FindByUrl(url));
            Assert.Null(db.FindByUrl("something other than url"));
        }

        private LinkPreviewLiteDbRepository GetTempDb()
        {
            return new LinkPreviewLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }
    }
}