using System;
using System.Collections.Generic;
using System.Linq;
using MySocialPortalLib.Model;
using MySocialPortalLib.Util;
using Xunit;

namespace MySocialPortalLibTest.Util
{
    public class TimelineItemUtilitiesTest
    {
        /// Case A
        /// #1 |-------------|
        /// #2                    |--------------|
        /// R  |-------------|    |--------------|
        [Fact]
        public void TestCleanupCaseA()
        {
            var intervals = new List<TimelineInterval>
            {
                new TimelineInterval {IntervalStart = 100, IntervalStop = 200},
                new TimelineInterval {IntervalStart = 300, IntervalStop = 400}
            };
            var mergedIntervals = TimelineIntervalUtilities.CleanupList(intervals);
            Assert.Equal(intervals[0].IntervalStart, mergedIntervals[0].IntervalStart);
            Assert.Equal(intervals[0].IntervalStop, mergedIntervals[0].IntervalStop);
            Assert.Equal(intervals[1].IntervalStart, mergedIntervals[1].IntervalStart);
            Assert.Equal(intervals[1].IntervalStop, mergedIntervals[1].IntervalStop);
        }
        
        /// Case B
        /// #1 |-------------|
        /// #2            |--------------|
        /// R  |-------------------------|
        [Fact]
        public void TestCleanupCaseB()
        {
            ulong expectedStart = 100;
            ulong expectedStop = 400;
            var intervals = new List<TimelineInterval>
            {
                new TimelineInterval {IntervalStart = 100, IntervalStop = 200},
                new TimelineInterval {IntervalStart = 180, IntervalStop = 400}
            };
            var mergedIntervals = TimelineIntervalUtilities.CleanupList(intervals);
            Assert.Single(mergedIntervals);
            Assert.Equal(expectedStart, mergedIntervals[0].IntervalStart);
            Assert.Equal(expectedStop, mergedIntervals[0].IntervalStop);
        }
        
        /// Case C
        /// #1 |-------------|
        /// #2                  |--------|
        /// R  |-------------------------|
        /// Where the gap is within a tolerance
        [Fact]
        public void TestCleanupCaseC()
        {
            ulong expectedStart = 100;
            ulong expectedStop = 400;
            ulong gapTolerance = 11;
            var intervals = new List<TimelineInterval>
            {
                new TimelineInterval {IntervalStart = 100, IntervalStop = 200},
                new TimelineInterval {IntervalStart = 210, IntervalStop = 400}
            };
            var mergedIntervals = TimelineIntervalUtilities.CleanupList(intervals, gapTolerance);
            Assert.Single(mergedIntervals);
            Assert.Equal(expectedStart, mergedIntervals[0].IntervalStart);
            Assert.Equal(expectedStop, mergedIntervals[0].IntervalStop);
        }
        
        /// Case D
        /// #1 |-----------------------|
        /// #2      |--------------|
        /// R  |-----------------------|
        [Fact]
        public void TestCleanupCaseD()
        {
            ulong expectedStart = 100;
            ulong expectedStop = 400;
            var intervals = new List<TimelineInterval>
            {
                new TimelineInterval {IntervalStart = 100, IntervalStop = 400},
                new TimelineInterval {IntervalStart = 200, IntervalStop = 300}
            };
            var mergedIntervals = TimelineIntervalUtilities.CleanupList(intervals);
            Assert.Single(mergedIntervals);
            Assert.Equal(expectedStart, mergedIntervals[0].IntervalStart);
            Assert.Equal(expectedStop, mergedIntervals[0].IntervalStop);
        }

        /// Case E
        /// # |----|
        /// #        |------------|
        /// #           |----|
        /// #              |----|
        /// #                |-------------|
        /// #                         |-------|
        /// #                                      |-------------|
        /// #                                                       |------|
        /// #                                                           |------|
        /// #
        /// R |----| |------------------------|    |-------------|  |----------|
        [Fact]
        public void TestCleanupCaseE()
        {
            var expectedCount = 4;
            var intervals = new List<TimelineInterval>
            {
                new TimelineInterval {IntervalStart = 100, IntervalStop = 150},
                new TimelineInterval {IntervalStart = 200, IntervalStop = 400},
                new TimelineInterval {IntervalStart = 250, IntervalStop = 300},
                new TimelineInterval {IntervalStart = 275, IntervalStop = 325},
                new TimelineInterval {IntervalStart = 300, IntervalStop = 500},
                new TimelineInterval {IntervalStart = 450, IntervalStop = 550},
                new TimelineInterval {IntervalStart = 600, IntervalStop = 700},
                new TimelineInterval {IntervalStart = 725, IntervalStop = 800},
                new TimelineInterval {IntervalStart = 775, IntervalStop = 850}
            };
            var mergedIntervals = TimelineIntervalUtilities.CleanupList(intervals);
            Assert.Equal(expectedCount, mergedIntervals.Count);
            Assert.Equal(intervals[0].IntervalStart, mergedIntervals[0].IntervalStart);
            Assert.Equal(intervals[0].IntervalStop, mergedIntervals[0].IntervalStop);
            Assert.Equal(intervals[1].IntervalStart, mergedIntervals[1].IntervalStart);
            Assert.Equal(intervals[5].IntervalStop, mergedIntervals[1].IntervalStop);
            Assert.Equal(intervals[6].IntervalStart, mergedIntervals[2].IntervalStart);
            Assert.Equal(intervals[6].IntervalStop, mergedIntervals[2].IntervalStop);
            Assert.Equal(intervals[7].IntervalStart, mergedIntervals[3].IntervalStart);
            Assert.Equal(intervals[8].IntervalStop, mergedIntervals[3].IntervalStop);
        }

        /// Case F
        /// no intervals nets no intervals
        [Fact]
        public void TestCleanupCaseF()
        {
//            var (added, deleted) = TimelineIntervalUtilities.CleanupList(new List<TimelineInterval>());
//            Assert.Empty(added);
//            Assert.Empty(deleted);
//            (added, deleted) = TimelineIntervalUtilities.CleanupList(null);
//            Assert.Empty(added);
//            Assert.Empty(deleted);
        }

        
        [Fact]
        public void TestBounds()
        {
            ulong expectedBoundsMinimum = 100;
            ulong expectedBoundsMaximum = 200;
            var label1 = "label";
            var label2 = "label";

            var interval1 = new TimelineInterval
            {
                IntervalStart = expectedBoundsMinimum,
                IntervalStop = expectedBoundsMinimum + 10,
                TimelineName = label1
            };
            
            var interval2 = new TimelineInterval
            {
                IntervalStart = expectedBoundsMaximum - 20,
                IntervalStop = expectedBoundsMaximum,
                TimelineName = label2                
            };

            var result = TimelineIntervalUtilities.Bounds(interval1, interval2);
            Assert.Equal(expectedBoundsMinimum, result.IntervalStart);
            Assert.Equal(expectedBoundsMaximum, result.IntervalStop);
            Assert.Equal(label1, result.TimelineName);
            
            result = TimelineIntervalUtilities.Bounds(interval2, interval1);
            Assert.Equal(expectedBoundsMinimum, result.IntervalStart);
            Assert.Equal(expectedBoundsMaximum, result.IntervalStop);
            Assert.Equal(label2, result.TimelineName);
        }

        /// <summary>
        /// Case A
        /// #1 |-------------|
        /// #2                    |--------------|
        /// </summary>
        [Fact]
        public void TestGapCaseA()
        {
            var label1 = "label1";
            var label2 = "label2";
            var interval1 = new TimelineInterval
            {
                IntervalStart = 1,
                IntervalStop = 3,
                TimelineName = label1
            };
            
            var interval2 = new TimelineInterval
            {
                IntervalStart = 5,
                IntervalStop = 6,
                TimelineName = label2
            };

            var result = TimelineIntervalUtilities.Gap(interval1, interval2);
            Assert.Equal(interval1.IntervalStop, result.IntervalStart);
            Assert.Equal(interval2.IntervalStart, result.IntervalStop);
            Assert.Equal(label1, result.TimelineName);
        }
        
        /// <summary>
        /// Case B
        /// #1 |----------------------|
        /// #2                    |--------------|
        /// </summary>
        [Fact]
        public void TestGapCaseB()
        {
            var label1 = "label1";
            var label2 = "label2";
            var interval1 = new TimelineInterval
            {
                IntervalStart = 1,
                IntervalStop = 6,
                TimelineName = label1
            };
            
            var interval2 = new TimelineInterval
            {
                IntervalStart = 5,
                IntervalStop = 7,
                TimelineName = label2
            };

            Assert.Null(TimelineIntervalUtilities.Gap(interval1, interval2));
        }

        /// <summary>
        /// Case C
        /// #1                    |--------------|
        /// #2 |----------------------|
        /// </summary>
        [Fact]
        public void TestGapCaseC()
        {
            var label1 = "label1";
            var label2 = "label2";
            var interval1= new TimelineInterval
            {
                IntervalStart = 5,
                IntervalStop = 7,
                TimelineName = label1
            };

            var interval2 = new TimelineInterval
            {
                IntervalStart = 1,
                IntervalStop = 6,
                TimelineName = label2
            };
            
            Assert.Null(TimelineIntervalUtilities.Gap(interval1, interval2));
        }

        /// <summary>
        /// Case D
        /// #1 |--------------|
        /// #2     |------|
        /// </summary>
        [Fact]
        public void TestGapCaseD()
        {
            var label1 = "label1";
            var label2 = "label2";
            var interval1= new TimelineInterval
            {
                IntervalStart = 1,
                IntervalStop = 7,
                TimelineName = label1
            };

            var interval2 = new TimelineInterval
            {
                IntervalStart = 3,
                IntervalStop = 4,
                TimelineName = label2
            };
            
            Assert.Null(TimelineIntervalUtilities.Gap(interval1, interval2));
        }

        /// <summary>
        /// Case E
        /// #1 |--------------|
        /// #2     |------|
        /// </summary>
        [Fact]
        public void TestGapCaseE()
        {
            var label1 = "label1";
            var label2 = "label2";
            var interval1= new TimelineInterval
            {
                IntervalStart = 3,
                IntervalStop = 4,
                TimelineName = label1
            };

            var interval2 = new TimelineInterval
            {
                IntervalStart = 1,
                IntervalStop = 7,
                TimelineName = label2
            };
            
            Assert.Null(TimelineIntervalUtilities.Gap(interval1, interval2));
        }

        /// <summary>
        /// Case F
        /// #1              |--------------|
        /// #2 |------|
        /// </summary>
        [Fact]
        public void TestGapCaseF()
        {
            var label1 = "label1";
            var label2 = "label2";
            var interval1= new TimelineInterval
            {
                IntervalStart = 5,
                IntervalStop = 6,
                TimelineName = label1
            };

            var interval2 = new TimelineInterval
            {
                IntervalStart = 1,
                IntervalStop = 3,
                TimelineName = label2
            };
            
            var result = TimelineIntervalUtilities.Gap(interval1, interval2);
            Assert.Equal(interval2.IntervalStop, result.IntervalStart);
            Assert.Equal(interval1.IntervalStart, result.IntervalStop);
            Assert.Equal(label1, result.TimelineName);
        }

    }
}