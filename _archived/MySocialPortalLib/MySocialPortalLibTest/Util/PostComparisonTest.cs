using System;
using System.Collections.Generic;
using MySocialPortalLib.Model;
using MySocialPortalLib.Util;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Util
{
    public class PostComparisonTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public PostComparisonTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void TestAscendingComparison()
        {
            var random = new Random();
            var count = 10;
            var list = new List<Post>();
            for (var i = 0; i < count; i++)
            {
                var post = new Post
                {
                    PostDateTime = DateTimeOffset.Now.AddDays(random.Next(0,100))
                };
                list.Add(post);
            }
            
            list.Sort(new PostComparison(true));
            for (var i = 0; i < count - 1; i++)
            {
                _testOutputHelper.WriteLine(list[i].PostDateTime.ToString());
                Assert.True(list[i].PostDateTime <= list[i+1].PostDateTime);
            }
        }

        [Fact]
        public void TestDescending()
        {
            var random = new Random();
            var count = 10;
            var list = new List<Post>();
            for (var i = 0; i < count; i++)
            {
                var post = new Post
                {
                    PostDateTime = DateTimeOffset.Now.AddDays(random.Next(0,100))
                };
                list.Add(post);
            }
            
            list.Sort(new PostComparison(false));
            for (var i = 0; i < count - 1; i++)
            {
                _testOutputHelper.WriteLine(list[i].PostDateTime.ToString());
                Assert.True(list[i].PostDateTime >= list[i+1].PostDateTime);
            }
        }
    }
}