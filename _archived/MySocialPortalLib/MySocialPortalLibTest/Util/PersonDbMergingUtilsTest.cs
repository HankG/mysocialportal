using System;
using System.Collections.Generic;
using System.Linq;
using MySocialPortalLib.Model;
using MySocialPortalLib.Util;
using Xunit;

namespace MySocialPortalLibTest.Util
{
    public class PersonDbMergingUtilsTest
    {
        [Fact]
        public void TestLabeledValueMergeSingle()
        {
            var values = new List<LabeledValue<string>>
            {
                new LabeledValue<string>("Label1", "Value1"),
                new LabeledValue<string>("Label2", "Value2"),
            };
            var value3 = new LabeledValue<string>("Label3", "Value3");
            values.AddIfNew(value3);
            Assert.Equal(3, values.Count);
            var newValue2 = new LabeledValue<string>("Label2", "NewValue2");
            values.AddIfNew(newValue2);
            Assert.Equal(3, values.Count);
            Assert.False(newValue2.Equals(values[1]));
            values[1].Value = "";
            values.AddIfNew(newValue2);
            Assert.Equal(3, values.Count);
            Assert.True(newValue2.Equals(values[1]));
        }

        [Fact]
        public void TestLabeledValueMergeMultiple()
        {
            var values1 = new List<LabeledValue<string>>
            {
                new LabeledValue<string>("Label1", "Value1"),
                new LabeledValue<string>("Label2", "Value2"),
                new LabeledValue<string>("Label3", "")
            };

            var values2 = new List<LabeledValue<string>>
            {
                new LabeledValue<string>("Label2", "NewValue2"),
                new LabeledValue<string>("Label3", "NewValue3"),
                new LabeledValue<string>("Label4", "NewValue4")
            };
            values1.AddIfNew(values2);
            Assert.Equal(4, values1.Count);
            Assert.Equal("Value2", values1[1].Value);
            Assert.Equal("NewValue3", values1[2].Value);
        }

        [Fact]
        public void TestSocialMediaAccountDataMerge()
        {
            var emptySma = new SocialMediaAccountData();
            var fullSma = GenerateFullSocialMediaData();
            var originalSma = fullSma.Copy();
            fullSma.Merge(emptySma, false);
            Assert.False(originalSma.AllEquals(fullSma));
            fullSma.Active = originalSma.Active;
            Assert.True(originalSma.AllEquals(fullSma));
            emptySma.Merge(fullSma);
            Assert.False(originalSma.AllEquals(emptySma));
            emptySma.Id = fullSma.Id;
            Assert.True(originalSma.AllEquals(emptySma));
            var newKey = "Value1Key";
            var fullSma2 = GenerateFullSocialMediaData();
            fullSma.AdditionalProperties[newKey] = "Value1Fullsma1";
            fullSma2.AdditionalProperties[newKey] = "Value1Fullsma2";
            fullSma.Merge(fullSma2);
            Assert.Equal(5, fullSma.AdditionalProperties.Count);
            Assert.Equal("Value1Fullsma2", fullSma.AdditionalProperties[newKey]);
            fullSma.Merge(fullSma2, true);
            Assert.Equal(3, fullSma.AdditionalProperties.Count);
            originalSma.AdditionalProperties.Keys.ToList().ForEach(k => Assert.DoesNotContain(k, fullSma.AdditionalProperties.Keys));
           
        }

        [Fact]
        public void TestSocialMediaDictionaryMerge()
        {
            var socialDictionary1 = new Dictionary<string, SocialMediaAccountData>
            {
                {"Network1", new SocialMediaAccountData {SocialMediaSystemName = "Network1"}},
                {"Network2", new SocialMediaAccountData {SocialMediaSystemName = "Network2"}}
            };
            var socialDictionary2 = new Dictionary<string, SocialMediaAccountData>
            {
                {"Network2", new SocialMediaAccountData {SocialMediaSystemName = "Network2"}},
                {"Network3", new SocialMediaAccountData {SocialMediaSystemName = "Network3"}}
            };
            socialDictionary1.AddIfNew(socialDictionary2);
            Assert.Equal(3, socialDictionary1.Count);
        }

        [Fact]
        public void TestPersonsListMerge()
        {
            var personList1 = new List<Person>
            {
                new Person
                {
                    Id = "1234",
                    Name = "Jane Smith",
                    SocialMediaAccounts = new Dictionary<string, SocialMediaAccountData>
                    {
                        {"Network1", new SocialMediaAccountData()}
                    }
                },
                new Person
                {
                    Name = "John Gooding",
                    SocialMediaAccounts = new Dictionary<string, SocialMediaAccountData>
                    {
                        {"Network1", new SocialMediaAccountData{ProfileId = "johngooding", SocialMediaSystemName = "Network1"}},
                        {"Network3", new SocialMediaAccountData{ProfileId = "john_gooding", SocialMediaSystemName = "Network3"}}
                    }

                },
                new Person
                {
                    Name = "Lisa Lopez",
                    SocialMediaAccounts = new Dictionary<string, SocialMediaAccountData>
                    {
                        {"Network1", new SocialMediaAccountData{ProfileId = "lisalopez", SocialMediaSystemName = "Network1"}},
                    }
                }
            };

            var personList2 = new List<Person>
            {
                new Person
                {
                    Id = "1234",
                    Name = "Jane A. Smith",
                    SocialMediaAccounts = new Dictionary<string, SocialMediaAccountData>
                    {
                        {"Network2", new SocialMediaAccountData()}
                    }
                },
                new Person
                {
                    Name = "JohnG",
                    SocialMediaAccounts = new Dictionary<string, SocialMediaAccountData>
                    {
                        {"Network1", new SocialMediaAccountData{ProfileId = "johngooding", SocialMediaSystemName = "Network1"}},
                        {"Network2", new SocialMediaAccountData{ProfileId = "johng", SocialMediaSystemName = "Network2"}},
                        {"Network3", new SocialMediaAccountData{ProfileId = "john_gooding", SocialMediaSystemName = "Network3"}}
                    }
                },
                new Person
                {
                    Name = "Johnathan G. Gooding IV",
                    SocialMediaAccounts = new Dictionary<string, SocialMediaAccountData>
                    {
                        {"Network3", new SocialMediaAccountData{ProfileId = "john_gooding", SocialMediaSystemName = "Network3"}},
                        {"Network4", new SocialMediaAccountData{ProfileId = "johng4", SocialMediaSystemName = "Network4"}}
                    }

                },
                new Person
                {
                    Name = "Kathy Morningside",
                    SocialMediaAccounts = new Dictionary<string, SocialMediaAccountData>
                    {
                        {"Network1", new SocialMediaAccountData{ProfileId = "kathym", SocialMediaSystemName = "Network1"}},
                    }
                }
            };

            var (newPeople, mergedPeople, unchangedPeople) = PersonDatabaseMergingUtils.Merge(personList1, personList2);
            Assert.Equal(2, mergedPeople.Count);
            Assert.Equal(personList1[0].Name, mergedPeople[0].Name);
            Assert.Equal(2, mergedPeople[0].SocialMediaAccounts.Count);
            Assert.Equal(personList1[1].Name, mergedPeople[1].Name);
            Assert.Equal(4, mergedPeople[1].SocialMediaAccounts.Count);
            Assert.Equal(1, newPeople.Count);
            Assert.Equal(newPeople.First(), personList2.Last());
            Assert.Equal(1, unchangedPeople.Count);
            Assert.Equal(unchangedPeople.First(), personList1.Last());
        }

        private SocialMediaAccountData GenerateFullSocialMediaData()
        {
            var fullSma = new SocialMediaAccountData
            {
                Active = false,
                ProfileId = Guid.NewGuid().ToString(),
                ProfileUrl = Guid.NewGuid().ToString(),
                RealName = Guid.NewGuid().ToString(),
                ProfilePhotoPath = Guid.NewGuid().ToString(),
                SocialMediaSystemName = Guid.NewGuid().ToString()
            };
            fullSma.AdditionalProperties[Guid.NewGuid().ToString()] = Guid.NewGuid().ToString();
            fullSma.AdditionalProperties[Guid.NewGuid().ToString()] = Guid.NewGuid().ToString();

            return fullSma;
        }
    }
}