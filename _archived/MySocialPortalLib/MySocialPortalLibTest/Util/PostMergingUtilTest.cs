using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using LinqToTwitter;
using MySocialPortalLib.Model;
using MySocialPortalLib.Util;
using Xunit;
using Xunit.Abstractions;
using Place = MySocialPortalLib.Model.Place;

namespace MySocialPortalLibTest.Util
{
    public class PostMergingUtilTest
    {
        [Fact]
        public void TestBlankPostMerging()
        {
            var id = "1234";
            var post1 = new Post()
            {
                Id = id
            };
            var post2 = new Post();
            post1.Merge(post2);
            Assert.Equal(id, post1.Id);
            post1.Merge(null);
            Assert.Equal(id, post1.Id);
        }

        [Fact]
        public void TestFullPostMerge()
        {
            var post1 = BuildPost(100);
            var post2 = BuildPost(200);
            var expected = BuildExpectedPost(post1, post2);

            post2.Id = post1.Id;
            post1.Merge(post2);
            Assert.True(expected.AllEquals(post1));
        }

        [Fact]
        public void TestEventsMerging()
        {
            var eventsList1 = new List<EventData>
            {
                new EventData
                {
                    Place = new Place
                    {
                        Altitude = 1,
                        Latitude = 2,
                        Longitude = 3,
                        HasLatLon = true,
                        Name = "Place1",
                        PhysicalAddress = "Physical Address 1",
                        Url = "http://url1"
                    },
                    Title = "Event 1",
                    StartTime = DateTimeOffset.Now,
                    StopTime = DateTimeOffset.Now
                },
                new EventData
                {
                    Place = new Place
                    {
                        Altitude = 3,
                        Latitude = 4,
                        Longitude = 5,
                        HasLatLon = true,
                        Name = "Place2",
                        PhysicalAddress = "Physical Address 2",
                        Url = "http://url2"
                    },
                    Title = "Event 2",
                    StartTime = DateTimeOffset.Now,
                    StopTime = DateTimeOffset.Now
                }
            };
            
            var eventsList2 = new List<EventData>
            {
                new EventData
                {
                    Id = eventsList1[0].Id,
                    Place = new Place
                    {
                        Id = eventsList1[0].Place.Id,
                        Altitude = 12,
                        Latitude = 22,
                        Longitude = 33,
                        HasLatLon = true,
                        Name = "Place1a",
                        PhysicalAddress = "Physical Address 1a",
                        Url = "http://url1a"
                    },
                    Title = "Event 1",
                    StartTime = DateTimeOffset.Now,
                    StopTime = DateTimeOffset.Now
                },
                new EventData
                {
                    Place = new Place
                    {
                        Altitude = 7,
                        Latitude = 8,
                        Longitude = 9,
                        HasLatLon = true,
                        Name = "Place3",
                        PhysicalAddress = "Physical Address 3",
                        Url = "http://url3"
                    },
                    Title = "Event 3",
                    StartTime = DateTimeOffset.Now,
                    StopTime = DateTimeOffset.Now
                }
            };
            
            var expected = new List<EventData>
            {
                new EventData
                {
                    Id = eventsList1[0].Id,
                    Place = new Place
                    {
                        Id = eventsList1[0].Place.Id,
                        Altitude = 12,
                        Latitude = 22,
                        Longitude = 32,
                        HasLatLon = true,
                        Name = "Place1a",
                        PhysicalAddress = "Physical Address 1a",
                        Url = "http://url1a"
                    },
                    Title = "Event 1",
                    StartTime = eventsList2[0].StartTime,
                    StopTime = eventsList2[0].StopTime
                },
                new EventData
                {
                    Id = eventsList1[1].Id,
                    Place = eventsList1[1].Place,
                    Title = "Event 2",
                    StartTime = eventsList1[1].StartTime,
                    StopTime = eventsList1[1].StopTime
                },
                new EventData
                {
                    Id = eventsList2[1].Id,
                    Place = eventsList2[1].Place,
                    Title = "Event 3",
                    StartTime = eventsList2[1].StartTime,
                    StopTime = eventsList2[1].StopTime
                }
            };
            
            eventsList1.AddIfNew(eventsList2);
            Assert.Equal(expected.Count, eventsList1.Count);
            for (var i = 0; i < expected.Count; i++)
            {
                Assert.True(expected[i].AllEquals(eventsList1[i]),$"{i}");
            }
        }

        [Fact]
        public void TestExternalLinksMerging()
        {
            var externalLinkList1 = new List<ExternalLink>
            {
                new ExternalLink
                {
                    Name = "Link 1",
                    Source = "Source 1",
                    Url = "http://url1",
                    PostId = "PostId1"
                },
                new ExternalLink
                {
                    Name = "Link 2",
                    Source = "Source 2",
                    Url = "http://url2",
                    PostId = "PostId2"
                }
            };
            
            var externalLinkList2 = new List<ExternalLink>
            {
                new ExternalLink
                {
                    Name = "Link 1a",
                    Source = "Source 1a",
                    Url = "http://url1",
                    PostId = "PostId1a"
                },
                new ExternalLink
                {
                    Name = "Link 3",
                    Source = "Source 3",
                    Url = "http://url3",
                    PostId = "PostId3"
                }
            };

            var expected = new List<ExternalLink>
            {
                new ExternalLink
                {
                    Id = externalLinkList1[0].Id,
                    Name = "Link 1a",
                    Source = "Source 1a",
                    Url = "http://url1",
                    PostId = "PostId1a"
                }, 
                new ExternalLink
                {
                    Id = externalLinkList1[1].Id,
                    Name = "Link 2",
                    Source = "Source 2",
                    Url = "http://url2",
                    PostId = "PostId2"
                },
                new ExternalLink
                {
                    Id = externalLinkList2[1].Id,
                    Name = "Link 3",
                    Source = "Source 3",
                    Url = "http://url3",
                    PostId = "PostId3"
                },
            };

            externalLinkList1.AddIfNew(externalLinkList2);
            Assert.Equal(expected.Count, externalLinkList1.Count);
            for (var i = 0; i < expected.Count; i++)
            {
                Assert.True(expected[i].AllEquals(externalLinkList1[i]), $"{i}");
            }
        }
        
        [Fact]
        public void TestMediaMerging()
        {
            var mediaList1 = new List<MediaData>
            {
                new MediaData
                {
                    Description = "Description 1",
                    CreationDateTime = DateTimeOffset.Now,
                    Title = "Title 1",
                    LocalPath = "/tmp/localpath1",
                    MediaType = "PNG",
                    OriginalUrl = "http://url1"
                },
                new MediaData
                {
                    Description = "Description 2",
                    CreationDateTime = DateTimeOffset.Now,
                    Title = "Title 2",
                    LocalPath = "/tmp/localpath2",
                    MediaType = "JPG",
                    OriginalUrl = "http://url2"
                }
            };
            
            var mediaList2 = new List<MediaData>
            {
                new MediaData
                {
                    Id = mediaList1[0].Id,
                    Description = "Description 1a",
                    CreationDateTime = DateTimeOffset.Now,
                    Title = "Title 1a",
                    LocalPath = "/tmp/localpath1a",
                    MediaType = "MP4",
                    OriginalUrl = "http://url1a"
                },
                new MediaData
                {
                    Description = "Description 3",
                    CreationDateTime = DateTimeOffset.Now,
                    Title = "Title 3",
                    LocalPath = "/tmp/localpath3",
                    MediaType = "TXT",
                    OriginalUrl = "http://url3"
                }
            };

            var expected = new List<MediaData>
            {
                new MediaData
                {
                    Id = mediaList1[0].Id,
                    Description = "Description 1a",
                    CreationDateTime = mediaList2[0].CreationDateTime,
                    Title = "Title 1a",
                    LocalPath = "/tmp/localpath1a",
                    MediaType = "MP4",
                    OriginalUrl = "http://url1a"
                },
                new MediaData
                {
                    Id = mediaList1[1].Id,
                    Description = "Description 2",
                    CreationDateTime = mediaList1[1].CreationDateTime,
                    Title = "Title 2",
                    LocalPath = "/tmp/localpath2",
                    MediaType = "JPG",
                    OriginalUrl = "http://url2"
                },
                new MediaData
                {
                    Id = mediaList2[1].Id,
                    Description = "Description 3",
                    CreationDateTime = mediaList2[1].CreationDateTime,
                    Title = "Title 3",
                    LocalPath = "/tmp/localpath3",
                    MediaType = "TXT",
                    OriginalUrl = "http://url3"
                }
            };

            mediaList1.AddIfNew(mediaList2);
            Assert.Equal(expected.Count, mediaList1.Count);
            for (var i = 0; i < expected.Count; i++)
            {
                Assert.True(expected[i].AllEquals(mediaList1[i]),$"{i}");
            }
        }

        [Fact]
        public void TestPlacesMerging()
        {
            var places1 = new List<Place>
            {
                new Place
                {
                    Altitude = 1,
                    Latitude = 2,
                    Longitude = 3,
                    Name = "Place 1",
                    Url = "http://url1",
                    PhysicalAddress = "Physical address1",
                    HasLatLon = true
                },
                new Place
                {
                    Altitude = 0,
                    Latitude = 0,
                    Longitude = 0,
                    Name = "Place 2",
                    Url = "http://url2",
                    PhysicalAddress = "Physical address2",
                    HasLatLon = false
                },
                new Place
                {
                    Altitude = 0,
                    Latitude = 0,
                    Longitude = 0,
                    Name = "Place 3",
                    Url = "http://url3",
                    PhysicalAddress = "Physical address3",
                    HasLatLon = false
                }
            };
            
            var places2 = new List<Place>
            {
                new Place
                {
                    Id = places1[0].Id,
                    Altitude = 5.1,
                    Latitude = 6.1,
                    Longitude = 7.1,
                    Name = "Place 1a",
                    Url = "http://url1a",
                    PhysicalAddress = "Physical address1a",
                    HasLatLon = true
                },
                new Place
                {
                    Altitude = 1,
                    Latitude = 2,
                    Longitude = 3,
                    Name = "Place 1",
                    Url = "http://url1",
                    PhysicalAddress = "Physical address1",
                    HasLatLon = true
                },
                new Place
                {
                    Id = places1[1].Id,
                    Altitude = 10,
                    Latitude = 11,
                    Longitude = 12,
                    Name = "Place 2a",
                    Url = "http://url2a",
                    PhysicalAddress = "Physical address2a",
                    HasLatLon = true
                },
                new Place
                {
                    Altitude = 14,
                    Latitude = 24,
                    Longitude = 34,
                    Name = "Place 4",
                    Url = "http://url4",
                    PhysicalAddress = "Physical address4",
                    HasLatLon = true
                },
            };

            var expected = new List<Place>
            {
                new Place
                {
                    Id = places1[0].Id,
                    Altitude = 5.1,
                    Latitude = 6.1,
                    Longitude = 7.1,
                    Name = "Place 1a",
                    Url = "http://url1a",
                    PhysicalAddress = "Physical address1a",
                    HasLatLon = true
                },
                new Place
                {
                    Id = places1[1].Id,
                    Altitude = 10,
                    Latitude = 11,
                    Longitude = 12,
                    Name = "Place 2a",
                    Url = "http://url2a",
                    PhysicalAddress = "Physical address2a",
                    HasLatLon = true
                },
                new Place
                {
                    Id = places1[2].Id,
                    Altitude = 0,
                    Latitude = 0,
                    Longitude = 0,
                    Name = "Place 3",
                    Url = "http://url3",
                    PhysicalAddress = "Physical address3",
                    HasLatLon = false
                },
                new Place
                {
                    Id = places2[1].Id,
                    Altitude = 1,
                    Latitude = 2,
                    Longitude = 3,
                    Name = "Place 1",
                    Url = "http://url1",
                    PhysicalAddress = "Physical address1",
                    HasLatLon = true
                },
                new Place
                {
                    Id = places2[3].Id,
                    Altitude = 14,
                    Latitude = 24,
                    Longitude = 34,
                    Name = "Place 4",
                    Url = "http://url4",
                    PhysicalAddress = "Physical address4",
                    HasLatLon = true
                }
            };
            
            places1.AddIfNew(places2);
            Assert.Equal(expected.Count, places1.Count);
            for (var i = 0; i < expected.Count; i++)
            {
                Assert.True(expected[i].AllEquals(places1[i]));
            }
        }

        [Fact]
        public void TestTagsMerging()
        {
            var tags1 = new List<string> {"tag1", "tag2", "Tag3"};
            var tags2 = new List<string> {"TAG1", "Tag2", "tag3", "tag4", "tag5"};
            var expected = new List<string> {"tag1", "tag2", "Tag3", "tag4", "tag5"};
            PostMergingUtils.MergeStringCollections(tags1, tags2);
            Assert.Equal(expected.Count, tags1.Count);
            for (var i = 0; i < expected.Count; i++)
            {
                Assert.Equal(expected[i], tags1[i]);
            }
        }
        
        [Fact]
        public void TestPollsMerging()
        {
            var polls1 = new List<Poll>
            {
                new Poll
                {
                    Question = "Poll 1",
                    Options = new List<PollOption>
                    {
                        new PollOption("P1A1", false),
                        new PollOption("P1A2", false),
                    }
                },
                new Poll
                {
                    Question = "Poll 2",
                    Options = new List<PollOption>
                    {
                        new PollOption("P2A1", false),
                        new PollOption("P2A2", false),
                    }
                }
            };
            
            var polls2 = new List<Poll>
            {
                new Poll
                {
                    Id = polls1[1].Id,
                    Question = "Poll 2a",
                    Options = new List<PollOption>
                    {
                        new PollOption("P2A1", false),
                        new PollOption("P2A2", false),
                        new PollOption("P2A3", false),
                    }
                },
                new Poll
                {
                    Question = "Poll 2",
                    Options = new List<PollOption>
                    {
                        new PollOption("P2dupA1", false),
                        new PollOption("P2dupA2", false),
                    }
                },
                new Poll
                {
                    Question = "Poll 3",
                    Options = new List<PollOption>
                    {
                        new PollOption("P3A1", false),
                        new PollOption("P3A2", false),
                    }
                }
            };

            var expected = new List<Poll>
            {
                new Poll
                {
                    Id = polls1[0].Id,
                    Question = "Poll 1",
                    Options = new List<PollOption>
                    {
                        new PollOption("P1A1", false),
                        new PollOption("P1A2", false),
                    }
                },
                new Poll
                {
                    Id = polls1[1].Id,
                    Question = "Poll 2a",
                    Options = new List<PollOption>
                    {
                        new PollOption("P2A1", false),
                        new PollOption("P2A2", false),
                        new PollOption("P2A3", false),
                    }
                },
                new Poll
                {
                    Id = polls2[1].Id,
                    Question = "Poll 2",
                    Options = new List<PollOption>
                    {
                        new PollOption("P2dupA1", false),
                        new PollOption("P2dupA2", false),
                    }
                },
                new Poll
                {
                    Id = polls2[2].Id,
                    Question = "Poll 3",
                    Options = new List<PollOption>
                    {
                        new PollOption("P3A1", false),
                        new PollOption("P3A2", false),
                    }
                }
            };

            polls1.AddIfNew(polls2);
            Assert.Equal(expected.Count, polls1.Count);
            for (var i = 0; i < expected.Count; i++)
            {
                Assert.True(expected[i].AllEquals(polls1[i]));
            }
        }

        [Fact]
        public void TestPollOptionsMerging()
        {
            var options1 = new List<PollOption>
            {
                new PollOption
                {
                    Text = "Text 1",
                    Vote = true
                },
                new PollOption
                {
                    Text = "Text 2",
                    Vote = false
                },
                new PollOption
                {
                    Text = "Text 3",
                    Vote = true
                },
                new PollOption
                {
                    Text = "Text 4",
                    Vote = false
                },
            };
            
            var options2 = new List<PollOption>
            {
                new PollOption
                {
                    Text = "Text 2",
                    Vote = true
                },
                new PollOption
                {
                    Text = "Text 3",
                    Vote = false
                },
                new PollOption
                {
                    Text = "Text 5",
                    Vote = false
                },
                new PollOption
                {
                    Text = "Text 6",
                    Vote = true
                }
            };
            var expected = new List<PollOption>
            {
                new PollOption
                {
                    Text = "Text 1",
                    Vote = true
                },
                new PollOption
                {
                    Text = "Text 2",
                    Vote = true
                },
                new PollOption
                {
                    Text = "Text 3",
                    Vote = false
                },
                new PollOption
                {
                    Text = "Text 4",
                    Vote = false
                },
                new PollOption
                {
                    Text = "Text 5",
                    Vote = false
                },
                new PollOption
                {
                    Text = "Text 6",
                    Vote = true
                }
            };

            options1.AddIfNew(options2);
            Assert.Equal(expected.Count, options1.Count);
            for (var i = 0; i < expected.Count; i++)
            {
                Assert.Equal(expected[i].Text, options1[i].Text);
                Assert.Equal(expected[i].Vote, options1[i].Vote);
            }
        }

        
        private Post BuildPost(int postId)
        {
            var post = new Post
            {
                Body = $"Body {postId}",
                Title = $"Title {postId}",
                UserId = $"userId{postId}",
                PostDateTime = DateTimeOffset.UnixEpoch.AddDays(100),
                OriginalSocialMediaSystem = $"socialMediaSystem{postId}",
                OriginalLinkUrlOrId = $"socialMediaSystem1Id{postId}",
                
            };
            post.Events.Add(new EventData
            {
                Place = new Place
                {
                    Altitude = 1, 
                    Latitude = 2, 
                    Longitude = 3, 
                    HasLatLon = true,
                    Name = $"Place{postId}", 
                    Url = "http://place1url", 
                    PhysicalAddress = $"physical address {postId}"
                },
                Title = $"Event {postId}",
                StartTime = DateTimeOffset.Now,
                StopTime = DateTimeOffset.Now
            });
            post.Events.Add(new EventData
            {
                Place = new Place
                {
                    Altitude = 2, 
                    Latitude = 3, 
                    Longitude = 4, 
                    HasLatLon = true,
                    Name = "Place2", 
                    Url = "http://place2url", 
                    PhysicalAddress = "physical address 2"
                },
                Title = "Event 2",
                StartTime = EventData.DateNotSetValue,
                StopTime = EventData.DateNotSetValue
            });
            
            post.Links.Add(new ExternalLink
            {
                Name = $"Link {postId}",
                Source = $"Source {postId}",
                PostId = $"Post {postId}",
                Url = $"http://url{postId}"
            });
            
            post.Links.Add(new ExternalLink
            {
                Name = "Link 1",
                Source = "Source 1",
                PostId = "Post 1",
                Url = "http://url1"
            });
            
            post.Media.Add(new MediaData
            {
                Description = "Description 1",
                CreationDateTime = DateTimeOffset.Now,
                Title = "Title 1",
                LocalPath = "/tmp/localpath1",
                MediaType = "PNG",
                OriginalUrl = "http://originalurl1"
            });
            
            post.Media.Add(new MediaData
            {
                Description = $"Description {postId}",
                CreationDateTime = DateTimeOffset.Now,
                Title = $"Title {postId}",
                LocalPath = $"/tmp/localpath{postId}",
                MediaType = "PNG",
                OriginalUrl = $"http://originalurl{postId}"
            });
            
            post.Places.Add(new Place
            {
                Altitude = 1,
                Latitude = 2,
                Longitude = 3,
                HasLatLon = true,
                Name = "Place 1",
                PhysicalAddress = "Physical address 1",
                Url = "http://placeurl1"
            });
            
            post.Places.Add(new Place
            {
                Altitude = 1,
                Latitude = 2,
                Longitude = 3,
                HasLatLon = true,
                Name = $"Place {postId}",
                PhysicalAddress = $"Physical address {postId}",
                Url = $"http://placeurl{postId}"
            });
            
            post.Polls.Add(new Poll
            {
                Question = $"Poll Question {postId}",
                Options = new List<PollOption>
                {
                    new PollOption("Option1", false),
                    new PollOption("Option2", false)
                }
            });

            post.AddTags(new List<string>{"tag1", "tag2", $"tag{postId}"});
            post.RelatedPosts.Add(new ExternalLink
            {
                Name = $"Related post {postId}",
                PostId = $"RelatedPostId_{postId}",
                Source = $"Source {postId}",
                Url = $"http://relatedposturl{postId}"
            });

            post.TaggedUsers.AddRange(new List<string> {$"taggeduser{postId}", "taggedUser1"});

            return post;
        }

        private Post BuildExpectedPost(Post post1, Post post2)
        {
            var expected = new Post
            {
                Id = post1.Id,
                Body = post2.Body,
                OriginalSocialMediaSystem = post2.OriginalSocialMediaSystem,
                OriginalLinkUrlOrId = post2.OriginalLinkUrlOrId,

                PostDateTime = post2.PostDateTime,
                UserId = post2.UserId,
                Title = post2.Title
            };

            expected.Events.Add(post1.Events[0]);
            expected.Events.Add(post1.Events[1]);
            expected.Events.Add(post2.Events[0]);
            expected.Events.Add(post2.Events[1]);

            expected.Links.Add(post1.Links[0]);
            expected.Links.Add(post1.Links[1]);
            expected.Links.Add(post2.Links[0]);

            expected.Media.Add(post1.Media[0]);
            expected.Media.Add(post1.Media[1]);
            expected.Media.Add(post2.Media[0]);
            expected.Media.Add(post2.Media[1]);

            expected.Places.Add(post1.Places[0]);
            expected.Places.Add(post1.Places[1]);
            expected.Places.Add(post2.Places[0]);
            expected.Places.Add(post2.Places[1]);

            expected.Polls.Add(post1.Polls[0]);
            expected.Polls.Add(post2.Polls[0]);

            expected.Tags.Add(post1.Tags[0]);
            expected.Tags.Add(post1.Tags[1]);
            expected.Tags.Add(post1.Tags[2]);
            expected.Tags.Add(post2.Tags[2]);

            expected.TaggedUsers.Add(post1.TaggedUsers[0]);
            expected.TaggedUsers.Add(post1.TaggedUsers[1]);
            expected.TaggedUsers.Add(post2.TaggedUsers[0]);

            expected.RelatedPosts.Add(post1.RelatedPosts[0]);
            expected.RelatedPosts.Add(post2.RelatedPosts[0]);

            return expected;
        }
    }
}