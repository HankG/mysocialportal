using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LiteDB;
using MySocialPortalDesktop.Factory;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;
using MySocialPortalLib.Util;
using Xunit;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace MySocialPortalLibTest.Util
{
    public class TestUserListPostUpdater
    {
        private const string UserListName1 = "userlist1";
        
        [Fact]
        public void TestUpdating()
        {
            var updater = GetUpdater();
            Populate(updater);
            updater.Update(UserListName1);
            var posts = updater.PostsRepository.GetPosts(100000).ToList();
            Assert.Equal(40, posts.Count);
            (updater.PostsRepository as PostsLiteDbRepository)?.Dispose();
            (updater.PersonsRepository as PersonsLiteDbRepository)?.Dispose();
        }

        private UserListPostUpdater GetUpdater()
        {
            var updater = new UserListPostUpdater(new SocialMediaConnectorFactoryStub(), 
                GetTempPostDb(), GetTempPersonDb(), 
                GetNamedListDb(), 10);

            return updater;
        }
        
        private PostsLiteDbRepository GetTempPostDb()
        {
            return new PostsLiteDbRepository(new FileStream(Path.GetTempFileName() + "posts.db", FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }
        
        private PersonsLiteDbRepository GetTempPersonDb()
        {
            return new PersonsLiteDbRepository(new FileStream(Path.GetTempFileName() + "persons.db", FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }
        
        private NamedListLiteDbRepository GetNamedListDb()
        {
            var fileStream = new FileStream(Path.GetTempFileName() + "lists.db", FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose);
            var repo = new LiteRepository(fileStream);
            var listRepo = new NamedListLiteDbRepository(repo);
            
            return listRepo;
        }

        private void Populate(UserListPostUpdater updater)
        {
            var person1 = new Person
            {
                Name = "Person1"
            };
            person1.SocialMediaAccounts[StandardSocialNetworkNames.Diaspora] = new SocialMediaAccountData
            {
                Id = "1234",
                ProfileId = "UserProfile1234",
                SocialMediaSystemName = StandardSocialNetworkNames.Diaspora
            };            
            person1.SocialMediaAccounts[SocialMediaConnectorFactoryStub.SocialMediaNames[0]] = new SocialMediaAccountData
            {
                Id = "1234",
                ProfileId = "UserProfile1234",
                SocialMediaSystemName = SocialMediaConnectorFactoryStub.SocialMediaNames[0]
            };
            person1.SocialMediaAccounts[SocialMediaConnectorFactoryStub.SocialMediaNames[1]] = new SocialMediaAccountData
            {
                Id = "1234",
                ProfileId = "UserProfile1234",
                SocialMediaSystemName = SocialMediaConnectorFactoryStub.SocialMediaNames[1]
            };
            updater.PersonsRepository.AddPerson(person1);
            updater.UsersLists.Add(person1.Id, UserListName1);
            
            var person2 = new Person
            {
                Name = "Person2"
            };
            person2.SocialMediaAccounts[StandardSocialNetworkNames.Diaspora] = new SocialMediaAccountData
            {
                Id = "2",
                ProfileId = "UserProfile2",
                SocialMediaSystemName = StandardSocialNetworkNames.Diaspora
            };            
            person2.SocialMediaAccounts[SocialMediaConnectorFactoryStub.SocialMediaNames[0]] = new SocialMediaAccountData
            {
                Id = "2",
                ProfileId = "UserProfile2",
                SocialMediaSystemName = SocialMediaConnectorFactoryStub.SocialMediaNames[0]
            };
            person2.SocialMediaAccounts[SocialMediaConnectorFactoryStub.SocialMediaNames[1]] = new SocialMediaAccountData
            {
                Id = "2",
                ProfileId = "UserProfile2",
                SocialMediaSystemName = SocialMediaConnectorFactoryStub.SocialMediaNames[1]
            };
            updater.PersonsRepository.AddPerson(person2);
            updater.UsersLists.Add(person2.Id, "OtherUserList");

            var person3 = new Person
            {
                Name = "Person3"
            };
            person3.SocialMediaAccounts[StandardSocialNetworkNames.Diaspora] = new SocialMediaAccountData
            {
                Id = "3",
                ProfileId = "UserProfile3",
                SocialMediaSystemName = StandardSocialNetworkNames.Diaspora
            };
            updater.PersonsRepository.AddPerson(person3);
            updater.UsersLists.Add(person3.Id, UserListName1);

            var person4 = new Person
            {
                Name = "Person4"
            };
            person4.SocialMediaAccounts[StandardSocialNetworkNames.Diaspora] = new SocialMediaAccountData
            {
                Id = "4",
                ProfileId = "UserProfile4",
                SocialMediaSystemName = StandardSocialNetworkNames.Diaspora
            };            
            person4.SocialMediaAccounts[SocialMediaConnectorFactoryStub.SocialMediaNames[0]] = new SocialMediaAccountData
            {
                Id = "4",
                ProfileId = "UserProfil4",
                SocialMediaSystemName = SocialMediaConnectorFactoryStub.SocialMediaNames[0]
            };
            person4.SocialMediaAccounts[SocialMediaConnectorFactoryStub.SocialMediaNames[1]] = new SocialMediaAccountData
            {
                Id = "4",
                ProfileId = "UserProfile4",
                SocialMediaSystemName = SocialMediaConnectorFactoryStub.SocialMediaNames[0]
            };
            updater.PersonsRepository.AddPerson(person4);
            updater.UsersLists.Add(person4.Id, UserListName1);
        }
        
        class SocialMediaConnectorFactoryStub : ISocialMediaConnectorFactory
        {
            public static readonly List<string> SocialMediaNames = new List<string> {"SocialNetwork1", "SocialNetwork2"};
            public ISocialMediaConnector GetConnectorByNetworkName(string name)
            {
                if (SocialMediaNames.Contains(name))
                {
                    return new SocialMediaConnectorStub(name);
                }

                return null;
            }
        }

        class SocialMediaConnectorStub : ISocialMediaConnector
        {
            public string NetworkName { get; set; }

            public SocialMediaConnectorStub(string networkName)
            {
                NetworkName = networkName;
            }
            
            public IEnumerable<Post> GetNewerHomeTimeline(int maxPosts)
            {
                throw new System.NotImplementedException();
            }

            public IEnumerable<Post> GetOlderHomeTimeline(int maxPosts)
            {
                throw new System.NotImplementedException();
            }

            public IEnumerable<Post> GetNewerUserTimeline(Person person, int maxPosts)
            {
                var newPosts = new List<Post>(maxPosts);
                for (var i = 0; i < maxPosts; i++)
                {
                    var post = new Post
                    {
                        Body = $"New Post at {DateTimeOffset.Now} for {person.Name}",
                        UserId = person.Id,
                        OriginalSocialMediaSystem = NetworkName,
                        OriginalLinkUrlOrId = Guid.NewGuid().ToString()
                    };
                    newPosts.Add(post);
                }

                return newPosts;
            }

            public IEnumerable<Post> GetOlderUserTimeline(Person person, int maxPosts)
            {
                throw new System.NotImplementedException();
            }

            public bool UserOnSocialNetwork(Person person)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}