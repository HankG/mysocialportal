using System.Collections.Generic;
using System.IO;
using System.Linq;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Util;
using Xunit;

namespace MySocialPortalLibTest.Util
{
    public class PostDatabaseMergingUtilsTest
    {
        [Fact]
        public void TestAddOrUpdateToRepository()
        {
            var postDb = GetTempPostDb();

            var post1 = new Post {Title = "Post 1"};
            postDb.AddOrUpdate(post1);
            var post2 = new Post {Title = "Post 2", OriginalSocialMediaSystem = "SM1", OriginalLinkUrlOrId = "SM1Post2"};
            postDb.AddOrUpdate(post2);
            postDb.AddOrUpdate(new Post());
            postDb.AddOrUpdate(new Post());
            postDb.AddOrUpdate(new Post());
            postDb.AddOrUpdate(new Post());

            var updatePost1 = new Post {Id = post1.Id, Title = "New Title Post 1"};
            var updatePost2 = new Post
            {
                Title = "New Title Post2",
                OriginalSocialMediaSystem = post2.OriginalSocialMediaSystem,
                OriginalLinkUrlOrId = post2.OriginalLinkUrlOrId
            };
            var newPosts = new List<Post> {updatePost2, new Post(), updatePost1, new Post(), new Post()};
            PostDatabaseMergingUtils.AddOrUpdateToRepository(postDb, newPosts);
            var allPosts = postDb.GetPosts(10000);
            Assert.Equal(9, allPosts.Count());
            var post1Latest = postDb.GetById(post1.Id);
            Assert.Equal(updatePost1.Title, post1Latest.Title);
            var post2Latest = postDb.GetById(post2.Id);
            Assert.Equal(updatePost2.Title, post2Latest.Title);
        }
        
        private PostsLiteDbRepository GetTempPostDb()
        {
            return new PostsLiteDbRepository(new FileStream(Path.GetTempFileName() + "posts.db", FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }
    }
}