using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Service
{
    public class TimelineManagementServiceTest
    {
        private const string ServiceName = "TestService";
        private readonly ITestOutputHelper _testOutputHelper;

        public TimelineManagementServiceTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(new TimelineManagementService(GetTempDb(), "TestService"));
        }

        [Fact]
        public void TestMarchingForwardOnly()
        {
            var db = GetTempDb();
            var service = new TimelineManagementService(db, ServiceName);
            var name = "Name";
            var requestInterval = service.GetNextSamplingInterval(name, 0, 500UL);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            var requestStep = 500UL;
            Assert.Equal(0UL, requestInterval.IntervalStart);
            Assert.Equal(500UL, requestInterval.IntervalStop);
            var resultsInterval = SimulateQuerySpan(requestInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            
            requestInterval = service.GetNextSamplingInterval(name, resultsInterval.IntervalStop, requestInterval.IntervalStop + requestStep);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));

            requestInterval = service.GetNextSamplingInterval(name, resultsInterval.IntervalStop, requestInterval.IntervalStop + requestStep);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval);
            service.UpdateRequestedInterval(resultsInterval);
            var finalIntervals = db.FindByTimelineName(name, ServiceName).ToList();
            finalIntervals.Sort((i1, i2) => i2.IntervalStop.CompareTo(i1.IntervalStop));
            PrintIntervals(finalIntervals);
            Assert.Single(finalIntervals);
            Assert.Equal(0UL, finalIntervals[0].IntervalStart);
            Assert.Equal(1500UL, finalIntervals[0].IntervalStop);
        }

        [Fact]
        public void TestMarchingForwardGaps()
        {
            var db = GetTempDb();
            var service = new TimelineManagementService(db, "TestService");
            var name = "Name";
            var requestInterval = service.GetNextSamplingInterval(name, 0, 500UL);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            var maxInterval = 100UL;
            var requestStep = 500UL;
            Assert.Equal(0UL, requestInterval.IntervalStart);
            Assert.Equal(500UL, requestInterval.IntervalStop);
            var resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            
            requestInterval = service.GetNextSamplingInterval(name, resultsInterval.IntervalStop, requestInterval.IntervalStop + requestStep);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));

            requestInterval = service.GetNextSamplingInterval(name, resultsInterval.IntervalStop, requestInterval.IntervalStop + requestStep);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            var finalIntervals = db.FindByTimelineName(name, ServiceName).ToList();
            finalIntervals.Sort((i1, i2) => i2.IntervalStop.CompareTo(i1.IntervalStop));
            PrintIntervals(finalIntervals);
            Assert.Equal(3, finalIntervals.Count);
            Assert.Equal(1400UL, finalIntervals[0].IntervalStart);
            Assert.Equal(1500UL, finalIntervals[0].IntervalStop);
            Assert.Equal(900UL, finalIntervals[1].IntervalStart);
            Assert.Equal(1000UL, finalIntervals[1].IntervalStop);
            Assert.Equal(400UL, finalIntervals[2].IntervalStart);
            Assert.Equal(500UL, finalIntervals[2].IntervalStop);
        }

        [Fact]
        public void TestMarchingBackwards()
        {
            var db = GetTempDb();
            var service = new TimelineManagementService(db, "TestService");
            var name = "Name";
            var initialStop = 1000UL;
            var requestInterval = service.GetPreviousSamplingInterval(name, 0, initialStop);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            var maxInterval = 100UL;
            var requestStep = 500UL;
            var resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            
            requestInterval = service.GetPreviousSamplingInterval(name, requestStep, requestInterval.IntervalStart);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            
            requestInterval = service.GetPreviousSamplingInterval(name, requestStep, requestInterval.IntervalStart);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            
            requestInterval = service.GetPreviousSamplingInterval(name, requestStep, requestInterval.IntervalStart);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));

            requestInterval = service.GetPreviousSamplingInterval(name, requestStep, requestInterval.IntervalStart);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));

            requestInterval = service.GetPreviousSamplingInterval(name, requestStep, requestInterval.IntervalStart);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            Assert.Null(requestInterval);
            var intervals = db.FindByTimelineName(name, ServiceName);
            Assert.Single(intervals);
            Assert.Equal(requestStep, intervals[0].IntervalStart);
            Assert.Equal(initialStop, intervals[0].IntervalStop);
        }

        [Fact]
        public void TestMarchingForwardGapsBackfill()
        {
            var db = GetTempDb();
            var service = new TimelineManagementService(db, "TestService");
            var name = "Name";
            var requestInterval = service.GetNextSamplingInterval(name, 0, 500UL);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            var maxInterval = 100UL;
            var requestStep = 500UL;
            Assert.Equal(0UL, requestInterval.IntervalStart);
            Assert.Equal(500UL, requestInterval.IntervalStop);
            var resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            
            requestInterval = service.GetNextSamplingInterval(name, resultsInterval.IntervalStop, requestInterval.IntervalStop + requestStep);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));

            requestInterval = service.GetNextSamplingInterval(name, resultsInterval.IntervalStop, requestInterval.IntervalStop + requestStep);
            PrintIntervals(db.FindByTimelineName(name, ServiceName));
            resultsInterval = SimulateQuerySpan(requestInterval, maxInterval);
            service.UpdateRequestedInterval(resultsInterval);
            var finalIntervals = db.FindByTimelineName(name, ServiceName).ToList();
            finalIntervals.Sort((i1, i2) => i2.IntervalStop.CompareTo(i1.IntervalStop));
            PrintIntervals(finalIntervals);
            Assert.Equal(3, finalIntervals.Count);
            Assert.Equal(1400UL, finalIntervals[0].IntervalStart);
            Assert.Equal(1500UL, finalIntervals[0].IntervalStop);
            Assert.Equal(900UL, finalIntervals[1].IntervalStart);
            Assert.Equal(1000UL, finalIntervals[1].IntervalStop);
            Assert.Equal(400UL, finalIntervals[2].IntervalStart);
            Assert.Equal(500UL, finalIntervals[2].IntervalStop);

            var latest = finalIntervals.Max(i => i.IntervalStop);
            _testOutputHelper.WriteLine("Marching backwards to try to fill");
            while (requestInterval != null)
            {
                requestInterval = service.GetPreviousSamplingInterval(name, 0, latest);
                PrintIntervals(db.FindByTimelineName(name, ServiceName));
                if (requestInterval == null)
                {
                    break;
                }
                resultsInterval = SimulateQuerySpan(requestInterval, maxInterval, true, true);
                service.UpdateRequestedInterval(resultsInterval);           
                PrintIntervals(db.FindByTimelineName(name, ServiceName));
            }

            finalIntervals = db.FindByTimelineName(name, ServiceName).ToList();
            Assert.Single(finalIntervals);
            Assert.Equal(0UL, finalIntervals[0].IntervalStart);
            Assert.Equal(1500UL, finalIntervals[0].IntervalStop);
        }

        private TimelineLiteDbRepository GetTempDb()
        {
            return new TimelineLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }

        private void PrintIntervals(IEnumerable<TimelineInterval> intervals)
        {
            _testOutputHelper.WriteLine("");
            var intervalsToPrint = intervals.ToList();
            intervalsToPrint.Sort((i1, i2) => i2.IntervalStop.CompareTo(i1.IntervalStop));
            intervalsToPrint.ForEach(i => _testOutputHelper.WriteLine($"{i.IntervalStart} -> {i.IntervalStop}"));
            _testOutputHelper.WriteLine("");
        }
        
        private TimelineInterval SimulateQuerySpan(TimelineInterval requestInterval, ulong maxDelta = ulong.MaxValue,
            bool forward = true, bool simulateNone = false)
        {
            var newInterval = requestInterval.Copy();
            var delta = requestInterval.IntervalStop - requestInterval.IntervalStart;
            var actualDelta = delta > maxDelta ? maxDelta : delta;

            if (simulateNone && new Random().NextDouble() > 0.5)
            {
                actualDelta = 0;
            }
   
            if (forward)
            {
                newInterval.IntervalStart = newInterval.IntervalStop - actualDelta;
            }
            else
            {
                newInterval.IntervalStop = newInterval.IntervalStart + actualDelta;
            }

            return newInterval;
        }
    }
}