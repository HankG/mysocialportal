using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;
using MySocialPortalLib.Service.SocialMediaConnectors;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace MySocialPortalLibTest.Service.SocialMediaConnectors
{
    public class TwitterConnectorTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public TwitterConnectorTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(new TwitterConnector(null, null));
        }

        [Fact]
        public void TestPersonHasTwitter()
        {
            var tc = new TwitterConnector(null, null);
            var hasTwitter = new Person();
            hasTwitter.SocialMediaAccounts.Add(StandardSocialNetworkNames.Twitter,
                new SocialMediaAccountData { SocialMediaSystemName = StandardSocialNetworkNames.Twitter});
            Assert.True(tc.UserOnSocialNetwork(hasTwitter));
            Assert.False(tc.UserOnSocialNetwork(new Person()));
        }

        [Fact (Skip = "Need Twitter credentials")]
        public void TestGetNewHomeTimeline()
        {
            var tc = new TwitterConnector(GetTempPersonDb(), GetService());
            var allPosts = new List<Post>();
            var pulledPosts = tc.GetNewerHomeTimeline(10);
            allPosts.AddRange(pulledPosts);
            _testOutputHelper.WriteLine("Pulled Posts #1");
            PrintPosts(pulledPosts);

            //Thread.Sleep(1000);
            pulledPosts = tc.GetNewerHomeTimeline(10);
            allPosts.AddRange(pulledPosts);
            _testOutputHelper.WriteLine("Pulled Posts #2");
            PrintPosts(pulledPosts);
            
            var postIds = new HashSet<string>(allPosts.Count());
            foreach (var p in allPosts)
            {
                Assert.DoesNotContain(p.OriginalLinkUrlOrId, postIds);
                postIds.Add(p.OriginalLinkUrlOrId);
            }
        }
        
        [Fact (Skip = "Need Twitter credentials")]
        public void TestGetOldHomeTimeline()
        {
            var tc = new TwitterConnector(GetTempPersonDb(), GetService());
            var allPosts = new List<Post>();
            var pulledPosts = tc.GetOlderHomeTimeline(10);
            allPosts.AddRange(pulledPosts);
            _testOutputHelper.WriteLine("Pulled Posts #1");
            PrintPosts(pulledPosts);
            
            pulledPosts = tc.GetOlderHomeTimeline(10);
            allPosts.AddRange(pulledPosts);
            _testOutputHelper.WriteLine("Pulled Posts #2");
            PrintPosts(pulledPosts);
            
            var postIds = new HashSet<string>(allPosts.Count());
            foreach (var p in allPosts)
            {
                Assert.DoesNotContain(p.OriginalLinkUrlOrId, postIds);
                postIds.Add(p.OriginalLinkUrlOrId);
            }
        }

        [Fact (Skip = "Need Twitter credentials")]
        public void TestGetNewerUserTimeline()
        {
            var jack = new Person
            {
                Name = "Jack Dorsey"
            };
            jack.SocialMediaAccounts.Add(StandardSocialNetworkNames.Twitter, new SocialMediaAccountData{ProfileId = "jack", SocialMediaSystemName = StandardSocialNetworkNames.Twitter});
            
            var tc = new TwitterConnector(GetTempPersonDb(), GetService());
            var allPosts = new List<Post>();
            var pulledPosts = tc.GetNewerUserTimeline(jack,10);
            allPosts.AddRange(pulledPosts);
            _testOutputHelper.WriteLine("Pulled Posts #1");
            PrintPosts(pulledPosts);

            pulledPosts = tc.GetNewerUserTimeline(jack,10);
            allPosts.AddRange(pulledPosts);
            _testOutputHelper.WriteLine("Pulled Posts #2");
            PrintPosts(pulledPosts);
            
            var postIds = new HashSet<string>(allPosts.Count());
            foreach (var p in allPosts)
            {
                Assert.DoesNotContain(p.OriginalLinkUrlOrId, postIds);
                postIds.Add(p.OriginalLinkUrlOrId);
            }
        }

        [Fact (Skip = "Need Twitter credentials")]
        public void TestGetOlderUserTimeline()
        {
            var jack = new Person
            {
                Name = "Jack Dorsey"
            };
            jack.SocialMediaAccounts.Add(StandardSocialNetworkNames.Twitter, new SocialMediaAccountData{ProfileId = "1234", SocialMediaSystemName = StandardSocialNetworkNames.Twitter});
            jack.SocialMediaAccounts[StandardSocialNetworkNames.Twitter].AdditionalProperties.Add("ScreenName", "jack");
            
            var tc = new TwitterConnector(GetTempPersonDb(), GetService());
            var allPosts = new List<Post>();
            var pulledPosts = tc.GetOlderUserTimeline(jack,10);
            allPosts.AddRange(pulledPosts);
            _testOutputHelper.WriteLine("Pulled Posts #1");
            PrintPosts(pulledPosts);

            pulledPosts = tc.GetOlderUserTimeline(jack,10);
            allPosts.AddRange(pulledPosts);
            _testOutputHelper.WriteLine("Pulled Posts #2");
            PrintPosts(pulledPosts);
            
            var postIds = new HashSet<string>(allPosts.Count());
            foreach (var p in allPosts)
            {
                Assert.DoesNotContain(p.OriginalLinkUrlOrId, postIds);
                postIds.Add(p.OriginalLinkUrlOrId);
            }
        }


        private TimelineManagementService GetService()
        {
            return new TimelineManagementService(GetTempTimelineDb(), "TestService");
        }
        private PersonsLiteDbRepository GetTempPersonDb()
        {
            var db = new PersonsLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
            return db;
        }
        
        private ITimelineRepository GetTempTimelineDb()
        {
            return new TimelineLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }

        private void PrintPosts(IEnumerable<Post> posts)
        {
            posts.ToList().ForEach(p =>
            {
                var length = p.Body.Length > 30 ? 30 : p.Body.Length;
                var text = p.Body.Substring(0, length);
                _testOutputHelper.WriteLine($"{p.OriginalLinkUrlOrId},{p.PostDateTime},{text}");
            });
        }
    }
}