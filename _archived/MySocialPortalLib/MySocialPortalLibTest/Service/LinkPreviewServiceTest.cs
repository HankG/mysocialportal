
using System;
using System.IO;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Service
{
    public class LinkPreviewServiceTest : IDisposable
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly string _tmpFolder = Path.Combine(Path.GetTempPath(), $"{Guid.NewGuid().ToString()}");
        private FileCacheLiteDbRepository _cacheRepository;
        private LinkPreviewLiteDbRepository _linkPreviewRepository;
        
        public LinkPreviewServiceTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            Directory.CreateDirectory(_tmpFolder);
        }

        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(GetTmpLinkPreviewService());
        }

        [Fact]
        public void TestGoodUrl()
        {
            var url = "https://en.wikipedia.org/wiki/Profile_Portrait_of_a_Young_Lady";
            var service = GetTmpLinkPreviewService();
            var preview = service.GetPreview(url).Result;
            Assert.NotNull(preview);
            Assert.True(preview.UrlFound);
            Assert.True(preview.HasPreviewImage);
            Assert.NotEmpty(preview.PreviewImageName);
            var fullPathToImage = Path.Combine(_tmpFolder, preview.PreviewImageName);
            Assert.True(File.Exists(fullPathToImage));
            Assert.NotEqual(0, new FileInfo(fullPathToImage).Length);
            Assert.NotEqual(DateTimeOffset.UnixEpoch, preview.TimeGeneraterated);
        }

        [Fact]
        public void TestDisposeLockup()
        {
            var url = "https://twitter.com/dcherring/status/1198968638285524995";
            var service = GetTmpLinkPreviewService();
            var preview = service.GetPreview(url).Result;
            Assert.NotNull(preview);
            _testOutputHelper.WriteLine(preview.PreviewImageName);
        }

        [Fact]
        public void TestBadUrl()
        {
            var url = "https://localhost/Profile_Portrait_of_a_Young_Lady";
            var service = GetTmpLinkPreviewService();
            var preview = service.GetPreview(url).Result;
            Assert.Null(preview);
        }
        
        private LinkPreviewService GetTmpLinkPreviewService()
        {
            _cacheRepository = new FileCacheLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
            var imageService =  new ImageService(_cacheRepository, _tmpFolder);
            
            _linkPreviewRepository = new LinkPreviewLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
            
            return new LinkPreviewService(_linkPreviewRepository, imageService);
        }

        public void Dispose()
        {
            _cacheRepository.Dispose();
            _linkPreviewRepository.Dispose();
            Directory.Delete(_tmpFolder, true);
        }
    }
}