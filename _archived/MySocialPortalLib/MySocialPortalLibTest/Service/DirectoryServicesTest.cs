using System;
using System.IO;
using MySocialPortalLib.Service;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Service
{
    public class DirectoryServicesTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public DirectoryServicesTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void TestGetExternalLinkMediaDirectory()
        {
            var expectedRelPath = Path.Combine("MySocialPortalLibTest", "ExternalLinkMedia");
            var settingsPath = DirectoryServices.Instance.ExternalLinkMediaDirectory();
            var relPath = ConvertToRelPath(settingsPath);
            Assert.Equal(expectedRelPath, relPath);
        }

        [Fact]
        public void TestGetUserAppBaseDirectory()
        {
            var expectedRelPath = "MySocialPortalLibTest";
            var userAppDir = DirectoryServices.Instance.UserAppBaseDirectory();
            var relPath = ConvertToRelPath(userAppDir);
            Assert.Equal(expectedRelPath, relPath);
        }

        [Fact]
        public void TestGetUserCredentialsSettingsDirectory()
        {
            var expectedRelPath = Path.Combine("MySocialPortalLibTest", "SocialNetworkCredentialsSettings");
            var settingsPath = DirectoryServices.Instance.UserCredentialsSettingsDirectory();
            var relPath = ConvertToRelPath(settingsPath);
            Assert.Equal(expectedRelPath, relPath);
           
        }
        
        [Fact]
        public void TestOpenGraphCacheDirectory()
        {
            var expectedRelPath = Path.Combine("MySocialPortalLibTest", "OpenGraphCache");
            var settingsPath = DirectoryServices.Instance.OpenGraphCacheDirectory();
            var relPath = ConvertToRelPath(settingsPath);
            Assert.Equal(expectedRelPath, relPath);
           
        }
        
        [Fact]
        public void TestGetPostMediaDirectory()
        {
            var expectedRelPath = Path.Combine("MySocialPortalLibTest", "PostMedia");
            var settingsPath = DirectoryServices.Instance.PostMediaDirectory();
            var relPath = ConvertToRelPath(settingsPath);
            Assert.Equal(expectedRelPath, relPath);
           
        }
        
        [Fact]
        public void TestGetProfileImagesDirectory()
        {
            var expectedRelPath = Path.Combine("MySocialPortalLibTest", "ProfileImages");
            var settingsPath = DirectoryServices.Instance.ProfileImagesDirectory();
            var relPath = ConvertToRelPath(settingsPath);
            Assert.Equal(expectedRelPath, relPath);
           
        }

        private string ConvertToRelPath(string fullPath)
        {
            var appPathBase = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            return Path.GetRelativePath(appPathBase, fullPath); 
        }
    }
}