using System;
using System.Collections.Generic;
using System.IO;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;
using Xunit;
using Xunit.Sdk;

namespace MySocialPortalLibTest.Service
{
    public class ImageServiceTest : IDisposable
    {
        private readonly Uri _validImageUrl = new Uri("https://upload.wikimedia.org/wikipedia/en/6/60/Commons-barnstar.png");
        private readonly string _tmpFolder = Path.Combine(Path.GetTempPath(), $"{Guid.NewGuid().ToString()}");

        public ImageServiceTest()
        {
            Directory.CreateDirectory(_tmpFolder);
        }
        
        [Fact]
        public void TestGetImage()
        {
            var (success, file) = GetImageService().GetRemoteImage(_validImageUrl).Result;
            Assert.True(success);
            Assert.True(file.Exists);
        }
        
        [Fact]
        public void TestGetImageReuseOkay()
        {
            var service = GetImageService();
            var (success1, file1) = service.GetRemoteImage(_validImageUrl).Result;
            Assert.True(success1);
            Assert.True(file1.Exists);
            var (success2, file2) = service.GetRemoteImage(_validImageUrl).Result;
            Assert.True(success2);
            Assert.True(file2.Exists);
            Assert.Equal(file1.DirectoryName, file2.DirectoryName);
            Assert.Equal(file1.Name, file2.Name);
        }

                
        [Fact]
        public void TestGetImageNoReuse()
        {
            var service = GetImageService();
            var (success1, file1) = service.GetRemoteImage(_validImageUrl, false).Result;
            Assert.True(success1);
            Assert.True(file1.Exists);
            var (success2, file2) = service.GetRemoteImage(_validImageUrl, false).Result;
            Assert.True(success2);
            Assert.True(file2.Exists);
            Assert.Equal(file1.DirectoryName, file2.DirectoryName);
            Assert.NotEqual(file1.Name, file2.Name);
        }
        
                
        [Fact]
        public void TestBadUrl()
        {
            var badUrl = new Uri("https://localhost/an_image.png");
            var (success1, file1) = GetImageService().GetRemoteImage(badUrl).Result;
            Assert.False(success1);
            Assert.Null(file1);
        }
        [Fact]
        
        public void TesNullUri()
        {
            var (success, file) = GetImageService().GetRemoteImage(null).Result;
            Assert.False(success);
            Assert.Null(file);
        }

        private ImageService GetImageService()
        {
            IFileCacheRepository cacheRepository = new FileCacheLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
            return new ImageService(cacheRepository, _tmpFolder);
        }

        public void Dispose()
        {
            Directory.Delete(_tmpFolder, true);
        }
    }
}