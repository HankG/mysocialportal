using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using LinqToTwitter;
using MySocialPortalLib.Converter;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;
using Xunit;

namespace MySocialPortalLibTest.Converter
{
    public class TwitterConverterTest
    {
        [Fact]
        public void TestTweetToPostConverterSimple()
        {
            var personDb = GetTempDb();
            var tweet = BuildSimpleTweet();
            var post = new TwitterConverter(personDb).TweetToPost(tweet);
            var person = personDb.FindById(post.UserId);
            CompareUser(tweet.User, person);
            CompareTweet(tweet, post);
        }
        
        [Fact]
        public void TestTweetToPostConverterFull()
        {
            var personDb = GetTempDb();
            var tweet = BuildFullTweet();
            var post = new TwitterConverter(personDb).TweetToPost(tweet);
            var person = personDb.FindById(post.UserId);
            CompareUser(tweet.User, person);
            CompareTweet(tweet, post);
        }

        private void CompareTweet(Status tweet, Post post)
        {
            Assert.Equal(tweet.FullText, post.Body);
            Assert.Equal(tweet.CreatedAt, post.PostDateTime);
            Assert.Equal(tweet.Entities.MediaEntities.Count, post.Media.Count);
            Assert.Equal(StandardSocialNetworkNames.Twitter, post.OriginalSocialMediaSystem);
            Assert.Equal(tweet.StatusID.ToString(CultureInfo.InvariantCulture), post.OriginalLinkUrlOrId);
            for (int i = 0; i < tweet.Entities.MediaEntities.Count; i++)
            {
                Assert.Equal(tweet.Entities.MediaEntities[i].AltText, post.Media[i].Description);
                Assert.Equal(tweet.Entities.MediaEntities[i].MediaUrlHttps, post.Media[i].OriginalUrl);
                Assert.Equal(tweet.Entities.MediaEntities[i].Type, post.Media[i].MediaType);
            }
            
            Assert.Equal(tweet.Entities.UrlEntities.Count, post.Links.Count);
            for (int i = 0; i < tweet.Entities.UrlEntities.Count; i++)
            {
                Assert.Equal(tweet.Entities.UrlEntities[i].ExpandedUrl, post.Links[i].Url);
                Assert.Equal(StandardSocialNetworkNames.Twitter, post.Links[i].Source);
            }
            
            Assert.Equal(tweet.Entities.HashTagEntities.Count, post.Tags.Count);
            for (int i = 0; i < tweet.Entities.HashTagEntities.Count; i++)
            {
                Assert.Equal(tweet.Entities.HashTagEntities[i].Text.ToUpperInvariant(), post.Tags[i]);
            }
            
            Assert.Equal(tweet.Entities.UserMentionEntities.Count, post.TaggedUsers.Count);
            for (int i = 0; i < tweet.Entities.UserMentionEntities.Count; i++)
            {
                Assert.Equal(tweet.Entities.UserMentionEntities[i].ScreenName, post.TaggedUsers[i]);
            }
        }

        private PersonsLiteDbRepository GetTempDb()
        {
            return new PersonsLiteDbRepository(new FileStream(Path.GetTempFileName(), FileMode.OpenOrCreate,
                FileAccess.ReadWrite, FileShare.None, 4096,
                FileOptions.RandomAccess | FileOptions.DeleteOnClose));
        }

        private Status BuildFullTweet()
        {
            var tweet = BuildSimpleTweet();
            tweet.Entities.MediaEntities.Add(new MediaEntity
            {
                MediaUrlHttps = "https://media/url",
                AltText = "alttext",
                Type = "mediatype"
            });
            
            tweet.Entities.HashTagEntities.Add(new HashTagEntity { Text = "hashtag1"});
            tweet.Entities.HashTagEntities.Add(new HashTagEntity { Text = "hashtag2"});
            tweet.Entities.HashTagEntities.Add(new HashTagEntity { Text = "hashtag3"});

            tweet.Entities.UserMentionEntities.Add(new UserMentionEntity
            {
                Name = "name",
                ScreenName = "screenname"
            });
            
            tweet.Entities.UserMentionEntities.Add(new UserMentionEntity
            {
                Name = "name2",
                ScreenName = "screenname2"
            });
            
            tweet.Entities.UrlEntities.Add(new UrlEntity {ExpandedUrl = "https://expandedurl1"});
            tweet.Entities.UrlEntities.Add(new UrlEntity {ExpandedUrl = "https://expandedurl2"});
            tweet.Entities.UrlEntities.Add(new UrlEntity {ExpandedUrl = "https://expandedurl3"});
            tweet.Entities.UrlEntities.Add(new UrlEntity {ExpandedUrl = "https://expandedurl4"});
            return tweet;
        }
        private Status BuildSimpleTweet()
        {
            var user = new User
            {
                UserIDResponse = "12345",
                Email = "user1@email.com",
                Name = "User1 Real Name",
                ProfileImageUrlHttps = "https://images/image1.jpg",
                Url = "https://profileurls/user1",
                Following = false,
                CreatedAt = DateTime.Now
            };
            var tweet = new Status
            {
                FullText = "This is a tweet",
                User = user,
                CreatedAt = DateTime.Now
            };
            tweet.Entities = new Entities();
            tweet.Entities.MediaEntities = new List<MediaEntity>();
            tweet.Entities.SymbolEntities = new List<SymbolEntity>();
            tweet.Entities.UrlEntities = new List<UrlEntity>();
            tweet.Entities.HashTagEntities = new List<HashTagEntity>();
            tweet.Entities.UserMentionEntities = new List<UserMentionEntity>();
            
            return tweet;
        }

        private void CompareUser(User user, Person person)
        {
            var smd = person.SocialMediaAccounts[StandardSocialNetworkNames.Twitter];
            Assert.Equal(user.UserIDResponse.ToString(), smd.ProfileId);
            Assert.Equal(user.Url, smd.ProfileUrl);
            Assert.Equal(user.ProfileImageUrlHttps, smd.ProfilePhotoPath);
            Assert.Equal(user.Following, Boolean.Parse(smd.AdditionalProperties["FollowingMe"]));
            Assert.Contains(user.Email, person.Emails.Select(e => e.Value));
            Assert.Equal(user.Name, person.Name);
        }
    }
}