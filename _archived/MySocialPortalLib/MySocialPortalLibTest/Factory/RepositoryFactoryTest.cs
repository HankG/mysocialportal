using MySocialPortalDesktop.Factory;
using Xunit;

namespace MySocialPortalLibTest.Factory
{
    public class RepositoryFactoryTest
    {
        [Fact]
        public void TestInstances()
        {
            var instance1 = RepositoryFactory.Instance;
            var instance2 = RepositoryFactory.Instance;
            Assert.NotNull(instance1);
            Assert.Equal(instance1, instance2);
        }
        
        [Fact]
        public void TestAllPostsRepository()
        {
            Assert.NotNull(RepositoryFactory.Instance.AllPostsRepository);
        }

        [Fact]
        public void TestLinkPreviewRepository()
        {
            Assert.NotNull(RepositoryFactory.Instance.LinkPreviewRepository);
        }
        
        [Fact]
        public void TestLinkPreviewImageCacheRepository()
        {
            Assert.NotNull(RepositoryFactory.Instance.LinkPreviewImageCacheRepository);
        }

        [Fact]
        public void TestListsRepository()
        {
            Assert.NotNull(RepositoryFactory.Instance.ListsRepository);
        }

        [Fact]
        public void TestMainPeopleRepository()
        {
            Assert.NotNull(RepositoryFactory.Instance.MainPeopleRepository);
        }
        
        [Fact]
        public void TestProfileImageServiceRepository()
        {
            Assert.NotNull(RepositoryFactory.Instance.ProfileImageCacheRepository);
        }

    }
}