using MySocialPortalDesktop.Factory;
using Xunit;

namespace MySocialPortalLibTest.Factory
{
    public class ServiceFactoryTest
    {
        [Fact]
        public void TestInstance()
        {
            Assert.NotNull(ServiceFactory.Instance);
        }

        [Fact]
        public void TestLinkPreviewService()
        {
            Assert.NotNull(ServiceFactory.Instance.LinkPreviewService);
        }

        [Fact]
        public void TestProfileImageService()
        {
            Assert.NotNull(ServiceFactory.Instance.ProfileImageService);
        }
    }
}