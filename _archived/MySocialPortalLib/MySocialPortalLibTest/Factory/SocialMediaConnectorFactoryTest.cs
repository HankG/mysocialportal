using MySocialPortalDesktop.Factory;
using Xunit;

namespace MySocialPortalLibTest.Factory
{
    public class SocialMediaConnectorFactoryTest
    {
        [Fact]
        public void TestTwitterConnector()
        {
            Assert.NotNull(SocialMediaConnectorFactory.GetNewTwitterConnector());
        }
    }
}