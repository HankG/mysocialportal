using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Model
{
    public class LabeledValueTest
    {
        [Fact]
        public void TestBasicValueTypes()
        {
            var intLabel = "MyInteger";
            var intValue = 9;
            var intLabeledValue= new LabeledValue<Int32>(intLabel, intValue);
            Assert.Equal(intLabel, intLabeledValue.Label);
            Assert.Equal(intValue, intLabeledValue.Value);

            var stringLabel = "MyString";
            var stringValue = "MyStringValue";
            var stringLabeledValue= new LabeledValue<string>(stringLabel, stringValue);
            Assert.Equal(stringLabel, stringLabeledValue.Label);
            Assert.Equal(stringValue, stringLabeledValue.Value);
            
            Assert.Equal("", new LabeledValue<int>().Label);
            Assert.Equal(0, new LabeledValue<int>().Value);
            
            Assert.Equal("", new LabeledValue<string>().Label);
            Assert.Null(new LabeledValue<string>().Value);
        }
        
        [Fact]
        public void TestListSerialization()
        {
            var intValueList = new List<LabeledValue<int>>();
            intValueList.Add(new LabeledValue<int>("label1", 1));
            intValueList.Add(new LabeledValue<int>("label2", 2));
            intValueList.Add(new LabeledValue<int>("label3", 3));
            intValueList.Add(new LabeledValue<int>());
            
            var intListJson = JsonSerializer.Serialize(intValueList, new JsonSerializerOptions {WriteIndented = true});
            var intListFromJson = JsonSerializer.Deserialize<List<LabeledValue<int>>>(intListJson);
            Assert.Equal(intValueList, intListFromJson);

            var stringValueList = new List<LabeledValue<string>>();
            stringValueList.Add(new LabeledValue<string>("label1", "string1"));
            stringValueList.Add(new LabeledValue<string>("label2", "string2"));
            stringValueList.Add(new LabeledValue<string>("label3", "string3"));
            stringValueList.Add(new LabeledValue<string>());
            
            var stringListJson = JsonSerializer.Serialize(stringValueList, new JsonSerializerOptions {WriteIndented = true});
            var stringListFromJson = JsonSerializer.Deserialize<List<LabeledValue<string>>>(stringListJson);
            Assert.Equal(stringValueList, stringListFromJson);
        }

        [Fact]
        public void TestDictionarySerialization()
        {
            var intValueDict = new Dictionary<string, LabeledValue<int>>();
            intValueDict.Add("label1", new LabeledValue<int>("label1", 1));
            intValueDict.Add("label2", new LabeledValue<int>("label2", 2));
            intValueDict.Add("label3", new LabeledValue<int>("label3", 3));

            var intDictJson = JsonSerializer.Serialize(intValueDict, new JsonSerializerOptions {WriteIndented = true});
            var intDictFromJson = JsonSerializer.Deserialize<Dictionary<string, LabeledValue<int>>>(intDictJson);
            Assert.Equal(intDictJson, intDictJson);

            var stringValueDict = new Dictionary<string, LabeledValue<string>>();
            stringValueDict.Add("label1", new LabeledValue<string>("label1", "string1"));
            stringValueDict.Add("label2", new LabeledValue<string>("label2", "string2"));
            stringValueDict.Add("label3", new LabeledValue<string>("label3", "string3"));

            var stringDictJson = JsonSerializer.Serialize(stringValueDict, new JsonSerializerOptions {WriteIndented = true});
            var stringDictFromJson = JsonSerializer.Deserialize<Dictionary<string, LabeledValue<string>>>(stringDictJson);
            Assert.Equal(stringValueDict, stringDictFromJson);
            
        }
    }
}