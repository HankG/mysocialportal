using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class InteractionTest
    {
        [Theory]
        [ClassData(typeof(InteractionDataGenerator))]
        public void TestSerialization(Interaction interaction)
        {
            var json = JsonSerializer.Serialize(interaction, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<Interaction>(json);
            Assert.Equal(interaction, fromJson);
            Assert.True(interaction.AllEquals(fromJson));
        }
    }
    
    public class InteractionDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new Interaction(), },
            new object[] {
                new Interaction()
                {
                    Type = "SomeType",
                    PostId = Guid.NewGuid().ToString(),
                    UserId = Guid.NewGuid().ToString()
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}