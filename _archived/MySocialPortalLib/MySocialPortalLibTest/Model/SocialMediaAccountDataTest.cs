using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class SocialMediaAccountDataTest
    {
        [Theory]
        [ClassData(typeof(SocialMediaAccountDataDataGenerator))]
        public void TestSerialization(SocialMediaAccountData d)
        {
            var json = JsonSerializer.Serialize(d, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<SocialMediaAccountData>(json);
            Assert.Equal(d, fromJson);
            Assert.True(d.AllEquals(fromJson));

        }

        [Fact]
        public void TestCopy()
        {
            var emptySma = new SocialMediaAccountData();
            Assert.True(emptySma.AllEquals(emptySma.Copy()));

            var sma = new SocialMediaAccountData
            {
                Active = false,
                ProfileId = Guid.NewGuid().ToString(),
                ProfileUrl = Guid.NewGuid().ToString(),
                RealName = Guid.NewGuid().ToString(),
                ProfilePhotoPath = Guid.NewGuid().ToString(),
                SocialMediaSystemName = Guid.NewGuid().ToString()
            };
            sma.AdditionalProperties[Guid.NewGuid().ToString()] = Guid.NewGuid().ToString();
            sma.AdditionalProperties[Guid.NewGuid().ToString()] = Guid.NewGuid().ToString();
            var smaCopy = sma.Copy();
            Assert.True(sma.AllEquals(smaCopy));
            sma.AdditionalProperties.Clear();
            Assert.False(sma.AllEquals(smaCopy));
        }
    }

    public class SocialMediaAccountDataDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new SocialMediaAccountData()},
            new object[] {
                new SocialMediaAccountData{
                    Active = true,
                    Id = "12345",
                    ProfileId = "otherSystemProfileId1234",
                    ProfileUrl = "https://othersystem.com/profile/otherSystemProfileId1234",
                    RealName = "Real name in other system",
                    ProfilePhotoPath = "photos/profile/1235.jpg",
                    SocialMediaSystemName = "OtherSocialNetwork"
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}