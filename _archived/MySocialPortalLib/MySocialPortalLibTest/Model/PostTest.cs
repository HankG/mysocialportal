using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class PostTest
    {
        [Theory]
        [ClassData(typeof(PostDataGenerator))]
        public void TestSerialization(Post p)
        {
            var json = JsonSerializer.Serialize(p, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<Post>(json); 
            Assert.Equal(p, fromJson);
            Assert.True(p.AllEquals(fromJson));
        }

    }
    
    public class PostDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new Post(), },
            new object[] {
                new Post(){
                    Body = "Post body",
                    Events = new List<EventData>{new EventData{}},
                    Id = "12345",
                    Interactions = new List<Interaction>{new Interaction()},
                    Links = new List<ExternalLink>{new ExternalLink()},
                    Media = new List<MediaData>{new MediaData()},
                    Places = new List<Place>{new Place()},
                    Polls = new List<Poll>{new Poll()},
                    Tags = new List<string>{"tag1"},
                    Title = "Post Title",
                    PostDateTime = DateTimeOffset.Now,
                    RelatedPosts = new List<ExternalLink>{new ExternalLink()},
                    TaggedUsers = new List<string>{"user1", "user2"},
                    OriginalLinkUrlOrId = "original link",
                    OriginalSocialMediaSystem = "socialmediathing"
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}