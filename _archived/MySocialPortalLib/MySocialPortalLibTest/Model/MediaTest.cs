using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class MediaTest
    {
        [Theory]
        [ClassData(typeof(MediaDataGenerator))]
        public void TestSerialization(MediaData m)
        {
            var json = JsonSerializer.Serialize(m, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<MediaData>(json);
            Assert.Equal(m, fromJson);
            Assert.True(m.AllEquals(fromJson));
        }

    }
    
    public class MediaDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new MediaData() },
            new object[] {
                new MediaData(){
                    Description = "Media description",
                    Title = "Media title",
                    CreationDateTime = DateTimeOffset.UnixEpoch.AddDays(1000),
                    LocalPath = "media/video/12456.mp4",
                    MediaType = "video/mp4",
                    OriginalUrl = "https://movies.com/preview1234.mp4"
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

}