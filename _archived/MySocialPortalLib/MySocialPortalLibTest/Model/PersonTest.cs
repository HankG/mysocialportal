using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class PersonTest
    {
        [Fact]
        public void TestCollectionsDefaultInitialization()
        {
            var person = new Person();
            Assert.NotNull(person.Addresses);
            Assert.NotNull(person.Birthday);
            Assert.NotNull(person.Emails);
            Assert.NotNull(person.Id);
            Assert.NotNull(person.Name);
            Assert.NotNull(person.Websites);
            Assert.NotNull(person.AdditionalProperties);
            Assert.NotNull(person.PhoneNumbers);
            Assert.NotNull(person.SocialMediaAccounts);
        }
        
        [Theory]
        [ClassData(typeof(PersonDataGenerator))]
        public void TestSerialization(Person p)
        {
            var json = JsonSerializer.Serialize(p, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<Person>(json);
            Assert.Equal(p, fromJson);
            Assert.True(p.AllEquals(fromJson));
        }
    }

    public class PersonDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new Person()},
            new object[] {
                new Person{
                    Addresses = new List<LabeledValue<string>>{new LabeledValue<string>("address1", "addressvalue1")},
                    Birthday = "1/1/2001",
                    Emails = new List<LabeledValue<string>>{new LabeledValue<string>("email1","name@email.com")},
                    Id = "12345",
                    Name = "My name",
                    Websites = new List<LabeledValue<string>>{new LabeledValue<string>("personal", "https://mysite.com")},
                    AdditionalProperties = new Dictionary<string, string>{{"k1","v1"},{"k2","v2"}},
                    PhoneNumbers = new List<LabeledValue<string>>{new LabeledValue<string>("phone1","555-1212")},
                    SocialMediaAccounts = new Dictionary<string, SocialMediaAccountData>
                    {
                        {
                            "othernet" , new SocialMediaAccountData
                            {
                                ProfileId = "profile1",
                                SocialMediaSystemName = "othernet"
                            }
                        }
                    }
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

}