using MySocialPortalLib.Model.Settings;
using MySocialPortalLib.Service.SocialMediaConnectors;
using Xunit;

namespace MySocialPortalLibTest.Model.Settings
{
    public class TwitterConnectorSettingsTest
    {
        [Fact]
        public void TestDefaultConstruction()
        {
            var tcs = new TwitterConnectorSettings();
            Assert.NotEmpty(tcs.CredentialsPath);
        }
    }
}