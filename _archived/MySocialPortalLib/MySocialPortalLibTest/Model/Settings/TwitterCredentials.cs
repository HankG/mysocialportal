using System;
using System.Text.Json;
using MySocialPortalLib.Service.SocialMediaConnectors;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MySocialPortalLibTest.Model.Settings
{
    public class TwitterCredentialsTest
    {
        [Fact]
        public void TestConstruction()
        {
            var tc = new TwitterCredentials();
            Assert.True(string.IsNullOrEmpty(tc.AccessToken));
            Assert.True(string.IsNullOrEmpty(tc.ConsumerKey));
            Assert.True(string.IsNullOrEmpty(tc.ConsumerSecret));
            Assert.True(string.IsNullOrEmpty(tc.AccessTokenSecret));
        }

        [Fact]
        public void TestSerialization()
        {
            var tc = new TwitterCredentials
            {
                AccessToken = Guid.NewGuid().ToString(),
                AccessTokenSecret = Guid.NewGuid().ToString(),
                ConsumerKey = Guid.NewGuid().ToString(),
                ConsumerSecret = Guid.NewGuid().ToString()
            };
            var json = JsonSerializer.Serialize(tc, new JsonSerializerOptions{WriteIndented = true});
            var tc2 = JsonSerializer.Deserialize<TwitterCredentials>(json);
            Assert.Equal(tc.AccessToken, tc2.AccessToken);
            Assert.Equal(tc.AccessTokenSecret, tc2.AccessTokenSecret);
            Assert.Equal(tc.ConsumerKey, tc2.ConsumerKey);
            Assert.Equal(tc.ConsumerSecret, tc2.ConsumerSecret);

        }
    }
}