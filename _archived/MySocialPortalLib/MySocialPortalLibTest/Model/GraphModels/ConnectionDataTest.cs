using System;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model.GraphModels;
using Xunit;

namespace MySocialPortalLibTest.Model.GraphModels
{
    public class ConnectionDataTest
    {
      
        [Fact]
        public void TestDefault()
        {
            var cd = new ConnectionData();
            Assert.NotNull(cd);
            Assert.NotNull(cd.AssociationAttributes);
            Assert.Empty(cd.AssociationAttributes);
            Assert.NotNull(cd.NetworkName);
        }

        [Fact]
        public void TesHasConnection()
        {
            var cd = new ConnectionData();
            Assert.False(cd.HasConnection);
        }

        [Fact]
        public void TestEmptySerialization()
        {
            var cd = new ConnectionData();
            var json = JsonSerializer.Serialize(cd);
            var cd2 = JsonSerializer.Deserialize<ConnectionData>(json);
            Assert.True(cd.AllEquals(cd2));
        }

        [Fact]
        public void TestFullSerialization()
        {
            var cd = new ConnectionData
            {
                NetworkName = Guid.NewGuid().ToString(),
                AssociationAttributes = new List<string>
                {
                    Guid.NewGuid().ToString(),
                    Guid.NewGuid().ToString(),
                    Guid.NewGuid().ToString()
                }
            };
            var json = JsonSerializer.Serialize(cd);
            var cd2 = JsonSerializer.Deserialize<ConnectionData>(json);
            Assert.True(cd.AllEquals(cd2));

        }
    }
}