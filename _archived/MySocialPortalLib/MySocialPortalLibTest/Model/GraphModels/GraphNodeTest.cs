using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using MySocialPortalLib.Model.GraphModels;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Model.GraphModels
{
    public class GraphNodeTest
    {
        private ITestOutputHelper _output;

        public GraphNodeTest(ITestOutputHelper output)
        {
            _output = output;
        }
        
        [Fact]
        public void TestDefaultConstruction()
        {
            var node = new GraphNode();
            Assert.NotNull(node);
            Assert.Equal("", node.Id);
            Assert.Empty(node.NeighborIds);
        }

        [Fact]
        public void TestEmptySerialization()
        {
            var node = new GraphNode();
            var json = JsonSerializer.Serialize(node, new JsonSerializerOptions{WriteIndented = true});
            var node2 = JsonSerializer.Deserialize<GraphNode>(json);
            Assert.True(node.AllEquals(node2));
        }

        [Fact]
        public void TestFullSerialization()
        {
            var node = GraphTestUtils.BuildStandardNode();
            var json = JsonSerializer.Serialize(node, new JsonSerializerOptions{WriteIndented = true});
            var node2 = JsonSerializer.Deserialize<GraphNode>(json);
            Assert.True(node.AllEquals(node2));
        }

        [Fact]
        public void TestUndirectedConnectionSerialization()
        {
            var n1 = new GraphNode("node1");
            var n2 = new GraphNode("node2");
            var nodes = new List<GraphNode> {n1, n2};
            var cd = new ConnectionData();
            n1.AddOrUpdateNeighbor(n2, cd);
            n2.AddOrUpdateNeighbor(n1, cd);
            JsonSerializer.Serialize(nodes, new JsonSerializerOptions {WriteIndented = true});
        }

        [Fact]
        public void TestAddingConnectionData()
        {
            var node = GraphTestUtils.BuildStandardNode();
            var connectionData = GraphTestUtils.BuildConnectionData();
            var newConnection = new GraphNode(Guid.NewGuid().ToString());
            var originalCount = node.NeighborIds.Count;
            node.AddOrUpdateNeighbor(newConnection, connectionData);
            Assert.Contains(newConnection.Id, node.NeighborIds);
            Assert.Equal(originalCount + 1, node.NeighborIds.Count);
            Assert.Equal(connectionData, node.GetConnectionData(newConnection, connectionData.NetworkName));
            Assert.Single(node.GetConnectionData(newConnection));
            Assert.Equal(node.NeighborIds.Count, node.NeighborConnectionData.Count);
        }

        [Fact]
        public void TestUpdatingConnectionData()
        {
            var node = GraphTestUtils.BuildStandardNode();
            var firstNode = node.NeighborIds.First();
            var firstNetworkName = node.NeighborConnectionData[firstNode].Keys.First();
            var connectionData = GraphTestUtils.BuildConnectionData();
            connectionData.NetworkName = firstNetworkName;
            Assert.NotEqual(connectionData, node.GetConnectionData(firstNode, firstNetworkName));
            node.AddOrUpdateNeighbor(firstNode, connectionData);
            Assert.Equal(connectionData, node.GetConnectionData(firstNode, firstNetworkName));
            Assert.Equal(node.NeighborIds.Count, node.NeighborConnectionData.Count);
        }

        [Fact]
        public void TestRemovingConnectionDataRemovesConnection()
        {
            var node = GraphTestUtils.BuildStandardNode();
            var connectionData = GraphTestUtils.BuildConnectionData();
            var newConnection = new GraphNode(Guid.NewGuid().ToString());
            var originalCount = node.NeighborIds.Count;
            node.AddOrUpdateNeighbor(newConnection, connectionData);
            Assert.Contains(newConnection.Id, node.NeighborIds);
            Assert.Equal(originalCount + 1, node.NeighborIds.Count);
            node.RemoveNetworkConnection(newConnection, connectionData.NetworkName);
            Assert.Equal(originalCount, node.NeighborIds.Count);
            Assert.Equal(node.NeighborIds.Count, node.NeighborConnectionData.Count);
        }

        [Fact]
        public void TestRemovingConnection()
        {
            var node = GraphTestUtils.BuildStandardNode();
            var nodeCount = node.NeighborIds.Count;
            var firstNode = node.NeighborIds.First();
            Assert.True(node.DeleteNeighbor(firstNode));
            Assert.Equal(nodeCount - 1, node.NeighborIds.Count);
            Assert.Equal(node.NeighborIds.Count, node.NeighborConnectionData.Count);
        }

    }
}