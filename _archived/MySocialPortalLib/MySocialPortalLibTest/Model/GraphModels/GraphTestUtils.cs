using System;
using System.Collections.Generic;
using MySocialPortalLib.Model.GraphModels;

namespace MySocialPortalLibTest.Model.GraphModels
{
    public class GraphTestUtils
    {
        public static readonly List<string> ConnectionAttributes = new List<string> {"Following", "Friend", "Family"};
        public static readonly List<string> NetworkNames = new List<string> {"Network1", "Network2", "Network3"};

        public static GraphNode BuildStandardNode(int count = 0)
        {
            var random = new Random();
            var connectionCount = count == 0 ? random.Next(1, 10) : count;
            var node = new GraphNode(Guid.NewGuid().ToString());
            for (var i = 0; i < connectionCount; i++)
            {
                var newNode = new GraphNode(Guid.NewGuid().ToString());
                node.AddOrUpdateNeighbor(newNode, BuildConnectionData());
            }

            return node;
        }
        
        public static ConnectionData BuildConnectionData()
        {
            var random = new Random();
            var name = NetworkNames[random.Next(NetworkNames.Count)];
            var cd =  new ConnectionData { NetworkName = name};
            cd.AssociationAttributes.Add(ConnectionAttributes[random.Next(ConnectionAttributes.Count)]);
            
            return cd;
        }

    }
}