using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using MySocialPortalLib.Model.GraphModels;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Model.GraphModels
{
    public class SocialGraphManagerTest
    {
        private ITestOutputHelper _output;

        public SocialGraphManagerTest(ITestOutputHelper output)
        {
            _output = output;
        }
        
        [Fact]
        public void TestDefaultInitialization()
        {
            var sgm = new SocialGraphManager();
            Assert.NotNull(sgm);
            Assert.NotNull(sgm.RawGraph);
            Assert.Empty(sgm.RawGraph.Nodes);
        }

        [Fact]
        public void TestEmptySerialization()
        {
            var sgm = new SocialGraphManager();
            var json = JsonSerializer.Serialize(sgm);
            var sgm2 = JsonSerializer.Deserialize<SocialGraphManager>(json);
            Assert.True(sgm.AllEquals(sgm2));
        }

        [Fact]
        public void TestFullSerialization()
        {
            var sgm = BuildFullSocialGraph();
            _output.WriteLine(sgm.ToCompactString());
            var json = JsonSerializer.Serialize(sgm);
            var sgm2 = JsonSerializer.Deserialize<SocialGraphManager>(json);
            Assert.True(sgm.AllEquals(sgm2));
        }
        
        [Fact]
        public void TestAddInitialConnection()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network = "Network1";
            var attribute = "Connected";
            sgm.AddNetworkConnection(user1, user2, network, attribute);
            Assert.NotEmpty(sgm.RawGraph.Nodes);
            Assert.Equal(user1, sgm.RawGraph.Nodes.Values.First().Id);
            Assert.Equal(user2, sgm.RawGraph.Nodes.Values.First().NeighborIds.First());
            _output.WriteLine(sgm.ToCompactString());
        }
        
        [Fact]
        public void TestAddTwoNetworkConnections()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network1 = "Network1";
            var network2 = "Network2";
            var attribute1 = "Connected1";
            var attribute2 = "Connected2";
            sgm.AddNetworkConnection(user1, user2, network1, attribute1);
            _output.WriteLine(sgm.ToCompactString());
            Assert.Equal(2, sgm.RawGraph.Nodes.Count);
            var updatedNode = sgm.RawGraph.Nodes.Values.First();
            Assert.Equal(user1, updatedNode.Id);
            Assert.Single(updatedNode.NeighborIds);
            Assert.Single(updatedNode.NeighborConnectionData);
            Assert.Equal(user2, updatedNode.NeighborIds.First());
            sgm.AddNetworkConnection(user1, user2, network2, attribute2);
            _output.WriteLine(sgm.ToCompactString());
            Assert.Equal(2, sgm.RawGraph.Nodes.Count);
            updatedNode = sgm.RawGraph.Nodes.Values.First();
            Assert.Equal(user1, updatedNode.Id);
            Assert.Single(updatedNode.NeighborIds);
            Assert.Single(updatedNode.NeighborConnectionData);
            Assert.Equal(user2, updatedNode.NeighborIds.First());
            var networkData = updatedNode.NeighborConnectionData[user2];
            Assert.Equal(2, networkData.Count);
            Assert.Contains(network1, networkData.Keys);
            Assert.Contains(network2, networkData.Keys);
            Assert.Contains(attribute1, networkData[network1].AssociationAttributes);
            Assert.Contains(attribute2, networkData[network2].AssociationAttributes);
        }


        [Fact]
        public void TestAddDuplicateConnectionDoesntDuplicate()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network = "Network1";
            var attribute = "Connected";
            sgm.AddNetworkConnection(user1, user2, network, attribute);
            Assert.NotEmpty(sgm.RawGraph.Nodes);
            Assert.Equal(user1, sgm.RawGraph.Nodes.Values.First().Id);
            Assert.Equal(user2, sgm.RawGraph.Nodes.Values.First().NeighborIds.First());
            _output.WriteLine(sgm.ToCompactString());
            sgm.AddNetworkConnection(user1, user2, network, attribute);
            var updatedNode = sgm.RawGraph.Nodes.Values.First();
            Assert.Single(updatedNode.NeighborIds);
            Assert.Single(updatedNode.NeighborConnectionData);
            Assert.Single(updatedNode.NeighborConnectionData.First().Value);
            Assert.Single(updatedNode.NeighborConnectionData.First().Value.Values);
        }

        [Fact]
        public void TestAddAdditionalConnectionData()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network = "Network1";
            var attribute = "Connected";
            sgm.AddNetworkConnection(user1, user2, network, attribute);
            var newAttributes = new List<string>(sgm.GetConnectedNetworkAttributes(user1, user2, network).AssociationAttributes);
            newAttributes.Add("Friends");
            Assert.True(sgm.UpdateConnectedNetworkAttributes(user1, user2, network, newAttributes));
            var updatedAttributes = sgm.GetConnectedNetworkAttributes(user1, user2, network);
            Assert.Equal(2,updatedAttributes.AssociationAttributes.Count);
            Assert.True(newAttributes.All(updatedAttributes.AssociationAttributes.Contains));
        }

        [Fact]
        public void TestPruneConnectionData()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network = "Network1";
            var initialAttributes = new List<string> {"Following", "Friend", "Work"};
            sgm.AddNetworkConnection(user1, user2, network, initialAttributes);
            var prunedAttributes = new List<string>(sgm.GetConnectedNetworkAttributes(user1, user2, network).AssociationAttributes);
            prunedAttributes.Remove("Work");
            Assert.True(sgm.UpdateConnectedNetworkAttributes(user1, user2, network, prunedAttributes, false));
            var updatedAttributes = sgm.GetConnectedNetworkAttributes(user1, user2, network);
            Assert.Equal(2,updatedAttributes.AssociationAttributes.Count);
        }

        [Fact]
        public void TestRemoveOneOfManyNetworkConnection()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network = "Network1";
            var attribute = "Connected";
            sgm.AddNetworkConnection(user1, user2, Guid.NewGuid().ToString(), attribute);
            sgm.AddNetworkConnection(user1, user2, Guid.NewGuid().ToString(), attribute);
            sgm.AddNetworkConnection(user1, user2, network, attribute);
            sgm.AddNetworkConnection(user1, user2, Guid.NewGuid().ToString(), attribute);
            _output.WriteLine(sgm.ToCompactString());
            var networks = sgm.GetConnectionNetworks(user1, user2);
            Assert.Contains(network, networks);
            Assert.Equal(4, networks.Count);
            sgm.RemoveNetworkConnection(user1, user2, network);
            networks = sgm.GetConnectionNetworks(user1, user2);
            Assert.DoesNotContain(network, networks);
            Assert.Equal(3, networks.Count);
        }

        [Fact]
        public void TestGetConnectedNetworks()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network1 = "Network1";
            var network2 = "Network2";
            var attribute = "Connected";
            sgm.AddNetworkConnection(user1, user2, network1, attribute);
            sgm.AddNetworkConnection(user1, user2, network2, attribute);
            _output.WriteLine(sgm.ToCompactString());
            var networks = sgm.GetConnectionNetworks(user1, user2);
            Assert.Equal(2, networks.Count);
            Assert.Contains(network1, networks);
            Assert.Contains(network2, networks);
        }

        [Fact]
        public void TestGetConnectedNetworksBadInputs()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network1 = "Network1";
            var attribute = "Connected";
            sgm.AddNetworkConnection(user1, user2, network1, attribute);
            Assert.Empty(sgm.GetConnectionNetworks(user2, user1));
            Assert.Empty(sgm.GetConnectionNetworks(user1, Guid.NewGuid().ToString()));
            Assert.Empty(sgm.GetConnectionNetworks(Guid.NewGuid().ToString(), user2));
        }

        [Fact]
        public void TestRemoveConnection()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network1 = "Network1";
            var attribute = "Connected";
            sgm.AddNetworkConnection(user1, user2, network1, attribute);
            sgm.RemoveConnection(user1, user2);
            Assert.False(sgm.IsReachable(user1, user2));
            Assert.Empty(sgm.GetConnectionNetworks(user1, user2));
        }
        
        [Fact]
        public void TestIncrementallyRemoveAllConnections()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var network1 = "Network1";
            var network2 = "Network2";
            var attribute = "Connected";
            sgm.AddNetworkConnection(user1, user2, network1, attribute);
            sgm.AddNetworkConnection(user1, user2, network2, attribute);
            Assert.True(sgm.IsReachable(user1, user2));
            _output.WriteLine(sgm.ToCompactString());
            var networks = sgm.GetConnectionNetworks(user1, user2);
            Assert.Equal(2, networks.Count);
            Assert.Contains(network1, networks);
            Assert.Contains(network2, networks);
            sgm.RemoveNetworkConnection(user1, user2, network1);
            networks = sgm.GetConnectionNetworks(user1, user2);
            Assert.Equal(1, networks.Count);
            Assert.DoesNotContain(network1, networks);
            Assert.Contains(network2, networks);
            Assert.True(sgm.IsReachable(user1, user2));

            //removing same network already removed changes nothing
            sgm.RemoveNetworkConnection(user1, user2, network1);
            networks = sgm.GetConnectionNetworks(user1, user2);
            Assert.Equal(1, networks.Count);
            Assert.DoesNotContain(network1, networks);
            Assert.Contains(network2, networks);
            Assert.True(sgm.IsReachable(user1, user2));
            
            //Cleared out network removes all connections
            sgm.RemoveNetworkConnection(user1, user2, network2);
            networks = sgm.GetConnectionNetworks(user1, user2);
            Assert.Empty(networks);
            Assert.False(sgm.IsReachable(user1, user2));
        }

        [Fact]
        public void TestIsReachable()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var user3 = "user3";
            var network1 = "Network1";
            var network2 = "Network2";
            var attribute = "Connected";
            sgm.AddNetworkConnection(user1, user2, network1, attribute);
            sgm.AddNetworkConnection(user2, user1, network1, attribute);
            sgm.AddNetworkConnection(user3, user1, network2, attribute);
            _output.WriteLine(sgm.ToCompactString());
            Assert.True(sgm.IsReachable(user1, user2));
            Assert.True(sgm.IsReachable(user2, user1));
            Assert.True(sgm.IsReachable(user3, user1));
            Assert.True(sgm.IsReachable(user3, user2));
            Assert.False(sgm.IsReachable(user1, user3));
            Assert.True(sgm.IsReachable(user3, user2));
            Assert.False(sgm.IsReachable(user1, "user4"));
            Assert.False(sgm.IsReachable("user4", user2));
        }

        private SocialGraphManager BuildFullSocialGraph()
        {
            var sgm = new SocialGraphManager();
            var user1 = "user1";
            var user2 = "user2";
            var user3 = "user3";
            var user4 = "user4";
            sgm.AddNetworkConnection(user1, user2, GraphTestUtils.NetworkNames[0], GraphTestUtils.ConnectionAttributes);
            sgm.AddNetworkConnection(user2, user1, GraphTestUtils.NetworkNames[0], GraphTestUtils.ConnectionAttributes[0]);
            sgm.AddNetworkConnection(user2, user1, GraphTestUtils.NetworkNames[1], GraphTestUtils.ConnectionAttributes[0]);
            sgm.AddNetworkConnection(user3, user2, GraphTestUtils.NetworkNames[0], GraphTestUtils.ConnectionAttributes[2]);
            sgm.AddNetworkConnection(user3, user4, GraphTestUtils.NetworkNames[2], GraphTestUtils.ConnectionAttributes[2]);
            _output.WriteLine(sgm.ToCompactString());
            return sgm;
        }

    }
}