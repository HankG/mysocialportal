using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using Xunit;
using MySocialPortalLib.Model.GraphModels;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Model.GraphModels
{
    public class GraphTest
    {
        private ITestOutputHelper _output;

        public GraphTest(ITestOutputHelper output)
        {
            _output = output;
        }
        
        [Fact]
        public void TestDefaultInitialization()
        {
            var graph = new Graph();
            Assert.NotNull(graph);
            Assert.Empty(graph.Nodes);
        }

        [Fact]
        public void TestFullInitialization()
        {
            var nodes = new Dictionary<string, GraphNode>();
            var graph = new Graph(nodes);
            Assert.Equal(nodes, graph.Nodes);
        }

        [Fact]
        public void TestEmptySerialization()
        {
        }

        [Fact]
        public void TestFullSerialization()
        {
            var graph = new Graph();
            var n1 = graph.AddNode(Guid.NewGuid().ToString());
            var n2 = graph.AddNode(Guid.NewGuid().ToString());
            var n3 = graph.AddNode(Guid.NewGuid().ToString());
            graph.AddNode(Guid.NewGuid().ToString());
            Graph.AddUndirectedEdge(n1, n2, GraphTestUtils.BuildConnectionData());
            Graph.AddDirectedEdge(n2, n3, GraphTestUtils.BuildConnectionData());
            var json = JsonSerializer.Serialize(graph, new JsonSerializerOptions{WriteIndented = true});
            var graph2 = JsonSerializer.Deserialize<Graph>(json);
            Assert.True(graph.AllEquals(graph2));
        }

        [Fact]
        public void TestAddingNode()
        {
            var graph = new Graph();
            var n1 = graph.AddNode(Guid.NewGuid().ToString());
            Assert.Equal(n1, graph.Nodes[n1.Id]);
        }

        [Fact]
        public void TestAddingDirectedConnection()
        {
            var graph = new Graph();
            var n1 = graph.AddNode(Guid.NewGuid().ToString());
            var n2 = graph.AddNode(Guid.NewGuid().ToString());
            var cd = GraphTestUtils.BuildConnectionData();
            Graph.AddDirectedEdge(n1, n2, cd);
            Assert.Contains(n2.Id, n1.NeighborIds);
            Assert.Equal(cd, n1.NeighborConnectionData[n2.Id].First().Value);
        }
        
        [Fact]
        public void TestAddingUnDirectedConnection()
        {
            var graph = new Graph();
            var n1 = graph.AddNode(Guid.NewGuid().ToString());
            var n2 = graph.AddNode(Guid.NewGuid().ToString());
            var cd = GraphTestUtils.BuildConnectionData();
            Graph.AddUndirectedEdge(n1, n2, cd);
            Assert.Contains(n2.Id, n1.NeighborIds);
            Assert.Equal(cd, n1.NeighborConnectionData[n2.Id].First().Value);
            Assert.Contains(n1.Id, n2.NeighborIds);
            Assert.Equal(cd, n2.NeighborConnectionData[n1.Id].First().Value);
        }

        [Fact]
        public void TestRemovingNode()
        {
            var graph = new Graph();
            var n1 = graph.AddNode(Guid.NewGuid().ToString());
            var n2 = graph.AddNode(Guid.NewGuid().ToString());
            var n3 = graph.AddNode(Guid.NewGuid().ToString());
            var cd12 = GraphTestUtils.BuildConnectionData();
            var cd23 = GraphTestUtils.BuildConnectionData();
            var cd13 = GraphTestUtils.BuildConnectionData();
            Graph.AddUndirectedEdge(n1, n2, cd12);
            Graph.AddUndirectedEdge(n2, n3, cd23);
            Graph.AddUndirectedEdge(n1, n3, cd13);
            Assert.Contains(n2.Id, n1.NeighborIds);
            Assert.Contains(n3.Id, n1.NeighborIds);
            Assert.Contains(n1.Id, n2.NeighborIds);
            Assert.Contains(n3.Id, n2.NeighborIds);
            Assert.Contains(n2.Id, n3.NeighborIds);
            Assert.Contains(n1.Id, n3.NeighborIds);
            graph.Remove(n2);
            Assert.DoesNotContain(n2, graph.Nodes.Values);
            Assert.Contains(n3.Id, n1.NeighborIds);
            Assert.Contains(n1.Id, n3.NeighborIds);
            Assert.Empty(n2.NeighborIds);
            Assert.Empty(n2.NeighborConnectionData);
        }

        [Fact]
        public void TestReachability()
        {
            var graph = new Graph();
            var n1 = graph.AddNode(Guid.NewGuid().ToString());
            var n2 = graph.AddNode(Guid.NewGuid().ToString());
            var n3 = graph.AddNode(Guid.NewGuid().ToString());
            var n4 = graph.AddNode(Guid.NewGuid().ToString());
            Graph.AddUndirectedEdge(n1, n2, new ConnectionData());
            Graph.AddUndirectedEdge(n2, n3, new ConnectionData());
            Graph.AddDirectedEdge(n4, n3, new ConnectionData());
            Assert.True(graph.IsReachable(n1, n2));
            Assert.True(graph.IsReachable(n2, n1));
            Assert.True(graph.IsReachable(n1, n3));
            Assert.True(graph.IsReachable(n3, n1));
            Assert.True(graph.IsReachable(n2, n3));
            Assert.True(graph.IsReachable(n3, n2));
            Assert.True(graph.IsReachable(n4, n3));
            Assert.True(graph.IsReachable(n4, n2));
            Assert.True(graph.IsReachable(n4, n1));
            Assert.False(graph.IsReachable(n1, n4));
            Assert.False(graph.IsReachable(n2, n4));
            Assert.False(graph.IsReachable(n3, n4));
        }
    }
}