using System;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class TimelineIntervalTest
    {
        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(new TimelineInterval());
        }

        [Fact]
        public void TestSerialization()
        {
            var interval = new TimelineInterval
            {
                IntervalStart = 100,
                IntervalStop = 200
            };
            
            var json = JsonSerializer.Serialize(interval);
            var interval2 = JsonSerializer.Deserialize<TimelineInterval>(json);
            Assert.True(interval.AllEquals(interval2));
        }
    }
}