using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Model
{
    public class EventTest
    {
        [Theory]
        [ClassData(typeof(EventDataGenerator))]
        public void TestSerialization(EventData e)
        {
            var json = JsonSerializer.Serialize(e, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<EventData>(json);
            Assert.Equal(e, fromJson);
            Assert.True(e.AllEquals(fromJson));
        }
    }

    public class EventDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new EventData()},
            new object[] {
                new EventData{
                    Place = new Place(),
                    Title = "Title 1",
                    StartTime = DateTimeOffset.UnixEpoch.AddDays(100),
                    StopTime = DateTimeOffset.UnixEpoch.AddDays(110)
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}