using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class CommentTest
    {
        [Theory]
        [ClassData(typeof(CommentDataGenerator))]
        public void TestSerialization(Comment comment)
        {
            var json = JsonSerializer.Serialize(comment, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<Comment>(json);
            Assert.Equal(comment, fromJson);
            Assert.True(comment.AllEquals(fromJson));
        }
    }
    
    public class CommentDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new Comment(), },
            new object[] {
                new Comment(){
                    Body = "Comment body",
                    Id = "12345",
                    Interactions = new List<Interaction>{new Interaction()},
                    Links = new List<ExternalLink>{new ExternalLink()},
                    Media = new List<MediaData>{new MediaData()},
                    Tags = new List<string>{"tag1"},
                    ParentComment = Guid.NewGuid().ToString(),
                    ParentPost = Guid.NewGuid().ToString(),
                    PostDateTime = DateTimeOffset.Now,
                    TaggedUsers = new List<string>{"user1", "user2"},
                    OriginalLinkUrlOrId = "original link",
                    OriginalSocialMediaSystem = "socialmediathing"
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

}