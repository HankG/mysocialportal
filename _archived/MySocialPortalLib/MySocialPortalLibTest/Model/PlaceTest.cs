using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class PlaceTest
    {
        [Fact]
        public void TestSetLatLonGood()
        {
            var lat = 20;
            var lon = 30;
            var place = new Place
            {
                Latitude = lat,
                Longitude = lon
            };

            Assert.Equal(lat, place.Latitude, 15);
            Assert.Equal(lat, place.Latitude, 15);
        }

        [Theory]
        [InlineData(-91,0)]
        [InlineData(91,0)]
        [InlineData(0,181)]
        [InlineData(0,-181)]
        public void TestSetLatLonException(double lat, double lon)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Place { Latitude = lat, Longitude = lon});
        }

        [Theory]
        [ClassData(typeof(PlaceDataGenerator))]
        public void TestSerialization(Place p)
        {
            var json = JsonSerializer.Serialize(p, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<Place>(json);
            Assert.Equal(p, fromJson);
            Assert.True(p.AllEquals(fromJson));
        }
    }
    
    public class PlaceDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new Place()},
            new object[] {
                new Place{
                    Name = "PlaceName",
                    Url = "https://someplace.com/index.php?k1=v1",
                    PhysicalAddress = "123 Some Street, Some City, Some Province, Some Country, 1235-56789",
                    Latitude = 10,
                    Longitude = 20,
                    Altitude = 30,
                    HasLatLon = true
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

}