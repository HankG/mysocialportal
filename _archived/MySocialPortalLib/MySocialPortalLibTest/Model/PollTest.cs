using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;
using Xunit.Abstractions;

namespace MySocialPortalLibTest.Model
{
    public class PollTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public PollTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Theory]
        [ClassData(typeof(PollDataGenerator))]
        public void TestSerialization(Poll p)
        {
            var json = JsonSerializer.Serialize(p, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<Poll>(json);
            Assert.Equal(p, fromJson);
            Assert.True(p.AllEquals(fromJson));
        }

    }
    
    public class PollDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new Poll(), },
            new object[] {
                new Poll(){
                    Options = new List<PollOption>
                    {
                        new PollOption("Option1", false), 
                        new PollOption("Option2", true)
                    },
                    Question = "Poll question?"
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}