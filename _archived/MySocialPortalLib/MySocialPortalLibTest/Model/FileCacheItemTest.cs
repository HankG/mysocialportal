using System;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class FileCacheItemTest
    {
        [Fact]
        public void TestConstruction()
        {
            Assert.NotNull(new FileCacheItem());
        }

        [Fact]
        public void TestSerialization()
        {
            var item = new FileCacheItem
            {
                LocalFileName = "local-file-name.jpg",
                CreationTime = DateTimeOffset.Now,
                OriginalUrl = "https://localhos/profile"
            };
            var json = JsonSerializer.Serialize(item);
            var item2 = JsonSerializer.Deserialize<FileCacheItem>(json);
            Assert.True(item.AllEquals(item2));
        }
    }
}