using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using MySocialPortalLib.Model;
using Xunit;

namespace MySocialPortalLibTest.Model
{
    public class ExternalLinkTest
    {
        [Theory]
        [ClassData(typeof(ExternalLinkDataGenerator))]
        public void TestSerialization(ExternalLink e)
        {
            var json = JsonSerializer.Serialize(e, new JsonSerializerOptions{WriteIndented = true});
            var fromJson = JsonSerializer.Deserialize<ExternalLink>(json);
            Assert.Equal(e, fromJson);
            Assert.True(e.AllEquals(fromJson));
        }
    }
    
    public class ExternalLinkDataGenerator : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] {new ExternalLink(), },
            new object[] {
                new ExternalLink(){
                    Name = "LinkName",
                    Source = "Some blog",
                    Url = "https://thelink.com/index.html",
                    PostId = "12345"
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

}