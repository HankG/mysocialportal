using System.Collections.Generic;

namespace MySocialPortalLib.Repository
{
    public interface INamedListRepository
    {
        void Add(string id, string listName);
        
        void RemoveIdFromList(string id, string listName);
        
        void RemoveList(string listName);

        void RemoveId(string id);
        
        IList<string> GetAllLists();
        
        IList<string> GetAllListsForId(string id);
        
        IList<string> GetAllIdsForList(string list);
    }
}