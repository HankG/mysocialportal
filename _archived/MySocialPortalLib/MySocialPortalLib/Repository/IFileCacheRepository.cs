using System.Diagnostics.CodeAnalysis;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Repository
{
    [SuppressMessage("ReSharper", "CA1054")]
    public interface IFileCacheRepository
    {
        void AddOrUpdate(FileCacheItem item);

        FileCacheItem? FindById(string id);

        FileCacheItem? FindByUrl(string url);

        bool RemoveById(string id);

        bool Remove(FileCacheItem item);
        
        long Count();
    }
}