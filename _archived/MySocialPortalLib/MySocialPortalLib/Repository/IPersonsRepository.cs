using System.Collections.Generic;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Repository
{
    public interface IPersonsRepository
    {
        void AddPerson(Person person);

        Person FindById(string id);

        Person FindByNetworkId(string networkName, string networkId);

        IList<Person> ListAll();

        void UpdatePerson(Person person);
    }
}