using System;
using System.Collections.Generic;
using System.Linq;
using LiteDB;

namespace MySocialPortalLib.Repository
{
    public class NamedListLiteDbRepository : INamedListRepository
    {
        private const string DefaultCollectionName = "NamedLists";
        
        private NamedListLiteDbRepository()
        {
            throw new MethodAccessException();
        }
        
        public NamedListLiteDbRepository(LiteRepository repository, string collectionName = DefaultCollectionName)
        {
            Repository = repository;
            CollectionName = collectionName;
        }
        
        public void Add(string id, string listName)
        {
            var entry = new Entry
            {
                ListName = listName,
                MemberId = id
            };
            if (Repository.Query<Entry>(CollectionName)
                    .Where(e => e.ListName == listName && e.MemberId == id)
                    .Count() == 0)
            {
                Repository.Insert(entry, CollectionName); 
            }
        }
        
        public IList<string> GetAllLists()
        {
            return Repository.Database.Execute($"SELECT DISTINCT(*.ListName) From {CollectionName}")
                .Current["expr"]
                .AsArray
                .Select(e => e.AsString)
                .ToList();
        }
        
        public IList<string> GetAllListsForId(string id)
        {
            return Repository.Query<Entry>(CollectionName)
                .Where(e => e.MemberId == id)
                .ToList()
                .Select(e => e.ListName)
                .ToList();
        }

        public IList<string> GetAllIdsForList(string list)
        {
            return Repository.Query<Entry>(CollectionName)
                .Where(e => e.ListName == list)
                .ToList()
                .Select(e => e.MemberId)
                .ToList();
        }
        
        
        public void RemoveIdFromList(string id, string listName)
        {
            Repository.DeleteMany<Entry>(e => e.MemberId == id && e.ListName == listName, CollectionName);
        }

        
        public void RemoveList(string listName)
        {
            Repository.DeleteMany<Entry>(e => e.ListName == listName, CollectionName);
        }

        
        public void RemoveId(string id)
        {
            Repository.DeleteMany<Entry>(e => e.MemberId == id, CollectionName);
        }
        
        private LiteRepository Repository { get; }
        
        private string CollectionName { get; }

        private class Entry
        {
            public string Id { get; set; }
            
            public string ListName { get; set; }
            
            public string MemberId { get; set; }

            public Entry()
            {
                Id = Guid.NewGuid().ToString();
            }
        }
    }
}