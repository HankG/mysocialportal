using System.Diagnostics.CodeAnalysis;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Repository
{
    [SuppressMessage("ReSharper", "CA1054")]
    public interface ILinkPreviewRepository
    {
        bool AddOrUpdate(LinkPreview preview);

        LinkPreview FindById(string id);

        LinkPreview FindByUrl(string url);
        
        long Count();
    }
}