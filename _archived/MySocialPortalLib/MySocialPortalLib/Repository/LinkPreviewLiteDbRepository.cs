using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using LiteDB;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Repository
{
    [SuppressMessage("ReSharper", "CA1054")]
    public class LinkPreviewLiteDbRepository : ILinkPreviewRepository, IDisposable
    {
        private const string DefaultLinkPreviewCollectionName = "linkpreviews";
        
        private bool _disposed = false;
        
        private LinkPreviewLiteDbRepository()
        {
            throw new MethodAccessException();
        }

        public LinkPreviewLiteDbRepository (Stream dbStream, 
            string postsCollectionName = DefaultLinkPreviewCollectionName)
        {
            LinkPreviewRepository = new LiteRepository(dbStream);
            LinkPreviewCollectionName = postsCollectionName;
        }

        public LinkPreviewLiteDbRepository(String filepath, string postsCollectionName = DefaultLinkPreviewCollectionName)
        {
            LinkPreviewRepository = new LiteRepository(filepath);
            LinkPreviewCollectionName = postsCollectionName;
        }

        public bool AddOrUpdate(LinkPreview preview)
        {
            return LinkPreviewRepository.Upsert(preview, LinkPreviewCollectionName);
        }

        public LinkPreview FindById(string id)
        {
            return LinkPreviewRepository.Query<LinkPreview>(LinkPreviewCollectionName)
                .Where(lp => lp.Id == id)
                .FirstOrDefault();
        }

        public LinkPreview FindByUrl(string url)
        {
            return LinkPreviewRepository.Query<LinkPreview>(LinkPreviewCollectionName)
                .Where(lp => lp.RequestUrl == url)
                .FirstOrDefault();
        }

        public long Count()
        {
            return LinkPreviewRepository.Query<LinkPreview>(LinkPreviewCollectionName)
                .LongCount();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return; 
      
            if (disposing) 
            {
                LinkPreviewRepository?.Dispose();
            }
      
            _disposed = true;
        }
        
        private LiteRepository LinkPreviewRepository { get; }
        
        private string LinkPreviewCollectionName { get; }

    }
}