using System;
using System.Collections.Generic;
using System.IO;
using LiteDB;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Repository
{
    public class PersonsLiteDbRepository : IPersonsRepository, IDisposable
    {
        private const string DefaultPersonsCollectionName = "persons";
        private bool _disposed = false;

        private PersonsLiteDbRepository()
        {
            throw new MethodAccessException();
        }

        public PersonsLiteDbRepository (Stream dbStream, 
            string postsCollectionName = DefaultPersonsCollectionName)
        {
            PersonsRepository = new LiteRepository(dbStream);
            PersonsCollectionName = postsCollectionName;
        }

        public PersonsLiteDbRepository(String filepath, string postsCollectionName = DefaultPersonsCollectionName)
        {
            PersonsRepository = new LiteRepository(filepath);
            PersonsCollectionName = postsCollectionName;
        }
        
        public void AddPerson(Person person)
        {
            PersonsRepository.Insert(person, PersonsCollectionName);
        }

        public long Count()
        {
            return PersonsRepository.Query<Post>(PersonsCollectionName).LongCount();
        }

        public Person FindById(string id)
        {
            return PersonsRepository.Query<Person>(PersonsCollectionName)
                       .Where(p => p.Id == id)
                       .FirstOrDefault();


        }

        public Person FindByNetworkId(string networkName, string networkId)
        {
            return PersonsRepository.Query<Person>(PersonsCollectionName)
                .Where(p => p.SocialMediaAccounts[networkName].ProfileId == networkId)
                .FirstOrDefault();
        }

        public IList<Person> ListAll()
        {
            return PersonsRepository.Query<Person>(PersonsCollectionName)
                .ToList();
        }

        public void UpdatePerson(Person person)
        {
            PersonsRepository.Upsert(person, PersonsCollectionName);
        }
        
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return; 
      
            if (disposing) 
            {
                PersonsRepository?.Dispose();
            }
      
            _disposed = true;
        }
        
        private LiteRepository PersonsRepository { get; }
        
        private string PersonsCollectionName { get;  }

    }
}