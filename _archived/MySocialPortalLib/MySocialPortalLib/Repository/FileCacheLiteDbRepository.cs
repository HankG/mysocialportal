using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using LiteDB;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Repository
{
    [SuppressMessage("ReSharper", "CA1054")]
    public class FileCacheLiteDbRepository : IFileCacheRepository, IDisposable
    {
        private const string DefaultFileCacheCollectionName = "file_cache";
        private bool _disposed = false;
        
        private FileCacheLiteDbRepository()
        {
            throw new MemberAccessException();
        }

        public FileCacheLiteDbRepository (Stream dbStream, 
            string postsCollectionName = DefaultFileCacheCollectionName)
        {
            FileCacheRepository = new LiteRepository(dbStream);
            FileCacheCollectionName = postsCollectionName;
        }

        public FileCacheLiteDbRepository(String filepath, string postsCollectionName = DefaultFileCacheCollectionName)
        {
            FileCacheRepository = new LiteRepository(filepath);
            FileCacheCollectionName = postsCollectionName;
        }
         
        public void AddOrUpdate(FileCacheItem item)
        {
            FileCacheRepository.Upsert(item, FileCacheCollectionName);
        }

        public FileCacheItem? FindById(string id)
        {
            return FileCacheRepository.Query<FileCacheItem>(FileCacheCollectionName)
                .Where(f => f.Id == id)
                .FirstOrDefault();
        }

        public FileCacheItem? FindByUrl(string url)
        {
            return FileCacheRepository.Query<FileCacheItem>(FileCacheCollectionName)
                .Where(f => f.OriginalUrl == url)
                .FirstOrDefault();
        }
        
        public long Count()
        {
            return FileCacheRepository.Query<FileCacheItem>(FileCacheCollectionName)
                .LongCount();
        }
        
        public bool RemoveById(string id)
        {
            return !string.IsNullOrWhiteSpace(id) 
                   && FileCacheRepository.Delete<FileCacheItem>(id, FileCacheCollectionName);
        }

        public bool Remove(FileCacheItem item)
        {
            return item != null && RemoveById(item.Id);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                FileCacheRepository?.Dispose();
            }

            _disposed = true;
        }
        
        private LiteRepository FileCacheRepository { get; }
        
        private string FileCacheCollectionName { get; }
    }
}