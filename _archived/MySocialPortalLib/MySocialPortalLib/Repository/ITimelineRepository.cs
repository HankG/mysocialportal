using System;
using System.Collections.Generic;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Repository
{
    public interface ITimelineRepository
    {
        bool AddOrUpdate (TimelineInterval interval, string serviceName);

        bool AddOrUpdate(IEnumerable<TimelineInterval> intervals, string serviceName);

        TimelineInterval? FindById(string id, string serviceName);

        IList<TimelineInterval> FindByInterval(ulong startValue, ulong stopValue, string serviceName);

        IList<TimelineInterval> FindByTimelineName(string timelineName, string serviceName);

        bool Remove (TimelineInterval interval, string serviceName);

        bool RemoveById(string id, string serviceName);

        bool RemoveRange(IEnumerable<TimelineInterval> intervals, string serviceName);

        bool RemoveByTimelineName(string name, string serviceName);
        
        long Count(string serviceName);

        long CountByTimelineName(string timelineName, string serviceName);
    }
}