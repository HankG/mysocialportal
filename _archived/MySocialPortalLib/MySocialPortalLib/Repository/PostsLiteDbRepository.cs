using System;
using System.Collections.Generic;
using System.IO;
using LiteDB;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Repository
{
    public class PostsLiteDbRepository : IPostsRepository, IDisposable
    {
        public const int DefaultMaxPosts = 50;
        private const string DefaultPostsCollectionName = "posts";
        private readonly LiteRepository _postsRepository;
        private readonly string _postsCollectionName;
        private bool _disposed = false;

        private PostsLiteDbRepository()
        {
            throw new MethodAccessException();
        }

        public PostsLiteDbRepository (Stream dbStream, 
            string postsCollectionName = DefaultPostsCollectionName)
        {
            _postsRepository = new LiteRepository(dbStream);
            _postsCollectionName = postsCollectionName;
        }

        public PostsLiteDbRepository (string filepath, 
            string postsCollectionName = DefaultPostsCollectionName)
        {
            _postsRepository = new LiteRepository(filepath);
            _postsCollectionName = postsCollectionName;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return; 
      
            if (disposing) {
                _postsRepository?.Dispose();
            }
      
            _disposed = true;
        }


        public void AddOrUpdate(Post post)
        {
            _postsRepository.Upsert(post, _postsCollectionName);
        }

        public void AddPosts(IEnumerable<Post> posts)
        {
            _postsRepository.Insert(posts, _postsCollectionName);
        }

        public long Count()
        {
            return _postsRepository.Query<Post>(_postsCollectionName).LongCount();
        }

        public Post? GetById(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }
            
            return _postsRepository.Query<Post>(_postsCollectionName)
                .Where(p => p.Id == id)
                .FirstOrDefault();
        }

        public Post GetByOriginalNetworkId(string network, string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }
            
            return _postsRepository.Query<Post>(_postsCollectionName)
                .Where(p => p.OriginalLinkUrlOrId == id && p.OriginalSocialMediaSystem == network)
                .FirstOrDefault();
        }

        public IEnumerable<Post> GetPosts(int maxPosts = DefaultMaxPosts, string personId = "", string networkName = "")
        {
            return GetPosts(DateTimeOffset.MinValue, DateTimeOffset.MaxValue, maxPosts, true, personId, networkName);
        }

        public IEnumerable<Post> GetPosts(DateTimeOffset newestPostTime, int maxPosts = DefaultMaxPosts, bool inclusive = true, string personId = "", string networkName = "")
        {
            return GetPosts(DateTimeOffset.MinValue, newestPostTime, maxPosts, inclusive, personId, networkName);
        }

        public IEnumerable<Post> GetPosts(DateTimeOffset oldestPostTime, DateTimeOffset newestPostTime, int maxPosts = DefaultMaxPosts, bool inclusive = true,  string personId = "", string networkName = "")
        {
            var baseQuery = GetDefaultQuery();
            var query = inclusive ? baseQuery.Where(p => p.PostDateTime >= oldestPostTime && p.PostDateTime <= newestPostTime) 
                : baseQuery.Where(p => p.PostDateTime > oldestPostTime && p.PostDateTime < newestPostTime) ;

            if (!string.IsNullOrWhiteSpace(personId))
            {
                query = query.Where(p => p.UserId == personId);
            }

            if (!string.IsNullOrWhiteSpace(networkName))
            {
                query = query.Where(p => p.OriginalSocialMediaSystem == networkName);
            }
            
            if (query.ToList().Count > 0)
            {
                return query
                    .OrderByDescending(p => p.PostDateTime)
                    .Limit(maxPosts)
                    .ToEnumerable();
            }

            return query.ToEnumerable();
        }

        private ILiteQueryable<Post> GetDefaultQuery()
        {
            return _postsRepository.Query<Post>(_postsCollectionName);
        }
    }
}