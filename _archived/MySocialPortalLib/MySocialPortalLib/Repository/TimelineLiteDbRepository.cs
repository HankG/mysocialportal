using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LiteDB;
using MySocialPortalLib.Model;
using MySocialPortalLib.Util;

namespace MySocialPortalLib.Repository
{
    public class TimelineLiteDbRepository : ITimelineRepository, IDisposable
    {
        private const string DefaultServiceName = "MySocialPortalInternal";
        
        private bool _disposed = false;

        public TimelineLiteDbRepository (Stream dbStream)
        {
            TimelineRepository = new LiteRepository(dbStream);
        }

        public TimelineLiteDbRepository(String filepath)
        {
            TimelineRepository = new LiteRepository(filepath);
        }

        public bool AddOrUpdate(TimelineInterval interval, string serviceName = DefaultServiceName)
        {
            return TimelineRepository.Upsert(interval, serviceName);
        }

        public bool AddOrUpdate(IEnumerable<TimelineInterval> intervals, string serviceName = DefaultServiceName)
        {
            TimelineRepository.Upsert(intervals, serviceName);
            return true;
        }

        public TimelineInterval? FindById(string id, string serviceName = DefaultServiceName)
        {
            return TimelineRepository.Query<TimelineInterval>(serviceName)
                .Where(i => i.Id == id)
                .FirstOrDefault();
        }

        public IList<TimelineInterval> FindByInterval(ulong startValue, 
                                                      ulong stopValue, 
                                                      string serviceName = DefaultServiceName)
        {
            //BSON doesn't have ulong type so need to bring the whole collection back and do the LINQ query at client
            return TimelineRepository.Query<TimelineInterval>(serviceName).ToList()
                .Where(i => (i.IntervalStart >= startValue && i.IntervalStop <= stopValue)
                            || (i.IntervalStart < startValue && i.IntervalStop >= startValue && i.IntervalStop <= stopValue)
                            || (i.IntervalStop > stopValue && i.IntervalStart >= startValue && i.IntervalStart <= stopValue))
                .ToList();
        }

        public IList<TimelineInterval> FindByTimelineInterval(string timelineName, 
                                                              ulong startValue, 
                                                              ulong stopValue, 
                                                              string serviceName = DefaultServiceName)
        {
            //BSON doesn't have ulong type so need to bring the whole collection back and do the LINQ query at client
            return TimelineRepository.Query<TimelineInterval>(serviceName)
                .Where(i => i.TimelineName == timelineName).ToList()
                .Where(i => (i.IntervalStart >= startValue && i.IntervalStop <= stopValue)
                            || (i.IntervalStart < startValue && i.IntervalStop >= startValue && i.IntervalStop <= stopValue)
                            || (i.IntervalStop > stopValue && i.IntervalStart >= startValue && i.IntervalStart <= stopValue))
                .ToList();
        }

        public IList<TimelineInterval> FindByTimelineName(string timelineName, string serviceName = DefaultServiceName)
        {
            return TimelineRepository.Query<TimelineInterval>(serviceName)
                .Where(i => i.TimelineName == timelineName)
                .ToList();
        }

        public bool Remove(TimelineInterval interval, string serviceName = DefaultServiceName)
        {
            return interval != null && RemoveById(interval.Id, serviceName);
        }

        public bool RemoveById(string id, string serviceName = DefaultServiceName)
        {
            return !string.IsNullOrWhiteSpace(id) && TimelineRepository.Delete<TimelineInterval>(id, serviceName);
        }

        public bool RemoveByTimelineName(string name, string serviceName = DefaultServiceName)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return false;
            } 
            
            TimelineRepository.DeleteMany<TimelineInterval>(
                interval => interval.TimelineName == name, 
                serviceName);
            return FindByTimelineName(name).Count == 0;
        }

        public bool RemoveRange(IEnumerable<TimelineInterval> intervals, string serviceName = DefaultServiceName)
        {
            return intervals.Aggregate(true, (current, interval) => current & Remove(interval, serviceName));
        }

        public long Count(string serviceName = DefaultServiceName)
        {
            return TimelineRepository.Query<TimelineInterval>(serviceName)
                .LongCount();
        }

        public long CountByTimelineName(string timelineName, string serviceName = DefaultServiceName)
        {
            return TimelineRepository.Query<TimelineInterval>(serviceName)
                .Where(i => i.TimelineName == timelineName)
                .LongCount();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                TimelineRepository?.Dispose();
            }

            _disposed = true;
        }
                
        private LiteRepository TimelineRepository { get; }
    }
}