using System;
using System.Collections.Generic;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Repository
{
    public interface IPostsRepository
    {
        void AddOrUpdate(Post post);

        void AddPosts(IEnumerable<Post> posts);

        Post? GetById(string id);

        Post? GetByOriginalNetworkId(string network, string id);
        
        IEnumerable<Post> GetPosts(int maxPosts, string personId = "", string networkName = "");

        IEnumerable<Post> GetPosts(DateTimeOffset newestPostTime, int maxPosts, bool inclusive, string personId = "", string networkName = "");
        
        IEnumerable<Post> GetPosts(DateTimeOffset oldestPostTime, DateTimeOffset newestPostTime, int maxPosts, bool inclusive, string personId = "", string networkName = "");
        
    }
}