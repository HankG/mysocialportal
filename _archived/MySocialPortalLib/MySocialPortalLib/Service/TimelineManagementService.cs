using System;
using System.Collections.Generic;
using System.Linq;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Util;

namespace MySocialPortalLib.Service
{
    public class TimelineManagementService
    {
        private TimelineManagementService()
        {
            throw new MethodAccessException();
        }
        
        public string ServiceName { get; }

        public TimelineManagementService(ITimelineRepository repository, string serviceName)
        {
            TimelineRepository = repository;
            ServiceName = serviceName;
        }

        public TimelineInterval? GetNextSamplingInterval(string timelineName, ulong earliestValue, ulong forwardValue)
        {
            if (TimelineRepository.CountByTimelineName(timelineName, ServiceName) == 0)
            {
                var interval = new TimelineInterval
                {
                    IntervalStart = earliestValue,
                    IntervalStop = forwardValue,
                    TimelineName = timelineName
                };

                TimelineRepository.AddOrUpdate(interval, ServiceName);
                return interval;
            }

            var intervals = new List<TimelineInterval>(TimelineRepository.FindByTimelineName(timelineName, ServiceName));
            intervals.Sort((i1, i2) => -i1.IntervalStop.CompareTo(i2.IntervalStop));
            var interval2 = new TimelineInterval
            {
                IntervalStart = intervals[0].IntervalStop,
                IntervalStop = forwardValue,
                TimelineName = timelineName
            };

            TimelineRepository.AddOrUpdate(interval2, ServiceName);

            return interval2;
        }

        public TimelineInterval? GetPreviousSamplingInterval(string timelineName, ulong initialStart, ulong initialStop)
        {
            if (TimelineRepository.CountByTimelineName(timelineName, ServiceName) == 0)
            {
                var interval = new TimelineInterval
                {
                    IntervalStart = initialStart,
                    IntervalStop = initialStop,
                    TimelineName = timelineName
                };

                TimelineRepository.AddOrUpdate(interval, ServiceName);
                return interval;
            }

            var intervals = new List<TimelineInterval>(TimelineRepository.FindByTimelineName(timelineName, ServiceName));
            intervals.Sort((i1, i2) => i2.IntervalStop.CompareTo(i1.IntervalStop));

            if (intervals.Count == 1)
            {
                if (intervals[0].IntervalStart <= initialStart)
                {
                    return null;
                }

                var newInterval = new TimelineInterval
                {
                    IntervalStart = initialStart,
                    IntervalStop = intervals[0].IntervalStart,
                    TimelineName = timelineName
                };
                TimelineRepository.AddOrUpdate(newInterval, ServiceName);
                return newInterval;
            }

            var earliest = ulong.MaxValue;
            for (var i = 0; i < intervals.Count - 1; i++)
            {
                var interval1 = intervals[i];
                var interval2 = intervals[i + 1];
                if (interval1.IntervalStart < earliest)
                {
                    earliest = interval1.IntervalStart;
                }
                var gap = TimelineIntervalUtilities.Gap(interval1, interval2);
                if (gap != null)
                {
                    TimelineRepository.AddOrUpdate(gap, ServiceName);
                    return gap;
                }
            }

            if (earliest <= initialStart)
            {
                return null;
            }

            var fillInterval = new TimelineInterval
            {
                IntervalStart = earliest,
                IntervalStop = initialStart,
                TimelineName = timelineName
            };

            TimelineRepository.AddOrUpdate(fillInterval, ServiceName);
            return fillInterval;
        }

        public bool UpdateRequestedInterval(TimelineInterval updatedInterval, bool withCleanup = true)
        {
            if (TimelineRepository.FindById(updatedInterval.Id, ServiceName) == null)
            {
                return false;
            }
           
            bool success = TimelineRepository.AddOrUpdate(updatedInterval, ServiceName);

            if (withCleanup)
            {
                CleanupIntervals(updatedInterval.TimelineName);
            }

            return success;
        }

        public bool RemoveRequestInterval(TimelineInterval interval)
        {
            return TimelineRepository.Remove(interval, ServiceName);
        }

        private ITimelineRepository TimelineRepository { get; }

        private void CleanupIntervals(string timelineName)
        {
            var gapTolerance = 2UL;
            var allIntervals = TimelineRepository.FindByTimelineName(timelineName, ServiceName);
            var mergedIntervals = TimelineIntervalUtilities.CleanupList(allIntervals, gapTolerance);
            TimelineRepository.RemoveByTimelineName(timelineName, ServiceName);
            TimelineRepository.AddOrUpdate(mergedIntervals, ServiceName);
        }
    }
}