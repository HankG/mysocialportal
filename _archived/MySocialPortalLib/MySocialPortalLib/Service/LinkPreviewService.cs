using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using static OpenGraphNet.OpenGraph;

namespace MySocialPortalLib.Service
{
    [SuppressMessage("ReSharper", "CA1054")]
    [SuppressMessage("ReSharper", "CA2234")]
    public class LinkPreviewService
    {

        public string ImageCacheFolderPath => ImageService?.LocalImageFolderPath ?? "";
        
        private LinkPreviewService()
        {
            throw new MethodAccessException();
        }
        
        public LinkPreviewService(ILinkPreviewRepository repository, ImageService imageService)
        {
            Repository = repository;
            ImageService = imageService;
        }

        public async Task<LinkPreview?> GetPreview(string url)
        {
            var preview = Repository.FindByUrl(url);

            if (preview != null)
            {
                return preview;
            }

            try
            {
                var graph = await ParseUrlAsync(url).ConfigureAwait(false);
                var description = "";
                if (graph.Metadata.TryGetValue("og:description", out var value))
                {
                    if (value.Count > 0)
                    {
                        description = value[0].Value;
                    }
                }

                var imageUrl = graph.Image;
                var imageName = "";
                var hasImagePreview = false;
                if (!string.IsNullOrEmpty(imageUrl?.AbsolutePath.Trim()))
                {
                    var (success, imageInfo) = await ImageService.GetRemoteImage(imageUrl).ConfigureAwait(false);
                    hasImagePreview = success;
                    imageName = imageInfo?.Name ?? "";
                }
                
                preview = new LinkPreview
                {
                    Title = graph.Title,
                    PreviewImageName = imageName,
                    HasPreviewImage = hasImagePreview,
                    TimeGeneraterated = DateTimeOffset.UtcNow,
                    Description = description,
                    RequestUrl = url,
                    UrlFound = true
                };

                Repository.AddOrUpdate(preview);
                
            }
            catch(System.ArgumentException e)
            {
                Console.WriteLine($"Error retrieving preview for: {url}");
                Console.WriteLine(e);
            }
            catch (System.Net.WebException e)
            {
                Console.WriteLine($"Error retrieving preview for: {url}");
                Console.WriteLine(e);
            }
            catch (System.UriFormatException e)
            {
                Console.WriteLine($"Error retrieving preview for: {url}");
                Console.WriteLine(e);
            }

            return preview;
        }
        
        private ILinkPreviewRepository Repository { get; }
        
        private ImageService ImageService { get; }
        
    }
}