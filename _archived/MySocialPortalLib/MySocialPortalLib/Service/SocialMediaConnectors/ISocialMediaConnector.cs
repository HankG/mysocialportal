using System.Collections.Generic;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Service
{
    public interface ISocialMediaConnector
    {
        IEnumerable<Post> GetNewerHomeTimeline(int maxPosts);
        
        IEnumerable<Post> GetOlderHomeTimeline(int maxPosts);

        IEnumerable<Post> GetNewerUserTimeline(Person person, int maxPosts);

        IEnumerable<Post> GetOlderUserTimeline(Person person, int maxPosts);

        bool UserOnSocialNetwork(Person person);

    }
}