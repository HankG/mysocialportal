using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using LinqToTwitter;
using MySocialPortalLib.Converter;
using MySocialPortalLib.Model;
using MySocialPortalLib.Model.Settings;
using MySocialPortalLib.Repository;

namespace MySocialPortalLib.Service.SocialMediaConnectors
{
    public class TwitterConnector : ISocialMediaConnector
    {
        public const int DefaultMaxPosts = 50;
        public const string HomeTimelineName = "Home_B3F632BB-33D5-4E05-8BE7-2D61D69AE4D4";
        public const ulong EarliestTweetValue = 1;
        public const ulong LatestTweetValue = ulong.MaxValue;
        private const string ScreenNameProperty = "ScreenName";
        
        public IPersonsRepository PersonsRepository { get; }
        
        public TimelineManagementService TimelineManager { get; }
        
        private TwitterConnector()
        {
            throw new MethodAccessException();
        }

        public TwitterConnector(IPersonsRepository personsRepository, TimelineManagementService timelineManager)
        {
            PersonsRepository = personsRepository;
            TimelineManager = timelineManager;
            Settings = new TwitterConnectorSettings();
            if (File.Exists(Settings?.CredentialsPath ?? ""))
            {
                Credentials =
                    JsonSerializer.Deserialize<TwitterCredentials>(File.ReadAllText(Settings?.CredentialsPath));
            }
            else
            {
                Credentials = new TwitterCredentials();
            }
        }
        
        public IEnumerable<Post> GetNewerHomeTimeline(int maxPosts = DefaultMaxPosts)
        {
            var newInterval = TimelineManager.GetNextSamplingInterval(HomeTimelineName, EarliestTweetValue, LatestTweetValue);
            return PullHomeTweets(newInterval, maxPosts);
        }

        public IEnumerable<Post> GetOlderHomeTimeline(int maxPosts = DefaultMaxPosts)
        {
            Console.WriteLine("Called GetOlderHomeTimeline");
            var newInterval = TimelineManager.GetPreviousSamplingInterval(HomeTimelineName, EarliestTweetValue, LatestTweetValue);
            Console.WriteLine($"Interval to query on returned: {newInterval}");
            if (newInterval != null && newInterval.IntervalStop != ulong.MaxValue)
            {
                newInterval.IntervalStop -= 1;
            }
            return PullHomeTweets(newInterval, maxPosts);
        }

        public IEnumerable<Post> GetNewerUserTimeline(Person person, int maxPosts)
        {
            var twitterId = "";
            person?.SocialMediaAccounts[StandardSocialNetworkNames.Twitter]?.AdditionalProperties
                .TryGetValue(ScreenNameProperty, out twitterId);
            if(string.IsNullOrWhiteSpace(twitterId))
            {
                Console.WriteLine($"No Twitter user for person: {person.Id} {person.Name}");
                return new List<Post>();
            }
            Console.WriteLine($"Getting newer Twitter posts from {person.Name} (@{twitterId})");
            var newInterval = TimelineManager.GetNextSamplingInterval(twitterId, EarliestTweetValue, LatestTweetValue);
            return PullUserTweets(twitterId, newInterval, maxPosts);
        }
        
        public IEnumerable<Post> GetOlderUserTimeline(Person person, int maxPosts)
        {
            var twitterId = "";
            person?.SocialMediaAccounts[StandardSocialNetworkNames.Twitter]?.AdditionalProperties
                .TryGetValue(ScreenNameProperty, out twitterId);
            if(string.IsNullOrWhiteSpace(twitterId))
            {
                Console.WriteLine($"No Twitter user for person: {person.Id} {person.Name}");
                return new List<Post>();
            }
            
            Console.WriteLine($"Getting older Twitter posts from {person.Name} (@{twitterId})");
            var newInterval = TimelineManager.GetPreviousSamplingInterval(twitterId, EarliestTweetValue, LatestTweetValue);
            if (newInterval != null && newInterval.IntervalStop != ulong.MaxValue)
            {
                newInterval.IntervalStop -= 1;
            }
            return PullUserTweets(twitterId, newInterval, maxPosts);
        }

        public bool UserOnSocialNetwork(Person person)
        {
            return person?.SocialMediaAccounts?.ContainsKey(StandardSocialNetworkNames.Twitter) ?? false;
        }
        
        private TwitterCredentials Credentials { get; set; }
        
        private TwitterConnectorSettings Settings { get; set; }

        private IAuthorizer DoSingleUserAuth()
        {
            var auth = new SingleUserAuthorizer
            {
                CredentialStore = new SingleUserInMemoryCredentialStore
                {
                    ConsumerKey = Credentials.ConsumerKey,
                    ConsumerSecret = Credentials.ConsumerSecret,
                    AccessToken = Credentials.AccessToken,
                    AccessTokenSecret = Credentials.AccessTokenSecret
                }
            };

            return auth;
        }
        
        private IQueryable<Status> GetNoUpperHomeStatusQuery(TimelineInterval interval, int maxPosts)
        {
            using var twitterCtx = new TwitterContext(DoSingleUserAuth());
            return (from tweet in twitterCtx.Status
                where tweet.Type == StatusType.Home &&
                      tweet.TweetMode == TweetMode.Extended &&
                      tweet.Count == maxPosts &&
                      tweet.SinceID == interval.IntervalStart
                select tweet);
        }

        private IQueryable<Status> GetNoUpperUserStatusQuery(string username, TimelineInterval interval, int maxPosts)
        {
            using var twitterCtx = new TwitterContext(DoSingleUserAuth());
            return (from tweet in twitterCtx.Status
                where tweet.Type == StatusType.User &&
                      tweet.TweetMode == TweetMode.Extended &&
                      tweet.ScreenName == username &&
                      tweet.Count == maxPosts &&
                      tweet.SinceID == interval.IntervalStart
                select tweet);
        }

        private IQueryable<Status> GetUpperLowerHomeStatusQuery(TimelineInterval interval, int maxPosts)
        {
            using var twitterCtx = new TwitterContext(DoSingleUserAuth());
            return (from tweet in twitterCtx.Status
                where tweet.Type == StatusType.Home &&
                      tweet.TweetMode == TweetMode.Extended &&
                      tweet.Count == maxPosts &&
                      tweet.MaxID == interval.IntervalStop && 
                      tweet.SinceID == interval.IntervalStart
                select tweet);
        }

        private IQueryable<Status> GetUpperLowerUserStatusQuery(string username, TimelineInterval interval, int maxPosts)
        {
            using var twitterCtx = new TwitterContext(DoSingleUserAuth());
            return (from tweet in twitterCtx.Status
                where tweet.Type == StatusType.User &&
                      tweet.TweetMode == TweetMode.Extended &&
                      tweet.Count == maxPosts &&
                      tweet.ScreenName == username &&
                      tweet.MaxID == interval.IntervalStop && 
                      tweet.SinceID == interval.IntervalStart
                select tweet);
        }

        private IList<Post> PullHomeTweets(TimelineInterval interval, int maxPosts)
        {
            IQueryable<Status> query;
            var updateUpper = true;
            var removeIntervalIfEmpty = false;
            Console.WriteLine($"PullHomeTweets {maxPosts} #posts for {interval}");
            if (interval.IntervalStop == ulong.MaxValue)
            {
                Console.WriteLine("Pulling home tweets with no upper bound");
                query = GetNoUpperHomeStatusQuery(interval, maxPosts);
                removeIntervalIfEmpty = true;

            }
            else
            {
                Console.WriteLine("Pulling upper and lower bound tweets");
                query = GetUpperLowerHomeStatusQuery(interval, maxPosts);
                updateUpper = false;
            }

            return PullTweets(interval, maxPosts, query, updateUpper, removeIntervalIfEmpty);
        }

        private IList<Post> PullUserTweets(string username, TimelineInterval interval, int maxPosts)
        {
            IQueryable<Status> query;
            bool updateUpper = true;
            bool removeIntervalIfEmpty = false;
            if (interval.IntervalStop == ulong.MaxValue)
            {
                Console.WriteLine($"Pulling tweets for Twitter user {username} with no upper bounds: {interval}");
                query = GetNoUpperUserStatusQuery(username, interval, maxPosts);
                removeIntervalIfEmpty = true;
            }
            else
            {
                Console.WriteLine($"Pulling tweets for Twitter user {username} with upper and lower bounds: {interval}");
                query = GetUpperLowerUserStatusQuery(username, interval, maxPosts);
                updateUpper = false;
            }

            return PullTweets(interval, maxPosts, query, updateUpper, removeIntervalIfEmpty);
        }

        private IList<Post> PullTweets(TimelineInterval newInterval, int maxPosts, IQueryable<Status> query, bool updateUpper, bool removeRequestIfEmpty)
        {
            var posts = new List<Post>();
            if (newInterval == null)
            {
                return posts;
            }
            
            List<Status> tweets = query.ToList();

            Console.WriteLine($"{tweets.Count} tweets returned");
            if (tweets.Count == 0)
            {
                if (removeRequestIfEmpty)
                {
                    TimelineManager.RemoveRequestInterval(newInterval);
                }
                return posts;
            }
            
            UpdateSampleInterval(tweets, newInterval, maxPosts, updateUpper);
            var converter = new TwitterConverter(PersonsRepository);
            posts.AddRange(tweets.Select(t => converter.TweetToPost(t)));
            return posts;
        }
        
        private void UpdateSampleInterval(IList<Status> tweets, TimelineInterval newInterval, int maxPosts, bool updateUpper)
        {
            var earliestTweet = tweets.Min(t => t.StatusID);
            var latestTweet = tweets.Max(t => t.StatusID);
            Console.WriteLine($"TweetIDs range: {earliestTweet} to {latestTweet}");

            if (updateUpper)
            {
                newInterval.IntervalStop = latestTweet;
            }
            
            if (tweets.Count == maxPosts)
            {
                Console.WriteLine("Max posts returned therefore setting interval start as well");
                newInterval.IntervalStart = earliestTweet;
            }
            
            Console.WriteLine($"Updated interval: {newInterval}");
            TimelineManager.UpdateRequestedInterval(newInterval);
        }

    }
}