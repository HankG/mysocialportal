using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using MySocialPortalLib.Model;
using MySocialPortalLib.Model.Settings;

namespace MySocialPortalLib.Service
{
    public class DirectoryServices
    {
        private const string CredentialsSettingsDirectoryName = "SocialNetworkCredentialsSettings";
        private const string ExternalLinkMediaDirectoryName = "ExternalLinkMedia";
        private const string OpenGraphCacheDirectoryName = "OpenGraphCache";
        private const string PostMediaDirectoryName = "PostMedia";
        private const string ProfileImagesDirectoryName = "ProfileImages";
        
        private static readonly Lazy<DirectoryServices> Singleton =
            new Lazy<DirectoryServices>(() => new DirectoryServices());
        
        public static DirectoryServices Instance => Singleton.Value;

        public DirectoryServices()
        {
            var settings = new MainSettings();
            ApplicationName = settings.ApplicationName;
        }

        public string ExternalLinkMediaDirectory()
        {
            return Path.Combine(UserAppBaseDirectory(), ExternalLinkMediaDirectoryName);
        }
        
        public string UserAppBaseDirectory()
        {
            var homeDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var baseDir = Path.Combine(homeDir, ApplicationName);

            return baseDir;
        }

        public string UserCredentialsSettingsDirectory()
        {
            return Path.Combine(UserAppBaseDirectory(), CredentialsSettingsDirectoryName);
        }

        public string OpenGraphCacheDirectory()
        {
            return Path.Combine(UserAppBaseDirectory(), OpenGraphCacheDirectoryName);
        }
        
        public string PostMediaDirectory()
        {
            return Path.Combine(UserAppBaseDirectory(), PostMediaDirectoryName);
        }
        
        public string ProfileImagesDirectory()
        {
            return Path.Combine(UserAppBaseDirectory(), ProfileImagesDirectoryName);
        }
        
        private string ApplicationName { get; set; }
    }
}