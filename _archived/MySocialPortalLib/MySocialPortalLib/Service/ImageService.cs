using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;

namespace MySocialPortalLib.Service
{
    [SuppressMessage("ReSharper", "CA1054")]
    public class ImageService
    {
        private const string DefaultExtension = ".jpg";
        
        public string LocalImageFolderPath { get; }
        
        private ImageService()
        {
            throw new NotSupportedException();
        }

        public ImageService(IFileCacheRepository cacheRepository, string localImageFolderPath)
        {
            CacheRepository = cacheRepository;
            LocalImageFolderPath = localImageFolderPath;
        }

        public async Task<(bool found, FileInfo? info)> GetRemoteImage(Uri imageUri, bool reuseExisting = true)
        {
            if (imageUri == null)
            {
                return (false, null);
            }
            
            string imageFilePath = "";
            try
            {
                var imageUrl = imageUri.AbsoluteUri;
                var imageExtension = new FileInfo(imageUri.AbsolutePath).Extension;
                if (string.IsNullOrWhiteSpace(imageExtension))
                {
                    imageExtension = DefaultExtension;
                }
                var imageCacheItem = CacheRepository.FindByUrl(imageUrl);
                var imageFileName = imageCacheItem?.LocalFileName ?? GetRandomName(imageExtension);
                imageFilePath = Path.Combine(LocalImageFolderPath, imageFileName);

                var alreadyExists = File.Exists(imageFilePath);
                if (reuseExisting && alreadyExists)
                {
                    return (true, new FileInfo(imageFilePath));
                }

                if (alreadyExists)
                {
                    File.Delete(imageFilePath);
                    imageFileName = GetRandomName(imageExtension);
                    imageFilePath = Path.Combine(LocalImageFolderPath, imageFileName);
                }

                using var client = new WebClient();
                await client.DownloadFileTaskAsync(imageUrl, imageFilePath).ConfigureAwait(false);
                if (!File.Exists(imageFilePath))
                {
                    if (imageCacheItem != null)
                    {
                        CacheRepository.Remove(imageCacheItem);
                    }
                    return (false, null);
                }

                if (new FileInfo(imageFilePath).Length != 0)
                {
                    if (imageCacheItem == null)
                    {
                        imageCacheItem = new FileCacheItem();
                    }

                    imageCacheItem.CreationTime = DateTimeOffset.Now;
                    imageCacheItem.OriginalUrl = imageUrl;
                    imageCacheItem.LocalFileName = imageFileName;
                    CacheRepository.AddOrUpdate(imageCacheItem);
                    
                    return (true, new FileInfo(imageFilePath));
                }

                File.Delete(imageFilePath);
                return (false, null);
            }
            catch (System.Net.WebException e)
            {
                Console.WriteLine(e);
                if (imageFilePath != null && File.Exists(imageFilePath))
                {
                    File.Delete(imageFilePath);
                }
            }

            return (false, null);
        }

        private static string GetRandomName(string imageExtension)
        {
            return $"{Guid.NewGuid().ToString()}{imageExtension}";
        }

        private IFileCacheRepository CacheRepository { get; }

    }
}