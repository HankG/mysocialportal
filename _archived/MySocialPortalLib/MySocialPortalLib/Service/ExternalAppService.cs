using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace MySocialPortalLib.Service
{
    [SuppressMessage("ReSharper", "CA1054")]
    public static class ExternalAppService
    {
        public static void OpenWebPage(string url)
        {
            if (url == null)
            {
                return;
            }
            
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                var cleanedUrl = url.Replace("&", "^&", StringComparison.InvariantCulture);
                var cmdFileName = "cmd";
                var arguments = $"/c start {cleanedUrl}";
                var processStartInfo = new ProcessStartInfo
                {
                    Arguments = arguments,
                    FileName = cmdFileName,
                    CreateNoWindow = true
                };
                
                Process.Start(processStartInfo);
                return;
            }
            
            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                Process.Start("open", url);
                return;
            }
            
            // Only Linux/Unix left now w/DE's left now...
            Process.Start("xdg-open", url);
        }
    }
}