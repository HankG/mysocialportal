using System;
using System.Collections.Generic;
using MySocialPortalLib.Model;
using Newtonsoft.Json.Linq;

namespace MySocialPortalLib.Util
{
    public static class TimelineIntervalUtilities
    {
        public static TimelineInterval Bounds(TimelineInterval item1, TimelineInterval item2)
        {
            if (item1 == null || item2 == null)
            {
                throw new NullReferenceException();
            }

            var start = item1.IntervalStart < item2.IntervalStart ? item1.IntervalStart : item2.IntervalStart;
            var stop = item1.IntervalStop > item2.IntervalStop ? item1.IntervalStop : item2.IntervalStop;

            return new TimelineInterval
            {
                IntervalStart = start,
                IntervalStop = stop,
                TimelineName = item1.TimelineName
            };
        }

        public static List<TimelineInterval> CleanupList(IList<TimelineInterval> originalList, ulong gapTolerance = 0)
        {
            var retainedIntervals = new List<TimelineInterval>();

            if (originalList == null || originalList.Count == 0)
            {
                return retainedIntervals;
            }
            
            var intervals = new List<TimelineInterval>(originalList);
            intervals.Sort((i1, i2) => i1.IntervalStart.CompareTo(i2.IntervalStart));
            foreach (var interval1 in intervals)
            {
                if (retainedIntervals.Count == 0)
                {
                    var newInterval = interval1.Copy();
                    newInterval.Id = Guid.NewGuid().ToString();
                    retainedIntervals.Add(newInterval);
                }

                var currentInterval = retainedIntervals[^1];
                var gap = Gap(interval1, currentInterval);
                if (gap != null && (gap.IntervalStop - gap.IntervalStart) > gapTolerance)
                {
                    retainedIntervals.Add(interval1);
                    continue;
                }
                
                var bounds = Bounds(interval1, currentInterval);
                currentInterval.IntervalStart = bounds.IntervalStart;
                currentInterval.IntervalStop = bounds.IntervalStop;
            }

            return retainedIntervals;
        }

        public static TimelineInterval? Gap(TimelineInterval item1, TimelineInterval item2)
        {
            if (item1 == null || item2 == null)
            {
                return null;
            }

            var earlier = item1;
            var later = item2;

            if (item2.IntervalStart < item1.IntervalStart)
            {
                earlier = item2;
                later = item1;
            }

            if (earlier.IntervalStop < later.IntervalStart)
            {
                return new TimelineInterval
                {
                    IntervalStart = earlier.IntervalStop,
                    IntervalStop = later.IntervalStart,
                    TimelineName = item1.TimelineName
                };
            }

            return null;
        }

    }
}