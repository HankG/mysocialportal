using System.Collections.Generic;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;

namespace MySocialPortalLib.Util
{
    public static class PostDatabaseMergingUtils
    {
        public static (int added, int updated) AddOrUpdateToRepository(IPostsRepository postsRepository, IEnumerable<Post> posts)
        {
            if (postsRepository == null || posts == null)
            {
                return (0, 0);
            }

            var addedPosts = 0;
            var updatePosts = 0;

            foreach (var post in posts)
            {
                var oldPost = postsRepository.GetById(post.Id) ?? 
                              postsRepository.GetByOriginalNetworkId(post.OriginalSocialMediaSystem,
                                  post.OriginalLinkUrlOrId);

                if (oldPost == null)
                {
                    postsRepository.AddOrUpdate(post);
                    addedPosts++;
                }
                else
                {
                    post.Id = oldPost.Id;
                    oldPost.Merge(post);
                    postsRepository.AddOrUpdate(oldPost);
                    updatePosts++;
                }
            }

            return (addedPosts, updatePosts);
        }
        
    }
}