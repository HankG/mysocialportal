using System;
using System.Collections.Generic;
using System.Linq;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Util
{
    public static class PostMergingUtils
    {
        public static void Merge(this Post post1, Post post2)
        {
            if (!IsReallySamePost(post1, post2))
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(post2.Body))
            {
                post1.Body = post2.Body;
            }
            
            post1.Events.AddIfNew(post2.Events);
            post1.Links.AddIfNew(post2.Links);
            post1.Media.AddIfNew(post2.Media);
            post1.Places.AddIfNew(post2.Places);
            post1.Polls.AddIfNew(post2.Polls);
            MergeStringCollections(post1.Tags, post2.Tags);
            MergeStringCollections(post1.TaggedUsers, post2.TaggedUsers);
            post1.RelatedPosts.AddIfNew(post2.RelatedPosts);
            post1.UserId = post2.UserId;
            post1.PostDateTime = post2.PostDateTime;
            post1.OriginalSocialMediaSystem = post2.OriginalSocialMediaSystem;
            post1.OriginalLinkUrlOrId = post2.OriginalLinkUrlOrId;
            post1.Title = post2.Title;
        }

        public static void AddIfNew(this List<EventData> currentEvents, List<EventData> newEvents)
        {
            if (currentEvents == null || newEvents == null)
            {
                return;
            }

            if (!currentEvents.Any())
            {
                currentEvents.AddRange(newEvents);
                return;
            }
            
            foreach(var evt in newEvents)
            {
                var sameOldEvent = currentEvents.FirstOrDefault(e1 => e1.Id == evt.Id);
                if (sameOldEvent != null)
                {
                    sameOldEvent.Merge(evt);
                }
                else
                {
                    currentEvents.Add(evt);
                }
            }
        }

        public static void AddIfNew(this List<ExternalLink> currentLinks, List<ExternalLink> newLinks)
        {
            if (currentLinks == null || newLinks == null)
            {
                return;
            }

            if (!currentLinks.Any())
            {
                currentLinks.AddRange(newLinks);
                return;
            }

            foreach (var link in newLinks)
            {
                var oldLink = currentLinks.FirstOrDefault(l => l.Url == link.Url);
                if (oldLink == null)
                {
                    currentLinks.Add(link);
                }
                else
                {
                    oldLink.Merge(link);
                }
            }
        }

        public static void AddIfNew(this List<MediaData> currentMedia, List<MediaData> newMedia)
        {
            if (currentMedia == null || newMedia == null)
            {
                return;
            }

            if (!currentMedia.Any())
            {
                currentMedia.AddRange(newMedia);
                return;
            }

            foreach (var media in newMedia)
            {
                var oldMedia = currentMedia.FirstOrDefault(m => m.Id == media.Id);
                if (oldMedia == null)
                {
                    currentMedia.Add(media);
                }
                else
                {
                    oldMedia.Merge(media);
                }
            }
        }

        public static void AddIfNew(this List<Place> currentPlaces, List<Place> newPlaces)
        {
            if (currentPlaces == null || newPlaces == null)
            {
                return;
            }

            if (!currentPlaces.Any())
            {
                currentPlaces.AddRange(newPlaces);
                return;
            }

            foreach (var place in newPlaces)
            {
                var oldPlace = currentPlaces.FirstOrDefault(p => p.Id == place.Id);

                if (oldPlace == null)
                {
                    currentPlaces.Add(place);
                }
                else
                {
                    oldPlace.Merge(place);
                }
            }
        }

        public static void AddIfNew(this List<Poll> currentPolls, List<Poll> newPolls)
        {
            if (currentPolls == null || newPolls == null)
            {
                return;
            }

            if (!currentPolls.Any())
            {
                currentPolls.AddRange(newPolls);
                return;
            }

            foreach (var poll in newPolls)
            {
                var oldPoll = currentPolls.FirstOrDefault(p => p.Id == poll.Id);

                if (oldPoll == null)
                {
                    currentPolls.Add(poll);
                }
                else
                {
                   oldPoll.Merge(poll);
                }
            }
        }
        
        public static void AddIfNew(this List<PollOption> currentPollOptions, List<PollOption> newPollOptions)
        {
            if (currentPollOptions == null || newPollOptions == null)
            {
                return;
            }

            if (!currentPollOptions.Any())
            {
                currentPollOptions.AddRange(newPollOptions);
                return;
            }

            foreach (var pollOption in newPollOptions)
            {
                var oldPollOption = currentPollOptions.FirstOrDefault(p => p.Text == pollOption.Text);

                if (oldPollOption == null)
                {
                    currentPollOptions.Add(pollOption);
                }
                else
                {
                    oldPollOption.Vote = pollOption.Vote;
                }
            }
        }

        public static void Merge(this EventData event1, EventData event2)
        {
            if (event1 == null || event2 == null)
            {
                return;
            }
            
            event1.Title = event2.Title;
            event1.StartTime = event2.StartTime;
            event1.StopTime = event2.StopTime;
            event1.Place.Merge(event2.Place);
        }

        public static void Merge(this ExternalLink link1, ExternalLink link2)
        {
            if (link1 == null || link2 == null)
            {
                return;
            }
            
            link1.Name = link2.Name;
            link1.Source = link2.Source;
            if (!string.IsNullOrWhiteSpace(link2.PostId))
            {
                link1.PostId = link2.PostId;
            }
        }

        public static void Merge(this MediaData media1, MediaData media2)
        {
            if (media1 == null || media2 == null)
            {
                return;
            }

            media1.Description = media2.Description;
            media1.Title = media2.Title;
            media1.LocalPath = media2.LocalPath;
            media1.MediaType = media2.MediaType;
            media1.OriginalUrl = media2.OriginalUrl;
            media1.CreationDateTime = media2.CreationDateTime;
        }
        
        public static void Merge(this Place place1, Place place2)
        {
            if (place1 == null || place2 == null)
            {
                return;
            }

            place1.Altitude = place2.Altitude;
            place1.Latitude = place2.Latitude;
            place1.Longitude = place2.Longitude;
            place1.Name = place2.Name;
            place1.Url = place2.Url;
            place1.PhysicalAddress = place2.PhysicalAddress;
            place1.HasLatLon = place2.HasLatLon;
        }
        
        public static void Merge(this Poll poll1, Poll poll2)
        {
            if (poll1 == null || poll2 == null)
            {
                return;
            }

            poll1.Question = poll2.Question;
            poll1.Options.AddIfNew(poll2.Options);
        }

        public static void MergeStringCollections(List<string> list1, IEnumerable<string> list2)
        {
            if (list1 == null || list2 == null)
            {
                return;
            }

            if (!list1.Any())
            {
                list1.AddRange(list2);
                return;
            }
            
            var unique = new HashSet<string>(list1.Select(i => i.ToUpperInvariant()));
            foreach (var i in list2)
            {
                if (unique.Contains(i.ToUpperInvariant()))
                {
                    continue;
                }
                
                list1.Add(i);
            }
        }

        private static bool IsReallySamePost(Post post1, Post post2)
        {
            if (post1 == null || post2 == null)
            {
                return false;
            }
            
            if (string.Equals(post1.Id, post2.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            if (string.Equals(post1.OriginalSocialMediaSystem,post2.OriginalSocialMediaSystem, StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }

            return string.Equals(post1.OriginalLinkUrlOrId, post2.OriginalLinkUrlOrId,
                StringComparison.InvariantCultureIgnoreCase);
        }
    }
}