using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MySocialPortalDesktop.Factory;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;

namespace MySocialPortalLib.Util
{
    public class UserListPostUpdater
    {
        public ISocialMediaConnectorFactory ConnectorFactory { get; }
        
        public int MaxPosts { get; }
        
        public IPersonsRepository PersonsRepository { get; }
        
        public IPostsRepository PostsRepository { get; }
        
        public INamedListRepository UsersLists { get; }

        public UserListPostUpdater(ISocialMediaConnectorFactory connectorFactory,
            IPostsRepository postsRepository, IPersonsRepository personsRepository,
            INamedListRepository usersLists,
            int maxPosts)
        {
            ConnectorFactory = connectorFactory;
            PersonsRepository = personsRepository;
            PostsRepository = postsRepository;
            UsersLists = usersLists;
            MaxPosts = maxPosts;
        }
        
        public async Task<List<Post>> Update(string userListName)
        {
            var idsToProcess = UsersLists.GetAllIdsForList(userListName);
            var newPosts = new List<Post>();

            if (idsToProcess == null || idsToProcess.Count == 0)
            {
                return newPosts;
            }

            foreach (var id in idsToProcess)
            {
                var person = PersonsRepository.FindById(id);
                if (person == null)
                {
                    continue;
                }
                
                foreach (var sma in person.SocialMediaAccounts.Values)
                {
                    var connector = ConnectorFactory.GetConnectorByNetworkName(sma.SocialMediaSystemName);
                    if (connector == null)
                    {
                        continue;
                    }

                    var posts = connector.GetNewerUserTimeline(person, MaxPosts);
                    foreach (var post in posts)
                    {
                        newPosts.Add(post);
                        var oldPost = PostsRepository.GetByOriginalNetworkId(post.OriginalSocialMediaSystem,
                            post.OriginalLinkUrlOrId);
                        if (oldPost == null)
                        {
                            PostsRepository.AddOrUpdate(post);
                        }
                        else
                        {
                            oldPost.Merge(post);
                            PostsRepository.AddOrUpdate(oldPost);
                        }
                    }
                }
            }

            return newPosts;
        }
    }
}