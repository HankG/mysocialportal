using System.Collections.Generic;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Util
{
    public class PostComparisonDescending : IComparer<Post>
    {
        public int Compare(Post x, Post y)
        {
            if (x == null || y == null)
            {
                return 0;
            }
            
            return -x.PostDateTime.CompareTo(y.PostDateTime);
        }
    }
}