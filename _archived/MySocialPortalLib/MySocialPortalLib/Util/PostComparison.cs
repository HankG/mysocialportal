using System.Collections.Generic;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Util
{
    public class PostComparison: IComparer<Post>
    {
        private readonly int _multiplier;
        
        public PostComparison(bool ascending)
        {
            _multiplier = ascending ? 1 : -1;
        }
        public int Compare(Post x, Post y)
        {
            if (x == null || y == null)
            {
                return 0;
            }

            return _multiplier * x.PostDateTime.CompareTo(y.PostDateTime);
        }
    }
}