using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using MySocialPortalLib.Model;

namespace MySocialPortalLib.Util
{
    public static class PersonDatabaseMergingUtils
    {
        public static (IList<Person> newPersons, IList<Person> mergedPersons, IList<Person> unchangedPersons) Merge(
            IList<Person> list1,
            IList<Person> list2)
        {
            var originalList1 = new List<Person>(list1);
            var originalList2 = new List<Person>(list2);
            var mergedPersons = new List<Person>();
            var list1Dictionary = originalList1.ToImmutableDictionary(p => p.Id, p => p, StringComparer.InvariantCulture);
            var list2Dictionary = originalList2.ToImmutableDictionary(p => p.Id, p => p, StringComparer.InvariantCulture);
            var commonIds = list1Dictionary.Keys.Intersect(list2Dictionary.Keys);

            foreach (var id in commonIds)
            {
                var person1 = list1Dictionary[id];
                var person2 = list2Dictionary[id];
                originalList1.Remove(person1);
                originalList2.Remove(person2);
                person1.Merge(person2);
                mergedPersons.Add(person1);
            }

            var socialNetworkNameDict1 = originalList1.ToSocialNetworkUsernameKeyedDictionary();
            var socialNetworkNameDict2 = originalList2.ToSocialNetworkUsernameKeyedDictionary();
            var commonNetworkIds = socialNetworkNameDict1.Keys.Intersect(socialNetworkNameDict2.Keys);
            foreach (var networkId in commonNetworkIds)
            {
                var person1 = socialNetworkNameDict1[networkId];
                var person2 = socialNetworkNameDict2[networkId];
                originalList1.Remove(person1);
                originalList2.Remove(person2);
                person1.Merge(person2);
                if (!mergedPersons.Contains(person1))
                {
                    mergedPersons.Add(person1);
                }
            }

            return (originalList2, mergedPersons, originalList1);
        }

        public static void Merge(this Person person1, Person person2)
        {
            person1.Emails.AddIfNew(person2.Emails);
            person1.Addresses.AddIfNew(person2.Addresses);
            person1.Websites.AddIfNew(person2.Websites);
            person1.PhoneNumbers.AddIfNew(person2.PhoneNumbers);
            person1.SocialMediaAccounts.AddIfNew(person2.SocialMediaAccounts);

            foreach (var k in person2.AdditionalProperties.Keys)
            {
                person1.AdditionalProperties[k] = person2.AdditionalProperties[k];
            }
        }

        public static void AddIfNew(this List<LabeledValue<string>> currentSet, LabeledValue<string> newValue)
        {
            var sameValue = currentSet.Find(v => v.Label == newValue.Label);
            if (sameValue == null)
            {
                currentSet.Add(newValue);
                return;
            }

            sameValue.Value = string.IsNullOrWhiteSpace(sameValue.Value) ? newValue.Value : sameValue.Value;
        }

        public static void AddIfNew(this List<LabeledValue<string>> currentSet, List<LabeledValue<string>> newValues)
        {
            newValues.ForEach(v => currentSet.AddIfNew(v));
        }

        public static void AddIfNew(this Dictionary<string, SocialMediaAccountData> currentSet,
            Dictionary<string, SocialMediaAccountData> newValues)
        {
            foreach (var (key, value) in newValues)
            {
                if (currentSet.TryGetValue(key, out var smd))
                {
                    smd.Merge(value);
                    continue;
                }

                currentSet.Add(key, value);
            }

        }

        public static void Merge(this SocialMediaAccountData cv, SocialMediaAccountData nv,
            bool flushAdditionalProps = false)
        {
            cv.Active = nv.Active;

            if (!string.IsNullOrEmpty(nv.ProfileId))
            {
                cv.ProfileId = nv.ProfileId;
            }

            if (!string.IsNullOrEmpty(nv.ProfileUrl))
            {
                cv.ProfileUrl = nv.ProfileUrl;
            }

            if (!string.IsNullOrEmpty(nv.RealName))
            {
                cv.RealName = nv.RealName;
            }

            if (!string.IsNullOrEmpty(nv.ProfilePhotoPath))
            {
                cv.ProfilePhotoPath = nv.ProfilePhotoPath;
            }

            if (!string.IsNullOrEmpty(nv.SocialMediaSystemName))
            {
                cv.SocialMediaSystemName = nv.SocialMediaSystemName;
            }

            if (flushAdditionalProps)
            {
                cv.AdditionalProperties.Clear();
            }

            foreach (var (k,v) in nv.AdditionalProperties)
            {
                cv.AdditionalProperties[k] = v;
            }

        }


        private static IDictionary<string, Person> ToSocialNetworkUsernameKeyedDictionary(this IEnumerable<Person> persons)
        {
            var results = new Dictionary<string, Person>();

            foreach (var p in persons)
            {
                foreach (var (network, smd) in p.SocialMediaAccounts)
                {
                    if (string.IsNullOrWhiteSpace(network) || string.IsNullOrWhiteSpace(smd.ProfileId))
                    {
                        continue;
                    }
                    var key = $"{network}{smd.ProfileId}";
                    results[key] = p;
                }
            }
            
            return results;
        }
}
}