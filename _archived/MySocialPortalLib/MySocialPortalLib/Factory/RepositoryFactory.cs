using System;
using System.IO;
using LiteDB;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;

namespace MySocialPortalDesktop.Factory
{
    public class RepositoryFactory : IDisposable
    {
        private const string AllPostsRepositoryDbName = "all_posts.db";
        private const string LinkPreviewRepositoryDbName = "link_preview.db";
        private const string LinkPreviewCacheRepositoryDbName = "link_preview_cache.db";
        private const string ListsRepositoryDbName = "lists.db";
        private const string MainPeopleRepositoryDbName = "all_people.db";
        private const string ProfileImageRepositoryDbName = "profile_images.db";
        private const string TimelineRepositoryDbName = "timeline_cache.db";

        private static readonly Lazy<RepositoryFactory> Singleton =
            new Lazy<RepositoryFactory>(() => new RepositoryFactory());
        
        public static readonly RepositoryFactory Instance = Singleton.Value;
        
        private bool _disposed = false;
        
        public IPostsRepository AllPostsRepository => AllPostsRepositoryLazy.Value;

        public ILinkPreviewRepository LinkPreviewRepository => LinkPreviewRepositoryLazy.Value;
        
        public IFileCacheRepository LinkPreviewImageCacheRepository => LinkPreviewImageCacheRepositoryLazy.Value;

        public INamedListRepository ListsRepository => ListsRepositoryLazy.Value;

        public IFileCacheRepository ProfileImageCacheRepository => ProfileImageCacheRepositoryLazy.Value;

        public IPersonsRepository MainPeopleRepository => MainPeopleRepositoryLazy.Value;

        public ITimelineRepository TimelineRepository => TimelineRepositoryLazy.Value;

        public RepositoryFactory()
        {
            AllPostsRepositoryLazy = new Lazy<IPostsRepository>(BuildAllPostsRepository);
            LinkPreviewRepositoryLazy = new Lazy<ILinkPreviewRepository>(BuildLinkPreviewRepository);
            LinkPreviewImageCacheRepositoryLazy = new Lazy<IFileCacheRepository>(BuildLinkPreviewImageCacheRepository);
            ListsRepositoryLazy = new Lazy<INamedListRepository>(BuildListRepository);
            ListsRepositoryBackingLazy = new Lazy<LiteRepository>(BuildListRepositoryBacking);
            MainPeopleRepositoryLazy = new Lazy<IPersonsRepository>(BuildPeopleRepository);
            ProfileImageCacheRepositoryLazy = new Lazy<IFileCacheRepository>(BuildProfileImageCacheRepository);
            TimelineRepositoryLazy = new Lazy<ITimelineRepository>(BuildTimelineRepository);
        }
        
        private Lazy<IPostsRepository> AllPostsRepositoryLazy { get; }
        
        private Lazy<ILinkPreviewRepository> LinkPreviewRepositoryLazy { get; }
        
        private Lazy<IFileCacheRepository> LinkPreviewImageCacheRepositoryLazy { get; }

        private Lazy<INamedListRepository> ListsRepositoryLazy { get; }

        private Lazy<LiteRepository> ListsRepositoryBackingLazy { get; }

        private Lazy<IPersonsRepository> MainPeopleRepositoryLazy { get; }

        private Lazy<IFileCacheRepository> ProfileImageCacheRepositoryLazy { get; }
        
        private Lazy<ITimelineRepository> TimelineRepositoryLazy { get; }

        private IPostsRepository BuildAllPostsRepository()
        {
            return new PostsLiteDbRepository(BuildDbPath(AllPostsRepositoryDbName));
        }

        private ILinkPreviewRepository BuildLinkPreviewRepository()
        {
            return new LinkPreviewLiteDbRepository(BuildDbPath(LinkPreviewRepositoryDbName));
        }

        private IFileCacheRepository BuildLinkPreviewImageCacheRepository()
        {
            return new FileCacheLiteDbRepository(BuildDbPath(LinkPreviewCacheRepositoryDbName));
        }

        private INamedListRepository BuildListRepository()
        {
            return new NamedListLiteDbRepository(ListsRepositoryBackingLazy.Value);
        }

        private LiteRepository BuildListRepositoryBacking()
        {
            return new LiteRepository(BuildDbPath(ListsRepositoryDbName));
        }
        
        private IFileCacheRepository BuildProfileImageCacheRepository()
        {
            return new FileCacheLiteDbRepository(BuildDbPath(ProfileImageRepositoryDbName));
        }
        
        private IPersonsRepository BuildPeopleRepository()
        {
             return new PersonsLiteDbRepository(BuildDbPath(MainPeopleRepositoryDbName));
        }

        private ITimelineRepository BuildTimelineRepository()
        {
            return new TimelineLiteDbRepository(BuildDbPath(TimelineRepositoryDbName));
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                (AllPostsRepository as IDisposable)?.Dispose();
                (LinkPreviewRepository as IDisposable)?.Dispose();
                (MainPeopleRepository as IDisposable)?.Dispose();
                (ProfileImageCacheRepository as IDisposable)?.Dispose();
                (TimelineRepository as IDisposable)?.Dispose();
                (LinkPreviewImageCacheRepository as IDisposable)?.Dispose();
                (ListsRepositoryBackingLazy.Value as IDisposable)?.Dispose();
            }

            _disposed = true;
        }

        private static string BuildDbPath(string databaseName)
        {
            return Path.Combine(DirectoryServices.Instance.UserAppBaseDirectory(), databaseName);
        }
    }
}