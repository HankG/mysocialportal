using MySocialPortalLib.Util;

namespace MySocialPortalDesktop.Factory
{
    public static class UserListPostUpdaterFactory
    {
        public const int DefaultMaxPosts = 25;
        
        public static UserListPostUpdater GetDefault(int maxPosts = DefaultMaxPosts)
        {
            var repoFactory = RepositoryFactory.Instance;
            return new UserListPostUpdater(new SocialMediaConnectorFactory(), 
                repoFactory.AllPostsRepository, repoFactory.MainPeopleRepository, repoFactory.ListsRepository, 
                maxPosts);
        }
    }
}