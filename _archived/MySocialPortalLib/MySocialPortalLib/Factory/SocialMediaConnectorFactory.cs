using System;
using System.Diagnostics.CodeAnalysis;
using MySocialPortalLib.Model;
using MySocialPortalLib.Service;
using MySocialPortalLib.Service.SocialMediaConnectors;

namespace MySocialPortalDesktop.Factory
{
    [SuppressMessage("ReSharper", "CA1822")]
    public class SocialMediaConnectorFactory : ISocialMediaConnectorFactory
    {
        public static TwitterConnector GetNewTwitterConnector()
        {
            return new TwitterConnector(RepositoryFactory.Instance.MainPeopleRepository, 
                ServiceFactory.BuildNewTimelineManagementService(StandardSocialNetworkNames.Twitter));
        }

        public ISocialMediaConnector GetConnectorByNetworkName(string name)
        {
            switch (name)
            {
                case StandardSocialNetworkNames.Twitter:
                    return GetNewTwitterConnector();
                default:
                    return null;
            }
        }
    }
}