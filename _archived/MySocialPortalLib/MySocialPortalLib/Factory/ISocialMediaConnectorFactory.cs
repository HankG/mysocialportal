using MySocialPortalLib.Service;

namespace MySocialPortalDesktop.Factory
{
    public interface ISocialMediaConnectorFactory
    {
        ISocialMediaConnector GetConnectorByNetworkName(string name);
    }
}