using System;
using MySocialPortalLib.Service;

namespace MySocialPortalDesktop.Factory
{
    public class ServiceFactory
    {
        private static readonly Lazy<ServiceFactory> Singleton =
            new Lazy<ServiceFactory>(() => new ServiceFactory());

        public static readonly ServiceFactory Instance = Singleton.Value;

        public LinkPreviewService LinkPreviewService => LinkPreviewServiceLazy.Value;

        public ImageService ProfileImageService => PreviewImageServiceLazy.Value;

        private ServiceFactory()
        {
            LinkPreviewServiceLazy = new Lazy<LinkPreviewService>(BuildLinkPreviewService);
            PreviewImageServiceLazy = new Lazy<ImageService>(BuildProfileImageService);
        }

        public static TimelineManagementService BuildNewTimelineManagementService(string serviceName)
        {
            return new TimelineManagementService(RepositoryFactory.Instance.TimelineRepository, serviceName);
        }

        private Lazy<LinkPreviewService> LinkPreviewServiceLazy { get; }
        
        private Lazy<ImageService> PreviewImageServiceLazy { get; }

        private LinkPreviewService BuildLinkPreviewService()
        {
            var imagePreviewCacheFolder = DirectoryServices.Instance.OpenGraphCacheDirectory();
            var linkPreviewImageService = new ImageService(RepositoryFactory.Instance.LinkPreviewImageCacheRepository,
                imagePreviewCacheFolder);
            return new LinkPreviewService(RepositoryFactory.Instance.LinkPreviewRepository, linkPreviewImageService);
        }

        private ImageService BuildProfileImageService()
        {
            var profileImageDir = DirectoryServices.Instance.ProfileImagesDirectory();
            return new ImageService(RepositoryFactory.Instance.ProfileImageCacheRepository, profileImageDir);
        }
    }
}