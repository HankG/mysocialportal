using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace MySocialPortalLib.Model
{
    [SuppressMessage("ReSharper", "CA1056")]
    public class SocialMediaAccountData
    {
        public bool Active { get; set; }
        
        public IDictionary<string, string> AdditionalProperties { get; set; }
        
        public string Id { get; set; }
        
        public string ProfileUrl { get; set; }
        
        public string ProfileId { get; set; }
        
        public string ProfilePhotoPath { get; set; }
        
        public string RealName { get; set; }
        
        public string SocialMediaSystemName { get; set; }

        public SocialMediaAccountData()
        {
            Active = true;
            AdditionalProperties = new Dictionary<string, string>();
            Id = Guid.NewGuid().ToString();
            ProfileId = "";
            ProfilePhotoPath = "";
            ProfileUrl = "";
            RealName = "";
            SocialMediaSystemName = "";
        }

        public bool AllEquals(SocialMediaAccountData other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Active == other.Active 
                   && AdditionalProperties.Count == other.AdditionalProperties.Count
                   && AdditionalProperties.All(other.AdditionalProperties.Contains)
                   && Id == other.Id 
                   && ProfileUrl == other.ProfileUrl 
                   && ProfileId == other.ProfileId 
                   && ProfilePhotoPath == other.ProfilePhotoPath 
                   && RealName == other.RealName 
                   && SocialMediaSystemName == other.SocialMediaSystemName;
        }

        public SocialMediaAccountData Copy()
        {
            var newValue = new SocialMediaAccountData
            {
                Id = this.Id,
                Active = this.Active,
                ProfileId = this.ProfileId,
                ProfilePhotoPath = this.ProfilePhotoPath,
                ProfileUrl = this.ProfileUrl,
                RealName = this.RealName,
                SocialMediaSystemName = this.SocialMediaSystemName
            };
            foreach (var (k,v)  in AdditionalProperties)
            {
                newValue.AdditionalProperties[k] = v;
            }

            return newValue;
        }

        protected bool Equals(SocialMediaAccountData other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((SocialMediaAccountData) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }
    }
}