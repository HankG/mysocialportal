using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace MySocialPortalLib.Model
{
    [SuppressMessage("ReSharper", "CA2227")]
    public class Person
    {
        public IDictionary<string, string> AdditionalProperties { get; set; }
        
        public List<LabeledValue<string>> Addresses { get; set; }
        
        public string Birthday { get; set; }
        
        public List<LabeledValue<string>> Emails { get; set; }
        
        public string Id { get; set; }
        
        public string Name { get; set; }
        
        public List<LabeledValue<string>> PhoneNumbers { get; set; }

        public Dictionary<string, SocialMediaAccountData> SocialMediaAccounts { get; set; }

        public IList<string> SocialMediaAccountNames => SocialMediaAccounts.Keys.ToList();
        
        public List<LabeledValue<string>> Websites { get; set; }

        public Person()
        {
            AdditionalProperties = new Dictionary<string, string>();
            Addresses = new List<LabeledValue<string>>();
            Birthday = "";
            Id = Guid.NewGuid().ToString();
            Name = "";
            Emails = new List<LabeledValue<string>>();
            PhoneNumbers = new List<LabeledValue<string>>();
            SocialMediaAccounts = new Dictionary<string, SocialMediaAccountData>();
            Websites = new List<LabeledValue<string>>();
        }

        public bool AllEquals(Person other)
        {
            if (other == null)
            {
                return false;
            }
            
            return AdditionalProperties.Keys.Count == other.AdditionalProperties.Keys.Count
                   && AdditionalProperties.All(other.AdditionalProperties.Contains)
                   && Addresses.Count == other.Addresses.Count
                   && Addresses.All(other.Addresses.Contains)
                   && Birthday == other.Birthday
                   && Emails.Count == other.Addresses.Count
                   && Emails.All(other.Emails.Contains)
                   && Id == other.Id
                   && Name == other.Name
                   && PhoneNumbers.Count == other.PhoneNumbers.Count
                   && PhoneNumbers.All(other.PhoneNumbers.Contains)
                   && SocialMediaAccounts.Count == other.SocialMediaAccounts.Count
                   && SocialMediaAccounts.All(other.SocialMediaAccounts.Contains)
                   && Websites.Count == other.Websites.Count
                   && Websites.All(other.Websites.Contains);
        }

        protected bool Equals(Person other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((Person) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }
    }
}