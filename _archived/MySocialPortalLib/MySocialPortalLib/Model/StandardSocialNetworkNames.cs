namespace MySocialPortalLib.Model
{
    public static class StandardSocialNetworkNames
    {
        public const string Diaspora = "Diaspora";
        public const string Facebook = "Facebook";
        public const string Friendica = "Friendica";
        public const string Instagram = "Instagram";
        public const string Mastodon = "Mastodon";
        public const string PixelFed = "PixelFed";
        public const string PeerTube = "PeerTube";
        public const string Pleroma = "Pleroma";
        public const string SocialHome = "SocialHome";
        public const string Twitter = "Twitter";
    }
}