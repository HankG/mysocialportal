using System;

namespace MySocialPortalLib.Model
{
    public class TimelineInterval
    {
        public string Id { get; set; }

        public ulong IntervalStart { get; set; }
        
        public ulong IntervalStop { get; set; }
        
        public string TimelineName { get; set; }

        public TimelineInterval()
        {
            Id = Guid.NewGuid().ToString();
            IntervalStart = 0;
            IntervalStop = 0;
            TimelineName = "";
        }

        public bool AllEquals(TimelineInterval other)
        {
            return Id == other.Id
                   && TimelineName == other.TimelineName
                   && IntervalStart == other.IntervalStart
                   && IntervalStop == other.IntervalStop;
        }

        public TimelineInterval Copy()
        {
            return new TimelineInterval
            {
                Id = this.Id,
                IntervalStart = this.IntervalStart,
                IntervalStop = this.IntervalStop,
                TimelineName = this.TimelineName
            };
        }

        protected bool Equals(TimelineInterval other)
        {
            if (other == null)
            {
                return false;
            }
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((TimelineInterval) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }

        public override string ToString()
        {
            return $"{TimelineName}: {IntervalStart} -> {IntervalStop}";
        }
    }
}