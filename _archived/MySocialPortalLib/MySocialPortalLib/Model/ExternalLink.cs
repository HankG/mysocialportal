using System;
using System.Diagnostics.CodeAnalysis;

namespace MySocialPortalLib.Model
{
    [SuppressMessage("ReSharper", "CA1056")]
    public class ExternalLink
    {
        public string Id { get; set; }
        public bool IsKnownPost => !String.IsNullOrWhiteSpace(PostId);
        public string Name { get; set; }
        
        public string PostId { get; set; }

        public string Source { get; set; }

        public string Url { get; set; }

        public ExternalLink()
        {
            Id = Guid.NewGuid().ToString();
            Name = "";
            PostId = "";
            Source = "";
            Url = "";
        }

        public bool AllEquals(ExternalLink other)
        {
            if (other == null) return false;
            return Id == other.Id 
                   && Name == other.Name 
                   && PostId == other.PostId 
                   && Source == other.Source 
                   && Url == other.Url;
        }

        protected bool Equals(ExternalLink other)
        {
            return Id == other?.Id;
        }

        public override bool Equals(object? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((ExternalLink) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }
    }
}