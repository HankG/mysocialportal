using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MySocialPortalLib.Util;

namespace MySocialPortalLib.Model
{
    [SuppressMessage("ReSharper", "CA1056")]
    public class Post
    {
        public string Body { get; set; }   
        
        public List<EventData> Events { get; set; }
        
        public string Id { get; set; }
        
        public List<Interaction> Interactions { get; set; }

        public List<ExternalLink> Links { get; set; }
        
        public List<MediaData> Media { get; set; }
        
        public string OriginalLinkUrlOrId { get; set; }
        
        public string OriginalSocialMediaSystem { get; set; }
        
        public List<Place> Places { get; set; }
        
        public List<Poll> Polls { get; set; }

        public DateTimeOffset PostDateTime { get; set; }

        public List<ExternalLink> RelatedPosts { get; set; }

        public string Title { get; set; }
        
        public List<string> TaggedUsers { get; set; }
        
        public List<string> Tags { get; set; }

        public string UserId { get; set; }

        public Post()
        {
            Body = "";
            Events = new List<EventData>();
            Id = Guid.NewGuid().ToString();
            Interactions = new List<Interaction>();
            Links = new List<ExternalLink>();
            Media = new List<MediaData>();
            OriginalLinkUrlOrId = "";
            OriginalSocialMediaSystem = "";
            Places = new List<Place>();
            Polls = new List<Poll>();
            PostDateTime = DateTimeOffset.UnixEpoch;
            RelatedPosts = new List<ExternalLink>();
            Title = "";
            TaggedUsers = new List<string>();
            Tags = new List<string>();
            UserId = "";
        }
        
        public bool AllEquals(Post other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Body == other.Body
                   && Events.Count == other.Events.Count
                   && Events.All(other.Events.Contains)
                   && Id == other.Id
                   && Interactions.Count == other.Interactions.Count
                   && Interactions.All(other.Interactions.Contains)
                   && Links.Count == other.Links.Count
                   && Links.All(other.Links.Contains)
                   && Media.Count == other.Media.Count
                   && Media.All(other.Media.Contains)
                   && OriginalLinkUrlOrId == other.OriginalLinkUrlOrId
                   && OriginalSocialMediaSystem == other.OriginalSocialMediaSystem
                   && Places.Count == other.Places.Count
                   && Places.All(other.Places.Contains)
                   && Polls.Count == other.Polls.Count
                   && Polls.All(other.Polls.Contains)
                   && PostDateTime == other.PostDateTime
                   && RelatedPosts.Count == other.RelatedPosts.Count
                   && RelatedPosts.All(other.RelatedPosts.Contains)
                   && Title == other.Title
                   && TaggedUsers.Count == other.TaggedUsers.Count
                   && TaggedUsers.All(other.TaggedUsers.Contains)
                   && Tags.Count == other.Tags.Count
                   && Tags.All(other.Tags.Contains)
                   && UserId == other.UserId;
        }

        public void AddTags(IEnumerable<string> newTags)
        {
            var tags = new List<string>(Tags);
            PostMergingUtils.MergeStringCollections(tags, newTags);
            Tags.Clear();
            Tags.AddRange(tags.Select(t => t.ToUpperInvariant()));
        }
        
        protected bool Equals(Post other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((Post) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }
    }
}
