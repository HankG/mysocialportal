using System.IO;
using MySocialPortalLib.Service;

namespace MySocialPortalLib.Model.Settings
{
    public class TwitterConnectorSettings
    {
        private const string ConfigSettingsSectionName = "TwitterConnectorSettings";
        private const string CredentialsPathSettingName = "CredentialsPath";

        public string? CredentialsPath { get; set; }

        public TwitterConnectorSettings()
        {
            var config = MainSettings.GetRootConfiguration();
            var twitterSettings = config.GetSection(ConfigSettingsSectionName);
            CredentialsPath = twitterSettings?[CredentialsPathSettingName];
            if (string.IsNullOrWhiteSpace(CredentialsPath))
            {
                CredentialsPath = Path.Combine(DirectoryServices.Instance.UserCredentialsSettingsDirectory(),
                    StandardSocialNetworkNames.Twitter + ".json");
            }
        }
    }
}