using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace MySocialPortalLib.Model.Settings
{
    public class MainSettings
    {
        private const string AppSettingsFilename = "appsettings.json";
        private const string ApplicationNameKey = "ApplicationName";
        private const string DefaultApplicationName = "MySocialPortalLib";
        private const string LibSectionName = "SocialPortalLib";

        public string ApplicationName { get; }

        public MainSettings()
        {
            var config = GetRootConfiguration();
            var libSettings = config.GetSection(LibSectionName);
            ApplicationName = libSettings?[ApplicationNameKey] ?? DefaultApplicationName;
        }

        public static IConfigurationRoot GetRootConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(AppSettingsFilename, true, false)
                .Build();
        }
    }
}