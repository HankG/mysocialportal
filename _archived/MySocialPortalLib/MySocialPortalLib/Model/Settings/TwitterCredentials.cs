namespace MySocialPortalLib.Service.SocialMediaConnectors
{
    public class TwitterCredentials
    {
        public string AccessToken { get; set; }
            
        public string AccessTokenSecret { get; set; }
            
        public string ConsumerKey { get; set; }
            
        public string ConsumerSecret { get; set; }

        public TwitterCredentials()
        {
            AccessToken = "";
            AccessTokenSecret = "";
            ConsumerKey = "";
            ConsumerSecret = "";
        }
    }
}