using System;
using System.Collections.Generic;
using System.Linq;

namespace MySocialPortalLib.Model
{
    public class Poll
    {
        public string Id { get; set; }
        public string Question { get; set; }
        
        public List<PollOption> Options { get; set; }

        public Poll()
        {
            Id = Guid.NewGuid().ToString();
            Options = new List<PollOption>();
            Question = "";
        }
        
        protected bool Equals(Poll other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((Poll) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }

        public bool AllEquals(Poll other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Question == other.Question
                   && Id == other.Id
                   && Options.Count == other.Options.Count
                   && Options.All(other.Options.Contains);
        }
    }
}