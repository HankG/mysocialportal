namespace MySocialPortalLib.Model
{
    public static class StandardInteractionNames
    {
        public const string Angry = "Angry";
        public const string Celebrate = "Celebrate";
        public const string Dislike = "Dislike";
        public const string Laugh = "Laugh";
        public const string Like = "Like";
        public const string Love = "Love";
        public const string Sad = "Sad";
        public const string Wow = "Wow";
    }
}