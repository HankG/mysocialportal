using System;
using System.Diagnostics.CodeAnalysis;

namespace MySocialPortalLib.Model
{
    [SuppressMessage("ReSharper", "CA1056")]
    public class FileCacheItem
    {
        public DateTimeOffset CreationTime { get; set; }

        public string Id { get; set; }
        
        public string LocalFileName { get; set; }
        
        public string OriginalUrl { get; set; }

        public FileCacheItem()
        {
            Id = Guid.NewGuid().ToString();
            OriginalUrl = "";
            LocalFileName = "";
            CreationTime = DateTimeOffset.UnixEpoch;
        }

        public bool AllEquals(FileCacheItem other)
        {
            const double threshold = 2.0;
            if (other == null)
            {
                return false;
            }
            return Math.Abs(CreationTime.Subtract(other.CreationTime).TotalMilliseconds) < threshold
                   && Id == other.Id 
                   && LocalFileName == other.LocalFileName 
                   && OriginalUrl == other.OriginalUrl;
        }

        protected bool Equals(FileCacheItem other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((FileCacheItem) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }
    }
}