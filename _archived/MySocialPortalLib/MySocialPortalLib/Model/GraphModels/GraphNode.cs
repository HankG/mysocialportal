using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.Json.Serialization;

namespace MySocialPortalLib.Model.GraphModels
{
    public class GraphNode
    {
        public string Id { get; set; }
        
        public Dictionary<string, Dictionary<string, ConnectionData>> NeighborConnectionData { get; set; }
        
        public List<string> NeighborIds { get; set; }

        public GraphNode():this("")
        {
            
        }
        
        public GraphNode(string id)
        {
            Id = id;
            NeighborConnectionData = new Dictionary<string, Dictionary<string, ConnectionData>>();
            NeighborIds = new List<string>();
        }

        public void AddOrUpdateNeighbor(GraphNode node, ConnectionData newData)
        {
            if (node == null)
            {
                throw new ArgumentNullException(nameof(node));
            }
            if (newData == null)
            {
                throw new ArgumentNullException(nameof(newData));
            }

            AddOrUpdateNeighbor(node.Id, newData);
        }

        public void AddOrUpdateNeighbor(string nodeId, ConnectionData newData)
        {
            if (newData == null)
            {
                throw new ArgumentNullException(nameof(newData));
            }
            
            if (!NeighborConnectionData.ContainsKey(nodeId))
            {
                NeighborConnectionData[nodeId] = new Dictionary<string, ConnectionData>();
                NeighborIds.Add(nodeId);
            }

            NeighborConnectionData[nodeId][newData.NetworkName] = newData;
        }

        public bool DeleteNeighbor(GraphNode node)
        {
            return node != null && DeleteNeighbor(node.Id);
        }

        public bool DeleteNeighbor(string nodeId)
        {
            if (!NeighborConnectionData.ContainsKey(nodeId))
            {
                return false;
            }

            return NeighborConnectionData.Remove(nodeId) && NeighborIds.Remove(nodeId);
        }

        public List<ConnectionData> GetConnectionData(GraphNode node)
        {
            if (node == null)
            {
                return new List<ConnectionData>();
            }
            
            return NeighborConnectionData.TryGetValue(node.Id, out var data) ? data.Values.ToList() 
                : new List<ConnectionData>();
        }

        public ConnectionData? GetConnectionData(GraphNode node, string networkName)
        {
            if (node == null || string.IsNullOrWhiteSpace(networkName))
            {
                return null;
            }
            
            return GetConnectionData(node.Id, networkName);
        }

        public ConnectionData? GetConnectionData(string nodeId, string networkName)
        {
            if (string.IsNullOrEmpty(nodeId) || string.IsNullOrEmpty(networkName))
            {
                return null;
            }
            
            return NeighborConnectionData[nodeId]?[networkName];
        }

        public bool RemoveNetworkConnection(GraphNode node, string networkName)
        {
            if (node == null || string.IsNullOrWhiteSpace(networkName))
            {
                return false;
            }
            
            if (!NeighborConnectionData.ContainsKey(node.Id))
            {
                return false;
            }

            var connectionData = NeighborConnectionData[node.Id];

            if (!connectionData.ContainsKey(networkName))
            {
                return false;
            }

            var success = connectionData.Remove(networkName);

            if (connectionData.Count == 0)
            {
                return DeleteNeighbor(node);
            }
            
            return success;
        }
        
        public bool AllEquals(GraphNode other)
        {
            if (other == null)
            {
                return false;
            }
            
            var idEquals = Id == other.Id;
            var neighborCount = NeighborConnectionData.Count == other.NeighborConnectionData.Count;
            var neighborKeys = NeighborConnectionData.Keys.All(other.NeighborConnectionData.ContainsKey);
            var valuesMatch = true;
            foreach (var k in NeighborConnectionData.Keys)
            {
                var cd1 = NeighborConnectionData[k];
                var cd2 = other.NeighborConnectionData[k];
                valuesMatch &= cd1.Count == cd2.Count;
                valuesMatch &= cd1.Keys.All(cd2.ContainsKey);
                foreach (var cdk in cd1.Keys)
                {
                    var nd1 = cd1[cdk];
                    var nd2 = cd2[cdk];
                    valuesMatch &= nd1.AllEquals(nd2);
                }
            }
            var nodesCount = NeighborIds.Count == other.NeighborIds.Count;
            var nodesEqual = NeighborIds.All(other.NeighborIds.Contains);

            return idEquals && neighborCount && neighborKeys && valuesMatch && nodesCount && nodesEqual;
        }
        
        protected bool Equals(GraphNode other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }

            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((GraphNode) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }
    }
}