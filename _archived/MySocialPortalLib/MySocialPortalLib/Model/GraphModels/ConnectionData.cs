using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace MySocialPortalLib.Model.GraphModels
{
    public class ConnectionData
    {
        public string NetworkName { get; set; }
        
        public List<string> AssociationAttributes { get; set; }
        
        public DateTimeOffset ConnectedSince { get; set; }

        public bool HasConnection => AssociationAttributes.Count > 0;

        public ConnectionData():this("", new List<string>())
        {

        }

        public ConnectionData(string networkName, List<string> attributes):this(networkName, DateTimeOffset.UtcNow, attributes)
        {

        }
        
        public ConnectionData(string networkName, DateTimeOffset connectedSince, List<string> attributes)
        {
            NetworkName = networkName;
            ConnectedSince = connectedSince;
            AssociationAttributes = attributes;
        }
        
        public bool AllEquals(ConnectionData other)
        {
            if (other == null) return false;
            return NetworkName == other.NetworkName
                   && ConnectedSince == other.ConnectedSince
                   && AssociationAttributes.Count == other.AssociationAttributes.Count
                   && AssociationAttributes.All(other.AssociationAttributes.Contains);
        }
    }
}