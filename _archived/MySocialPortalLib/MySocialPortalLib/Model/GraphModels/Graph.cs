using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace MySocialPortalLib.Model.GraphModels
{
    public class Graph
    {
        public Dictionary<string, GraphNode> Nodes { get; set; }

        public Graph() : this(new Dictionary<string, GraphNode>()) {}
        
        public Graph(Dictionary<string, GraphNode> nodes)
        {
           Nodes = nodes;
        }

        public void AddNode(GraphNode node)
        {
            if (node == null)
            {
                return;
            }
            
            Nodes.Add(node.Id, node);
        }

        public GraphNode AddNode(string id)
        {
            var node = new GraphNode(id);
            Nodes.Add(id, node);
            return node;
        }

        public static void AddDirectedEdge(GraphNode from, GraphNode to, ConnectionData data)
        {
            if (from == null)
            {
                throw new ArgumentNullException(nameof(from));
            }
            
            if (to == null)
            {
                throw new ArgumentNullException(nameof(to));
            }
            
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            
            from.AddOrUpdateNeighbor(to, data);
        }

        public static void AddUndirectedEdge(GraphNode from, GraphNode to, ConnectionData data)
        {
            if (from == null)
            {
                throw new ArgumentNullException(nameof(from));
            }
            
            if (to == null)
            {
                throw new ArgumentNullException(nameof(to));
            }
            
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            from.AddOrUpdateNeighbor(to, data);
            to.AddOrUpdateNeighbor(from, data);
        }

        public bool Remove(GraphNode node)
        {
            if (node == null)
            {
                return false;
            }
            
            return Remove(node.Id);
        }

        public bool Remove(string id)
        {
            if (!Nodes.TryGetValue(id, out var node))
            {
                return false;
            }
            node.NeighborIds.Clear();
            node.NeighborConnectionData.Clear();
            foreach (var otherNode in Nodes.Values)
            {
                otherNode.NeighborIds.Remove(id);
                otherNode.NeighborConnectionData.Remove(id);
            }
                
            Nodes.Remove(id);

            return true;

        }
        
        public bool IsReachable(GraphNode source, GraphNode destination)
        {
            if (source == null || destination == null)
            {
                return false;
            }
            
            var visited = new Dictionary<GraphNode, bool>();
            var queue = new Queue<GraphNode>();

            if (!Nodes.ContainsKey(source.Id) || !Nodes.ContainsKey(destination.Id))
            {
                return false;
            }
            
            visited[source] = true;
            queue.Enqueue(source);

            while (queue.Count != 0)
            {
                var currentNode = queue.Dequeue();
                foreach (var neighborId in currentNode.NeighborIds)
                {
                    var neighbor = Nodes[neighborId];
                    if (neighbor == null)
                    {
                        continue;
                    }
                    
                    if (neighbor.Equals(destination))
                    {
                        return true;
                    }

                    if (visited.ContainsKey(neighbor))
                    {
                        continue;
                    }

                    visited[neighbor] = true;
                    queue.Enqueue(neighbor);
                }
            }
            
            return false;
        }

        public bool AllEquals(Graph other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Nodes.Count == other.Nodes.Count
                   && Nodes.All(other.Nodes.Contains);
        }
    }

}