using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace MySocialPortalLib.Model.GraphModels
{
    public class SocialGraphManager
    {
        public Graph RawGraph { get; set; }

        public SocialGraphManager()
        {
            RawGraph = new Graph();
        }

        public bool AddNetworkConnection(string personId1, string personId2, string networkName, string networkAttribute)
        {
            return AddNetworkConnection(personId1, personId2, networkName, new List<string>{networkAttribute});
        }

        public bool AddNetworkConnection(string personId1, string personId2, string networkName,
            List<string> networkAttributes)
        {
            if (string.IsNullOrEmpty(personId1) || string.IsNullOrEmpty(personId2) ||
                string.IsNullOrEmpty(networkName) || networkAttributes == null ||
                networkAttributes.Count == 0)
            {
                return false;
            }
            
            if (!RawGraph.Nodes.TryGetValue(personId1, out var node1))
            {
                node1 = RawGraph.AddNode(personId1);
            }
            
            if (!RawGraph.Nodes.TryGetValue(personId2, out var node2))
            {
                node2 = RawGraph.AddNode(personId2);
            }

            if (!node1.NeighborIds.Contains(personId2))
            {
                Graph.AddDirectedEdge(node1, node2, new ConnectionData(networkName, networkAttributes));
                return true;
            }

            if (!node1.NeighborConnectionData[personId2].Keys.Contains(networkName))
            {
                Graph.AddDirectedEdge(node1, node2, new ConnectionData(networkName, networkAttributes));
                return true;
            }

            var attributes = node1.NeighborConnectionData[personId2][networkName].AssociationAttributes;
            foreach (var newAttr in networkAttributes.Where(newAttr => !attributes.Contains(newAttr)))
            {
                attributes.Add(newAttr);
            }
            
            return true;
        }

        public IList<string> GetConnectionNetworks(string personId1, string personId2)
        {
            var emptyList = new List<string>();
            if (!RawGraph.Nodes.TryGetValue(personId1, out var node))
            {
                return emptyList;
            }

            if (!node.NeighborConnectionData.TryGetValue(personId2, out var ncd))
            {
                return emptyList;
            }
            
            
            return ncd.Keys.ToImmutableList();
        }

        public bool IsReachable(string personId1, string personId2)
        {
            if (!RawGraph.Nodes.TryGetValue(personId1, out var node1))
            {
                return false;
            }

            if (!RawGraph.Nodes.TryGetValue(personId2, out var node2))
            {
                return false;
            }

            return RawGraph.IsReachable(node1, node2);
        }

        public ConnectionData GetConnectedNetworkAttributes(string personId1, string personId2, string network)
        {
            var emptyList = new ConnectionData{NetworkName = "None"};
            if (!RawGraph.Nodes.TryGetValue(personId1, out var node))
            {
                return emptyList;
            }

            if (!node.NeighborConnectionData.TryGetValue(personId2, out var ncd))
            {
                return emptyList;
            }

            if (!ncd.TryGetValue(network, out var connectionData))
            {
                return emptyList;
            }

            return connectionData;
        }
        
        public bool RemoveNetworkConnection(string personId1, string personId2, string networkName)
        {
            if (!RawGraph.Nodes.TryGetValue(personId1, out var node))
            {
                return false;
            }

            if (!node.NeighborConnectionData.TryGetValue(personId2, out var ncd))
            {
                return false;
            }

            if (!ncd.TryGetValue(networkName, out var connectionData))
            {
                return false;
            }

            ncd.Remove(networkName);
            if (ncd.Count == 0)
            {
                return RemoveConnection(personId1, personId2);
            }

            return true;
        }

        public bool RemoveConnection(string personId1, string personId2)
        {
            if (!RawGraph.Nodes.TryGetValue(personId1, out var node))
            {
                return false;
            }

            if (!node.NeighborConnectionData.TryGetValue(personId2, out var ncd))
            {
                return false;
            }

            node.NeighborIds.Remove(personId2);
            node.NeighborConnectionData.Remove(personId2);

            return true;
        }

        public bool UpdateConnectedNetworkAttributes(string personId1, string personId2, string network, List<string> newAttributes, bool merge = true)
        {
            if (string.IsNullOrEmpty(personId1) || string.IsNullOrEmpty(personId2) ||
                string.IsNullOrEmpty(network) || newAttributes == null || newAttributes.Count == 0)
            {
                return false;
            }
            
            if (!RawGraph.Nodes.TryGetValue(personId1, out var node1))
            {
                return false;
            }
            
            if (!RawGraph.Nodes.TryGetValue(personId2, out var node2))
            {
                return false;
            }

            if (!node1.NeighborIds.Contains(personId2))
            {
                return false;
            }

            if (!node1.NeighborConnectionData[personId2].Keys.Contains(network))
            {
                return false;
            }

            var attributes = node1.NeighborConnectionData[personId2][network].AssociationAttributes;
            if (merge)
            {
                foreach (var newAttr in newAttributes.Where(newAttr => !attributes.Contains(newAttr)))
                {
                    attributes.Add(newAttr);
                }
            }
            else
            {
                attributes.Clear();
                attributes.AddRange(newAttributes);
            }
            
            return true;

        }

        public bool AllEquals(SocialGraphManager other)
        {
            return other != null && RawGraph.AllEquals(other.RawGraph);
        }

        public string ToCompactString()
        {
            var sb = new StringBuilder();
            foreach (var node in RawGraph.Nodes.Values)
            {
                sb.Append(node.Id).Append(Environment.NewLine);
                foreach (var kv in node.NeighborConnectionData)
                {
                    var networkAttributes = new List<string>();
                    foreach (var ncbd in kv.Value.Values)
                    {
                        networkAttributes.Add($"{ncbd.NetworkName} = {string.Join("|", ncbd.AssociationAttributes)}");
                    }
                    
                    sb.Append($"   => {kv.Key}: {string.Join(",", networkAttributes)}").Append(Environment.NewLine);
                }
            }

            return sb.ToString();
        }

    }
}