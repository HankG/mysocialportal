using System;
using System.Diagnostics.CodeAnalysis;

namespace MySocialPortalLib.Model
{
    [SuppressMessage("ReSharper", "CA1056")]
    public class LinkPreview
    {
        public string Description { get; set; }
        
        public bool HasPreviewImage { get; set; }

        public string Id { get; set; }
        
        public string PreviewImageName { get; set; }

        public string RequestUrl { get; set; }
        
        public DateTimeOffset TimeGeneraterated { get; set; }
        public string Title { get; set; }
        
        public bool UrlFound { get; set; }

        public LinkPreview()
        {
            Id = Guid.NewGuid().ToString();
            UrlFound = false;
            HasPreviewImage = false;
            PreviewImageName = "";
            Title = "";
            Description = "";
            TimeGeneraterated = DateTimeOffset.UtcNow;
            RequestUrl = "";
        }

        protected bool Equals(LinkPreview other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((LinkPreview) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }

        public bool AllEquals(LinkPreview other)
        {
            const double threshold = 2.0;
            if (other == null)
            {
                return false;
            }
            
            return Description == other.Description 
                   && HasPreviewImage == other.HasPreviewImage 
                   && Id == other.Id 
                   && PreviewImageName == other.PreviewImageName 
                   && RequestUrl == other.RequestUrl 
                   && Math.Abs(TimeGeneraterated.Subtract(other.TimeGeneraterated).TotalMilliseconds) < threshold
                   && Title == other.Title 
                   && UrlFound == other.UrlFound;
        }
        
    }
}