using System;

namespace MySocialPortalLib.Model
{
    public class EventData
    {
        public static readonly DateTimeOffset DateNotSetValue = DateTimeOffset.MinValue;
        public string Id { get; set; }
        
        public string Title { get; set; }

        public Place Place { get; set; }

        public DateTimeOffset StartTime { get; set; }
        
        public DateTimeOffset StopTime { get; set; }

        public EventData()
        {
            Id = Guid.NewGuid().ToString();
            Title = "";
            StartTime = DateNotSetValue;
            StopTime = DateNotSetValue;
            Place = new Place();
        }

        
        public bool AllEquals(EventData other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id 
                   && Title == other.Title
                   && StartTime == other.StartTime 
                   && StartTime == other.StartTime 
                   && Equals(Place, other.Place);
        }

        protected bool Equals(EventData other)
        {
            return Id == other?.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((EventData) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? string.GetHashCode(Id, StringComparison.InvariantCulture) : 0);
        }
    }
}