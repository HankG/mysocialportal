using System;
using System.Diagnostics.CodeAnalysis;

namespace MySocialPortalLib.Model
{
    /// <summary>
    /// Data class for locations, including name, physical address and latitude longitude values (all optional)
    /// </summary>
    [SuppressMessage("ReSharper", "CA1056")]
    public class Place
    {
        private const double MaxLatitude = 90.0;
        private const double MinLatitude = -90.0;
        private const double MaxLongitude = 180.0;
        private const double MinLongitude = -180.0;
        
        private double _longitude;
        private double _latitude;
        
        /// <summary>
        /// Altitude should be represented in meters if being set
        /// </summary>
        public double Altitude { get; set; }
        
        public string Id { get; set; }
        
        public string Name { get; set; }
        
        public string Url { get; set; }
        
        public string PhysicalAddress { get; set; }

        /// <summary>
        /// Should be decimal values between -90 and 90
        /// </summary>
        public double Latitude
        {
            get => _latitude;
            set
            {
                if (value > MaxLatitude || value < MinLatitude)
                {
                    throw new ArgumentOutOfRangeException($"Latitude must be between {MinLatitude} and {MaxLatitude}: {value}");
                }
                _latitude = value;
                HasLatLon = true;
            }
        }
        
        /// <summary>
        /// Should be decimal values between -180 and 180
        /// </summary>
        public double Longitude
        {
            get => _longitude;
            set
            {
                if (value > MaxLongitude || value < MinLongitude)
                {
                    throw new ArgumentOutOfRangeException($"Latitude must be between {MinLongitude} and {MaxLongitude}: {value}");
                }
                _longitude = value;
                HasLatLon = true;
            }
        }
        
        public bool HasLatLon { get; set; }

        public Place()
        {
            HasLatLon = false;
            Id = Guid.NewGuid().ToString();
            Name = "";
            Url = "";
            PhysicalAddress = "";
        }

        public bool AllEquals(Place other)
        {
            if (other == null)
            {
                return false;
            }
            
            return _longitude.Equals(other._longitude) 
                   && _latitude.Equals(other._latitude) 
                   && Altitude.Equals(other.Altitude) 
                   && Id == other.Id
                   && Name == other.Name 
                   && Url == other.Url 
                   && PhysicalAddress == other.PhysicalAddress 
                   && HasLatLon == other.HasLatLon;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((Place) other);
        }

        protected bool Equals(Place other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }
    }
}