using System;
using System.Collections.Generic;

namespace MySocialPortalLib.Model
{
    public class LabeledValue<T>
    {
        public string Label { get; set; }
        
        public T Value { get; set; }

        public LabeledValue()
        {
            Label = "";
            Value = default;
        }

        public LabeledValue(string label, T value)
        {
            Label = label;
            Value = value;
        }

        protected bool Equals(LabeledValue<T> other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Label == other.Label && EqualityComparer<T>.Default.Equals(Value, other.Value);
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((LabeledValue<T>) other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Label != null ? Label.GetHashCode(StringComparison.InvariantCulture) : 0) * 397) ^ EqualityComparer<T>.Default.GetHashCode(Value);
            }
        }
    }
}
