using System;
using System.Diagnostics.CodeAnalysis;

namespace MySocialPortalLib.Model
{
    [SuppressMessage("ReSharper", "CA1056")]
    public class MediaData
    {
        public DateTimeOffset CreationDateTime { get; set; }

        public string Description { get; set; }
        
        public string Id { get; set; }
        
        public string LocalPath { get; set; }

        public string MediaType { get; set; }

        public string Title { get; set; }

        public string OriginalUrl { get; set; }

        public MediaData()
        {
            Id = Guid.NewGuid().ToString();
            Description = "";
            LocalPath = "";
            MediaType = "";
            Title = "";
            OriginalUrl = "";
            CreationDateTime = DateTimeOffset.UnixEpoch;
        }

        public bool AllEquals(MediaData other)
        {
            if (other == null)
            {
                return false;
            }
            
            return CreationDateTime == other.CreationDateTime 
                   && Id == other.Id
                   && Title == other.Title 
                   && Description == other.Description 
                   && LocalPath == other.LocalPath 
                   && MediaType == other.MediaType 
                   && OriginalUrl == other.OriginalUrl;
        }

        protected bool Equals(MediaData other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((MediaData) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }
    }
}