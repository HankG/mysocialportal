using System;
using System.Collections.Generic;
using System.Linq;
using MySocialPortalLib.Util;

namespace MySocialPortalLib.Model
{
    public class Comment
    {
        public string Body { get; set; }   
        
        public string Id { get; set; }
        
        public List<Interaction> Interactions { get; set; }
        
        public List<ExternalLink> Links { get; set; }
        
        public List<MediaData> Media { get; set; }
        
        public string OriginalLinkUrlOrId { get; set; }
        
        public string OriginalSocialMediaSystem { get; set; }
        
        public string ParentPost { get; set; }
        
        public string ParentComment { get; set; }

        public DateTimeOffset PostDateTime { get; set; }

        public List<string> TaggedUsers { get; set; }
        
        public List<string> Tags { get; set; }

        public string UserId { get; set; }

        public Comment()
        {
            Body = "";
            Id = Guid.NewGuid().ToString();
            Interactions = new List<Interaction>();
            Links = new List<ExternalLink>();
            Media = new List<MediaData>();
            OriginalLinkUrlOrId = "";
            OriginalSocialMediaSystem = "";
            PostDateTime = DateTimeOffset.UnixEpoch;
            TaggedUsers = new List<string>();
            Tags = new List<string>();
            UserId = "";
        }
        
        public bool AllEquals(Comment other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Body == other.Body
                   && Id == other.Id
                   && Interactions.Count == other.Interactions.Count
                   && Interactions.All(other.Interactions.Contains)
                   && Links.Count == other.Links.Count
                   && Links.All(other.Links.Contains)
                   && Media.Count == other.Media.Count
                   && Media.All(other.Media.Contains)
                   && OriginalLinkUrlOrId == other.OriginalLinkUrlOrId
                   && OriginalSocialMediaSystem == other.OriginalSocialMediaSystem
                   && ParentComment == other.ParentComment
                   && ParentPost == other.ParentPost
                   && PostDateTime == other.PostDateTime
                   && TaggedUsers.Count == other.TaggedUsers.Count
                   && TaggedUsers.All(other.TaggedUsers.Contains)
                   && Tags.Count == other.Tags.Count
                   && Tags.All(other.Tags.Contains)
                   && UserId == other.UserId;
        }

        public void AddTags(IEnumerable<string> newTags)
        {
            var tags = new List<string>(Tags);
            PostMergingUtils.MergeStringCollections(tags, newTags);
            Tags.Clear();
            Tags.AddRange(tags.Select(t => t.ToUpperInvariant()));
        }
        
        protected bool Equals(Comment other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((Comment) other);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode(StringComparison.InvariantCulture) : 0);
        }        
    }
}