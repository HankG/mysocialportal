using System;

namespace MySocialPortalLib.Model
{
    public class PollOption
    {
        public string Text { get; set; }
            
        public bool Vote { get; set; }

        public PollOption() : this("", false)
        {
                
        }

        public PollOption(string text, bool vote)
        {
            Text = text;
            Vote = vote;
        }

        protected bool Equals(PollOption other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Text == other.Text && Vote == other.Vote;
        }

        public override bool Equals(object? other)
        {
            if (other == null)
            {
                return false;
            }
            
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((PollOption) other);
        }

        public override int GetHashCode()
        {
            return ((Text != null ? Text.GetHashCode(StringComparison.InvariantCulture) : 0) * 397) ^ Vote.GetHashCode();
        }
    }

}