using System;

namespace MySocialPortalLib.Model
{
    public class Interaction
    {
        public string Id { get; set; }
        
        public string PostId { get; set; }
        
        public string Type { get; set; }
        
        public string UserId { get; set; }

        public Interaction()
        {
            Id = Guid.NewGuid().ToString();
            PostId = "";
            Type = "";
            UserId = "";
        }

        public bool AllEquals(Interaction other)
        {
            if (other == null)
            {
                return false;
            }
            
            return Id == other.Id && PostId == other.PostId && Type == other.Type && UserId == other.UserId;
        }

        protected bool Equals(Interaction other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Interaction) obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}