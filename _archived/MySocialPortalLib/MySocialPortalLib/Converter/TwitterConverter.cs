using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using LinqToTwitter;
using MySocialPortalLib.Model;
using MySocialPortalLib.Repository;
using MySocialPortalLib.Service;
using Place = MySocialPortalLib.Model.Place;

namespace MySocialPortalLib.Converter
{
    public class TwitterConverter
    {
        public IPersonsRepository PersonsRepository { get; }

        public TwitterConverter(IPersonsRepository personsRepository)
        {
            PersonsRepository = personsRepository;
        }
        
        
        public Post TweetToPost(Status tweet)
        {
            if (tweet == null)
            {
                throw new ArgumentNullException(nameof(tweet));
            }
            
            var person = TwitterUserToPerson(tweet.User);
            if (person == null)
            {
                throw new NullReferenceException();
            }
            
            var post = new Post
            {
                Body = string.IsNullOrWhiteSpace(tweet.Text) ? tweet.FullText : tweet.Text ?? "",
                UserId = person.Id,
                OriginalSocialMediaSystem = StandardSocialNetworkNames.Twitter,
                OriginalLinkUrlOrId = tweet.StatusID.ToString(CultureInfo.InvariantCulture),
                PostDateTime = tweet.CreatedAt
            };
            if (tweet?.Entities?.MediaEntities?.Count > 0)
            {
                post.Media.AddRange(TwitterMediaEntitiesToMedia(tweet.Entities.MediaEntities));
            }

            if (tweet?.Entities?.UrlEntities?.Count > 0)
            {
                post.Links.AddRange(TwitterUrlEntitiesToLinks(tweet.Entities.UrlEntities));
            }

            if (tweet?.Entities?.HashTagEntities?.Count > 0)
            {
                post.AddTags(tweet.Entities.HashTagEntities.Select(h => h.Text));
            }

            if (tweet?.Entities?.UserMentionEntities?.Count > 0)
            {
                post.TaggedUsers.AddRange(tweet.Entities.UserMentionEntities.Select(u => u.ScreenName));
            }

            var place = new Place();
            var hasPlace = false;
            if (tweet?.Coordinates?.IsLocationAvailable == true)
            {
                hasPlace = true;
                place.Latitude = tweet.Coordinates.Latitude;
                place.Longitude = tweet.Coordinates.Longitude;
            }

            if (!string.IsNullOrWhiteSpace(tweet?.Place?.ID))
            {
                hasPlace = true;
                place.Name = tweet.Place?.Name ?? "";
                place.Url = tweet.Place?.Url ?? "";
            }

            if (hasPlace)
            {
                post.Places.Add(place);
            }

            return post;
        }

        private Person? TwitterUserToPerson(User user, bool createIfNotFound = true)
        {
            var fromTwitterEmailLabel = "From Twitter Profile";
            var createdAtProperty = "CreatedAt";
            var followingMeProperty = "FollowingMe";
            var descriptionProperty = "Description";
            var screenNameProperty = "ScreenName";
            var twitterIdAsString = user.UserIDResponse;
            var person = PersonsRepository.FindByNetworkId(StandardSocialNetworkNames.Twitter, twitterIdAsString);
            if (person != null || !createIfNotFound) return person;
            
            person = new Person
            {
                Name = user.Name,
            };
            if (!string.IsNullOrWhiteSpace(user.Email))
            {
                person.Emails.Add(new LabeledValue<string>(fromTwitterEmailLabel,user.Email));
            }
                
            var smd = new SocialMediaAccountData
            {
                SocialMediaSystemName = StandardSocialNetworkNames.Twitter,
                ProfileId = twitterIdAsString,
                ProfilePhotoPath = user.ProfileImageUrlHttps,
                ProfileUrl = user.Url,
                RealName = user.Name,
                Active = true,
            };
            smd.AdditionalProperties.Add(createdAtProperty, user.CreatedAt.ToString(CultureInfo.InvariantCulture));
            smd.AdditionalProperties.Add(followingMeProperty, user.Following.ToString(CultureInfo.InvariantCulture));
            smd.AdditionalProperties.Add(descriptionProperty, user.Description);
            smd.AdditionalProperties.Add(screenNameProperty, user.ScreenNameResponse);
            person.SocialMediaAccounts.Add(StandardSocialNetworkNames.Twitter, smd);
            PersonsRepository.AddPerson(person);

            return person;
        }

        private static List<MediaData> TwitterMediaEntitiesToMedia(List<MediaEntity> entities)
        {
            var media = new List<MediaData>();
            
            entities.ForEach(e =>
            {
                var nm = new MediaData
                {
                    OriginalUrl = e.MediaUrlHttps,
                    Description = e.AltText,
                    MediaType = e.Type
                };
                media.Add(nm);
            });
            
            return media;
        }

        private static List<ExternalLink> TwitterUrlEntitiesToLinks(List<UrlEntity> entities)
        {
            var links = new List<ExternalLink>();

            entities.ForEach(e =>
            {
                var link = new ExternalLink
                {
                    Source = StandardSocialNetworkNames.Twitter,
                    Url = e.ExpandedUrl
                };
                links.Add(link);
            });
            return links;
        }
    }
}