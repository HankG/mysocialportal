#!/bin/bash
version="1.0.0-alpha3"
library_path="MySocialPortalLib/bin/Debug/MySocialPortalLib"
nuget_lib_folder="mysocialportallib"

dotnet build
rm -rf ~/.private_nuget/$nuget_lib_folder/$version
rm -rf ~/.nuget/packages/$nuget_lib_folder/$version

nuget add $library_path.$version.nupkg -Source ~/.private_nuget/

