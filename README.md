# MySocialPortal

My Social Portal is an ecosystem of software to make usefully accessing
our data in various social media systems easier without requiring us to
work directly within their walled gardens and pre-defined user
experience workflows. 

## Licensing

Unless stated otherwise software within a sub-folder, the license of the
software in this repository is the copy-left Mozilla Public License,
as shown in the root LICENSE file. 

## Structure

* **Kyanite**: A Flutter-based cross platform desktop
application for viewing the Facebook account archive that a user can
request from the system. This takes the form of a zip file that can be
unzipped on your desktop system and have this app point to it.
