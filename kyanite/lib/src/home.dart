import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/screens/facebook_conversations_screen.dart';
import 'package:kyanite/src/facebook/screens/facebook_friends_screen.dart';
import 'package:kyanite/src/facebook/screens/facebook_geospatial_screen.dart';
import 'package:kyanite/src/facebook/screens/facebook_saved_items_screen.dart';
import 'package:kyanite/src/facebook/screens/facebook_stats_screen.dart';
import 'package:kyanite/src/facebook/screens/facebook_videos_screen.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_reader.dart';

import 'facebook/screens/facebook_comments_screen.dart';
import 'facebook/screens/facebook_events_screen.dart';
import 'facebook/screens/facebook_photo_album_browser_screen.dart';
import 'facebook/screens/facebook_posts_screen.dart';
import 'settings/settings_controller.dart';
import 'settings/settings_view.dart';

class Home extends StatefulWidget {
  final SettingsController settingsController;

  const Home({Key? key, required this.settingsController}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  static final Widget notInitialiedWidget = Container();
  final List<AppPageData> _pageData = [];
  final List<Widget> _pages = [];
  int _selectedIndex = 0;

  @override
  void initState() {
    _pageData.addAll([
      AppPageData('Posts', Icons.home, () => const FacebookPostsScreen()),
      AppPageData('Map', Icons.map, () => const FacebookGeospatialViewScreen()),
      AppPageData('Saved Links', Icons.bookmark,
          () => const FacebookSavedItemsScreen()),
      AppPageData(
          'Chats', Icons.message, () => const FacebookConversationScreen()),
      AppPageData('Photos', Icons.photo_library,
          () => const FacebookPhotoAlbumsBrowserScreen()),
      AppPageData(
          'Videos', Icons.video_library, () => const FacebookVideosScreen()),
      AppPageData('Events', Icons.event, () => const FacebookEventsScreen()),
      AppPageData('Friends', Icons.people, () => const FacebookFriendsScreen()),
      AppPageData(
          'Comments', Icons.notes, () => const FacebookCommentsScreen()),
      AppPageData('Stats', Icons.bar_chart, () => const FacebookStatsScreen()),
      AppPageData('Settings', Icons.settings, () => _buildSettingsView()),
    ]);
    for (var i = 0; i < _pageData.length; i++) {
      _pages.add(notInitialiedWidget);
    }

    if (FacebookArchiveFolderReader.validateCanReadArchive(
        widget.settingsController.rootFolder)) {
      _setSelectedIndex(0);
    } else {
      _setSelectedIndex(_pageData.length - 1);
    }

    super.initState();
  }

  @override
  void dispose() {
    _pages.clear();
    super.dispose();
  }

  void _setSelectedIndex(int value) {
    setState(() {
      if (_pages[value] == notInitialiedWidget) {
        _pages[value] = _pageData[value].widget;
      }
      _selectedIndex = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          _buildNavBar(),
          SizedBox(width: 1, child: Container(color: Colors.grey)),
          _buildMainArea(),
        ],
      ),
    );
  }

  Widget _buildNavBar() {
    return LayoutBuilder(builder: (context, constraint) {
      return Scrollbar(
        isAlwaysShown: true,
        child: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child: NavigationRail(
                destinations:
                    _pageData.map((p) => p.navRailDestination).toList(),
                selectedIndex: _selectedIndex,
                onDestinationSelected: _setSelectedIndex,
                labelType: NavigationRailLabelType.selected,
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget _buildMainArea() {
    return Expanded(
        child: IndexedStack(index: _selectedIndex, children: _pages));
  }

  Widget _buildSettingsView() {
    return SettingsView(controller: widget.settingsController);
  }
}

class AppPageData {
  final String label;
  final IconData icon;
  final Widget Function() _widgetBuilder;
  late final Widget widget = _widgetBuilder();
  final NavigationRailDestination navRailDestination;

  AppPageData(this.label, this.icon, widgetBuilder)
      : _widgetBuilder = widgetBuilder,
        navRailDestination =
            NavigationRailDestination(icon: Icon(icon), label: Text(label));
}
