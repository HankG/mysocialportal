import 'dart:convert';
import 'dart:io';

import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';

final _facebookFileReadingLogger = Logger('File.FacebookFileReading');

extension FacebookFileReading on File {
  FutureResult<String, ExecError> readFacebookEncodedFileAsString() async {
    const leadingSlash = 92;
    const leadingU = 117;
    final data = await readAsBytes();
    final buffer = StringBuffer();
    int i = 0;
    try {
      while (i < data.length) {
        if (data[i] == leadingSlash && data[i + 1] == leadingU) {
          final byteBuffer = <int>[];
          while (i < data.length - 1 &&
              data[i] == leadingSlash &&
              data[i + 1] == leadingU) {
            final chars = data
                .sublist(i + 2, i + 6)
                .map((e) => e < 97 ? e - 48 : e - 87)
                .toList(growable: false);
            final byte = (chars[0] << 12) +
                (chars[1] << 8) +
                (chars[2] << 4) +
                (chars[3]);
            byteBuffer.add(byte);
            i += 6;
          }
          final unicodeChar = utf8.decode(byteBuffer);
          buffer.write(unicodeChar);
        } else {
          buffer.writeCharCode(data[i]);
          i++;
        }
      }
    } catch (e) {
      _facebookFileReadingLogger.severe('Error parsing $path, $e');
      return Result.error(ExecError(
          exception: e as Exception, errorMessage: buffer.toString()));
    }

    return Result.ok(buffer.toString());
  }
}
