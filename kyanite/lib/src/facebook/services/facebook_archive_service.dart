import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:kyanite/src/facebook/models/facebook_album.dart';
import 'package:kyanite/src/facebook/models/facebook_comment.dart';
import 'package:kyanite/src/facebook/models/facebook_event.dart';
import 'package:kyanite/src/facebook/models/facebook_friend.dart';
import 'package:kyanite/src/facebook/models/facebook_media_attachment.dart';
import 'package:kyanite/src/facebook/models/facebook_messenger_conversation.dart';
import 'package:kyanite/src/facebook/models/facebook_post.dart';
import 'package:kyanite/src/facebook/models/facebook_saved_item.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_reader.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:path/path.dart' as p;
import 'package:result_monad/result_monad.dart';

class FacebookArchiveDataService extends ChangeNotifier {
  static final _logger = Logger('$FacebookArchiveDataService');
  final PathMappingService pathMappingService;
  final String appDataDirectory;
  final List<FacebookAlbum> albums = [];
  final List<FacebookPost> posts = [];
  final List<FacebookComment> comments = [];
  final List<FacebookEvent> events = [];
  final List<FacebookFriend> friends = [];
  final List<FacebookMessengerConversation> convos = [];
  final List<FacebookSavedItem> savedItems = [];
  bool canUseConvoCacheFile = true;

  FacebookArchiveDataService(
      {required this.pathMappingService, required this.appDataDirectory}) {
    _logger.info('Facebook Archive Service created');
  }

  void clearCaches() {
    _logger.fine('clearCaches called');
    _logger.finer('Clearing caches');
    albums.clear();
    posts.clear();
    comments.clear();
    events.clear();
    convos.clear();
    friends.clear();
    savedItems.clear();
    notifyListeners();
    canUseConvoCacheFile = false;
    _logger.finer('Deleting files');
    try {
      final convoCacheFile = File(_conversationCachePath);
      if (convoCacheFile.existsSync()) {
        convoCacheFile.deleteSync();
      }

      if (convoCacheFile.existsSync()) {
        _logger.severe(
            'Attempted to delete conversations cache file but it did not succeed. ${convoCacheFile.path}');
      }
    } catch (e) {
      _logger.severe(
          'Exception thrown while attempting to clear conversations cache file: $e');
    }
    canUseConvoCacheFile = true;
    _logger.fine('clearCaches complete');
  }

  FutureResult<List<FacebookPost>, ExecError> getPosts() async {
    _logger.fine('Request for posts');
    if (posts.isNotEmpty) {
      _logger.fine(
          'Posts already loaded, returning existing ${posts.length} posts');
      return Result.ok(List.unmodifiable(posts));
    }
    _logger.finer('No previously pulled posts reading from disk');
    final postsResult = await _readAllPosts();
    postsResult.match(
        onSuccess: (newPosts) {
          posts.clear();
          posts.addAll(newPosts);
          posts.sort((p1, p2) =>
              -p1.creationTimestamp.compareTo(p2.creationTimestamp));
        },
        onError: (error) => _logger.severe('Error loading posts: $error'));

    _logger.fine('Returning ${posts.length} posts');
    return Result.ok(List.unmodifiable(posts));
  }

  FutureResult<List<FacebookComment>, ExecError> getComments() async {
    _logger.fine('Request for comments');
    if (comments.isNotEmpty) {
      _logger.fine(
          'Comments already loaded, returning existing ${comments.length} comments');
      return Result.ok(List.unmodifiable(comments));
    }
    _logger.finer('No previously pulled comments reading from disk');
    final commentsResult = await _readAllComments();
    commentsResult.match(
        onSuccess: (newComments) {
          comments.clear();
          comments.addAll(newComments);
          comments.sort((c1, c2) =>
              -c1.creationTimestamp.compareTo(c2.creationTimestamp));
        },
        onError: (error) => _logger.severe('Error loading comments: $error'));

    _logger.fine('Returning ${comments.length} comments');
    return Result.ok(List.unmodifiable(comments));
  }

  FutureResult<List<FacebookEvent>, ExecError> getEvents() async {
    _logger.fine('Request for events');
    if (events.isNotEmpty) {
      _logger.fine(
          'Events already loaded, returning existing ${events.length} events');
      return Result.ok(List.unmodifiable(events));
    }
    _logger.finer('No previously pulled events reading from disk');
    final eventsResult = await _readAllEvents();
    eventsResult.match(
        onSuccess: (newEvents) {
          events.clear();
          events.addAll(newEvents);
          events.sort((e1, e2) =>
              -e1.creationTimestamp.compareTo(e2.creationTimestamp));
        },
        onError: (error) => _logger.severe('Error loading events: $error'));

    _logger.fine('Returning ${comments.length} events');
    return Result.ok(List.unmodifiable(events));
  }

  FutureResult<List<FacebookFriend>, ExecError> getFriends() async {
    _logger.fine('Request for friends');
    if (friends.isNotEmpty) {
      _logger.fine(
          'Friends already loaded, returning existing ${friends.length} friends');
      return Result.ok(List.unmodifiable(friends));
    }
    _logger.finer('No previously pulled friends reading from disk');
    final friendResult = await _readAllFriends();
    friendResult.match(
        onSuccess: (newFriends) {
          friends.clear();
          friends.addAll(newFriends);
        },
        onError: (error) => _logger.severe('Error loading friends: $error'));

    _logger.fine('Returning ${friends.length} friends');
    return Result.ok(List.unmodifiable(friends));
  }

  FutureResult<List<FacebookAlbum>, ExecError> getAlbums() async {
    _logger.fine('Request for albums');
    if (albums.isNotEmpty) {
      _logger.fine(
          'Albums already loaded, returning existing ${albums.length} albums');
      return Result.ok(List.unmodifiable(albums));
    }
    _logger.finer('No previously pulled albums reading from disk');

    final albumResult = await _readAllAlbums();
    albumResult.match(
        onSuccess: (newAlbums) {
          albums.clear();
          albums.addAll(newAlbums);
        },
        onError: (error) => _logger.severe('Error loading albums: $error'));

    final postsAlbum = await _generatePostsAlbum();
    postsAlbum.match(
        onSuccess: (album) => albums.add(album),
        onError: (error) =>
            _logger.severe('Error generating posts album: $error'));

    albums.sort((a1, a2) =>
        -a1.lastModifiedTimestamp.compareTo(a2.lastModifiedTimestamp));

    _logger.fine('Returning ${albums.length} albums');
    return Result.ok(List.unmodifiable(albums));
  }

  FutureResult<List<FacebookMessengerConversation>, ExecError>
      getConvos() async {
    _logger.fine('Request for conversations');
    if (convos.isNotEmpty) {
      _logger.fine(
          'Conversations already loaded, returning existing ${convos.length} posts');
      return Result.ok(List.unmodifiable(convos));
    }

    final convoCacheFile = File(_conversationCachePath);
    try {
      if (canUseConvoCacheFile && convoCacheFile.existsSync()) {
        _logger.finer(
            'Attempt to load conversations from: $_conversationCachePath');
        final newConvosTextResult = await convoCacheFile.readAsString();
        if (newConvosTextResult.isNotEmpty) {
          final newConvosData =
              jsonDecode(newConvosTextResult) as List<dynamic>;
          final newConvos = newConvosData
              .map((json) => FacebookMessengerConversation.fromJson(json))
              .toList();
          convos.clear();
          convos.addAll(newConvos);
          _logger.fine(
              '${newConvos.length} conversations loaded from disk. Returning ${convos.length} conversations');
          return Result.ok(List.unmodifiable(convos));
        }
      }
    } catch (e) {
      _logger.severe('Exception thrown trying to read from cache, $e');
    }

    _logger.finer('No cache data available so reading from original archive');

    final conversationsResult = await _readAllConvos();
    conversationsResult.match(onSuccess: (newConversations) {
      convos.clear();
      convos.addAll(newConversations);
      convos.sort((c1, c2) =>
          -c1.latestTimestampMS().compareTo(c2.latestTimestampMS()));
    }, onError: (error) {
      _logger.severe('Error loading posts: $error');
    });
    try {
      _logger.finer(
          'Writing ${convos.length} to conversation cache file $_conversationCachePath');
      String json = jsonEncode(convos);
      await convoCacheFile.writeAsString(json, flush: true);
    } catch (e) {
      _logger.severe('Error trying to write to cache file, $e');
    }

    _logger.fine('Returning ${convos.length} conversations');
    return Result.ok(List.unmodifiable(convos));
  }

  FutureResult<List<FacebookSavedItem>, ExecError> getSavedItems() async {
    _logger.fine('Request for saved items');
    if (savedItems.isNotEmpty) {
      _logger.fine(
          'Saved items already loaded, returning existing ${savedItems.length} comments');
      return Result.ok(List.unmodifiable(savedItems));
    }
    _logger.finer('No previously pulled saved items, reading from disk');
    final savedItemsResult = await _readAllSavedItems();
    savedItemsResult.match(
        onSuccess: (newSavedItems) {
          savedItems.clear();
          savedItems.addAll(newSavedItems);
          savedItems.sort((c1, c2) => -c1.timestamp.compareTo(c2.timestamp));
        },
        onError: (error) => _logger.severe('Error loading savedItems: $error'));

    _logger.fine('Returning ${savedItems.length} saved items');
    return Result.ok(List.unmodifiable(savedItems));
  }

  String get _conversationCachePath =>
      p.join(appDataDirectory, 'convo_cache.json');

  FutureResult<List<FacebookPost>, ExecError> _readAllPosts() async {
    final allPosts = <FacebookPost>[];
    bool hadSuccess = false;
    for (final topLevelDir in _topLevelDirs) {
      try {
        _logger.fine(
            'Attempting to find/parse Post JSON data in ${topLevelDir.path}');
        final reader = FacebookArchiveFolderReader(topLevelDir.path);
        final postsResult = await reader.readPosts();
        postsResult.match(
            onSuccess: (newPosts) {
              allPosts.addAll(newPosts);
              hadSuccess = true;
            },
            onError: (error) => _logger.fine(error));
      } catch (e) {
        _logger.severe('Exception thrown trying to read posts, $e');
      }
    }

    if (hadSuccess) {
      return Result.ok(allPosts);
    }

    return Result.error(ExecError.message(
        'Unable to find any post JSON files in $_baseArchiveFolder'));
  }

  FutureResult<List<FacebookComment>, ExecError> _readAllComments() async {
    final allComments = <FacebookComment>[];
    bool hadSuccess = false;
    for (final topLevelDir in _topLevelDirs) {
      try {
        _logger.fine(
            'Attempting to find/parse comment JSON data in ${topLevelDir.path}');
        final reader = FacebookArchiveFolderReader(topLevelDir.path);
        final commentsResult = await reader.readComments();
        commentsResult.match(
            onSuccess: (newEvents) {
              allComments.addAll(newEvents);
              hadSuccess = true;
            },
            onError: (error) => _logger.fine(error));
      } catch (e) {
        _logger.severe('Exception thrown trying to read comments, $e');
      }
    }

    if (hadSuccess) {
      return Result.ok(allComments);
    }

    return Result.error(ExecError.message(
        'Unable to find any comment JSON files in $_baseArchiveFolder'));
  }

  FutureResult<List<FacebookEvent>, ExecError> _readAllEvents() async {
    final allEvents = <FacebookEvent>[];
    bool hadSuccess = false;
    for (final topLevelDir in _topLevelDirs) {
      try {
        _logger.fine(
            'Attempting to find/parse event JSON data in ${topLevelDir.path}');
        final reader = FacebookArchiveFolderReader(topLevelDir.path);
        final eventsResult = await reader.readEvents();
        eventsResult.match(
            onSuccess: (newEvents) {
              allEvents.addAll(newEvents);
              hadSuccess = true;
            },
            onError: (error) => _logger.fine(error));
      } catch (e) {
        _logger.severe('Exception thrown trying to read events, $e');
      }
    }

    if (hadSuccess) {
      return Result.ok(allEvents);
    }

    return Result.error(ExecError.message(
        'Unable to find any event JSON files in $_baseArchiveFolder'));
  }

  FutureResult<List<FacebookFriend>, ExecError> _readAllFriends() async {
    final allFriends = <FacebookFriend>[];
    bool hadSuccess = false;
    for (final topLevelDir in _topLevelDirs) {
      try {
        _logger.fine(
            'Attempting to find/parse friend JSON data in ${topLevelDir.path}');
        final reader = FacebookArchiveFolderReader(topLevelDir.path);
        final friendsResult = await reader.readFriends();
        friendsResult.match(
            onSuccess: (newFriends) {
              allFriends.addAll(newFriends);
              hadSuccess = true;
            },
            onError: (error) => _logger.fine(error));
      } catch (e) {
        _logger.severe('Exception thrown trying to read friends, $e');
      }
    }

    if (hadSuccess) {
      return Result.ok(allFriends);
    }

    return Result.error(ExecError.message(
        'Unable to find any album JSON files in $_baseArchiveFolder'));
  }

  FutureResult<List<FacebookAlbum>, ExecError> _readAllAlbums() async {
    final allAlbums = <FacebookAlbum>[];
    bool hadSuccess = false;
    for (final topLevelDir in _topLevelDirs) {
      try {
        _logger.fine(
            'Attempting to find/parse album JSON data in ${topLevelDir.path}');
        final reader = FacebookArchiveFolderReader(topLevelDir.path);
        final albumResult = await reader.readPhotoAlbums();
        albumResult.match(
            onSuccess: (newAlbums) {
              allAlbums.addAll(newAlbums);
              hadSuccess = true;
            },
            onError: (error) => _logger.fine(error));
      } catch (e) {
        _logger.severe('Exception thrown trying to read albums, $e');
      }
    }

    if (hadSuccess) {
      return Result.ok(allAlbums);
    }

    return Result.error(ExecError.message(
        'Unable to find any album JSON files in $_baseArchiveFolder'));
  }

  FutureResult<List<FacebookMessengerConversation>, ExecError>
      _readAllConvos() async {
    final allConvos = <FacebookMessengerConversation>[];
    bool hadSuccess = false;
    for (final topLevelDir in _topLevelDirs) {
      try {
        _logger.fine(
            'Attempting to find/parse conversation JSON data in ${topLevelDir.path}');
        final reader = FacebookArchiveFolderReader(topLevelDir.path);
        final convosResult = await reader.readConversations();
        convosResult.match(
            onSuccess: (newConvos) {
              allConvos.addAll(newConvos);
              hadSuccess = true;
            },
            onError: (error) => _logger.fine(error));
      } catch (e) {
        _logger.severe('Exception thrown trying to read conversations, $e');
      }
    }

    if (hadSuccess) {
      return Result.ok(allConvos);
    }

    return Result.error(ExecError.message(
        'Unable to find any event JSON files in $_baseArchiveFolder'));
  }

  FutureResult<List<FacebookSavedItem>, ExecError> _readAllSavedItems() async {
    final allSavedItems = <FacebookSavedItem>[];
    bool hadSuccess = false;
    for (final topLevelDir in _topLevelDirs) {
      try {
        _logger.fine(
            'Attempting to find/parse saved items JSON data in ${topLevelDir.path}');
        final reader = FacebookArchiveFolderReader(topLevelDir.path);
        final savedItemsResult = await reader.readSavedItems();
        savedItemsResult.match(
            onSuccess: (newSavedItem) {
              allSavedItems.addAll(newSavedItem);
              hadSuccess = true;
            },
            onError: (error) => _logger.fine(error));
      } catch (e) {
        _logger.severe('Exception thrown trying to read saved items, $e');
      }
    }

    if (hadSuccess) {
      return Result.ok(allSavedItems);
    }

    return Result.error(ExecError.message(
        'Unable to find any saved items JSON files in $_baseArchiveFolder'));
  }

  FutureResult<FacebookAlbum, ExecError> _generatePostsAlbum() async {
    const name = 'Photos in Posts';
    const description = 'Photos that were added to posts';
    final posts = await getPosts();

    if (posts.isFailure) {
      return Result.error(posts.error);
    }

    final photos = posts.value
        .map((p) => p.mediaAttachments)
        .expand((m) => m)
        .where((m) => m.estimatedType() == FacebookAttachmentMediaType.image)
        .toList();
    photos
        .sort((p1, p2) => p1.creationTimestamp.compareTo(p2.creationTimestamp));
    final lastModified = photos.isEmpty ? 0 : photos.last.creationTimestamp;
    final coverPhoto =
        photos.isEmpty ? FacebookMediaAttachment.blank() : photos.last;

    final album = FacebookAlbum(
        name: name,
        description: description,
        lastModifiedTimestamp: lastModified,
        coverPhoto: coverPhoto,
        photos: photos,
        comments: []);
    return Result.ok(album);
  }

  String get _baseArchiveFolder => pathMappingService.rootFolder;

  List<FileSystemEntity> get _topLevelDirs =>
      pathMappingService.archiveDirectories;
}
