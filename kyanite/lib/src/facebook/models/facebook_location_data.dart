import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:kyanite/src/facebook/components/facebook_link_elements_component.dart';

import 'model_utils.dart';

class FacebookLocationData {
  final String name;

  final double latitude;

  final double longitude;

  final double altitude;

  final bool hasPosition;

  final String address;

  final String url;

  const FacebookLocationData(
      {this.name = '',
      this.latitude = 0.0,
      this.longitude = 0.0,
      this.altitude = 0.0,
      this.hasPosition = false,
      this.address = '',
      this.url = ''});

  FacebookLocationData.randomBuilt()
      : name = 'Location name ${randomId()}',
        latitude = Random().nextDouble(),
        longitude = Random().nextDouble(),
        altitude = Random().nextDouble(),
        hasPosition = true,
        address = 'Address ${randomId()}',
        url = 'http://localhost/${randomId()}';

  @override
  String toString() {
    return 'FacebookLocationData{name: $name, latitude: $latitude, longitude: $longitude, altitude: $altitude, hasPosition: $hasPosition, address: $address, url: $url}';
  }

  String toHumanString() {
    if (!hasPosition) {
      return '';
    }

    return [
      if (name.isNotEmpty) 'Name: $name',
      if (address.isNotEmpty) 'Address: $address',
      'Latitude: $latitude',
      'Longitude: $longitude',
    ].join('\n');
  }

  bool hasData() =>
      name.isNotEmpty || address.isNotEmpty || url.isNotEmpty || hasPosition;

  static FacebookLocationData fromJson(Map<String, dynamic> json) {
    final name = json['name'] ?? '';
    final address = json['address'] ?? '';
    final url = json['url'] ?? '';
    var latitude = 0.0;
    var longitude = 0.0;
    var altitude = 0.0;
    var hasPosition = json.containsKey('coordinate');
    if (hasPosition) {
      final position = json['coordinate'];
      latitude = position['latitude'] ?? 0.0;
      longitude = position['longitude'] ?? 0.0;
      altitude = position['altitude'] ?? 0.0;
    }

    return FacebookLocationData(
        name: name,
        address: address,
        url: url,
        hasPosition: hasPosition,
        latitude: latitude,
        longitude: longitude,
        altitude: altitude);
  }
}

extension WidgetExtensions on FacebookLocationData {
  Widget toWidget(double spacingHeight) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'At: ',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (name.isNotEmpty) ...[Text(name)],
              if (address.isNotEmpty) ...[
                SizedBox(height: spacingHeight),
                Text(address)
              ],
              if (name.isEmpty && hasPosition) ...[
                SizedBox(height: spacingHeight),
                Text('Latitude: $latitude, Longitude: $longitude')
              ],
              if (url.isNotEmpty) ...[
                SizedBox(height: spacingHeight),
                FacebookLinkElementsComponent(
                  links: [Uri.parse(url)],
                ),
              ],
            ],
          ),
        ]);
  }
}
