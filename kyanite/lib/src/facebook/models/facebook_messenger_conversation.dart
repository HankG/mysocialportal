import 'package:kyanite/src/facebook/models/facebook_messenger_message.dart';
import 'package:logging/logging.dart';
import 'package:uuid/uuid.dart';

import 'model_utils.dart';

class Copy<T> {
  T? copy() => null;
}

class FacebookMessengerConversation with Copy<FacebookMessengerConversation> {
  static final _logger = Logger('$FacebookMessengerConversation');

  final String id;
  final Set<String> participants;
  final List<FacebookMessengerMessage> messages;
  final String title;

  FacebookMessengerConversation(
      {required this.id,
      required this.participants,
      required this.messages,
      required this.title});

  factory FacebookMessengerConversation.empty() =>
      FacebookMessengerConversation(
          id: '', participants: {}, messages: [], title: '');

  @override
  FacebookMessengerConversation copy() => FacebookMessengerConversation(
      id: id,
      participants: {...participants},
      messages: [...messages],
      title: title);

  @override
  String toString() {
    return 'FacebookMessengerConversation{participants: $participants, messages: $messages, title: $title}';
  }

  int earliestTimestampMS() => messages.isEmpty ? 0 : messages.last.timestampMS;

  int latestTimestampMS() => messages.isEmpty ? 0 : messages.first.timestampMS;

  bool hasImages() => messages.where((m) => m.hasImages()).isNotEmpty;

  bool hasVideos() => messages.where((m) => m.hasVideos()).isNotEmpty;

  FacebookMessengerConversation.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? '',
        participants = {...json['participants'] as List<dynamic>? ?? []},
        messages = (json['messages'] as List<dynamic>? ?? [])
            .map((j) => FacebookMessengerMessage.fromJson(j))
            .toList(),
        title = json['title'] ?? '';

  Map<String, dynamic> toJson() => {
        'id': id,
        'participants': participants.toList(),
        'messages': messages.map((m) => m.toJson()).toList(),
        'title': title,
      };

  static FacebookMessengerConversation fromFacebookJson(
      Map<String, dynamic> json) {
    final id = json['thread_path'] ?? const Uuid().v4();
    const knownTopLevelKeys = [
      'participants',
      'messages',
      'title',
      'is_still_participant',
      'thread_type',
      'thread_path',
      'magic_words',
    ];

    logAdditionalKeys(knownTopLevelKeys, json.keys, _logger, Level.WARNING,
        'Unknown top level conversation keys: ');

    final title = json['title'] ?? '';
    final participants = <String>{};
    final messages = <FacebookMessengerMessage>[];

    for (Map<String, dynamic> p in json['messages'] ?? <Map, dynamic>{}) {
      messages.add(FacebookMessengerMessage.fromFacebookJson(p));
    }

    for (Map<String, dynamic> p in json['participants'] ?? <Map, dynamic>{}) {
      participants.add(p['name'] ?? '');
    }

    return FacebookMessengerConversation(
        id: id, participants: participants, messages: messages, title: title);
  }
}
