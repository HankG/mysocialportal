import 'package:intl/intl.dart';
import 'package:kyanite/src/facebook/models/facebook_media_attachment.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:logging/logging.dart';

import 'model_utils.dart';

class FacebookMessengerMessage {
  static final _logger = Logger('$FacebookMessengerMessage');

  final String from;
  final String message;
  final int timestampMS;
  final List<FacebookMediaAttachment> media;
  final List<FacebookMediaAttachment> stickers;
  final List<Uri> links;
  final Map<String, String> reactions;

  FacebookMessengerMessage(
      {required this.from,
      required this.message,
      required this.timestampMS,
      this.media = const [],
      this.stickers = const [],
      this.links = const [],
      this.reactions = const {}});

  @override
  String toString() {
    return 'FacebookMessengerMessage{from: $from, message: $message, timestampMS: $timestampMS, media: $media, stickers: $stickers, links: $links, reactions: $reactions}';
  }

  String toHumanString(PathMappingService mapper, DateFormat formatter) {
    final creationDateString = formatter
        .format(DateTime.fromMillisecondsSinceEpoch(timestampMS).toLocal());
    return [
      'Creation At: $creationDateString',
      if (message.isNotEmpty) 'Message: $message',
      '',
      if (links.isNotEmpty) 'Links:',
      ...links.map((e) => e.toString()),
      '',
      if (stickers.isNotEmpty) 'Stickers:',
      ...stickers.map((e) => e.toHumanString(mapper)),
      if (media.isNotEmpty) 'Media:',
      ...media.map((e) => e.toHumanString(mapper)),
    ].join('\n');
  }

  FacebookMessengerMessage copy(
      {String? from,
      String? message,
      int? timestampMS,
      List<FacebookMediaAttachment>? media,
      List<FacebookMediaAttachment>? stickers,
      List<Uri>? links,
      Map<String, String>? reactions}) {
    return FacebookMessengerMessage(
      from: from ?? this.from,
      message: message ?? this.message,
      timestampMS: timestampMS ?? this.timestampMS,
      media: media ?? this.media,
      stickers: stickers ?? this.stickers,
      links: links ?? this.links,
      reactions: reactions ?? this.reactions,
    );
  }

  FacebookMessengerMessage.fromJson(Map<String, dynamic> json)
      : from = json['from'] ?? '',
        message = json['message'] ?? '',
        timestampMS = json['timestampMS'] ?? '',
        media = (json['media'] as List<dynamic>? ?? [])
            .map((j) => FacebookMediaAttachment.fromJson(j))
            .toList(),
        stickers = (json['stickers'] as List<dynamic>? ?? [])
            .map((j) => FacebookMediaAttachment.fromJson(j))
            .toList(),
        links = (json['links'] as List<dynamic>? ?? [])
            .map((j) => Uri.parse(j))
            .toList(),
        reactions = (json['reactions'] as Map<String, dynamic>? ?? {})
            .map((key, value) => MapEntry(key, value.toString()));

  Map<String, dynamic> toJson() => {
        'from': from,
        'message': message,
        'timestampMS': timestampMS,
        'media': media.map((m) => m.toJson()).toList(),
        'stickers': stickers.map((m) => m.toJson()).toList(),
        'links': links.map((e) => e.toString()).toList(),
        'reactions': reactions,
      };

  bool hasImages() => media
      .where((element) =>
          element.estimatedType() == FacebookAttachmentMediaType.image)
      .isNotEmpty;

  bool hasVideos() => media
      .where((element) =>
          element.estimatedType() == FacebookAttachmentMediaType.video)
      .isNotEmpty;

  static FacebookMessengerMessage fromFacebookJson(Map<String, dynamic> json) {
    const knownTopLevelKeys = [
      'sender_name',
      'timestamp_ms',
      'photos',
      'reactions',
      'gifs',
      'content',
      'type',
      'share',
      'videos',
      'users',
      'sticker',
      'files',
      'call_duration',
      'missed',
      'audio_files',
      'is_unsent',
      'ip',
    ];

    logAdditionalKeys(knownTopLevelKeys, json.keys, _logger, Level.WARNING,
        'Unknown top level message keys: ');

    final from = json['sender_name'] ?? '';
    final timestamp = json['timestamp_ms'] ?? 0;
    final message = json['content'] ?? '';
    final type = json['Generic'] ?? 'Generic';
    if (!['Generic', 'Share'].contains(type)) {
      _logger.severe("New message type: $type");
    }

    final links = <Uri>[];
    final String linkString = json['share']?['link'] ?? '';
    if (linkString.isNotEmpty) {
      links.add(Uri.parse(linkString));
    }

    // TODO Add Reactions
    List<FacebookMediaAttachment> mediaAttachments = [];
    for (Map<String, dynamic> photo in json['photos'] ?? []) {
      final media = FacebookMediaAttachment.fromFacebookJson(photo);
      mediaAttachments.add(media);
    }

    for (Map<String, dynamic> video in json['videos'] ?? []) {
      final media = FacebookMediaAttachment.fromFacebookJson(video);
      mediaAttachments.add(media);
    }

    for (Map<String, dynamic> audioFile in json['audio_files'] ?? []) {
      final path = audioFile['uri'];
      links.add(Uri.file(path));
    }

    for (Map<String, dynamic> gif in json['gifs'] ?? []) {
      final media = FacebookMediaAttachment.fromFacebookJson(gif);
      mediaAttachments.add(media);
    }

    final stickers = <FacebookMediaAttachment>[];
    final String path = json['sticker']?['uri'] ?? '';
    if (path.isNotEmpty) {
      stickers.add(FacebookMediaAttachment.fromUriOnly(Uri.file(path)));
    }

    return FacebookMessengerMessage(
        from: from,
        message: message,
        timestampMS: timestamp,
        media: mediaAttachments,
        stickers: stickers,
        links: links);
  }
}
