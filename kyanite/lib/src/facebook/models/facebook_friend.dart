import 'package:logging/logging.dart';

import 'model_utils.dart';

class FacebookFriend {
  static final _logger = Logger('$FacebookFriend');

  final FriendStatus status;
  final String name;
  final String contactInfo;
  final int friendSinceTimestamp;
  final int receivedTimestamp;
  final int rejectedTimestamp;
  final int removeTimestamp;
  final int sentTimestamp;
  final bool markedAsSpam;

  FacebookFriend(
      {this.status = FriendStatus.unknown,
      required this.name,
      this.contactInfo = '',
      this.friendSinceTimestamp = 0,
      this.receivedTimestamp = 0,
      this.rejectedTimestamp = 0,
      this.removeTimestamp = 0,
      this.sentTimestamp = 0,
      this.markedAsSpam = false});

  @override
  String toString() {
    return 'FacebookFriend{status: $status, name: $name, contactInfo: $contactInfo, friendSinceTimestamp: $friendSinceTimestamp, receivedTimestamp: $receivedTimestamp, rejectedTimestamp: $rejectedTimestamp, removeTimestamp: $removeTimestamp, sentTimestamp: $sentTimestamp, markedAsSpam: $markedAsSpam}';
  }

  static FacebookFriend fromJson(
      Map<String, dynamic> json, FriendStatus status) {
    final knownTopLevelKeys = [
      'timestamp',
      'name',
      'contact_info',
      'marked_as_spam'
    ];
    int timestamp = json['timestamp'] ?? 0;
    final name = json['name'] ?? '';
    final contactInfo = json['contact_info'] ?? '';
    final markedAsSpam = json['marked_as_spam'] ?? false;

    logAdditionalKeys(knownTopLevelKeys, json.keys, _logger, Level.WARNING,
        'Unknown top level friend keys');

    switch (status) {
      case FriendStatus.friends:
        return FacebookFriend(
            name: name,
            status: status,
            contactInfo: contactInfo,
            markedAsSpam: markedAsSpam,
            friendSinceTimestamp: timestamp);
      case FriendStatus.requestReceived:
        return FacebookFriend(
            name: name,
            status: status,
            contactInfo: contactInfo,
            markedAsSpam: markedAsSpam,
            receivedTimestamp: timestamp);
      case FriendStatus.rejectedRequest:
        return FacebookFriend(
            name: name,
            status: status,
            contactInfo: contactInfo,
            markedAsSpam: markedAsSpam,
            rejectedTimestamp: timestamp);
      case FriendStatus.removed:
        return FacebookFriend(
            name: name,
            status: status,
            contactInfo: contactInfo,
            markedAsSpam: markedAsSpam,
            removeTimestamp: timestamp);
      case FriendStatus.sentFriendRequest:
        return FacebookFriend(
            name: name,
            status: status,
            contactInfo: contactInfo,
            markedAsSpam: markedAsSpam,
            sentTimestamp: timestamp);
      case FriendStatus.unknown:
        return FacebookFriend(
          name: name,
          status: status,
          contactInfo: contactInfo,
          markedAsSpam: markedAsSpam,
        );
    }
  }
}

enum FriendStatus {
  friends,
  requestReceived,
  rejectedRequest,
  removed,
  sentFriendRequest,
  unknown,
}

extension FriendStatusWriter on FriendStatus {
  String name() {
    switch (this) {
      case FriendStatus.friends:
        return "Friends";
      case FriendStatus.requestReceived:
        return "Requested";
      case FriendStatus.rejectedRequest:
        return "Rejected";
      case FriendStatus.removed:
        return "Removed";
      case FriendStatus.sentFriendRequest:
        return "Sent Request";
      case FriendStatus.unknown:
        return "Unknown";
    }
  }
}
