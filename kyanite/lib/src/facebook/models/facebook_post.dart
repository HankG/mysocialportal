import 'package:intl/intl.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:logging/logging.dart';

import 'facebook_location_data.dart';
import 'facebook_media_attachment.dart';
import 'model_utils.dart';

class FacebookPost {
  static final _logger = Logger('$FacebookPost');

  final int creationTimestamp;

  final int backdatedTimestamp;

  final int modificationTimestamp;

  final String post;

  final String title;

  final List<FacebookMediaAttachment> mediaAttachments;

  final FacebookLocationData locationData;

  final List<Uri> links;

  FacebookPost(
      {this.creationTimestamp = 0,
      this.backdatedTimestamp = 0,
      this.modificationTimestamp = 0,
      this.post = '',
      this.title = '',
      this.locationData = const FacebookLocationData(),
      List<FacebookMediaAttachment>? mediaAttachments,
      List<Uri>? links})
      : mediaAttachments = mediaAttachments ?? <FacebookMediaAttachment>[],
        links = links ?? <Uri>[];

  FacebookPost.randomBuilt()
      : creationTimestamp = DateTime.now().millisecondsSinceEpoch,
        backdatedTimestamp = DateTime.now().millisecondsSinceEpoch,
        modificationTimestamp = DateTime.now().millisecondsSinceEpoch,
        post = 'Random post text ${randomId()}',
        title = 'Random title ${randomId()}',
        locationData = FacebookLocationData.randomBuilt(),
        links = [
          Uri.parse('http://localhost/${randomId()}'),
          Uri.parse('http://localhost/${randomId()}')
        ],
        mediaAttachments = [
          FacebookMediaAttachment.randomBuilt(),
          FacebookMediaAttachment.randomBuilt()
        ];

  FacebookPost copy(
      {int? creationTimestamp,
      int? backdatedTimestamp,
      int? modificationTimestamp,
      String? post,
      String? title,
      FacebookLocationData? locationData,
      List<FacebookMediaAttachment>? mediaAttachments,
      List<Uri>? links}) {
    return FacebookPost(
        creationTimestamp: creationTimestamp ?? this.creationTimestamp,
        backdatedTimestamp: backdatedTimestamp ?? this.backdatedTimestamp,
        modificationTimestamp:
            modificationTimestamp ?? this.modificationTimestamp,
        post: post ?? this.post,
        title: title ?? this.title,
        locationData: locationData ?? this.locationData,
        mediaAttachments: mediaAttachments ?? this.mediaAttachments,
        links: links ?? this.links);
  }

  @override
  String toString() {
    return 'FacebookPost{creationTimestamp: $creationTimestamp, modificationTimestamp: $modificationTimestamp, backdatedTimeStamp: $backdatedTimestamp, post: $post, title: $title, mediaAttachments: $mediaAttachments, links: $links}';
  }

  String toHumanString(PathMappingService mapper, DateFormat formatter) {
    final creationDateString = formatter.format(
        DateTime.fromMillisecondsSinceEpoch(creationTimestamp * 1000)
            .toLocal());
    return [
      'Title: $title',
      'Creation At: $creationDateString',
      'Text:',
      post,
      '',
      if (links.isNotEmpty) 'Links:',
      ...links.map((e) => e.toString()),
      '',
      if (mediaAttachments.isNotEmpty) 'Photos and Videos:',
      ...mediaAttachments.map((e) => e.toHumanString(mapper)),
      if (locationData.hasPosition) locationData.toHumanString(),
    ].join('\n');
  }

  bool hasImages() => mediaAttachments
      .where((element) =>
          element.estimatedType() == FacebookAttachmentMediaType.image)
      .isNotEmpty;

  bool hasVideos() => mediaAttachments
      .where((element) =>
          element.estimatedType() == FacebookAttachmentMediaType.video)
      .isNotEmpty;

  static FacebookPost fromJson(Map<String, dynamic> json) {
    final int timestamp = json['timestamp'] ?? 0;
    var modificationTimestamp = timestamp;
    var backdatedTimestamp = timestamp;
    var locationData = const FacebookLocationData();
    String post = '';
    if (json.containsKey('data')) {
      final data = json['data'];
      for (var dataItem in data) {
        if (dataItem.containsKey('post')) {
          post = dataItem['post'];
        } else if (dataItem.containsKey('update_timestamp')) {
          modificationTimestamp = dataItem['update_timestamp'];
        } else if (dataItem.containsKey('backdated_timestamp')) {
          backdatedTimestamp = dataItem['backdated_timestamp'];
        } else {
          _logger.fine(
              "No post or update key sequence in post @$timestamp: ${dataItem.keys}");
        }
      }
    }

    final String title = json['title'] ?? '';
    final links = <Uri>[];
    final mediaAttachments = <FacebookMediaAttachment>[];

    if (json.containsKey('attachments')) {
      for (Map<String, dynamic> attachment in json['attachments']) {
        if (!attachment.containsKey('data')) {
          continue;
        }

        for (var dataItem in attachment['data']) {
          if (dataItem.containsKey('external_context')) {
            final String linkText = dataItem['external_context']['url'] ?? '';
            if (linkText.isNotEmpty) {
              links.add(Uri.parse(linkText));
            }
          } else if (dataItem.containsKey('media')) {
            mediaAttachments.add(
                FacebookMediaAttachment.fromFacebookJson(dataItem['media']));
          } else if (dataItem.containsKey('place')) {
            locationData = FacebookLocationData.fromJson(dataItem['place']);
          } else {
            //TODO Add Facebook Post Poll Processing
            if (dataItem.containsKey('poll')) continue;
            //TODO Add Facebook Post attachment text processing
            if (dataItem.containsKey('text')) continue;
            //TODO Add Facebook Post external context detailed link processing (not just the URL)
            if (dataItem.containsKey('name')) continue;
            //TODO Add Facebook Post event processing
            if (dataItem.containsKey('event')) continue;

            _logger.fine('Unknown post key type: ${dataItem.keys}');
          }
        }
      }
    }

    late final FacebookLocationData actualLocationData;
    if (locationData.hasPosition) {
      actualLocationData = locationData;
    } else {
      final mediaWithPosition = mediaAttachments.where((m) =>
          m.metadata.containsKey('latitude') &&
          m.metadata.containsKey('longitude'));
      if (mediaWithPosition.isNotEmpty) {
        final metadata = mediaWithPosition.first.metadata;
        final latitude = double.tryParse(metadata['latitude'] ?? '') ?? 0.0;
        final longitude = double.tryParse(metadata['longitude'] ?? '') ?? 0.0;
        actualLocationData = FacebookLocationData(
            latitude: latitude, longitude: longitude, hasPosition: true);
      } else {
        actualLocationData = locationData;
      }
    }

    final String actualTitle = title.isNotEmpty
        ? title
        : mediaAttachments
            .map((m) => m.title)
            .firstWhere((t) => t.isNotEmpty, orElse: () => '');

    return FacebookPost(
        creationTimestamp: timestamp,
        modificationTimestamp: modificationTimestamp,
        backdatedTimestamp: backdatedTimestamp,
        locationData: actualLocationData,
        post: post,
        title: actualTitle,
        links: links,
        mediaAttachments: mediaAttachments);
  }
}
