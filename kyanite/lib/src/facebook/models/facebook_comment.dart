import 'package:intl/intl.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:logging/logging.dart';

import 'facebook_media_attachment.dart';
import 'model_utils.dart';

class FacebookComment {
  static final _logger = Logger('$FacebookComment');

  final int creationTimestamp;

  final String author;

  final String comment;

  final String group;

  final String title;

  final List<FacebookMediaAttachment> mediaAttachments;

  final List<Uri> links;

  FacebookComment(
      {this.creationTimestamp = 0,
      this.author = '',
      this.comment = '',
      this.group = '',
      this.title = '',
      List<FacebookMediaAttachment>? mediaAttachments,
      List<Uri>? links})
      : mediaAttachments = mediaAttachments ?? <FacebookMediaAttachment>[],
        links = links ?? <Uri>[];

  FacebookComment.randomBuilt()
      : creationTimestamp = DateTime.now().millisecondsSinceEpoch,
        author = 'Random Author ${randomId()}',
        comment = 'Random comment text ${randomId()}',
        group = 'Random Group ${randomId()}',
        title = 'Random title ${randomId()}',
        links = [
          Uri.parse('http://localhost/${randomId()}'),
          Uri.parse('http://localhost/${randomId()}')
        ],
        mediaAttachments = [
          FacebookMediaAttachment.randomBuilt(),
          FacebookMediaAttachment.randomBuilt()
        ];

  FacebookComment copy(
      {int? creationTimestamp,
      String? author,
      String? comment,
      String? group,
      String? title,
      List<FacebookMediaAttachment>? mediaAttachments,
      List<Uri>? links}) {
    return FacebookComment(
        creationTimestamp: creationTimestamp ?? this.creationTimestamp,
        author: author ?? this.author,
        comment: comment ?? this.comment,
        group: group ?? this.group,
        title: title ?? this.title,
        mediaAttachments: mediaAttachments ?? this.mediaAttachments,
        links: links ?? this.links);
  }

  @override
  String toString() {
    return 'FacebookPost{creationTimestamp: $creationTimestamp, comment: $comment, author, $author, group: $group, title: $title, mediaAttachments: $mediaAttachments, links: $links}';
  }

  String toHumanString(PathMappingService mapper, DateFormat formatter) {
    final creationDateString = formatter.format(
        DateTime.fromMillisecondsSinceEpoch(creationTimestamp * 1000)
            .toLocal());
    return [
      'Title: $title',
      'Creation At: $creationDateString',
      if (group.isNotEmpty) 'Group: $group',
      'Text:',
      comment,
      '',
      if (links.isNotEmpty) 'Links:',
      ...links.map((e) => e.toString()),
      '',
      if (mediaAttachments.isNotEmpty) 'Photos and Videos:',
      ...mediaAttachments.map((e) => e.toHumanString(mapper)),
    ].join('\n');
  }

  FacebookComment.fromJson(Map<String, dynamic> json)
      : creationTimestamp = json['creationTimeStamp'] ?? 0,
        author = json['author'] ?? '',
        comment = json['comment'] ?? '',
        group = json['group'] ?? '',
        title = json['title'] ?? '',
        mediaAttachments = (json['mediaAttachments'] as List<dynamic>? ?? [])
            .map((j) => FacebookMediaAttachment.fromJson(j))
            .toList(),
        links = (json['links'] as List<dynamic>? ?? [])
            .map((j) => Uri.parse(j))
            .toList();

  Map<String, dynamic> toJson() => {
        'creationTimestamp': creationTimestamp,
        'author': author,
        'comment': comment,
        'group': group,
        'title': title,
        'mediaAttachments': mediaAttachments.map((m) => m.toJson()).toList(),
        'links': links.map((e) => e.path).toList(),
      };

  bool hasImages() => mediaAttachments
      .where((element) =>
          element.estimatedType() == FacebookAttachmentMediaType.image)
      .isNotEmpty;

  bool hasVideos() => mediaAttachments
      .where((element) =>
          element.estimatedType() == FacebookAttachmentMediaType.video)
      .isNotEmpty;

  static FacebookComment fromInnerCommentJson(
      Map<String, dynamic> commentSubData) {
    final knownCommentKeys = ['comment', 'timestamp', 'group', 'author'];
    if (_logger.isLoggable(Level.WARNING)) {
      logAdditionalKeys(knownCommentKeys, commentSubData.keys, _logger,
          Level.WARNING, 'Unknown comment level comment keys');
    }
    final comment = commentSubData['comment'] ?? '';
    final group = commentSubData['group'] ?? '';
    final author = commentSubData['author'] ?? '';
    final timestamp = commentSubData['timestamp'] ?? 0;

    return FacebookComment(
      creationTimestamp: timestamp,
      author: author,
      group: group,
      comment: comment,
    );
  }

  static FacebookComment fromFacebookJson(Map<String, dynamic> json) {
    final knownTopLevelKeys = ['timestamp', 'data', 'title', 'attachments'];
    final knownExternalContextKeys = ['external_context', 'media', 'name'];
    int timestamp = json['timestamp'] ?? 0;

    logAdditionalKeys(knownTopLevelKeys, json.keys, _logger, Level.WARNING,
        'Unknown top level comment keys');

    FacebookComment basicCommentData = FacebookComment();
    if (json.containsKey('data')) {
      final data = json['data'];
      for (var dataItem in data) {
        if (dataItem.containsKey('comment')) {
          basicCommentData =
              FacebookComment.fromInnerCommentJson(dataItem['comment']);
        } else {
          _logger.warning(
              "No comment or update key sequence in post @$timestamp: ${dataItem.keys}");
        }
      }
    }

    final String title = json['title'] ?? '';
    final links = <Uri>[];
    final mediaAttachments = <FacebookMediaAttachment>[];

    if (json.containsKey('attachments')) {
      for (Map<String, dynamic> attachment in json['attachments']) {
        if (!attachment.containsKey('data')) {
          continue;
        }

        for (var dataItem in attachment['data']) {
          if (_logger.isLoggable(Level.WARNING)) {
            logAdditionalKeys(
                knownExternalContextKeys,
                dataItem.keys,
                _logger,
                Level.WARNING,
                'Unknown comment external context key level keys in attachment data');
          }
          if (dataItem.containsKey('external_context')) {
            final String linkText = dataItem['external_context']['url'] ?? '';
            if (linkText.isNotEmpty) {
              links.add(Uri.parse(linkText));
            }
          } else if (dataItem.containsKey('media')) {
            mediaAttachments.add(
                FacebookMediaAttachment.fromFacebookJson(dataItem['media']));
          }
        }
      }
    }

    return FacebookComment(
        creationTimestamp: timestamp,
        author: basicCommentData.author,
        comment: basicCommentData.comment,
        group: basicCommentData.group,
        title: title,
        links: links,
        mediaAttachments: mediaAttachments);
  }
}
