import 'package:logging/logging.dart';

import 'model_utils.dart';

class FacebookSavedItem {
  static final _logger = Logger('$FacebookSavedItem');
  final String externalName;
  final int timestamp;
  final String title;
  final String text;
  final Uri uri;

  FacebookSavedItem(
      {this.externalName = '',
      this.timestamp = 0,
      this.title = '',
      this.text = '',
      required this.uri});

  FacebookSavedItem copy(
      {String? externalName,
      int? timestamp,
      String? title,
      String? text,
      Uri? uri}) {
    return FacebookSavedItem(
      externalName: externalName ?? this.externalName,
      timestamp: timestamp ?? this.timestamp,
      title: title ?? this.title,
      text: text ?? this.text,
      uri: uri ?? this.uri,
    );
  }

  static FacebookSavedItem fromFacebookJson(Map<String, dynamic> json) {
    final knownTopLevelKeys = ['attachments', 'title', 'timestamp'];
    final knownExternalContextKeys = ['name', 'source', 'url'];
    int timestamp = json['timestamp'] ?? 0;

    logAdditionalKeys(knownTopLevelKeys, json.keys, _logger, Level.WARNING,
        'Unknown root key');

    final title = json['title'] ?? '';
    var name = '';
    var linkUri = Uri.parse('');
    var externalName = '';

    if (json.containsKey('attachments')) {
      final attachments = json['attachments'] ?? <Map<String, dynamic>>[];
      if (attachments.length > 1) {
        _logger.severe(
            'Saved item has multiple attachment items, will only use first: ${attachments.length}');
      }
      var found = false;
      for (Map<String, dynamic> attachment in attachments) {
        final dataItem = attachment['data'] ?? <Map<String, dynamic>>[];
        if (dataItem.length > 1) {
          _logger.severe(
              'Attachment has multiple data items, will only use first: ${dataItem.length}');
        }
        for (Map<String, dynamic> externalItem in dataItem) {
          logAdditionalKeys(['external_context'], externalItem.keys, _logger,
              Level.WARNING, 'Unknown external data item key');
          final externalData =
              externalItem['external_context'] ?? <String, String>{};
          logAdditionalKeys(knownExternalContextKeys, externalData.keys,
              _logger, Level.WARNING, 'Unknown external context key');

          name = externalData['name'] ?? '';
          final source = externalData['source'] ?? '';
          final url = externalData['url'] ?? '';

          final sourceUri = Uri.parse(source);
          final urlUri = Uri.parse(url);

          if (sourceUri.scheme.startsWith('http')) {
            linkUri = sourceUri;
            externalName = url;
          } else {
            linkUri = urlUri;
            externalName = source;
          }

          found = true;
          break;
        }

        if (found) {
          break;
        }
      }
    }

    return FacebookSavedItem(
        timestamp: timestamp,
        externalName: externalName,
        title: title,
        text: name,
        uri: linkUri);
  }
}
