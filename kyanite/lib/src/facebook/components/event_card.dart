import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/models/facebook_event.dart';
import 'package:kyanite/src/facebook/models/facebook_location_data.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/clipboard_helper.dart';
import 'package:provider/provider.dart';

class EventCard extends StatelessWidget {
  final FacebookEvent event;

  const EventCard({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const double spacingHeight = 5.0;
    final formatter =
        Provider.of<SettingsController>(context).dateTimeFormatter;
    final copyButton = Tooltip(
        message: 'Copy text version of event to clipboard',
        child: IconButton(
            onPressed: () async => await copyToClipboard(
                context: context,
                text: event.toHumanString(formatter),
                snackbarMessage: 'Copied Event to clipboard'),
            icon: const Icon(Icons.copy)));

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Wrap(
            direction: Axis.horizontal,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(
                event.name,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              copyButton,
            ],
          ),
          if (event.description.isNotEmpty) ...[
            const SizedBox(height: spacingHeight),
            Text(event.description)
          ],
          _buildStatusLine('You are:', _eventStatusToString(event.eventStatus)),
          const SizedBox(height: spacingHeight),
          _buildStatusLine(
              'Starts: ',
              formatter.format(DateTime.fromMillisecondsSinceEpoch(
                  event.startTimestamp * 1000))),
          if (event.endTimestamp != 0) ...[
            const SizedBox(height: spacingHeight),
            _buildStatusLine(
                'Stops: ',
                formatter.format(DateTime.fromMillisecondsSinceEpoch(
                    event.endTimestamp * 1000))),
          ],
          const SizedBox(height: spacingHeight),
          if (event.location.hasData()) event.location.toWidget(spacingHeight),
        ],
      ),
    );
  }

  Widget _buildStatusLine(String label, String status) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        const SizedBox(width: 2),
        Text(status),
      ],
    );
  }

  String _eventStatusToString(FacebookEventStatus status) {
    switch (status) {
      case FacebookEventStatus.declined:
        return 'Declined';
      case FacebookEventStatus.interested:
        return 'Interested';
      case FacebookEventStatus.invited:
        return 'Invited';
      case FacebookEventStatus.joined:
        return 'Joined';
      case FacebookEventStatus.owner:
        return 'Owner';
      case FacebookEventStatus.unknown:
        return 'Unknown';
    }
  }
}
