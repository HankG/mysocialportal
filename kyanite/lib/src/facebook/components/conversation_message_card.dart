import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/facebook_media_wrapper_component.dart';
import 'package:kyanite/src/facebook/models/facebook_messenger_message.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/clipboard_helper.dart';
import 'package:provider/provider.dart';

import 'facebook_link_elements_component.dart';
import 'facebook_media_timeline_component.dart';

class ConversationMessageCard extends StatelessWidget {
  final FacebookMessengerMessage message;

  const ConversationMessageCard({Key? key, required this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (Scrollable.recommendDeferredLoadingForContext(context)) {
      return const SizedBox();
    }

    const double spacingHeight = 5.0;
    const double stickerSize = 64.0;
    final settings = Provider.of<SettingsController>(context);
    final formatter = settings.dateTimeFormatter;
    final mapper = Provider.of<PathMappingService>(context);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Tooltip(
        message: formatter
            .format(DateTime.fromMillisecondsSinceEpoch(message.timestampMS)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Tooltip(
                    message: 'Copy text version of line to clipboard',
                    child: IconButton(
                        onPressed: () async => await copyToClipboard(
                            context: context,
                            text: message.toHumanString(mapper, formatter),
                            snackbarMessage:
                                'Copied Messenger line to clipboard'),
                        icon: const Icon(Icons.copy)),
                  ),
                  Text('${message.from}: ',
                      style: const TextStyle(fontWeight: FontWeight.bold)),
                  Expanded(
                      child: Text(
                    message.message,
                  )),
                ]),
            if (message.media.isNotEmpty) ...[
              const SizedBox(height: spacingHeight),
              FacebookMediaTimelineComponent(mediaAttachments: message.media)
            ],
            if (message.stickers.isNotEmpty) ...[
              const SizedBox(height: spacingHeight),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: message.stickers
                    .map((s) => FacebookMediaWrapperComponent(
                          mediaAttachment: s,
                          preferredWidth: stickerSize,
                          preferredHeight: stickerSize,
                        ))
                    .toList(),
              )
            ],
            if (message.links.isNotEmpty) ...[
              const SizedBox(height: spacingHeight),
              FacebookLinkElementsComponent(links: message.links)
            ],
          ],
        ),
      ),
    );
  }
}
