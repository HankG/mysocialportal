import 'dart:ui';

import 'package:kyanite/src/facebook/models/facebook_post.dart';

class MarkerData {
  final List<FacebookPost> posts;
  final Offset pos;
  final Color color;

  MarkerData(post, this.pos, this.color) : posts = [post];

  String toLabel() {
    if (posts.isEmpty) {
      return 'No Posts';
    }

    if (posts.length == 1) {
      return '1 Post';
    }
    return '${posts.length} posts';
  }

  String subLabel() {
    final mediaCount = posts
        .map((p) => p.mediaAttachments.length)
        .reduce((value, element) => value + element);
    if (mediaCount == 0) {
      return '';
    }

    return '$mediaCount images/videos';
  }
}
