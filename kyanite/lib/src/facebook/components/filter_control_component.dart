import 'package:flutter/material.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class FilterControl<T1, T2> extends StatefulWidget {
  final List<T1> allItems;
  final List<T1> filteredItems = [];
  final bool Function(T1)? imagesOnlyFilterFunction;
  final bool Function(T1)? videosOnlyFilterFunction;
  final bool Function(T1, String)? textSearchFilterFunction;
  final DateTime Function(T1) itemToDateTimeFunction;
  final bool Function(T1, DateTime, DateTime) dateRangeFilterFunction;
  final T1 Function(T1)? copyPrimary;
  final List<T2> Function(T1)? getSecondary;
  final bool Function(T2)? secondaryImagesOnlyFilterFunction;
  final bool Function(T2)? secondaryVideosOnlyFilterFunction;
  final bool Function(T2, String)? secondaryTextSearchFilterFunction;
  final DateTime Function(T2)? secondaryItemToDateTimeFunction;
  final bool Function(T2, DateTime, DateTime)? secondaryDateRangeFilterFunction;
  final Widget Function(BuildContext, List<T1>) builder;
  final bool hasSecondaryFunctions;

  FilterControl(
      {Key? key,
      required this.allItems,
      this.imagesOnlyFilterFunction,
      this.textSearchFilterFunction,
      this.videosOnlyFilterFunction,
      required this.itemToDateTimeFunction,
      required this.dateRangeFilterFunction,
      required this.builder,
      this.copyPrimary,
      this.getSecondary,
      this.secondaryImagesOnlyFilterFunction,
      this.secondaryVideosOnlyFilterFunction,
      this.secondaryTextSearchFilterFunction,
      this.secondaryItemToDateTimeFunction,
      this.secondaryDateRangeFilterFunction})
      : hasSecondaryFunctions = getSecondary != null ||
            secondaryDateRangeFilterFunction != null ||
            secondaryImagesOnlyFilterFunction != null ||
            secondaryItemToDateTimeFunction != null ||
            secondaryTextSearchFilterFunction != null ||
            secondaryVideosOnlyFilterFunction != null,
        super(key: key) {
    if (hasSecondaryFunctions && getSecondary == null) {
      throw Exception(
          'Secondary filtering functions defined but "getSecondary" method is not.');
    }

    if (hasSecondaryFunctions && copyPrimary == null) {
      throw Exception(
          'Primary copy method not defined even though secondary filtering is occurring.');
    }
  }

  @override
  State<FilterControl<T1, T2>> createState() => _FilterControlState<T1, T2>();
}

class _FilterControlState<T1, T2> extends State<FilterControl<T1, T2>> {
  static final _logger = Logger('$_FilterControlState');
  bool _withImagesOnly = false;
  bool _withVideosOnly = false;
  bool _withDateFilter = false;
  bool _withTextFilter = false;
  DateTime _filterStartDate = DateTime.now();
  DateTime _filterEndDate = DateTime.now();
  DateTime _earliestPossibleDate = DateTime.now();
  DateTime _latestPossibleDate = DateTime.now();
  final _searchText = TextEditingController();
  bool _showSearch = false;

  @override
  void initState() {
    _logger.fine('Init state');
    final times =
        widget.allItems.map((e) => widget.itemToDateTimeFunction(e)).toList();
    if (times.isNotEmpty) {
      times.sort((t1, t2) => t1.compareTo(t2));
      _earliestPossibleDate = times.first;
      _latestPossibleDate = times.last;
      _filterStartDate = _earliestPossibleDate;
      _filterEndDate = _latestPossibleDate;
    }

    _searchText.text = '';
    _searchText.addListener(_updateFilter);
    _updateFilter();
    super.initState();
  }

  void _updateFilter() {
    _logger.fine('Update Filter');
    final bool testForText = _withTextFilter && _searchText.text.length > 2;
    final String searchTerm = _searchText.text.trim();

    final times =
        widget.allItems.map((e) => widget.itemToDateTimeFunction(e)).toList();
    if (times.isNotEmpty) {
      times.sort((t1, t2) => t1.compareTo(t2));
      _earliestPossibleDate = times.first;
      _latestPossibleDate = times.last;
    }

    var currentFilteredItems = widget.allItems.where((p) {
      bool passes = true;
      if (_withImagesOnly && widget.imagesOnlyFilterFunction != null) {
        passes &= widget.imagesOnlyFilterFunction!(p);
      }

      if (passes &&
          _withVideosOnly &&
          widget.videosOnlyFilterFunction != null) {
        passes &= widget.videosOnlyFilterFunction!(p);
      }

      if (passes && _withDateFilter) {
        passes &=
            widget.dateRangeFilterFunction(p, _filterStartDate, _filterEndDate);
      }

      if (passes && testForText && widget.textSearchFilterFunction != null) {
        passes &= widget.textSearchFilterFunction!(p, searchTerm);
      }
      return passes;
    });

    if (widget.hasSecondaryFunctions) {
      final finalFilteredItems = <T1>[];
      for (var item in currentFilteredItems) {
        final subList = widget.getSecondary!(item);
        final filteredSubList = subList.where((i) {
          bool passes = true;
          if (_withImagesOnly &&
              widget.secondaryImagesOnlyFilterFunction != null) {
            passes &= widget.secondaryImagesOnlyFilterFunction!(i);
          }

          if (passes &&
              _withVideosOnly &&
              widget.secondaryVideosOnlyFilterFunction != null) {
            passes &= widget.secondaryVideosOnlyFilterFunction!(i);
          }

          if (passes &&
              _withDateFilter &&
              widget.secondaryDateRangeFilterFunction != null) {
            passes &= widget.secondaryDateRangeFilterFunction!(
                i, _filterStartDate, _filterEndDate);
          }

          if (passes &&
              testForText &&
              widget.secondaryTextSearchFilterFunction != null) {
            passes &= widget.secondaryTextSearchFilterFunction!(i, searchTerm);
          }
          return passes;
        });
        if (subList.length != filteredSubList.length) {
          final finalItem = widget.copyPrimary!(item);
          final finalSublist = widget.getSecondary!(finalItem);
          finalSublist.clear();
          finalSublist.addAll(filteredSubList);
          finalFilteredItems.add(finalItem);
        } else {
          finalFilteredItems.add(item);
        }
      }
      setState(() {
        widget.filteredItems.clear();
        widget.filteredItems.addAll(finalFilteredItems);
      });
    } else {
      setState(() {
        widget.filteredItems.clear();
        widget.filteredItems.addAll(currentFilteredItems);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _logger.fine('Redrawing');
    _updateFilter();

    return Scaffold(
      body: Column(children: [
        if (_showSearch) ...[
          _buildFilterBox(context),
          const Divider(),
        ],
        Expanded(child: widget.builder(context, widget.filteredItems)),
      ]),
      floatingActionButton: FloatingActionButton(
        heroTag: null,
        child: const Icon(Icons.search),
        tooltip: 'Toggle filter dialog visibility',
        onPressed: () {
          setState(() {
            _logger.fine('Toggling show search');
            _showSearch = !_showSearch;
          });
        },
      ),
    );
  }

  Widget _buildFilterBox(BuildContext context) {
    return Column(children: [
      if (widget.textSearchFilterFunction != null) _buildTextFilter(context),
      _buildDateFilter(context),
      if (widget.imagesOnlyFilterFunction != null) _buildImagesOnly(context),
      if (widget.videosOnlyFilterFunction != null) _buildVideosOnly(context),
      _buildStatusLine(),
    ]);
  }

  Widget _buildStatusLine() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
          '${widget.filteredItems.length} of ${widget.allItems.length} items visible'),
    );
  }

  Widget _buildVideosOnly(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(children: [
        Checkbox(
            value: _withVideosOnly,
            onChanged: (value) => setState(() {
                  _withVideosOnly = value ?? false;
                  _updateFilter();
                })),
        const SizedBox(width: 1),
        const Text('Only with videos'),
      ]),
    );
  }

  Widget _buildImagesOnly(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(children: [
        Checkbox(
            value: _withImagesOnly,
            onChanged: (value) => setState(() {
                  _withImagesOnly = value ?? false;
                  _updateFilter();
                })),
        const SizedBox(width: 1),
        const Text('Only with images'),
      ]),
    );
  }

  Widget _buildTextFilter(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(children: [
        Checkbox(
            value: _withTextFilter,
            onChanged: (value) => setState(() {
                  _withTextFilter = value ?? false;
                  _updateFilter();
                })),
        const Text(
          'Search Text:',
        ),
        const SizedBox(width: 5),
        TextField(
            enabled: _withTextFilter,
            readOnly: !_withTextFilter,
            controller: _searchText,
            decoration: const InputDecoration(
              constraints: BoxConstraints(maxWidth: 500.0),
              hintText: 'Limit posts to only those with this exact text',
            )),
      ]),
    );
  }

  Widget _buildDateFilter(BuildContext context) {
    final formatter = Provider.of<SettingsController>(context).dateFormatter;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(children: [
        Checkbox(
            value: _withDateFilter,
            onChanged: (value) => setState(() {
                  _withDateFilter = value ?? false;
                  _updateFilter();
                })),
        const Text(
          'Only between dates:',
        ),
        const SizedBox(width: 5),
        SizedBox(
            width: 150,
            child: TextField(
              enabled: _withDateFilter,
              readOnly: true,
              controller: TextEditingController(
                  text: formatter.format(_filterStartDate)),
              textAlign: TextAlign.center,
              decoration: const InputDecoration(
                hintText: 'Earliest',
              ),
            )),
        const SizedBox(width: 5),
        ElevatedButton(
            onPressed: !_withDateFilter
                ? null
                : () async {
                    final selectedDate = await showDatePicker(
                      context: context,
                      initialDate: _filterStartDate,
                      firstDate: _earliestPossibleDate,
                      lastDate: _filterEndDate,
                      currentDate: DateTime.now(),
                      helpText: 'Select starting date filter',
                    );
                    if (selectedDate != null) {
                      setState(() {
                        _filterStartDate = selectedDate;
                      });
                    }
                  },
            child: const Text('Set Start')),
        const SizedBox(width: 5),
        const Text('to'),
        const SizedBox(width: 5),
        SizedBox(
            width: 150,
            child: TextField(
              enabled: _withDateFilter,
              readOnly: true,
              controller:
                  TextEditingController(text: formatter.format(_filterEndDate)),
              textAlign: TextAlign.center,
            )),
        const SizedBox(width: 5),
        ElevatedButton(
            onPressed: !_withDateFilter
                ? null
                : () async {
                    final selectedDate = await showDatePicker(
                      context: context,
                      initialDate: _filterEndDate,
                      firstDate: _filterStartDate,
                      lastDate: _latestPossibleDate,
                      currentDate: DateTime.now(),
                      helpText: 'Select ending date filter',
                    );
                    if (selectedDate != null) {
                      setState(() {
                        _filterEndDate = selectedDate;
                      });
                    }
                  },
            child: const Text('Set Stop')),
        const SizedBox(width: 5),
        ElevatedButton(
            onPressed: !_withDateFilter
                ? null
                : () {
                    setState(() {
                      _filterStartDate = _earliestPossibleDate;
                      _filterEndDate = _latestPossibleDate;
                    });
                  },
            child: const Text('Reset')),
      ]),
    );
  }
}
