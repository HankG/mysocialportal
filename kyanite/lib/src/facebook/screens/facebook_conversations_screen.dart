import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/conversation_message_card.dart';
import 'package:kyanite/src/facebook/components/filter_control_component.dart';
import 'package:kyanite/src/facebook/models/facebook_messenger_conversation.dart';
import 'package:kyanite/src/facebook/models/facebook_messenger_message.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/screens/error_screen.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

import '../../screens/loading_status_screen.dart';
import '../../screens/standin_status_screen.dart';

class FacebookConversationScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookConversationScreen');

  const FacebookConversationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final service = Provider.of<FacebookArchiveDataService>(context);
    _logger.info('Build Facebook Conversation Screen');

    return FutureBuilder<
            Result<List<FacebookMessengerConversation>, ExecError>>(
        future: service.getConvos(),
        builder: (context, snapshot) {
          _logger.fine('Future Conversation builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            _logger.finer('No data yet, just return status screen');
            return const LoadingStatusScreen(
              title: 'Loading Conversations',
              subTitle:
                  'This can take several minutes the first time loading the archive.',
            );
          }

          final convoResult = snapshot.requireData;
          if (convoResult.isFailure) {
            return ErrorScreen(error: convoResult.error);
          }

          _logger.finer(
              'Now have data! ${snapshot.requireData.value.length} conversations');

          final conversations = convoResult.value;

          if (conversations.isEmpty) {
            return const StandInStatusScreen(
                title: 'No conversations were found');
          }

          return _FacebookConversionsFilteredWidget(
              conversations: conversations);
        });
  }
}

class _FacebookConversionsFilteredWidget extends StatelessWidget {
  final List<FacebookMessengerConversation> conversations;

  const _FacebookConversionsFilteredWidget(
      {Key? key, required this.conversations})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FilterControl<FacebookMessengerConversation,
            FacebookMessengerMessage>(
        allItems: conversations,
        imagesOnlyFilterFunction: (convo) => convo.hasImages(),
        videosOnlyFilterFunction: (convo) => convo.hasVideos(),
        textSearchFilterFunction: (convo, text) =>
            convo.title.contains(text) ||
            convo.messages
                .map((e) => e.message)
                .where((element) => element.contains(text))
                .isNotEmpty,
        itemToDateTimeFunction: (convo) =>
            DateTime.fromMillisecondsSinceEpoch(convo.latestTimestampMS()),
        dateRangeFilterFunction: (convo, start, stop) =>
            timestampInRange(convo.earliestTimestampMS(), start, stop) ||
            timestampInRange(convo.latestTimestampMS(), start, stop),
        getSecondary: (convo) => convo.messages,
        copyPrimary: (convo) => convo.copy(),
        secondaryItemToDateTimeFunction: (message) =>
            DateTime.fromMillisecondsSinceEpoch(message.timestampMS),
        secondaryDateRangeFilterFunction: (message, start, stop) =>
            timestampInRange(message.timestampMS, start, stop),
        secondaryImagesOnlyFilterFunction: (message) =>
            message.hasImages() || message.stickers.isNotEmpty,
        secondaryVideosOnlyFilterFunction: (message) => message.hasVideos(),
        secondaryTextSearchFilterFunction: (message, text) =>
            message.message.contains(text),
        builder: (context, conversations) {
          return _FacebookConversationsScreenWidget(
              conversations: conversations);
        });
  }
}

class _FacebookConversationsScreenWidget extends StatefulWidget {
  final List<FacebookMessengerConversation> conversations;

  const _FacebookConversationsScreenWidget(
      {Key? key, required this.conversations})
      : super(key: key);

  @override
  State<_FacebookConversationsScreenWidget> createState() =>
      _FacebookConversationsScreenWidgetState();
}

class _FacebookConversationsScreenWidgetState
    extends State<_FacebookConversationsScreenWidget> {
  static final _logger = Logger('$_FacebookConversationsScreenWidget');

  static final FacebookMessengerConversation noConversationSelected =
      FacebookMessengerConversation.empty();
  FacebookMessengerConversation _currentConversation = noConversationSelected;
  final ItemScrollController _scrollController = ItemScrollController();

  _setConversation(int index) {
    if (index > widget.conversations.length) {
      _logger.severe(
          'Requested participants index greater then max: $index > ${widget.conversations.length}');
      return;
    }

    final conversation =
        index < 0 ? noConversationSelected : widget.conversations[index];
    if (conversation == _currentConversation) {
      return;
    }

    _logger.finer('Jumping to $index');
    final scrollToIndex = index > 0 ? index - 1 : 0;
    _scrollController.scrollTo(
        index: scrollToIndex, duration: const Duration(seconds: 1));
    setState(() {
      _currentConversation = conversation;
    });
  }

  @override
  Widget build(BuildContext context) {
    _logger.fine('Build _FacebookConversationsScreenWidget');
    if (!widget.conversations.contains(_currentConversation)) {
      final selectedIndex = widget.conversations
          .indexWhere((c) => c.id == _currentConversation.id);
      _setConversation(selectedIndex);
    }
    return Row(
      children: [
        SizedBox(
          width: 200,
          child:
              _buildConversationParticipantsList(context, widget.conversations),
        ),
        SizedBox(width: 1, child: Container(color: Colors.grey)),
        Expanded(child: _buildConversationPanel(context)),
      ],
    );
  }

  Widget _buildConversationParticipantsList(
      BuildContext context, List<FacebookMessengerConversation> conversations) {
    _logger.fine('Build _buildConversationParticipantsList');
    final textTheme = Theme.of(context).textTheme;
    return ScrollablePositionedList.separated(
        itemScrollController: _scrollController,
        itemCount: conversations.length,
        itemBuilder: (context, index) {
          final conversation = conversations[index];
          return TextButton(
              onPressed: () => _setConversation(index),
              style: _currentConversation == conversation
                  ? TextButton.styleFrom(
                      backgroundColor:
                          textTheme.bodyText1?.decorationColor ?? Colors.blue)
                  : null,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(conversation.title,
                    softWrap: true,
                    textAlign: TextAlign.start,
                    style: _currentConversation == conversation
                        ? textTheme.bodyText1
                        : textTheme.bodyText2),
              ));
        },
        separatorBuilder: (context, index) {
          return const Divider(
            color: Colors.black,
            thickness: 0.2,
          );
        });
  }

  Widget _buildConversationPanel(BuildContext context) {
    _logger.fine('Build _buildConversationPanel');
    if (_currentConversation == noConversationSelected) {
      return const StandInStatusScreen(
        title: 'No conversation selected',
        subTitle: 'Select a conversation to display here',
      );
    }

    final settings = Provider.of<SettingsController>(context);
    final username = settings.facebookName;

    return ListView.separated(
        primary: false,
        restorationId: 'facebookConversationPane',
        itemCount: _currentConversation.messages.length,
        itemBuilder: (context, index) {
          final msg = _currentConversation.messages[index];
          return ConversationMessageCard(
              message: msg.from == username ? msg.copy(from: 'You') : msg);
        },
        separatorBuilder: (context, index) {
          return const SizedBox(height: 5);
        });
  }
}
