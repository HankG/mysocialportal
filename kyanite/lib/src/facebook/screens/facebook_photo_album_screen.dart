import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/facebook_media_wrapper_component.dart';
import 'package:kyanite/src/facebook/components/filter_control_component.dart';
import 'package:kyanite/src/facebook/models/facebook_album.dart';
import 'package:kyanite/src/facebook/models/facebook_media_attachment.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:kyanite/src/screens/standin_status_screen.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import 'facebook_media_slideshow_screen.dart';

class FacebookPhotoAlbumScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookPhotoAlbumScreen');
  final FacebookAlbum album;

  const FacebookPhotoAlbumScreen({Key? key, required this.album})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine(
        'Build FacebookPhotoAlbumScreen for ${album.name} w/ ${album.photos.length} photos');

    return album.photos.isEmpty
        ? _buildEmptyGalleryScrene(context)
        : FilterControl<FacebookMediaAttachment, dynamic>(
            allItems: album.photos,
            textSearchFilterFunction: (photo, text) =>
                photo.title.contains(text) || photo.description.contains(text),
            itemToDateTimeFunction: (photo) =>
                DateTime.fromMillisecondsSinceEpoch(
                    photo.creationTimestamp * 1000),
            dateRangeFilterFunction: (photo, start, stop) =>
                timestampInRange(photo.creationTimestamp * 1000, start, stop),
            builder: (context, photos) => _FacebookPhotoAlbumScreenWidget(
              photos: photos,
              albumName: album.name,
              albumDescription: album.description,
            ),
          );
  }

  _buildEmptyGalleryScrene(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(album.name),
          backgroundColor: Theme.of(context).canvasColor,
          foregroundColor: Theme.of(context).primaryColor,
          elevation: 0.0,
        ),
        body: const StandInStatusScreen(title: 'No photos in album'));
  }
}

class _FacebookPhotoAlbumScreenWidget extends StatelessWidget {
  static final _logger = Logger('$_FacebookPhotoAlbumScreenWidget');
  final List<FacebookMediaAttachment> photos;
  final String albumName;
  final String albumDescription;

  const _FacebookPhotoAlbumScreenWidget(
      {Key? key,
      required this.photos,
      this.albumName = '',
      this.albumDescription = ''})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine('Rebuilding album widget w/${photos.length} photos');
    final pathMapper = Provider.of<PathMappingService>(context);
    final settingsController = Provider.of<SettingsController>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text(albumName),
          backgroundColor: Theme.of(context).canvasColor,
          foregroundColor: Theme.of(context).primaryColor,
          elevation: 0.0,
        ),
        body: Padding(
            padding: const EdgeInsets.only(
              left: 16,
              right: 16,
              top: 16,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (albumDescription.isNotEmpty) ...[
                  Text(
                    albumDescription,
                    softWrap: true,
                  ),
                  const SizedBox(height: 5)
                ],
                Expanded(
                  child: GridView.builder(
                    itemCount: photos.length,
                    gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                            mainAxisExtent: 400.0, maxCrossAxisExtent: 400.0),
                    itemBuilder: (itemBuilderContext, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: () async {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return MultiProvider(
                                  providers: [
                                    ChangeNotifierProvider.value(
                                        value: settingsController),
                                    Provider.value(value: pathMapper)
                                  ],
                                  child: FacebookMediaSlideshowScreen(
                                      mediaAttachments: photos,
                                      initialIndex: index));
                            }));
                          },
                          child: FacebookMediaWrapperComponent(
                            mediaAttachment: photos[index],
                            preferredWidth: 300,
                            preferredHeight: 300,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            )));
  }
}
