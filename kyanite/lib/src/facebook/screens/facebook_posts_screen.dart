import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/filter_control_component.dart';
import 'package:kyanite/src/facebook/components/post_card.dart';
import 'package:kyanite/src/facebook/models/facebook_post.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/screens/error_screen.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';

import '../../screens/loading_status_screen.dart';
import '../../screens/standin_status_screen.dart';

class FacebookPostsScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookPostsScreen');

  const FacebookPostsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.info('Build FacebookPostListView');
    final service = Provider.of<FacebookArchiveDataService>(context);
    final username = Provider.of<SettingsController>(context).facebookName;

    return FutureBuilder<Result<List<FacebookPost>, ExecError>>(
        future: service.getPosts(),
        builder: (context, snapshot) {
          _logger.info('FacebookPostListView Future builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            return const LoadingStatusScreen(title: 'Loading posts');
          }

          final postsResult = snapshot.requireData;

          if (postsResult.isFailure) {
            return ErrorScreen(
                title: 'Error getting posts', error: postsResult.error);
          }

          final allPosts = postsResult.value;
          final filteredPosts = username.isEmpty
              ? allPosts
              : allPosts.where((p) =>
                  p.title != username ||
                  p.post.isNotEmpty ||
                  p.mediaAttachments.isNotEmpty ||
                  p.links.isNotEmpty);

          final posts = username.isEmpty
              ? filteredPosts.toList()
              : filteredPosts.map((p) {
                  var newTitle = p.title;
                  if (p.title == username) {
                    newTitle = 'You posted';
                  } else {
                    newTitle = p.title
                        .replaceAll(username, 'You')
                        .replaceAll(wholeWordRegEx('his'), 'your')
                        .replaceAll(wholeWordRegEx('her'), 'your');
                  }
                  if (newTitle == p.title) {
                    return p;
                  } else {
                    return p.copy(title: newTitle);
                  }
                }).toList();
          if (posts.isEmpty) {
            return const StandInStatusScreen(title: 'No posts were found');
          }

          _logger.fine('Build Posts ListView');
          return _FacebookPostsScreenWidget(posts: posts);
        });
  }
}

class _FacebookPostsScreenWidget extends StatelessWidget {
  static final _logger = Logger('$_FacebookPostsScreenWidget');

  final List<FacebookPost> posts;

  const _FacebookPostsScreenWidget({Key? key, required this.posts})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine('Redrawing');
    return FilterControl<FacebookPost, dynamic>(
        allItems: posts,
        imagesOnlyFilterFunction: (post) => post.hasImages(),
        videosOnlyFilterFunction: (post) => post.hasVideos(),
        textSearchFilterFunction: (post, text) =>
            post.title.contains(text) || post.post.contains(text),
        itemToDateTimeFunction: (post) =>
            DateTime.fromMillisecondsSinceEpoch(post.creationTimestamp * 1000),
        dateRangeFilterFunction: (post, start, stop) =>
            timestampInRange(post.creationTimestamp * 1000, start, stop),
        builder: (context, items) {
          if (items.isEmpty) {
            return const StandInStatusScreen(
                title: 'No posts meet filter criteria');
          }

          return ScrollConfiguration(
            behavior:
                ScrollConfiguration.of(context).copyWith(scrollbars: false),
            child: ListView.separated(
                primary: false,
                physics: const RangeMaintainingScrollPhysics(),
                restorationId: 'facebookPostsListView',
                itemCount: items.length,
                itemBuilder: (context, index) {
                  _logger.finer('Rendering FacebookPost List Item');
                  return PostCard(post: items[index]);
                },
                separatorBuilder: (context, index) {
                  return const Divider(
                    color: Colors.black,
                    thickness: 0.2,
                  );
                }),
          );
        });
  }
}
