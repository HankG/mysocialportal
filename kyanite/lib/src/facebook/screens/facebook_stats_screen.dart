import 'package:flutter/material.dart';
import 'package:kyanite/src/components/heatmap_widget.dart';
import 'package:kyanite/src/components/timechart_widget.dart';
import 'package:kyanite/src/components/word_frequency_widget.dart';
import 'package:kyanite/src/facebook/components/filter_control_component.dart';
import 'package:kyanite/src/facebook/models/facebook_media_attachment.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/models/time_element.dart';
import 'package:kyanite/src/screens/standin_status_screen.dart';
import 'package:kyanite/src/utils/snackbar_status_builder.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class FacebookStatsScreen extends StatefulWidget {
  const FacebookStatsScreen({Key? key}) : super(key: key);

  @override
  State<FacebookStatsScreen> createState() => _FacebookStatsScreenState();
}

class _FacebookStatsScreenState extends State<FacebookStatsScreen> {
  static final _logger = Logger("$_FacebookStatsScreenState");
  FacebookArchiveDataService? archiveDataService;
  final allItems = <TimeElement>[];
  StatType statType = StatType.selectType;
  bool hasText = true;

  @override
  void initState() {
    super.initState();
  }

  void _updateSelection(BuildContext context, StatType newType) async {
    statType = newType;
    await _updateItems(context);
    setState(() {});
  }

  Future<void> _updateItems(BuildContext context) async {
    if (archiveDataService == null) {
      _logger.severe(
          "Can't update stats because archive data service is not set yet");
    }
    allItems.clear();
    Iterable<TimeElement> newItems = [];
    switch (statType) {
      case StatType.post:
        newItems = (await archiveDataService!.getPosts()).fold(
            onSuccess: (posts) => posts.map((e) => TimeElement(
                timeInMS: e.creationTimestamp * 1000,
                hasImages: e.hasImages(),
                hasVideos: e.hasVideos(),
                title: e.title,
                text: e.post)),
            onError: (error) {
              _logger.severe('Error getting posts: $error');
              return [];
            });
        break;
      case StatType.comment:
        newItems = (await archiveDataService!.getComments()).fold(
            onSuccess: (comments) => comments.map((e) => TimeElement(
                timeInMS: e.creationTimestamp * 1000,
                hasImages: e.hasImages(),
                hasVideos: e.hasVideos(),
                title: e.title,
                text: e.comment)),
            onError: (error) {
              _logger.severe('Error getting comments: $error');
              return [];
            });
        break;
      case StatType.photo:
        newItems = (await archiveDataService!.getAlbums()).fold(
            onSuccess: (albums) => albums.expand((album) => album.photos).map(
                (photo) => TimeElement(
                    timeInMS: photo.creationTimestamp * 1000,
                    hasImages: true,
                    hasVideos: false,
                    title: photo.title,
                    text: photo.description)),
            onError: (error) {
              _logger.severe('Error getting photos: $error');
              return [];
            });
        break;
      case StatType.video:
        newItems = (await archiveDataService!.getPosts()).fold(
            onSuccess: (posts) => posts
                .where((post) => post.hasVideos())
                .expand((post) => post.mediaAttachments.where((m) =>
                    m.estimatedType() == FacebookAttachmentMediaType.video))
                .map((e) => TimeElement(
                    timeInMS: e.creationTimestamp * 1000,
                    hasImages: false,
                    hasVideos: true,
                    title: e.title,
                    text: e.description)),
            onError: (error) {
              _logger.severe('Error getting comments: $error');
              return [];
            });
        break;
      case StatType.selectType:
        break;
      default:
        _logger.severe('Unknown stat type');
        Future.delayed(
            Duration.zero,
            () => SnackBarStatusBuilder.buildSnackbar(
                context, 'Unknown stat type'));
    }

    allItems.addAll(newItems);
  }

  @override
  Widget build(BuildContext context) {
    archiveDataService = Provider.of<FacebookArchiveDataService>(context);

    return FilterControl<TimeElement, dynamic>(
        key: UniqueKey(),
        allItems: allItems,
        imagesOnlyFilterFunction: (item) => item.hasImages,
        videosOnlyFilterFunction: (item) => item.hasVideos,
        textSearchFilterFunction: (item, text) => item.hasText(text),
        itemToDateTimeFunction: (item) => item.timestamp,
        dateRangeFilterFunction: (item, start, stop) =>
            dateTimeInRange(item.timestamp, start, stop),
        builder: (context, items) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              constraints: const BoxConstraints(maxWidth: 800),
              child: Column(
                children: [
                  Row(
                    children: [
                      const Text('Statistic Type: '),
                      DropdownButton(
                          hint: const Text('Select type'),
                          value:
                              statType == StatType.selectType ? null : statType,
                          onChanged: (StatType? type) =>
                              _updateSelection(context, type!),
                          items: StatType.values
                              .map((value) => DropdownMenuItem(
                                  enabled: value != StatType.selectType,
                                  value: value,
                                  child: Text(value.toLabel())))
                              .where((element) => element.enabled)
                              .toList()),
                    ],
                  ),
                  statType == StatType.selectType
                      ? const Expanded(
                          child: StandInStatusScreen(
                              title: 'Select data type to show graphs'),
                        )
                      : _buildChartPanel(context, items)
                ],
              ),
            ),
          );
        });
  }

  Widget _buildChartPanel(BuildContext context, List<TimeElement> items) {
    if (items.isEmpty) {
      return Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            StandInStatusScreen(
              title: 'No items for statistics',
              subTitle: 'Adjust the filter or select a new archive',
            )
          ],
        ),
      );
    }

    return Expanded(
        child: SingleChildScrollView(
      primary: false,
      child: Column(children: [
        ..._buildGraphScreens(context, items),
        const Divider(),
        WordFrequencyWidget(items),
      ]),
    ));
  }

  List<Widget> _buildGraphScreens(
      BuildContext context, List<TimeElement> items) {
    return [
      TimeChartWidget(timeElements: items),
      const Divider(),
      HeatMapWidget(key: UniqueKey(), timeElements: items),
    ];
  }
}

enum StatType {
  post,
  comment,
  photo,
  video,
  selectType,
}

extension StatTypeString on StatType {
  String toLabel() {
    switch (this) {
      case StatType.post:
        return "Posts";
      case StatType.comment:
        return "Comments";
      case StatType.photo:
        return "Photos";
      case StatType.video:
        return "Videos";
      case StatType.selectType:
        return "Select Type";
    }
  }

  StatType fromLabel(String text) {
    if (text == 'Posts') {
      return StatType.post;
    }

    if (text == 'Comments') {
      return StatType.comment;
    }

    if (text == 'Photos') {
      return StatType.photo;
    }

    if (text == 'Videos') {
      return StatType.video;
    }

    if (text == 'Select Type') {
      return StatType.selectType;
    }

    throw ArgumentError(['Unknown enum type: $text', 'text']);
  }
}
