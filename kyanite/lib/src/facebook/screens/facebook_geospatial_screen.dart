import 'dart:math';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kyanite/src/facebook/components/geo/geo_extensions.dart';
import 'package:kyanite/src/facebook/components/geo/map_bounds.dart';
import 'package:kyanite/src/facebook/components/geo/marker_data.dart';
import 'package:kyanite/src/facebook/components/post_card.dart';
import 'package:kyanite/src/facebook/models/facebook_post.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/facebook/services/path_mapping_service.dart';
import 'package:kyanite/src/screens/error_screen.dart';
import 'package:kyanite/src/screens/loading_status_screen.dart';
import 'package:kyanite/src/screens/standin_status_screen.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:kyanite/src/utils/temp_file_builder.dart';
import 'package:latlng/latlng.dart';
import 'package:logging/logging.dart';
import 'package:map/map.dart';
import 'package:multi_split_view/multi_split_view.dart';
import 'package:network_to_file_image/network_to_file_image.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';

class FacebookGeospatialViewScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookGeospatialViewScreen');

  const FacebookGeospatialViewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.info('Build FacebookGeospatialViewScreen');
    final service = Provider.of<FacebookArchiveDataService>(context);
    final username = Provider.of<SettingsController>(context).facebookName;

    return FutureBuilder<Result<List<FacebookPost>, ExecError>>(
        future: service.getPosts(),
        builder: (context, snapshot) {
          _logger.info('FacebookGeospatialViewScreen Future builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            return const LoadingStatusScreen(title: 'Loading posts');
          }

          final postsResult = snapshot.requireData;

          if (postsResult.isFailure) {
            return ErrorScreen(
                title: 'Error getting posts', error: postsResult.error);
          }

          final allPosts = postsResult.value;
          final filteredPosts =
              allPosts.where((p) => p.locationData.hasPosition);

          final posts = username.isEmpty
              ? filteredPosts.toList()
              : filteredPosts.map((p) {
                  var newTitle = p.title;
                  if (p.title == username) {
                    newTitle = 'You posted';
                  } else {
                    newTitle = p.title
                        .replaceAll(username, 'You')
                        .replaceAll(wholeWordRegEx('his'), 'your')
                        .replaceAll(wholeWordRegEx('her'), 'your');
                  }
                  if (newTitle == p.title) {
                    return p;
                  } else {
                    return p.copy(title: newTitle);
                  }
                }).toList();
          if (posts.isEmpty) {
            return const StandInStatusScreen(title: 'No posts were found');
          }

          _logger.fine('Build Posts ListView');
          return GeospatialView(posts: posts);
        });
  }
}

class GeospatialView extends StatefulWidget {
  final List<FacebookPost> posts;

  const GeospatialView({Key? key, required this.posts}) : super(key: key);

  @override
  _GeospatialViewState createState() => _GeospatialViewState();
}

class _GeospatialViewState extends State<GeospatialView> {
  static final _logger = Logger('$_GeospatialViewState');
  static const billboardXSize = 150.0;
  static const billboardYSize = 60.0;
  static const maxZoom = 19.957;
  static const minZoom = 2.0;

  MapBounds bounds = MapBounds.globe;
  final controller = MapController(
    location: LatLng(0.0, 0.0),
    zoom: 3,
  );

  Offset? dragStart;
  final postsInList = <FacebookPost>[];
  final postsInView = <FacebookPost>[];
  double scaleStart = 1.0;

  @override
  void initState() {
    _logger.finer('_GeospatialViewState initState');
    double latitudeSum = 0.0;
    double longitudeSum = 0.0;
    for (final p in widget.posts) {
      latitudeSum += p.locationData.latitude;
      longitudeSum += p.locationData.longitude;
    }

    double averageLatitude = latitudeSum / widget.posts.length.toDouble();
    double averageLongitude = longitudeSum / widget.posts.length.toDouble();
    controller.center = LatLng(averageLatitude, averageLongitude);
    super.initState();
  }

  void _onDoubleTap() {
    controller.zoom += 0.5;
    setState(() {});
  }

  void _updatePostsInBoundsFilter() {
    postsInView.clear();
    postsInView.addAll(widget.posts.where((p) => bounds.pointInBounds(
        p.locationData.latitude, p.locationData.longitude)));
    _logger.finest(() => 'Posts in view? ${postsInView.length}');
  }

  void _onScaleStart(ScaleStartDetails details) {
    _logger.finest('Drag update');
    dragStart = details.focalPoint;
    scaleStart = 1.0;
  }

  void _onScaleUpdate(ScaleUpdateDetails details, MapTransformer transformer) {
    _logger.finest('_onScaleUpdate');
    final now = details.focalPoint;
    final scaleDiff = details.scale - scaleStart;
    scaleStart = details.scale;

    if (scaleDiff > 0) {
      _tryZoom(controller.zoom + 0.02, transformer);
    } else if (scaleDiff < 0) {
      _tryZoom(controller.zoom - 0.02, transformer);
    } else {
      final diff = now - dragStart!;
      dragStart = now;
      controller.drag(diff.dx, diff.dy);
      _logger.finest('Dragged map by: ${diff.dx}, ${diff.dy}');
      if (MapBounds.computed(transformer).isOverflowed()) {
        controller.drag(-diff.dx, -diff.dy);
      }
    }

    setState(() {});
  }

  void _tryZoom(double newZoom, MapTransformer transformer) {
    final originalZoom = controller.zoom;
    final tryZoomValue = max(minZoom, min(maxZoom, newZoom));
    controller.zoom = tryZoomValue;
    if (MapBounds.computed(transformer).isOverflowed()) {
      _logger.finest(
          () => 'This zoom overflowed map so setting back: ${controller.zoom}');
      controller.zoom = originalZoom;
    } else {
      _logger.finest(() => 'New zoom: ${controller.zoom}');
    }
  }

  void _fixOutOfBounds(MapTransformer transformer, {double increment = 0.5}) {
    _logger.finest(
        'Map somehow out of bounds (maybe window enlargement), attempting to correct by zooming in');
    var overflowed = true;
    while (overflowed && controller.zoom < (maxZoom - increment)) {
      controller.zoom += increment;
      bounds = MapBounds.computed(transformer);
      overflowed = bounds.isOverflowed();
    }
  }

  @override
  Widget build(BuildContext context) {
    _logger.finer('Call Geospatial builder');
    final formatter =
        Provider.of<SettingsController>(context).dateTimeFormatter;
    final mapper = Provider.of<PathMappingService>(context);

    _updatePostsInBoundsFilter();
    final map = _buildMap(context, formatter, mapper);
    final postList = _buildPostList(context, formatter, mapper);
    final panel = MultiSplitView(
      axis: Axis.vertical,
      children: [
        map,
        postList,
      ],
      initialWeights: const [0.3],
      minimalWeight: 0.2,
    );

    return MultiSplitViewTheme(
        child: panel,
        data: MultiSplitViewThemeData(
            dividerPainter: DividerPainters.grooved1(
                size: 50,
                highlightedSize: 75,
                color: Colors.indigo[100]!,
                highlightedColor: Colors.indigo[900]!)));
  }

  Widget _buildPostList(
      BuildContext context, DateFormat formatter, PathMappingService mapper) {
    _logger.finest(() => 'Building PostList with ${postsInList.length} items');
    if (postsInList.isEmpty) {
      return const StandInStatusScreen(
        title: 'No Selected Posts',
        subTitle:
            'Click on summary bubbles to select posts\n(and right click on map to clear selection)',
      );
    }

    return ScrollConfiguration(
      behavior: ScrollConfiguration.of(context).copyWith(scrollbars: false),
      child: ListView.separated(
          itemBuilder: (context, index) => PostCard(post: postsInList[index]),
          separatorBuilder: (context, index) => const Divider(height: 1),
          itemCount: postsInList.length),
    );
  }

  Widget _buildMap(
      BuildContext context, DateFormat formatter, PathMappingService mapper) {
    final settings = Provider.of<SettingsController>(context);

    final shouldDebugCache =
        _logger.level <= Level.FINEST; // compare to logger level
    return MapLayoutBuilder(
      controller: controller,
      builder: (context, transformer) {
        _logger.finer('Call MapLayoutBuilder');
        bounds = MapBounds.computed(transformer);
        if (bounds.isOverflowed()) {
          _fixOutOfBounds(transformer);
        }
        _updatePostsInBoundsFilter();

        final markerData =
            postsInView.map((p) => p.toMarkerData(transformer, Colors.blue));
        final collapsedMarkerData = <MarkerData>[];

        _logger.finest(() =>
            'Markers in view (of ${widget.posts.length}): ${markerData.length}');
        for (final data in markerData) {
          if (collapsedMarkerData.isEmpty) {
            collapsedMarkerData.add(data);
            continue;
          }

          MarkerData? includedMarker;
          for (final cd in collapsedMarkerData) {
            final dx = (cd.pos.dx - data.pos.dx).abs();
            final dy = (cd.pos.dy - data.pos.dy).abs();
            if (dx <= billboardXSize && dy <= billboardYSize) {
              includedMarker = cd;
              break;
            }
          }

          if (includedMarker != null) {
            includedMarker.posts.addAll(data.posts);
          } else {
            collapsedMarkerData.add(data);
          }
        }

        final markerWidgets = collapsedMarkerData
            .map((m) => _buildMarkerWidget(m, formatter, mapper));

        return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onDoubleTap: _onDoubleTap,
          onScaleStart: _onScaleStart,
          onScaleUpdate: (details) => _onScaleUpdate(details, transformer),
          onSecondaryTapUp: (event) {
            setState(() {
              postsInList.clear();
            });
          },
          child: Listener(
            behavior: HitTestBehavior.opaque,
            onPointerSignal: (event) {
              if (event is PointerScrollEvent) {
                final delta = event.scrollDelta;
                final newZoom = controller.zoom - (delta.dy / 1000.0);
                setState(() {
                  _tryZoom(newZoom, transformer);
                });
              }
            },
            child: Stack(
              children: [
                Map(
                  controller: controller,
                  builder: (context, x, y, z) {
                    final filename = '${z}_${x}_$y.png';
                    final imageFile =
                        getTileCachedFile(settings.geoCacheDirectory, filename);
                    //Legal notice: This url is only used for demo and educational purposes. You need a license key for production use.

                    //Google Maps
                    // final url =
                    //     'https://www.google.com/maps/vt/pb=!1m4!1m3!1i$z!2i$x!3i$y!2m3!1e0!2sm!3i420120488!3m7!2sen!5e1105!12m4!1e68!2m2!1sset!2sRoadmap!4e0!5m1!1e0!23i4111425';
                    //
                    // final darkUrl =
                    //     'https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i$z!2i$x!3i$y!4i256!2m3!1e0!2sm!3i556279080!3m17!2sen-US!3sUS!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC52Om9uLHMuZTpsfHAudjpvZmZ8cC5zOi0xMDAscy5lOmwudC5mfHAuczozNnxwLmM6I2ZmMDAwMDAwfHAubDo0MHxwLnY6b2ZmLHMuZTpsLnQuc3xwLnY6b2ZmfHAuYzojZmYwMDAwMDB8cC5sOjE2LHMuZTpsLml8cC52Om9mZixzLnQ6MXxzLmU6Zy5mfHAuYzojZmYwMDAwMDB8cC5sOjIwLHMudDoxfHMuZTpnLnN8cC5jOiNmZjAwMDAwMHxwLmw6MTd8cC53OjEuMixzLnQ6NXxzLmU6Z3xwLmM6I2ZmMDAwMDAwfHAubDoyMCxzLnQ6NXxzLmU6Zy5mfHAuYzojZmY0ZDYwNTkscy50OjV8cy5lOmcuc3xwLmM6I2ZmNGQ2MDU5LHMudDo4MnxzLmU6Zy5mfHAuYzojZmY0ZDYwNTkscy50OjJ8cy5lOmd8cC5sOjIxLHMudDoyfHMuZTpnLmZ8cC5jOiNmZjRkNjA1OSxzLnQ6MnxzLmU6Zy5zfHAuYzojZmY0ZDYwNTkscy50OjN8cy5lOmd8cC52Om9ufHAuYzojZmY3ZjhkODkscy50OjN8cy5lOmcuZnxwLmM6I2ZmN2Y4ZDg5LHMudDo0OXxzLmU6Zy5mfHAuYzojZmY3ZjhkODl8cC5sOjE3LHMudDo0OXxzLmU6Zy5zfHAuYzojZmY3ZjhkODl8cC5sOjI5fHAudzowLjIscy50OjUwfHMuZTpnfHAuYzojZmYwMDAwMDB8cC5sOjE4LHMudDo1MHxzLmU6Zy5mfHAuYzojZmY3ZjhkODkscy50OjUwfHMuZTpnLnN8cC5jOiNmZjdmOGQ4OSxzLnQ6NTF8cy5lOmd8cC5jOiNmZjAwMDAwMHxwLmw6MTYscy50OjUxfHMuZTpnLmZ8cC5jOiNmZjdmOGQ4OSxzLnQ6NTF8cy5lOmcuc3xwLmM6I2ZmN2Y4ZDg5LHMudDo0fHMuZTpnfHAuYzojZmYwMDAwMDB8cC5sOjE5LHMudDo2fHAuYzojZmYyYjM2Mzh8cC52Om9uLHMudDo2fHMuZTpnfHAuYzojZmYyYjM2Mzh8cC5sOjE3LHMudDo2fHMuZTpnLmZ8cC5jOiNmZjI0MjgyYixzLnQ6NnxzLmU6Zy5zfHAuYzojZmYyNDI4MmIscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6NnxzLmU6bC50fHAudjpvZmYscy50OjZ8cy5lOmwudC5mfHAudjpvZmYscy50OjZ8cy5lOmwudC5zfHAudjpvZmYscy50OjZ8cy5lOmwuaXxwLnY6b2Zm!4e0&key=AIzaSyAOqYYyBbtXQEtcHG7hwAwyCPQSYidG8yU&token=31440';
                    //Mapbox Streets
                    //  final url =
                    //      'https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/$z/$x/$y';

                    final url = 'https://tile.openstreetmap.org/$z/$x/$y.png';
                    _logger
                        .finest(() => 'Attempting to display tile from $url');
                    return Image(
                      image: NetworkToFileImage(
                          url: url, file: imageFile, debug: shouldDebugCache),
                      fit: BoxFit.cover,
                    );
                  },
                ),
                ...markerWidgets,
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildMarkerWidget(
      MarkerData data, DateFormat formatter, PathMappingService mapper) {
    return Positioned(
      left: data.pos.dx - 16,
      top: data.pos.dy - 16,
      width: billboardXSize,
      height: billboardYSize,
      child: InkWell(
          onTap: () {
            setState(() {
              postsInList.clear();
              postsInList.addAll(data.posts);
              _logger.finest(
                  () => 'Reset post list with ${data.posts.length} posts');
            });
          },
          child: Center(
            child: SizedBox(
                width: billboardXSize,
                height: billboardYSize,
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      data.toLabel() + '\n' + data.subLabel(),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )),
          )),
    );
  }
}
