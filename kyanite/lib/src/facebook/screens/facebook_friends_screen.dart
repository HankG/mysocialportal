import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kyanite/src/facebook/models/facebook_friend.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/screens/error_screen.dart';
import 'package:kyanite/src/screens/loading_status_screen.dart';
import 'package:kyanite/src/screens/standin_status_screen.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';

class FacebookFriendsScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookFriendsScreen');

  const FacebookFriendsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final service = Provider.of<FacebookArchiveDataService>(context);
    final rootPath = Provider.of<SettingsController>(context).rootFolder;
    _logger.fine('Build FacebookFriendsScreen');

    return FutureBuilder<Result<List<FacebookFriend>, ExecError>>(
        future: service.getFriends(),
        builder: (context, snapshot) {
          _logger.fine('Future Friends builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            return const LoadingStatusScreen(title: 'Loading Friends');
          }

          final friendsResult = snapshot.requireData;
          if (friendsResult.isFailure) {
            return ErrorScreen(
                title: 'Error getting friends', error: friendsResult.error);
          }

          final friends = friendsResult.value;
          if (friends.isEmpty) {
            return const StandInStatusScreen(title: 'No friends were found');
          }
          _logger.fine('Build Friends Data Grid View');
          return _FacebookFriendsScreenWidget(
              friends: friends, rootPath: rootPath);
        });
  }
}

class _FacebookFriendsScreenWidget extends StatelessWidget {
  final List<FacebookFriend> friends;
  final String rootPath;

  const _FacebookFriendsScreenWidget(
      {Key? key, required this.friends, required this.rootPath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formatter = Provider.of<SettingsController>(context).dateFormatter;

    final headerStyle = Theme.of(context)
        .textTheme
        .bodyText1
        ?.copyWith(fontWeight: FontWeight.bold);

    const nameSize = 250.0;
    const statusSize = 100.0;
    const dateSize = 150.0;

    return ListView.separated(
      restorationId: 'friendListView',
      itemCount: friends.length + 1,
      itemBuilder: (context, index) {
        if (index == 0) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                SizedBox(
                    width: nameSize, child: Text('Title', style: headerStyle)),
                SizedBox(
                    width: statusSize,
                    child: Text('Status', style: headerStyle)),
                SizedBox(
                    width: dateSize,
                    child: Text('Friends Since', style: headerStyle)),
              ],
            ),
          );
        }

        final friend = friends[index - 1];
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              SizedBox(width: nameSize, child: SelectableText(friend.name)),
              SizedBox(width: statusSize, child: Text(friend.status.name())),
              SizedBox(
                  width: dateSize,
                  child:
                      Text(_dateText(friend.friendSinceTimestamp, formatter))),
            ],
          ),
        );
      },
      separatorBuilder: (context, index) {
        return Divider(
          color: Colors.black,
          thickness: index == 0 ? 1.0 : 0.2,
        );
      },
    );
  }

  String _dateText(int timestamp, DateFormat formatter) => timestamp == 0
      ? 'Not Available'
      : formatter.format(DateTime.fromMillisecondsSinceEpoch(timestamp * 1000));
}
