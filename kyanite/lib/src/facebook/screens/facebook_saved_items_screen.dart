import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/filter_control_component.dart';
import 'package:kyanite/src/facebook/components/post_card.dart';
import 'package:kyanite/src/facebook/models/facebook_post.dart';
import 'package:kyanite/src/facebook/models/facebook_saved_item.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/screens/error_screen.dart';
import 'package:kyanite/src/settings/settings_controller.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';

import '../../screens/loading_status_screen.dart';
import '../../screens/standin_status_screen.dart';

class FacebookSavedItemsScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookSavedItemsScreen');

  const FacebookSavedItemsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.info('Build FacebookSavedItemsScreen');
    final service = Provider.of<FacebookArchiveDataService>(context);
    final username = Provider.of<SettingsController>(context).facebookName;

    return FutureBuilder<Result<List<FacebookSavedItem>, ExecError>>(
        future: service.getSavedItems(),
        builder: (context, snapshot) {
          _logger.info('FacebookSavedItemsScreen Future builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            return const LoadingStatusScreen(title: 'Loading savedItems');
          }

          final savedItemsResult = snapshot.requireData;

          if (savedItemsResult.isFailure) {
            return ErrorScreen(
                title: 'Error getting saved items',
                error: savedItemsResult.error);
          }

          final allSavedItems = savedItemsResult.value;

          final savedItems = username.isEmpty
              ? allSavedItems.toList()
              : allSavedItems.map((item) {
                  var newTitle = item.title;
                  if (item.title == username) {
                    newTitle = 'You posted';
                  } else {
                    newTitle = item.title
                        .replaceAll(username, 'You')
                        .replaceAll(wholeWordRegEx('his'), 'your')
                        .replaceAll(wholeWordRegEx('her'), 'your');
                  }
                  if (newTitle == item.title) {
                    return item;
                  } else {
                    return item.copy(title: newTitle);
                  }
                }).toList();
          if (savedItems.isEmpty) {
            return const StandInStatusScreen(
                title: 'No saved items were found');
          }

          _logger.fine('Build Saved Items ListView');
          final savedItemsAsPosts = savedItems
              .map((item) => FacebookPost(
                  creationTimestamp: item.timestamp,
                  title: item.title,
                  post: item.text,
                  links: item.uri.toString().isNotEmpty ? [item.uri] : []))
              .toList();
          return _FacebookSavedItemsScreenWidget(
              savedItemsAsPosts: savedItemsAsPosts);
        });
  }
}

class _FacebookSavedItemsScreenWidget extends StatelessWidget {
  static final _logger = Logger('$_FacebookSavedItemsScreenWidget');

  final List<FacebookPost> savedItemsAsPosts;

  const _FacebookSavedItemsScreenWidget(
      {Key? key, required this.savedItemsAsPosts})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine('Redrawing');
    return FilterControl<FacebookPost, dynamic>(
        allItems: savedItemsAsPosts,
        textSearchFilterFunction: (post, text) =>
            post.title.contains(text) ||
            post.post.contains(text) ||
            post.links.where((l) => l.toString().contains(text)).isNotEmpty,
        itemToDateTimeFunction: (post) =>
            DateTime.fromMillisecondsSinceEpoch(post.creationTimestamp * 1000),
        dateRangeFilterFunction: (post, start, stop) =>
            timestampInRange(post.creationTimestamp * 1000, start, stop),
        builder: (context, items) {
          if (items.isEmpty) {
            return const StandInStatusScreen(
                title: 'No saved items meet filter criteria');
          }

          return ScrollConfiguration(
            behavior:
                ScrollConfiguration.of(context).copyWith(scrollbars: false),
            child: ListView.separated(
                primary: false,
                restorationId: 'facebookSavedItemsListView',
                itemCount: items.length,
                itemBuilder: (context, index) {
                  _logger.finer('Rendering Saved Item List Item');
                  return PostCard(post: items[index]);
                },
                separatorBuilder: (context, index) {
                  return const Divider(
                    color: Colors.black,
                    thickness: 0.2,
                  );
                }),
          );
        });
  }
}
