import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/components/event_card.dart';
import 'package:kyanite/src/facebook/components/filter_control_component.dart';
import 'package:kyanite/src/facebook/models/facebook_event.dart';
import 'package:kyanite/src/facebook/models/model_utils.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_service.dart';
import 'package:kyanite/src/screens/error_screen.dart';
import 'package:kyanite/src/utils/exec_error.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:result_monad/result_monad.dart';

import '../../screens/loading_status_screen.dart';
import '../../screens/standin_status_screen.dart';

class FacebookEventsScreen extends StatelessWidget {
  static final _logger = Logger('$FacebookEventsScreen');

  const FacebookEventsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final service = Provider.of<FacebookArchiveDataService>(context);
    _logger.fine('Build FacebookEventsScreen');

    return FutureBuilder<Result<List<FacebookEvent>, ExecError>>(
        future: service.getEvents(),
        builder: (context, snapshot) {
          _logger.fine('Future Events builder called');

          if (!snapshot.hasData ||
              snapshot.connectionState != ConnectionState.done) {
            return const LoadingStatusScreen(title: 'Loading events');
          }

          final eventsResult = snapshot.requireData;
          if (eventsResult.isFailure) {
            return ErrorScreen(error: eventsResult.error);
          }

          final events = eventsResult.value;

          if (events.isEmpty) {
            return const StandInStatusScreen(title: 'No events were found');
          }
          _logger.fine('Build events ListView');
          return _FacebookEventsScreenWidget(events: events);
        });
  }
}

class _FacebookEventsScreenWidget extends StatelessWidget {
  static final _logger = Logger('$_FacebookEventsScreenWidget');
  final List<FacebookEvent> events;

  const _FacebookEventsScreenWidget({Key? key, required this.events})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FilterControl<FacebookEvent, dynamic>(
        allItems: events,
        textSearchFilterFunction: (event, text) =>
            event.name.contains(text) ||
            event.description.contains(text) ||
            event.location.name.contains(text) ||
            event.location.address.contains(text),
        itemToDateTimeFunction: (event) {
          if (event.endTimestamp == 0) {
            return DateTime.fromMillisecondsSinceEpoch(
                event.startTimestamp * 1000);
          }
          return DateTime.fromMillisecondsSinceEpoch(event.endTimestamp * 1000);
        },
        dateRangeFilterFunction: (event, start, stop) =>
            timestampInRange(event.startTimestamp * 1000, start, stop) ||
            timestampInRange(event.endTimestamp * 1000, start, stop),
        builder: (context, items) {
          if (items.isEmpty) {
            return const StandInStatusScreen(
                title: 'No events meet filter criteria');
          }

          return ListView.separated(
              primary: false,
              restorationId: 'facebookEventsListView',
              itemCount: items.length,
              itemBuilder: (context, index) {
                _logger.finer('Rendering Facebook Event List Item');
                return EventCard(event: items[index]);
              },
              separatorBuilder: (context, index) {
                return const Divider(
                  color: Colors.black,
                  thickness: 0.2,
                );
              });
        });
  }
}
