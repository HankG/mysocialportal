import 'package:flutter/material.dart';

enum VideoPlayerSettingType {
  windows, //
  macOS, //open
  linuxVlc, //vlc
  linuxTotem, //totem
  linukMpv, //gnome-mpv
  custom,
}

extension VideoPathMapping on VideoPlayerSettingType {
  String toAppPath() {
    switch (this) {
      case VideoPlayerSettingType.custom:
        return '';
      case VideoPlayerSettingType.linuxVlc:
        return 'vlc';
      case VideoPlayerSettingType.linuxTotem:
        return 'totem';
      case VideoPlayerSettingType.linukMpv:
        return 'gnome-mpv';
      case VideoPlayerSettingType.macOS:
        return 'open';
      case VideoPlayerSettingType.windows:
        return 'C:\\Program Files\\Windows Media Player\\wmplayer.exe';
    }
  }

  DropdownMenuItem<VideoPlayerSettingType> toDropDownMenuItem() {
    switch (this) {
      case VideoPlayerSettingType.custom:
        return const DropdownMenuItem(
          value: VideoPlayerSettingType.custom,
          child: Text('Custom'),
        );
      case VideoPlayerSettingType.linuxVlc:
        return const DropdownMenuItem(
          value: VideoPlayerSettingType.linuxVlc,
          child: Text('VLC (Linux)'),
        );
      case VideoPlayerSettingType.linuxTotem:
        return const DropdownMenuItem(
          value: VideoPlayerSettingType.linuxTotem,
          child: Text('Totem (Linux)'),
        );
      case VideoPlayerSettingType.linukMpv:
        return const DropdownMenuItem(
          value: VideoPlayerSettingType.linukMpv,
          child: Text('MPV (Linux)'),
        );
      case VideoPlayerSettingType.macOS:
        return const DropdownMenuItem(
          value: VideoPlayerSettingType.macOS,
          child: Text('macOS'),
        );
      case VideoPlayerSettingType.windows:
        return const DropdownMenuItem(
          value: VideoPlayerSettingType.windows,
          child: Text('Windows'),
        );
    }
  }
}
