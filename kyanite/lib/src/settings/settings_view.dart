import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_reader.dart';
import 'package:kyanite/src/settings/video_player_settings.dart';
import 'package:kyanite/src/utils/clipboard_helper.dart';
import 'package:kyanite/src/utils/snackbar_status_builder.dart';
import 'package:logging/logging.dart';

import 'settings_controller.dart';

class SettingsView extends StatefulWidget {
  const SettingsView({Key? key, required SettingsController controller})
      : _settingsController = controller,
        super(key: key);

  static const routeName = '/settings';

  final SettingsController _settingsController;

  @override
  State<SettingsView> createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  static final _logger = Logger('$_SettingsViewState');
  final _facebookNameController = TextEditingController();
  final _folderPathController = TextEditingController();
  final _videoPlayerPathController = TextEditingController();
  String? _invalidFolderString;
  VideoPlayerSettingType _videoPlayerTypeOption = VideoPlayerSettingType.custom;
  bool _validRootFolder = false;
  bool _differentSettingValues = false;
  Level _logLevel = Level.SEVERE;

  @override
  void initState() {
    _folderPathController.addListener(_validateRootFolder);
    _facebookNameController.addListener(() {
      _updateSettingsValueDiffs();
    });
    _videoPlayerPathController.addListener(() {
      _updateSettingsValueDiffs();
    });
    _setInitialValues();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _updateSettingsValueDiffs();
    return Scaffold(
        appBar: AppBar(
          title: const Text('Settings'),
          backgroundColor: Theme.of(context).canvasColor,
          foregroundColor: Theme.of(context).primaryColor,
          elevation: 0.0,
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(children: [
            _buildThemeOptions(context),
            const SizedBox(height: 10),
            const Divider(),
            const SizedBox(height: 10),
            _buildLoggingOptions(context),
            const SizedBox(height: 10),
            _buildFacebookNameOptions(context),
            const SizedBox(height: 10),
            _buildLogFilePath(context),
            const SizedBox(height: 10),
            _buildGeocacheOptions(context),
            const SizedBox(height: 10),
            _buildRootFolderOption(context),
            const SizedBox(height: 10),
            _buildVideoPlayerOption(context),
            const SizedBox(height: 10),
            _buildSaveCancelButtonRow(),
          ]),
        ));
  }

  Widget _buildLoggingOptions(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Logging Level: ', style: Theme.of(context).textTheme.bodyText1),
          const SizedBox(width: 10),
          DropdownButton<Level>(
              value: _logLevel,
              onChanged: (newLevel) async {
                _logLevel = newLevel ?? Level.INFO;
                setState(() {});
              },
              items: Level.LEVELS
                  .map((level) =>
                      DropdownMenuItem(value: level, child: Text(level.name)))
                  .toList()),
        ]);
  }

  Widget _buildLogFilePath(BuildContext context) {
    final path = widget._settingsController.logPath;
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Log file: ', style: Theme.of(context).textTheme.bodyText1),
          const SizedBox(width: 10),
          Expanded(
              child: Text(path,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText2)),
          const SizedBox(width: 10),
          IconButton(
              onPressed: () async {
                await copyToClipboard(
                    context: context,
                    text: path,
                    snackbarMessage: 'Copied "$path" to clipboard');
              },
              icon: const Icon(Icons.copy)),
        ]);
  }

  Widget _buildGeocacheOptions(BuildContext context) {
    final path = widget._settingsController.geoCacheDirectory.path;
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Map Tile Directory: ',
              style: Theme.of(context).textTheme.bodyText1),
          const SizedBox(width: 10),
          Expanded(
              child: Text(path,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText2)),
          const SizedBox(width: 10),
          IconButton(
              onPressed: () async {
                try {
                  _logger.fine('Flushing tile cache folder: $path');
                  await Directory(path).delete(recursive: true);
                  Directory(path).createSync(recursive: true);
                  SnackBarStatusBuilder.buildSnackbar(
                      context, 'Geocache cleared');
                  _logger.fine('Tile cache cleared: $path');
                } catch (e) {
                  _logger.severe('Error flushing tile cache: $e');
                }
              },
              icon: const Icon(Icons.delete_sweep)),
        ]);
  }

  Widget _buildRootFolderOption(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Archive Folder: ',
              style: Theme.of(context).textTheme.bodyText1),
          const SizedBox(width: 10),
          Expanded(
              child: TextField(
                  controller: _folderPathController,
                  decoration: InputDecoration(
                    hintText:
                        'Root folder of the unzipped Facebook archive file',
                    errorText: _invalidFolderString,
                  ))),
          const SizedBox(width: 15),
          IconButton(
              onPressed: _setNewRootFolder,
              icon: const Icon(Icons.folder_outlined)),
        ]);
  }

  Widget _buildFacebookNameOptions(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("Facebook User's Name:",
              style: Theme.of(context).textTheme.bodyText1),
          const SizedBox(width: 10),
          Expanded(
              child: TextField(
                  controller: _facebookNameController,
                  decoration: const InputDecoration(
                    hintText: 'Displayed user name (used for filtering titles)',
                  ))),
        ]);
  }

  Widget _buildThemeOptions(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text('Application Theme: ',
            style: Theme.of(context).textTheme.bodyText1),
        const SizedBox(width: 10),
        DropdownButton<ThemeMode>(
          value: widget._settingsController.themeMode,
          onChanged: (newMode) async {
            await widget._settingsController.updateThemeMode(newMode);
            setState(() {});
          },
          items: const [
            DropdownMenuItem(
              value: ThemeMode.system,
              child: Text('System Theme'),
            ),
            DropdownMenuItem(
              value: ThemeMode.light,
              child: Text('Light Theme'),
            ),
            DropdownMenuItem(
              value: ThemeMode.dark,
              child: Text('Dark Theme'),
            )
          ],
        ),
      ],
    );
  }

  Widget _buildVideoPlayerOption(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Video Player: ', style: Theme.of(context).textTheme.bodyText1),
          const SizedBox(width: 10),
          DropdownButton<VideoPlayerSettingType>(
            value: _videoPlayerTypeOption,
            onChanged: (newPlayer) async {
              setState(() {
                _videoPlayerTypeOption =
                    newPlayer ?? VideoPlayerSettingType.custom;
                _videoPlayerPathController.text =
                    _videoPlayerTypeOption.toAppPath();
              });
            },
            items: VideoPlayerSettingType.values
                .map((e) => e.toDropDownMenuItem())
                .toList(),
          ),
          const SizedBox(width: 10),
          Expanded(
              child: TextField(
                  enabled:
                      _videoPlayerTypeOption == VideoPlayerSettingType.custom,
                  controller: _videoPlayerPathController,
                  decoration: const InputDecoration(
                    hintText: 'Command to play videos',
                  ))),
          const SizedBox(width: 15),
          IconButton(
              onPressed: _setNewCustomPlayerPath,
              icon: const Icon(Icons.folder_outlined)),
        ]);
  }

  Widget _buildSaveCancelButtonRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            onPressed: _differentSettingValues ? _saveSettings : null,
            child: const Text('Save Settings')),
        const SizedBox(width: 10),
        ElevatedButton(
            onPressed: _setInitialValues, child: const Text('Cancel Changes'))
      ],
    );
  }

  Future<void> _saveSettings() async {
    await widget._settingsController
        .updateRootFolder(_folderPathController.text);
    await widget._settingsController
        .updateVideoPlayerSettingType(_videoPlayerTypeOption);
    if (_videoPlayerTypeOption == VideoPlayerSettingType.custom) {
      await widget._settingsController
          .updateVideoPlayerCommand(_videoPlayerPathController.text);
    }
    await widget._settingsController.updateLogLevel(_logLevel);
    await widget._settingsController
        .updateFacebookName(_facebookNameController.text);
    setState(() {});
  }

  void _setInitialValues() {
    _folderPathController.text = widget._settingsController.rootFolder;
    _validateRootFolder();
    _videoPlayerTypeOption = widget._settingsController.videoPlayerSettingType;
    _videoPlayerPathController.text =
        widget._settingsController.videoPlayerCommand;
    _logLevel = widget._settingsController.logLevel;
    _facebookNameController.text = widget._settingsController.facebookName;
  }

  void _updateSettingsValueDiffs() {
    bool oldValue = _differentSettingValues;
    bool newValue = false;
    newValue |=
        (_folderPathController.text != widget._settingsController.rootFolder &&
            _validRootFolder);
    newValue |= (_videoPlayerTypeOption !=
        widget._settingsController.videoPlayerSettingType);
    newValue |= (_videoPlayerPathController.text !=
        widget._settingsController.videoPlayerCommand);
    newValue |= (_logLevel != widget._settingsController.logLevel);
    newValue |= (_facebookNameController.text !=
        widget._settingsController.facebookName);
    if (oldValue == newValue) return;
    setState(() {
      _differentSettingValues = newValue;
    });
  }

  void _validateRootFolder() {
    setState(() {
      _validRootFolder = false;
      if (!Directory(_folderPathController.text).existsSync()) {
        _invalidFolderString = 'Choose an existing folder';
        return;
      }

      if (!FacebookArchiveFolderReader.validateCanReadArchive(
          _folderPathController.text)) {
        _invalidFolderString =
            'Choose a folder that is a Facebook Archive and accessible.\nOn Macs make sure root folder is in Downloads directory.';
        return;
      }

      _invalidFolderString = null;
      _validRootFolder = true;
    });
  }

  void _setNewRootFolder() async {
    final path = await FilePicker.platform.getDirectoryPath();

    if (path == null) {
      return;
    }

    setState(() {
      _folderPathController.text = path;
    });
  }

  void _setNewCustomPlayerPath() async {
    final picked = await FilePicker.platform.pickFiles(
        dialogTitle: 'Pick Video player',
        type: FileType.any,
        allowMultiple: false);

    if (picked == null || picked.paths.isEmpty) {
      return;
    }

    setState(() {
      _videoPlayerPathController.text = picked.paths.first ?? '';
    });
  }
}
