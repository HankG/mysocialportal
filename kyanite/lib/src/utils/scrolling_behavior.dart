import 'dart:ui';

import 'package:flutter/material.dart';

class FacebookAppScrollingBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}
