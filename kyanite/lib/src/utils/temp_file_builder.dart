import 'dart:io';

import 'package:intl/intl.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

Future<String> getTempFile(String prefix, String extension) async {
  final tempDirPath = await customGetTempDirectory();
  final dateString = DateFormat('yyyyMMdd_HHmmss').format(DateTime.now());
  return '$tempDirPath$prefix$dateString$extension';
}

Future<String> customGetTempDirectory() async {
  if (Platform.isMacOS) {
    final tempDirPathFromEnv = Platform.environment['TMPDIR'];
    if (tempDirPathFromEnv != null) {
      return tempDirPathFromEnv;
    }
  }

  final tempDirPath = await getTemporaryDirectory();
  return tempDirPath.path + Platform.pathSeparator;
}

Future<Directory> getTileCachedDirectory() async {
  final base = await getApplicationSupportDirectory();
  final cachePath = p.join(base.path, 'geocache');
  final cacheDir = Directory(cachePath);
  await cacheDir.create(recursive: true);
  return cacheDir;
}

File getTileCachedFile(Directory cacheDirectory, String filename) {
  final path = p.join(cacheDirectory.path, filename);
  return File(path);
}
