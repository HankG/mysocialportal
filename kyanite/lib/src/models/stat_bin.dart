class StatBin {
  static final DateTime noData = DateTime.fromMillisecondsSinceEpoch(0);
  final DateTime? _binEpoch;
  final int _index;
  int _count;

  DateTime get binEpoch => _binEpoch ?? noData;

  bool get hasEpoch => _binEpoch != null;

  int get count => _count;

  int get index => _index;

  StatBin({required index, DateTime? binEpoch, int initialCount = 0})
      : _count = initialCount,
        _index = index,
        _binEpoch = binEpoch;

  int increment({int amount = 1}) {
    _count += amount;
    return _count;
  }

  @override
  String toString() {
    return 'StatBin{index: $_index, binEpoch: $_binEpoch, count: $_count}';
  }
}
