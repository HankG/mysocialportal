# Kyanite: A Facebook Archive Viewer

A Flutter-based cross platform desktop
application for viewing the Facebook account archive that a user can
request from the system. The archive takes the form of a zip file that can be
unzipped on your desktop system and have this app point to it.

## Installation

To install Kyanite you simply have to download the latest release from 
[the project release directory](https://gitlab.com/HankG/mysocialportal/-/releases) 
for your given platform. Then unzip the folder and you are ready to run. On Mac 
and Windows you will get a warning about an "unknown publisher" since this is beta
software that is not installed or signed through the respective app stores. App store
versions will come in the near future.

## Building
In order to build this application you will need to have installed [Flutter](https://flutter.dev). 
Installation instructions for various platforms are [here](https://flutter.dev/docs/get-started/install).
Once you have that installed it is as easy as navigating to the respective directory on the command
line and executing:

On Linux:
```bash
flutter run -d linux
```

On Mac:
```bash
flutter run -d macos
```

On Windows:
```bash
flutter run -d windows
```

## Using

To use Kynaite you need a copy of your Facebook Archive. This unfortunately has to be generated manually. [This post explains how to get your archive from Facebook](https://nequalsonelifestyle.com/2021/11/17/getting-your-facebook-archive/). There are also blog posts for each of the releases:
* [Beta 1 (v0.1.1) release blog post](https://nequalsonelifestyle.com/2021/11/17/harnessing-your-facebook-data-with-kyanite/)
* [Beta 2 (v0.1.2) release blog post](https://nequalsonelifestyle.com/2021/12/07/kyanite-beta2-updates/)
* [Beta 3 (v0.1.3) release blogpost[(https://nequalsonelifestyle.com/2021/12/20/kyanite-beta3-updates/)

## Bugs/Issues

Please report any bugs or feature requests [with our issue tracker](https://gitlab.com/HankG/mysocialportal/-/issues).
