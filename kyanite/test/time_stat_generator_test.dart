// ignore_for_file: avoid_print

import 'package:flutter_test/flutter_test.dart';
import 'package:kyanite/src/models/time_element.dart';
import 'package:kyanite/src/utils/time_stat_generator.dart';
import 'package:logging/logging.dart';

void main() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((event) {
    print(
        '${event.level.name} - ${event.loggerName} @ ${event.time}: ${event.message}');
  });

  group('Test Daily stats', () {
    test('With no data', () {
      final bins = TimeStatGenerator([]).calculateDailyStats();
      bins.forEach(print);
      expect(bins.length, equals(0));
      for (var element in bins) {
        expect(element.count, equals(0));
      }
    });

    test('With Data', () {
      final data = [
        TimeElement(timeInMS: DateTime(2020, 10, 19).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 20, 12,00,00).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 20, 13,00,00).millisecondsSinceEpoch),
         TimeElement(timeInMS: DateTime(2020, 10, 23).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 24).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 25).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2021, 10, 26).millisecondsSinceEpoch),
      ];
      final bins = TimeStatGenerator(data).calculateDailyStats();
      bins.forEach(print);
      expect(bins.length, equals(6));
      for (var i = 0; i < bins.length; i++) {
        final expected = i == 0
            ? 1
            : i == 1
            ? 2
            : 1;
        expect(bins[i].count, equals(expected));
      }
    });
  });

  group('Test Weekly stats', () {
    test('With no data', () {
      final bins = TimeStatGenerator([]).calculateByDayOfWeekStats();
      bins.forEach(print);
      expect(bins.length, equals(7));
      for (var element in bins) {
        expect(element.count, equals(0));
      }
    });

    test('With Data', () {
      final data = [
        TimeElement(timeInMS: DateTime(2020, 10, 19).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 20).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 21).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 22).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 23).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 24).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 25).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 26).millisecondsSinceEpoch),
        TimeElement(
            timeInMS: DateTime(2020, 10, 27, 10, 0, 0).millisecondsSinceEpoch),
        TimeElement(
            timeInMS: DateTime(2020, 10, 27, 11, 0, 0).millisecondsSinceEpoch),
        TimeElement(
            timeInMS: DateTime(2020, 10, 27, 12, 0, 0).millisecondsSinceEpoch),
      ];
      final bins = TimeStatGenerator(data).calculateByDayOfWeekStats();
      bins.forEach(print);
      expect(bins.length, equals(7));
      for (var i = 0; i < bins.length; i++) {
        final expected = i == 0
            ? 2
            : i == 1
            ? 4
            : 1;
        expect(bins[i].count, equals(expected));
      }
    });
  });

  group('Test Monthly stats', () {
    test('With no data', () {
      final bins = TimeStatGenerator([]).calculateByMonthStats();
      bins.forEach(print);
      expect(bins.length, equals(12));
      for (var element in bins) {
        expect(element.count, equals(0));
      }
    });

    test('With Data', () {
      final data = [
        TimeElement(timeInMS: DateTime(2020, 10, 19).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 20).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 21).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 22).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 23).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 24).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 25).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 26).millisecondsSinceEpoch),
        TimeElement(
            timeInMS: DateTime(2020, 10, 27, 10, 0, 0).millisecondsSinceEpoch),
        TimeElement(
            timeInMS: DateTime(2020, 10, 27, 11, 0, 0).millisecondsSinceEpoch),
        TimeElement(
            timeInMS: DateTime(2020, 10, 27, 12, 0, 0).millisecondsSinceEpoch),
      ];
      final bins = TimeStatGenerator(data).calculateByDayOfWeekStats();
      bins.forEach(print);
      expect(bins.length, equals(7));
      for (var i = 0; i < bins.length; i++) {
        final expected = i == 0
            ? 2
            : i == 1
            ? 4
            : 1;
        expect(bins[i].count, equals(expected));
      }
    });
  });


  group('Test Yearly stats', () {
    test('With no data', () {
      final bins = TimeStatGenerator([]).calculateStatsByYear();
      bins.forEach(print);
      expect(bins.isEmpty, true);
    });

    test('With Data', () {
      final data = [
        TimeElement(timeInMS: DateTime(2020, 10, 17).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 18).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 19).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2020, 10, 20).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2021, 10, 21).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2021, 10, 22).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2021, 10, 23).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2022, 10, 24).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2022, 10, 25).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2023, 10, 26).millisecondsSinceEpoch),
        TimeElement(timeInMS: DateTime(2023, 10, 27).millisecondsSinceEpoch),
      ];
      final bins = TimeStatGenerator(data).calculateStatsByYear();
      bins.forEach(print);
      expect(bins.length, equals(4));
      for (var i = 0; i < bins.length; i++) {
        final expected = i == 0
            ? 4
            : i == 1
            ? 3
            : 2;
        expect(bins[i].count, equals(expected));
      }
    });
  });

}
