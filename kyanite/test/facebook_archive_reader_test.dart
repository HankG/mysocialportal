// ignore_for_file: avoid_print

import 'package:flutter_test/flutter_test.dart';
import 'package:kyanite/src/facebook/models/facebook_event.dart';
import 'package:kyanite/src/facebook/services/facebook_archive_reader.dart';
import 'package:logging/logging.dart';

void main() {
  const String rootPath = 'test_assets/test_facebook_archive';

  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((event) {
    print(
        '${event.level.name} - ${event.loggerName} @ ${event.time}: ${event.message}');
  });

  group('Test Read Posts JSON', () {
    test('Read posts from disk', () async {
      final posts =
          (await FacebookArchiveFolderReader(rootPath).readPosts()).value;
      expect(posts.length, equals(6));
      posts.forEach(print);
    });
  });

  group('Test Read Comments JSON', () {
    test('Read from disk', () async {
      final comments =
          await FacebookArchiveFolderReader(rootPath).readComments();
      expect(comments.value.length, equals(3));
      comments.value.forEach(print);
    });
  });

  group('Test Read Photos JSON', () {
    test('Read photos from disk', () async {
      final albums =
          await FacebookArchiveFolderReader(rootPath).readPhotoAlbums();
      expect(albums.value.length, equals(1));
      albums.value.forEach(print);
    });
  });

  group('Test Read Events JSON', () {
    test('Read from events disk', () async {
      final events = await FacebookArchiveFolderReader(rootPath).readEvents();
      expect(events.value.length, equals(11));
      events.value
          .where((element) => element.eventStatus == FacebookEventStatus.owner)
          .forEach(print);
    });
  });

  group('Test Read Friends JSON', () {
    test('Read from friends disk', () async {
      final friendsResult =
          await FacebookArchiveFolderReader(rootPath).readFriends();
      friendsResult.match(
          onSuccess: (friends) => expect(friends.length, equals(13)),
          onError: (error) => fail(error.toString()));
    });
  });

  group('Test Read Conversation JSON', () {
    test('Read conversations from disk', () async {
      final conversations =
          await FacebookArchiveFolderReader(rootPath).readConversations();
      expect(conversations.value.length, equals(1));
      conversations.value.forEach(print);
    });
  });

  group('Test Read Saved Items JSON', () {
    test('Read savedItems from disk', () async {
      final savedItems =
          (await FacebookArchiveFolderReader(rootPath).readSavedItems()).value;
      expect(savedItems.length, equals(6));
      savedItems.forEach(print);
    });
  });
}
